using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Galactica
{
    class DisparoJefe:GameComun
    {
        private static List<Texture2D> Texturas;
        public enum tipo { maximo, normal}
        public tipo tipoActual;
        private int textura;
        private Vector2 posNave;
        private int velocidadX,velocidadY;
        public bool Disparar,postDisparo;
        public bool estaEnColision;
        public float x, y, Tiempo;
        public static int adicionDanoDisNave;
        private Color color;

        public DisparoJefe()
        { 
        }

        public DisparoJefe(Vector2 posEnem,tipo t)
        {
            if (t == tipo.maximo)
            {
                Position.X = posEnem.X;
                Position.Y = posEnem.Y;
                tipoActual = t;
                textura = 0;
                numeroCuadros = 4;
                Escala = 1.4f;
                defineTrozoAnim(0, 0, 78, 78);
                setOrigen();
                radioCollition = (int)(40 * Escala);
                color = Color.White;
            }else
            if (t == tipo.normal)
            {
                Position.X = posEnem.X;
                Position.Y = posEnem.Y;
                tipoActual = t;
                Disparar = false;
                postDisparo = false;
                defineTrozoAnim(0, 0, 75, 75);
                Tiempo = 0;
                textura = 1;
                numeroCuadros = 1;
                Escala = 0.4f;
                setOrigen();
                radioCollition = (int)(38 * Escala);
                color = Color.LightGreen;
            }
            estaEnColision = false;
        }


        public static void load(Textura t)
        {
            adicionDanoDisNave = 0;
            setTextura(t);
        }

        public void update(GameTime gameTime)
        {
            if (tipoActual == tipo.maximo)
            { movimDisparoMaximo(gameTime); }
            else
            if (tipoActual == tipo.normal)
            { movimDisparoNormal(); }
        }

        public void draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Texturas[textura], Position, TrozoAnim, color,
                0.0f, Origen, Escala, SpriteEffects.None, 0.439f);

        }


        ///*************************************///////

        public void movimDisparoMaximo(GameTime gt)
        {

            defineTrozoAnim(actualframe * 80, 0, 80, 80);
            setOrigen();

            Position.Y = Position.Y + 10;

            velocidadAnimacion(gt);

            if (colision.colisionDispANave(this))
            {
                estaEnColision = true;
                Nave.Vida = Nave.Vida - (20 + adicionDanoDisNave);
            }
        }

        public void movimDisparoNormal()
        {

            if (Disparar)
            {
                Tiempo = 0;
                posNave = Nave.PositionNav;
                x = Position.X;
                y = Position.Y;
                velocidadX = (int)((posNave.X - x)) * 1 / 45;
                velocidadY = (int)((posNave.Y - y)) * 1 / 45;
                postDisparo = true;
                Disparar = false;
            }
            if (postDisparo)
            {
                Tiempo++;
                Position.X = x + velocidadX * Tiempo;
                Position.Y = y + velocidadY * Tiempo;
            }

            if (colision.colisionDispANave(this))
            {
                estaEnColision = true;
                Nave.Vida = Nave.Vida - (4 + adicionDanoDisNave);
            }
        }

         public void setPosition(Vector2 p,bool Derecha)
        {
            if (tipoActual == tipo.normal)
            {
                if (Derecha == true)
                {
                    Position.X = p.X + 130;
                    Position.Y = p.Y + 160;
                }

                if (Derecha == false)
                {
                    Position.X = p.X - 130;
                    Position.Y = p.Y + 160;
                }
            }
    
        }

        public void setPosition(Vector2 p)
        {
             Position.X = p.X;
             Position.Y = p.Y + 200;
        }

        public Vector2 getPosition()
        {
            return Position;
        }

        public static void setTextura(Textura t)
        {
            Texturas = new List<Texture2D>();
            Texturas.Add(t.getTextura("JefeDisparoMax"));//0
            Texturas.Add(t.getTextura("JefeDisparoNormal"));//1
        }

        public void setOrigen()
        {
            Origen.X = TrozoAnim.Width / 2;
            Origen.Y = TrozoAnim.Height / 2;
        }

        //define el cuandro de la animacion actual
        public void defineTrozoAnim(int x, int y, int width, int height)
        {
            TrozoAnim.X = x;
            TrozoAnim.Y = y;
            TrozoAnim.Width = width;
            TrozoAnim.Height = height;
        }

        public void velocidadAnimacion(GameTime gameTime)
        {

            dato += gameTime.ElapsedGameTime;
            if (dato.TotalMilliseconds >=
                ((gameTime.ElapsedGameTime.Milliseconds) * velocidad))
            {
                actualframe = actualframe + 1;
                dato = TimeSpan.Zero;
            }

            if (actualframe >= numeroCuadros) { actualframe = 0; }
        }
    
    }
}
