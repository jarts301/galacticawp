using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Galactica
{
    class CollitionJefe
    {
        private float distancia;
        private int i = 0, j = 0, w = 0;
        public static Disparo.tipo ultimaColisionTipoDisparo;
        public static Vector2 positionUltimoDisparo=new Vector2();
        public static int ultimoTipoColision;//1 da�o 0 no da�o
        public double X1, Y1, X1p, Y1p;

        public bool colisionDisparoALaxo(Jefe jefe, Jefe.tipo tipoJefe)
        {
            //Colision zona no da�o Laxo
            if (tipoJefe == Jefe.tipo.Laxo && Nave.disparos.Count > 0)
            {
                for (i = 0; i < Nave.disparos.Count; i++)
                {
                    distancia = (float)Math.Sqrt(
                      Math.Pow(Nave.disparos[i].Position.X - jefe.Position.X, 2) +
                      Math.Pow(Nave.disparos[i].Position.Y - jefe.Position.Y, 2)
                    );

                    if (distancia <= Nave.disparos[i].radioCollition + Jefe.radioCollitionLaxo)
                    {
                        positionUltimoDisparo.X = Nave.disparos[i].getPosition().X;
                        positionUltimoDisparo.Y = Nave.disparos[i].getPosition().Y;
                        ultimoTipoColision = 0;
                        confirmaEliminaDisparoNave(i);
                        return true;
                    }
                }
                for (i = 0; i < Nave.disparos.Count; i++)
                {
                    for (j = 0; j < 3; j++)
                    {
                        distancia = (float)Math.Sqrt(
                          Math.Pow(Nave.disparos[i].Position.X -
                          (jefe.Position.X + Jefe.collitionLaxoX[j]), 2) +
                          Math.Pow(Nave.disparos[i].Position.Y -
                          (jefe.Position.Y + Jefe.collitionLaxoY[j]), 2)
                        );

                        if (distancia <= Nave.disparos[i].radioCollition + Jefe.radioCollitionDanoLaxo)
                        {
                            positionUltimoDisparo.X = Nave.disparos[i].getPosition().X;
                            positionUltimoDisparo.Y = Nave.disparos[i].getPosition().Y;
                            ultimoTipoColision = 1;
                            confirmaEliminaDisparoNave(i);
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        /*Colision PoderGalactica a Laxo*/
        public bool colisionPoderGalacticaALaxo(Jefe jefe, Jefe.tipo tipoJefe)
        {
            if (tipoJefe == Jefe.tipo.Laxo && Nave.poderGalactica.Count > 0)
            {
                for (i = 0; i < Nave.poderGalactica.Count; i++)
                {
                    for (j = 0; j < 3; j++)
                    {
                        distancia = (float)Math.Sqrt(
                          Math.Pow(Nave.poderGalactica[i].Position.X -
                          (jefe.Position.X + Jefe.collitionLaxoX[j]), 2) +
                          Math.Pow(Nave.poderGalactica[i].Position.Y -
                          (jefe.Position.Y + Jefe.collitionLaxoY[j]), 2)
                        );

                        if (distancia <= Poderes.radioCollitPGalactica + Jefe.radioCollitionDanoLaxo)
                        {
                            positionUltimoDisparo.X = Nave.poderGalactica[i].Position.X;
                            positionUltimoDisparo.Y = Nave.poderGalactica[i].Position.Y;
                            ultimoTipoColision = 1;
                            Nave.poderGalacticaTotal.Add(Nave.poderGalactica[i]);
                            Nave.poderGalactica.RemoveAt(i);
                            return true;
                        }
                    }
                }
                for (i = 0; i < Nave.poderGalactica.Count; i++)
                {
                    distancia = (float)Math.Sqrt(
                      Math.Pow(Nave.poderGalactica[i].Position.X - jefe.Position.X, 2) +
                      Math.Pow(Nave.poderGalactica[i].Position.Y - jefe.Position.Y, 2)
                    );

                    if (distancia <= Poderes.radioCollitPGalactica + Jefe.radioCollitionLaxo)
                    {
                        positionUltimoDisparo.X = Nave.poderGalactica[i].Position.X;
                        positionUltimoDisparo.Y = Nave.poderGalactica[i].Position.Y;
                        ultimoTipoColision = 0;
                        Nave.poderGalacticaTotal.Add(Nave.poderGalactica[i]);
                        Nave.poderGalactica.RemoveAt(i);
                        return true;
                    }
                }
            }

            return false;
        }


        /*Colision PoderCarafe a Laxo*/
        public bool colisionPoderCarafeALaxo(Jefe jefe, Jefe.tipo tipoJefe)
        {
            if (tipoJefe == Jefe.tipo.Laxo && Nave.poderCarafe.Count > 0)
            {
                for (i = 0; i < Nave.poderCarafe.Count; i++)
                {
                    for (j = 0; j < 3; j++)
                    {
                        distancia = (float)Math.Sqrt(
                          Math.Pow(Nave.poderCarafe[i].Position.X -
                          (jefe.Position.X + Jefe.collitionLaxoX[j]), 2) +
                          Math.Pow(Nave.poderCarafe[i].Position.Y -
                          (jefe.Position.Y + Jefe.collitionLaxoY[j]), 2)
                        );

                        if (distancia <= Poderes.radioCollitPCarafe + Jefe.radioCollitionDanoLaxo)
                        {
                            positionUltimoDisparo.X = Nave.poderCarafe[i].Position.X;
                            positionUltimoDisparo.Y = Nave.poderCarafe[i].Position.Y;
                            ultimoTipoColision = 1;
                            confirmaEliminaPoderCarafe(i);
                            return true;
                        }
                    }
                }
                for (i = 0; i < Nave.poderCarafe.Count; i++)
                {
                        distancia = (float)Math.Sqrt(
                          Math.Pow(Nave.poderCarafe[i].Position.X -jefe.Position.X, 2) +
                          Math.Pow(Nave.poderCarafe[i].Position.Y -jefe.Position.Y, 2)
                        );

                        if (distancia <= Poderes.radioCollitPCarafe + Jefe.radioCollitionLaxo)
                        {
                            positionUltimoDisparo.X = Nave.poderCarafe[i].Position.X;
                            positionUltimoDisparo.Y = Nave.poderCarafe[i].Position.Y;
                            ultimoTipoColision = 0;
                            confirmaEliminaPoderCarafe(i);
                            return true;
                        }
                }
            }

            return false;
        }


         /*Colision PoderHelix a Laxo*/
        public bool colisionPoderHelixALaxo(Jefe jefe, Jefe.tipo tipoJefe)
        {
            for (i = 0; i < Nave.poderHelix.Count; i++)
            {
                for (j = 0; j < 3; j++)
                {
                    distancia = (float)Math.Sqrt(
                      Math.Pow(Nave.poderHelix[i].Position.X -
                      (jefe.Position.X + Jefe.collitionLaxoX[j]), 2) +
                      Math.Pow(Nave.poderHelix[i].Position.Y -
                      (jefe.Position.Y + Jefe.collitionLaxoY[j]), 2)
                    );

                    if (distancia <= Poderes.radioCollitPHelix + Jefe.radioCollitionDanoLaxo)
                    {
                        positionUltimoDisparo.X = Nave.poderHelix[i].Position.X;
                        positionUltimoDisparo.Y = Nave.poderHelix[i].Position.Y;
                        ultimoTipoColision = 1;
                        Nave.poderHelixTotal.Add(Nave.poderHelix[i]);
                        Nave.poderHelix.RemoveAt(i);
                        return true;
                    }
                }
            }
                for (i = 0; i < Nave.poderHelix.Count; i++)
                {
                    distancia = (float)Math.Sqrt(
                      Math.Pow(Nave.poderHelix[i].Position.X - jefe.Position.X, 2) +
                      Math.Pow(Nave.poderHelix[i].Position.Y - jefe.Position.Y, 2)
                    );

                    if (distancia <= Poderes.radioCollitPHelix + Jefe.radioCollitionLaxo)
                    {
                        positionUltimoDisparo.X = Nave.poderHelix[i].Position.X;
                        positionUltimoDisparo.Y = Nave.poderHelix[i].Position.Y;
                        ultimoTipoColision = 0;
                        Nave.poderHelixTotal.Add(Nave.poderHelix[i]);
                        Nave.poderHelix.RemoveAt(i);
                        return true;
                    }
                }

            return false;
        }

        /*Colision Poder Arp a Laxo*/
        public bool colisionPoderArpALaxo(Jefe jefe, Jefe.tipo tipoJefe)
        {
            for (i = 0; i < Nave.poderArp.Count; i++)
            {
                for (w= 0; w < 6; w++)
                {
                    for (j = 0; j < 3; j++)
                    {
                        distancia = (float)Math.Sqrt(
                          Math.Pow((Nave.poderArp[i].Position.X + Poderes.collitionPArpX[w]) -
                          (jefe.Position.X + Jefe.collitionLaxoX[j]), 2) +
                          Math.Pow((Nave.poderArp[i].Position.Y + Poderes.collitionPArpY[w]) -
                          (jefe.Position.Y + Jefe.collitionLaxoY[j]), 2)
                        );

                        if (distancia <= Poderes.radioCollitPArp + Jefe.radioCollitionDanoLaxo)
                        {
                            positionUltimoDisparo.X = Nave.poderArp[i].Position.X;
                            positionUltimoDisparo.Y = Nave.poderArp[i].Position.Y;
                            ultimoTipoColision = 1;
                            Nave.poderArpTotal.Add(Nave.poderArp[i]);
                            Nave.poderArp.RemoveAt(i);
                            return true;
                        }
                    }
                }
            }
            for (i = 0; i < Nave.poderArp.Count; i++)
            {
                for (w = 0; w < 6; w++)
                {
                        distancia = (float)Math.Sqrt(
                          Math.Pow((Nave.poderArp[i].Position.X + Poderes.collitionPArpX[w]) -
                          jefe.Position.X, 2) +
                          Math.Pow((Nave.poderArp[i].Position.Y + Poderes.collitionPArpY[w]) -
                          jefe.Position.Y, 2)
                        );

                        if (distancia <= Poderes.radioCollitPArp + Jefe.radioCollitionLaxo)
                        {
                            positionUltimoDisparo.X = Nave.poderArp[i].Position.X;
                            positionUltimoDisparo.Y = Nave.poderArp[i].Position.Y;
                            ultimoTipoColision = 0;
                            return true;
                        }
                }
            }
            return false;
        }


        /*Colision Poder Perseus a Laxo*/
        public bool colisionPoderPerseusALaxo(Jefe jefe, Jefe.tipo tipoJefe)
        {
            for (i = 0; i < Nave.poderPerseus.Count; i++)
            {
                for (j = 0; j < 3; j++)
                {
                    distancia = (float)Math.Sqrt(
                      Math.Pow(Nave.poderPerseus[i].Position.X -
                      (jefe.Position.X + Jefe.collitionLaxoX[j]), 2) +
                      Math.Pow(Nave.poderPerseus[i].Position.Y -
                      (jefe.Position.Y + Jefe.collitionLaxoY[j]), 2)
                    );

                    if (distancia <= Poderes.radioCollitPPerseus + Jefe.radioCollitionDanoLaxo)
                    {
                        positionUltimoDisparo.X = jefe.Position.X;
                        positionUltimoDisparo.Y = jefe.Position.Y;
                        ultimoTipoColision = 1;
                        Nave.poderPerseus[i].Escala = 0.1f;
                        Nave.poderPerseusTotal.Add(Nave.poderPerseus[i]);
                        Nave.poderPerseus.RemoveAt(i);
                        return true;
                    }
                }
            }
            for (i = 0; i < Nave.poderPerseus.Count; i++)
            {
                    distancia = (float)Math.Sqrt(
                      Math.Pow(Nave.poderPerseus[i].Position.X - jefe.Position.X, 2) +
                      Math.Pow(Nave.poderPerseus[i].Position.Y - jefe.Position.Y, 2)
                    );

                    if (distancia <= Poderes.radioCollitPPerseus + Jefe.radioCollitionLaxo)
                    {
                        positionUltimoDisparo.X = jefe.Position.X;
                        positionUltimoDisparo.Y = jefe.Position.Y;
                        ultimoTipoColision = 0;
                        return true;
                    }
            }
            return false;
        }

        /*Colision Poder Pisces a Laxo*/
        public bool colisionPoderPiscesALaxo(Jefe jefe, Jefe.tipo tipoJefe)
        {
            for (i = 0; i < Nave.poderPisces.Count; i++)
            {
                for (j = 0; j < 3; j++)
                {
                    distancia = (float)Math.Sqrt(
                      Math.Pow(Nave.poderPisces[i].Position.X -
                      (jefe.Position.X + Jefe.collitionLaxoX[j]), 2) +
                      Math.Pow(Nave.poderPisces[i].Position.Y -
                      (jefe.Position.Y + Jefe.collitionLaxoY[j]), 2)
                    );

                    if (distancia <= Poderes.radioCollitPPisces + Jefe.radioCollitionDanoLaxo)
                    {
                        positionUltimoDisparo.X = Nave.poderPisces[i].Position.X;
                        positionUltimoDisparo.Y = Nave.poderPisces[i].Position.Y;
                        ultimoTipoColision = 1;
                        Nave.poderPiscesTotal.Add(Nave.poderPisces[i]);
                        Nave.poderPisces.RemoveAt(i);
                        return true;
                    }
                }
            }
            for (i = 0; i < Nave.poderPisces.Count; i++)
            {
                    distancia = (float)Math.Sqrt(
                      Math.Pow(Nave.poderPisces[i].Position.X -jefe.Position.X, 2) +
                      Math.Pow(Nave.poderPisces[i].Position.Y -jefe.Position.Y, 2)
                    );

                    if (distancia <= Poderes.radioCollitPPisces + Jefe.radioCollitionLaxo)
                    {
                        positionUltimoDisparo.X = Nave.poderPisces[i].Position.X;
                        positionUltimoDisparo.Y = Nave.poderPisces[i].Position.Y;
                        ultimoTipoColision = 0;
                        Nave.poderPiscesTotal.Add(Nave.poderPisces[i]);
                        Nave.poderPisces.RemoveAt(i);
                        return true;
                    }
            }
            return false;
        }


        //Colision disparo a brazo 1 esp
        public bool colisionDisparoABrazo1Esp(Vector2 PosBrazo, float Rotacion)
        {
            if (Nave.disparos.Count > 0)
            {
                for (i = 0; i < Nave.disparos.Count; i++)
                {
                    for (j = 0; j < 9; j++)
                    {
                        X1p = (Jefe.collitionBrazoEspX[j]);
                        Y1p = (Jefe.collitionBrazoEspY[j]);

                        X1 = (PosBrazo.X) + ((X1p * Math.Cos(MathHelper.ToRadians(Rotacion))) -
                              (Y1p * Math.Sin(MathHelper.ToRadians(Rotacion))));
                        Y1 = (PosBrazo.Y) + ((X1p * Math.Sin(MathHelper.ToRadians(Rotacion))) +
                              (Y1p * Math.Cos(MathHelper.ToRadians(Rotacion))));

                        distancia = (float)Math.Sqrt(
                          Math.Pow(Nave.disparos[i].Position.X - X1, 2) +
                          Math.Pow(Nave.disparos[i].Position.Y - Y1, 2)
                        );

                        if (distancia <= Nave.disparos[i].radioCollition +
                            Jefe.radioCollitionBrazoEsp)
                        {
                            positionUltimoDisparo.X = Nave.disparos[i].getPosition().X;
                            positionUltimoDisparo.Y = Nave.disparos[i].getPosition().Y;
                            confirmaEliminaDisparoNave(i);
                            return true;
                        }
                    }

                }
            }
            return false;
        }

        //**********Colision Disparo a brazo 2 esp
        public bool colisionDisparoABrazo2Esp(Vector2 PosBrazo, float Rotacion)
        {
            if (Nave.disparos.Count > 0)
            {
                for (i = 0; i < Nave.disparos.Count; i++)
                {
                    for (j = 9; j < 18; j++)
                    {
                        X1p = (Jefe.collitionBrazoEspX[j]);
                        Y1p = (Jefe.collitionBrazoEspY[j]);

                        X1 = (PosBrazo.X) + ((X1p * Math.Cos(MathHelper.ToRadians(Rotacion))) -
                              (Y1p * Math.Sin(MathHelper.ToRadians(Rotacion))));
                        Y1 = (PosBrazo.Y) + ((X1p * Math.Sin(MathHelper.ToRadians(Rotacion))) +
                              (Y1p * Math.Cos(MathHelper.ToRadians(Rotacion))));

                        distancia = (float)Math.Sqrt(
                          Math.Pow(Nave.disparos[i].Position.X - X1, 2) +
                          Math.Pow(Nave.disparos[i].Position.Y - Y1, 2)
                        );

                        if (distancia <= Nave.disparos[i].radioCollition +
                            Jefe.radioCollitionBrazoEsp)
                        {
                            positionUltimoDisparo.X = Nave.disparos[i].getPosition().X;
                            positionUltimoDisparo.Y = Nave.disparos[i].getPosition().Y;
                            confirmaEliminaDisparoNave(i);
                            return true;
                        }
                    }

                }
            }
            return false;
        }

        //*******Colision Poder Galactica Brazo 1
        public bool colisionPGalacticaABrazo1(Vector2 PosBrazo, float Rotacion)
        {
            if (Nave.poderGalactica.Count> 0)
            {
                for (i = 0; i < Nave.poderGalactica.Count; i++)
                {
                    for (j = 0; j < 9; j++)
                    {
                        X1p = (Jefe.collitionBrazoEspX[j]);
                        Y1p = (Jefe.collitionBrazoEspY[j]);

                        X1 = (PosBrazo.X) + ((X1p * Math.Cos(MathHelper.ToRadians(Rotacion))) -
                              (Y1p * Math.Sin(MathHelper.ToRadians(Rotacion))));
                        Y1 = (PosBrazo.Y) + ((X1p * Math.Sin(MathHelper.ToRadians(Rotacion))) +
                              (Y1p * Math.Cos(MathHelper.ToRadians(Rotacion))));

                        distancia = (float)Math.Sqrt(
                          Math.Pow(Nave.poderGalactica[i].Position.X - X1, 2) +
                          Math.Pow(Nave.poderGalactica[i].Position.Y - Y1, 2)
                        );

                        if (distancia <= Poderes.radioCollitPGalactica +
                            Jefe.radioCollitionBrazoEsp)
                        {
                            positionUltimoDisparo.X = Nave.poderGalactica[i].getPosition().X;
                            positionUltimoDisparo.Y = Nave.poderGalactica[i].getPosition().Y;
                            Nave.poderGalacticaTotal.Add(Nave.poderGalactica[i]);
                            Nave.poderGalactica.RemoveAt(i);
                            return true;
                        }
                    }

                }
            }
            return false;
        }

        //*******Colision Poder Galactica Brazo 2
        public bool colisionPGalacticaABrazo2(Vector2 PosBrazo, float Rotacion)
        {
            if (Nave.poderGalactica.Count > 0)
            {
                for (i = 0; i < Nave.poderGalactica.Count; i++)
                {
                    for (j = 9; j < 18; j++)
                    {
                        X1p = (Jefe.collitionBrazoEspX[j]);
                        Y1p = (Jefe.collitionBrazoEspY[j]);

                        X1 = (PosBrazo.X) + ((X1p * Math.Cos(MathHelper.ToRadians(Rotacion))) -
                              (Y1p * Math.Sin(MathHelper.ToRadians(Rotacion))));
                        Y1 = (PosBrazo.Y) + ((X1p * Math.Sin(MathHelper.ToRadians(Rotacion))) +
                              (Y1p * Math.Cos(MathHelper.ToRadians(Rotacion))));

                        distancia = (float)Math.Sqrt(
                          Math.Pow(Nave.poderGalactica[i].Position.X - X1, 2) +
                          Math.Pow(Nave.poderGalactica[i].Position.Y - Y1, 2)
                        );

                        if (distancia <= Poderes.radioCollitPGalactica +
                            Jefe.radioCollitionBrazoEsp)
                        {
                            positionUltimoDisparo.X = Nave.poderGalactica[i].getPosition().X;
                            positionUltimoDisparo.Y = Nave.poderGalactica[i].getPosition().Y;
                            Nave.poderGalacticaTotal.Add(Nave.poderGalactica[i]);
                            Nave.poderGalactica.RemoveAt(i);
                            return true;
                        }
                    }

                }
            }
            return false;
        }

        //*******Colision Poder Carafe Brazo 1
        public bool colisionPCarafeABrazo1(Vector2 PosBrazo, float Rotacion)
        {
            if (Nave.poderCarafe.Count > 0)
            {
                for (i = 0; i < Nave.poderCarafe.Count; i++)
                {
                    for (j = 0; j < 9; j++)
                    {
                        X1p = (Jefe.collitionBrazoEspX[j]);
                        Y1p = (Jefe.collitionBrazoEspY[j]);

                        X1 = (PosBrazo.X) + ((X1p * Math.Cos(MathHelper.ToRadians(Rotacion))) -
                              (Y1p * Math.Sin(MathHelper.ToRadians(Rotacion))));
                        Y1 = (PosBrazo.Y) + ((X1p * Math.Sin(MathHelper.ToRadians(Rotacion))) +
                              (Y1p * Math.Cos(MathHelper.ToRadians(Rotacion))));

                        distancia = (float)Math.Sqrt(
                          Math.Pow(Nave.poderCarafe[i].Position.X - X1, 2) +
                          Math.Pow(Nave.poderCarafe[i].Position.Y - Y1, 2)
                        );

                        if (distancia <= Poderes.radioCollitPCarafe +
                            Jefe.radioCollitionBrazoEsp)
                        {
                            positionUltimoDisparo.X = Nave.poderCarafe[i].getPosition().X;
                            positionUltimoDisparo.Y = Nave.poderCarafe[i].getPosition().Y;
                            confirmaEliminaPoderCarafe(i);
                            return true;
                        }
                    }

                }
            }
            return false;
        }

        //*******Colision Poder Carafe Brazo 2
        public bool colisionPCarafeABrazo2(Vector2 PosBrazo, float Rotacion)
        {
            if (Nave.poderCarafe.Count > 0)
            {
                for (i = 0; i < Nave.poderCarafe.Count; i++)
                {
                    for (j = 9; j < 18; j++)
                    {
                        X1p = (Jefe.collitionBrazoEspX[j]);
                        Y1p = (Jefe.collitionBrazoEspY[j]);

                        X1 = (PosBrazo.X) + ((X1p * Math.Cos(MathHelper.ToRadians(Rotacion))) -
                              (Y1p * Math.Sin(MathHelper.ToRadians(Rotacion))));
                        Y1 = (PosBrazo.Y) + ((X1p * Math.Sin(MathHelper.ToRadians(Rotacion))) +
                              (Y1p * Math.Cos(MathHelper.ToRadians(Rotacion))));

                        distancia = (float)Math.Sqrt(
                          Math.Pow(Nave.poderCarafe[i].Position.X - X1, 2) +
                          Math.Pow(Nave.poderCarafe[i].Position.Y - Y1, 2)
                        );

                        if (distancia <= Poderes.radioCollitPCarafe +
                            Jefe.radioCollitionBrazoEsp)
                        {
                            positionUltimoDisparo.X = Nave.poderCarafe[i].getPosition().X;
                            positionUltimoDisparo.Y = Nave.poderCarafe[i].getPosition().Y;
                            confirmaEliminaPoderCarafe(i);
                            return true;
                        }
                    }

                }
            }
            return false;
        }


        //*******Colision Poder Helix Brazo 1
        public bool colisionPHelixABrazo1(Vector2 PosBrazo, float Rotacion)
        {
            if (Nave.poderHelix.Count > 0)
            {
                for (i = 0; i < Nave.poderHelix.Count; i++)
                {
                    for (j = 0; j < 9; j++)
                    {
                        X1p = (Jefe.collitionBrazoEspX[j]);
                        Y1p = (Jefe.collitionBrazoEspY[j]);

                        X1 = (PosBrazo.X) + ((X1p * Math.Cos(MathHelper.ToRadians(Rotacion))) -
                              (Y1p * Math.Sin(MathHelper.ToRadians(Rotacion))));
                        Y1 = (PosBrazo.Y) + ((X1p * Math.Sin(MathHelper.ToRadians(Rotacion))) +
                              (Y1p * Math.Cos(MathHelper.ToRadians(Rotacion))));

                        distancia = (float)Math.Sqrt(
                          Math.Pow(Nave.poderHelix[i].Position.X - X1, 2) +
                          Math.Pow(Nave.poderHelix[i].Position.Y - Y1, 2)
                        );

                        if (distancia <= Poderes.radioCollitPHelix +
                            Jefe.radioCollitionBrazoEsp)
                        {
                            positionUltimoDisparo.X = Nave.poderHelix[i].getPosition().X;
                            positionUltimoDisparo.Y = Nave.poderHelix[i].getPosition().Y;
                            Nave.poderHelixTotal.Add(Nave.poderHelix[i]);
                            Nave.poderHelix.RemoveAt(i);
                            return true;
                        }
                    }

                }
            }
            return false;
        }


        //*******Colision Poder Helix Brazo 2
        public bool colisionPHelixABrazo2(Vector2 PosBrazo, float Rotacion)
        {
            if (Nave.poderHelix.Count > 0)
            {
                for (i = 0; i < Nave.poderHelix.Count; i++)
                {
                    for (j = 9; j < 18; j++)
                    {
                        X1p = (Jefe.collitionBrazoEspX[j]);
                        Y1p = (Jefe.collitionBrazoEspY[j]);

                        X1 = (PosBrazo.X) + ((X1p * Math.Cos(MathHelper.ToRadians(Rotacion))) -
                              (Y1p * Math.Sin(MathHelper.ToRadians(Rotacion))));
                        Y1 = (PosBrazo.Y) + ((X1p * Math.Sin(MathHelper.ToRadians(Rotacion))) +
                              (Y1p * Math.Cos(MathHelper.ToRadians(Rotacion))));

                        distancia = (float)Math.Sqrt(
                          Math.Pow(Nave.poderHelix[i].Position.X - X1, 2) +
                          Math.Pow(Nave.poderHelix[i].Position.Y - Y1, 2)
                        );

                        if (distancia <= Poderes.radioCollitPHelix +
                            Jefe.radioCollitionBrazoEsp)
                        {
                            positionUltimoDisparo.X = Nave.poderHelix[i].getPosition().X;
                            positionUltimoDisparo.Y = Nave.poderHelix[i].getPosition().Y;
                            Nave.poderHelixTotal.Add(Nave.poderHelix[i]);
                            Nave.poderHelix.RemoveAt(i);
                            return true;
                        }
                    }

                }
            }
            return false;
        }


        //*******Colision Poder Pisces Brazo 1
        public bool colisionPPiscesABrazo1(Vector2 PosBrazo, float Rotacion)
        {
            if (Nave.poderPisces.Count > 0)
            {
                for (i = 0; i < Nave.poderPisces.Count; i++)
                {
                    for (j = 0; j < 9; j++)
                    {
                        X1p = (Jefe.collitionBrazoEspX[j]);
                        Y1p = (Jefe.collitionBrazoEspY[j]);

                        X1 = (PosBrazo.X) + ((X1p * Math.Cos(MathHelper.ToRadians(Rotacion))) -
                              (Y1p * Math.Sin(MathHelper.ToRadians(Rotacion))));
                        Y1 = (PosBrazo.Y) + ((X1p * Math.Sin(MathHelper.ToRadians(Rotacion))) +
                              (Y1p * Math.Cos(MathHelper.ToRadians(Rotacion))));

                        distancia = (float)Math.Sqrt(
                          Math.Pow(Nave.poderPisces[i].Position.X - X1, 2) +
                          Math.Pow(Nave.poderPisces[i].Position.Y - Y1, 2)
                        );

                        if (distancia <= Poderes.radioCollitPPisces +
                            Jefe.radioCollitionBrazoEsp)
                        {
                            positionUltimoDisparo.X = Nave.poderPisces[i].getPosition().X;
                            positionUltimoDisparo.Y = Nave.poderPisces[i].getPosition().Y;
                            Nave.poderPiscesTotal.Add(Nave.poderPisces[i]);
                            Nave.poderPisces.RemoveAt(i);
                            return true;
                        }
                    }

                }
            }
            return false;
        }


        //*******Colision Poder Pisces Brazo 2
        public bool colisionPPiscesABrazo2(Vector2 PosBrazo, float Rotacion)
        {
            if (Nave.poderPisces.Count > 0)
            {
                for (i = 0; i < Nave.poderPisces.Count; i++)
                {
                    for (j = 9; j < 18; j++)
                    {
                        X1p = (Jefe.collitionBrazoEspX[j]);
                        Y1p = (Jefe.collitionBrazoEspY[j]);

                        X1 = (PosBrazo.X) + ((X1p * Math.Cos(MathHelper.ToRadians(Rotacion))) -
                              (Y1p * Math.Sin(MathHelper.ToRadians(Rotacion))));
                        Y1 = (PosBrazo.Y) + ((X1p * Math.Sin(MathHelper.ToRadians(Rotacion))) +
                              (Y1p * Math.Cos(MathHelper.ToRadians(Rotacion))));

                        distancia = (float)Math.Sqrt(
                          Math.Pow(Nave.poderPisces[i].Position.X - X1, 2) +
                          Math.Pow(Nave.poderPisces[i].Position.Y - Y1, 2)
                        );

                        if (distancia <= Poderes.radioCollitPPisces +
                            Jefe.radioCollitionBrazoEsp)
                        {
                            positionUltimoDisparo.X = Nave.poderPisces[i].getPosition().X;
                            positionUltimoDisparo.Y = Nave.poderPisces[i].getPosition().Y;
                            Nave.poderPiscesTotal.Add(Nave.poderPisces[i]);
                            Nave.poderPisces.RemoveAt(i);
                            return true;
                        }
                    }

                }
            }
            return false;
        }


        //*********Colision disparo a Esp
        public bool colisionDisparoAEsp(Jefe jefe, Jefe.tipo tipoJefe)
        {
            if (tipoJefe == Jefe.tipo.Esp && Nave.disparos.Count > 0)
            {
                for (i = 0; i < Nave.disparos.Count; i++)
                {
                    for (j = 0; j < 7; j++)
                    {
                        distancia = (float)Math.Sqrt(
                          Math.Pow(Nave.disparos[i].Position.X -
                          (jefe.Position.X + Jefe.collitionEspX[j]), 2) +
                          Math.Pow(Nave.disparos[i].Position.Y -
                          (jefe.Position.Y + Jefe.collitionEspY[j]), 2)
                        );

                        if (distancia <= Nave.disparos[i].radioCollition + Jefe.radioCollitionEsp)
                        {
                            positionUltimoDisparo.X = Nave.disparos[i].getPosition().X;
                            positionUltimoDisparo.Y = Nave.disparos[i].getPosition().Y;
                            ultimoTipoColision = 1;
                            confirmaEliminaDisparoNave(i);
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        //*********Colision Poder Galactica a Esp
        public bool colisionPGalacticaAEsp(Jefe jefe, Jefe.tipo tipoJefe)
        {
            if (tipoJefe == Jefe.tipo.Esp && Nave.poderGalactica.Count > 0)
            {
                for (i = 0; i < Nave.poderGalactica.Count; i++)
                {
                    for (j = 0; j < 7; j++)
                    {
                        distancia = (float)Math.Sqrt(
                          Math.Pow(Nave.poderGalactica[i].Position.X -
                          (jefe.Position.X + Jefe.collitionEspX[j]), 2) +
                          Math.Pow(Nave.poderGalactica[i].Position.Y -
                          (jefe.Position.Y + Jefe.collitionEspY[j]), 2)
                        );

                        if (distancia <= Poderes.radioCollitPGalactica + Jefe.radioCollitionEsp)
                        {
                            positionUltimoDisparo.X = Nave.poderGalactica[i].getPosition().X;
                            positionUltimoDisparo.Y = Nave.poderGalactica[i].getPosition().Y;
                            ultimoTipoColision = 1;
                            Nave.poderGalacticaTotal.Add(Nave.poderGalactica[i]);
                            Nave.poderGalactica.RemoveAt(i);
                            return true;
                        }
                    }
                }
            }
            return false;
        }


        //*********Colision Poder Carafe a Esp
        public bool colisionPCarafeAEsp(Jefe jefe, Jefe.tipo tipoJefe)
        {
            if (tipoJefe == Jefe.tipo.Esp && Nave.poderCarafe.Count > 0)
            {
                for (i = 0; i < Nave.poderCarafe.Count; i++)
                {
                    for (j = 0; j < 7; j++)
                    {
                        distancia = (float)Math.Sqrt(
                          Math.Pow(Nave.poderCarafe[i].Position.X -
                          (jefe.Position.X + Jefe.collitionEspX[j]), 2) +
                          Math.Pow(Nave.poderCarafe[i].Position.Y -
                          (jefe.Position.Y + Jefe.collitionEspY[j]), 2)
                        );

                        if (distancia <= Poderes.radioCollitPCarafe + Jefe.radioCollitionEsp)
                        {
                            positionUltimoDisparo.X = Nave.poderCarafe[i].getPosition().X;
                            positionUltimoDisparo.Y = Nave.poderCarafe[i].getPosition().Y;
                            ultimoTipoColision = 1;
                            confirmaEliminaPoderCarafe(i);
                            return true;
                        }
                    }
                }
            }
            return false;
        }


        //*********Colision Poder Helix a Esp
        public bool colisionPHelixAEsp(Jefe jefe, Jefe.tipo tipoJefe)
        {
            if (tipoJefe == Jefe.tipo.Esp && Nave.poderHelix.Count > 0)
            {
                for (i = 0; i < Nave.poderHelix.Count; i++)
                {
                    for (j = 0; j < 7; j++)
                    {
                        distancia = (float)Math.Sqrt(
                          Math.Pow(Nave.poderHelix[i].Position.X -
                          (jefe.Position.X + Jefe.collitionEspX[j]), 2) +
                          Math.Pow(Nave.poderHelix[i].Position.Y -
                          (jefe.Position.Y + Jefe.collitionEspY[j]), 2)
                        );

                        if (distancia <= Poderes.radioCollitPHelix + Jefe.radioCollitionEsp)
                        {
                            positionUltimoDisparo.X = Nave.poderHelix[i].getPosition().X;
                            positionUltimoDisparo.Y = Nave.poderHelix[i].getPosition().Y;
                            ultimoTipoColision = 1;
                            Nave.poderHelixTotal.Add(Nave.poderHelix[i]);
                            Nave.poderHelix.RemoveAt(i);
                            return true;
                        }
                    }
                }
            }
            return false;
        }


        //*********Colision Poder Arp a Esp
        public bool colisionPArpAEsp(Jefe jefe, Jefe.tipo tipoJefe)
        {
            if (tipoJefe == Jefe.tipo.Esp && Nave.poderArp.Count > 0)
            {
                for (i = 0; i < Nave.poderArp.Count; i++)
                {
                    for (j = 0; j < 7; j++)
                    {
                        for (w = 0; w < 6; w++)
                        {
                            distancia = (float)Math.Sqrt(
                              Math.Pow((Nave.poderArp[i].Position.X + Poderes.collitionPArpX[w]) -
                              (jefe.Position.X + Jefe.collitionEspX[j]), 2) +
                              Math.Pow((Nave.poderArp[i].Position.Y + Poderes.collitionPArpY[w]) -
                              (jefe.Position.Y + Jefe.collitionEspY[j]), 2)
                            );

                            if (distancia <= Poderes.radioCollitPArp + Jefe.radioCollitionEsp)
                            {
                                positionUltimoDisparo.X = Nave.poderArp[i].getPosition().X;
                                positionUltimoDisparo.Y = Nave.poderArp[i].getPosition().Y;
                                ultimoTipoColision = 1;
                                Nave.poderArpTotal.Add(Nave.poderArp[i]);
                                Nave.poderArp.RemoveAt(i);
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }


        //*********Colision Poder Perseus a Esp
        public bool colisionPPerseusAEsp(Jefe jefe, Jefe.tipo tipoJefe)
        {
            if (tipoJefe == Jefe.tipo.Esp && Nave.poderPerseus.Count > 0)
            {
                for (i = 0; i < Nave.poderPerseus.Count; i++)
                {
                    for (j = 0; j < 7; j++)
                    {
                        distancia = (float)Math.Sqrt(
                          Math.Pow(Nave.poderPerseus[i].Position.X -
                          (jefe.Position.X + Jefe.collitionEspX[j]), 2) +
                          Math.Pow(Nave.poderPerseus[i].Position.Y -
                          (jefe.Position.Y + Jefe.collitionEspY[j]), 2)
                        );

                        if (distancia <= Poderes.radioCollitPPerseus + Jefe.radioCollitionEsp)
                        {
                            positionUltimoDisparo.X = jefe.Position.X;
                            positionUltimoDisparo.Y = jefe.Position.Y;
                            ultimoTipoColision = 1;
                            Nave.poderPerseus[i].Escala = 0.1f;
                            Nave.poderPerseusTotal.Add(Nave.poderPerseus[i]);
                            Nave.poderPerseus.RemoveAt(i);
                            return true;
                        }
                    }
                }
            }
            return false;
        }


        //*********Colision Poder Pisces a Esp
        public bool colisionPPiscesAEsp(Jefe jefe, Jefe.tipo tipoJefe)
        {
            if (tipoJefe == Jefe.tipo.Esp && Nave.poderPisces.Count > 0)
            {
                for (i = 0; i < Nave.poderPisces.Count; i++)
                {
                    for (j = 0; j < 7; j++)
                    {
                        distancia = (float)Math.Sqrt(
                          Math.Pow(Nave.poderPisces[i].Position.X -
                          (jefe.Position.X + Jefe.collitionEspX[j]), 2) +
                          Math.Pow(Nave.poderPisces[i].Position.Y -
                          (jefe.Position.Y + Jefe.collitionEspY[j]), 2)
                        );

                        if (distancia <= Poderes.radioCollitPPisces + Jefe.radioCollitionEsp)
                        {
                            positionUltimoDisparo.X = Nave.poderPisces[i].getPosition().X;
                            positionUltimoDisparo.Y = Nave.poderPisces[i].getPosition().Y;
                            ultimoTipoColision = 1;
                            Nave.poderPiscesTotal.Add(Nave.poderPisces[i]);
                            Nave.poderPisces.RemoveAt(i);
                            return true;
                        }
                    }
                }
            }
            return false;
        }


        //*********Colision disparo a Quad
        public bool colisionDisparoAQuad(Jefe jefe, Jefe.tipo tipoJefe)
        {
            if (Nave.disparos.Count > 0)
            {
                for (i = 0; i < Nave.disparos.Count; i++)
                {
                    for (j = 0; j < 4; j++)
                    {
                        distancia = (float)Math.Sqrt(
                          Math.Pow(Nave.disparos[i].Position.X -
                          (jefe.Position.X + Jefe.collitionQuadX[j]), 2) +
                          Math.Pow(Nave.disparos[i].Position.Y -
                          (jefe.Position.Y + Jefe.collitionQuadY[j]), 2)
                          );

                        if (distancia <= Nave.disparos[i].radioCollition +
                            Jefe.radioCollitionQuad)
                        {
                            positionUltimoDisparo.X = Nave.disparos[i].getPosition().X;
                            positionUltimoDisparo.Y = Nave.disparos[i].getPosition().Y;
                            confirmaEliminaDisparoNave(i);
                            return true;
                        }
                    }
                }
            }
            return false;
        }


        //*********Colision Poder Galactica a Quad
        public bool colisionPGalacticaAQuad(Jefe jefe, Jefe.tipo tipoJefe)
        {
            if (Nave.poderGalactica.Count > 0)
            {
                for (i = 0; i < Nave.poderGalactica.Count; i++)
                {
                    for (j = 0; j < 4; j++)
                    {
                        distancia = (float)Math.Sqrt(
                          Math.Pow(Nave.poderGalactica[i].Position.X -
                          (jefe.Position.X + Jefe.collitionQuadX[j]), 2) +
                          Math.Pow(Nave.poderGalactica[i].Position.Y -
                          (jefe.Position.Y + Jefe.collitionQuadY[j]), 2)
                          );

                        if (distancia <= Poderes.radioCollitPGalactica +Jefe.radioCollitionQuad)
                        {
                            positionUltimoDisparo.X = Nave.poderGalactica[i].getPosition().X;
                            positionUltimoDisparo.Y = Nave.poderGalactica[i].getPosition().Y;
                            Nave.poderGalacticaTotal.Add(Nave.poderGalactica[i]);
                            Nave.poderGalactica.RemoveAt(i);
                            return true;
                        }
                    }
                }
            }
            return false;
        }


        //*********Colision Poder Carafe a Quad
        public bool colisionPCarafeAQuad(Jefe jefe, Jefe.tipo tipoJefe)
        {
            if (Nave.poderCarafe.Count > 0)
            {
                for (i = 0; i < Nave.poderCarafe.Count; i++)
                {
                    for (j = 0; j < 4; j++)
                    {
                        distancia = (float)Math.Sqrt(
                          Math.Pow(Nave.poderCarafe[i].Position.X -
                          (jefe.Position.X + Jefe.collitionQuadX[j]), 2) +
                          Math.Pow(Nave.poderCarafe[i].Position.Y -
                          (jefe.Position.Y + Jefe.collitionQuadY[j]), 2)
                          );

                        if (distancia <= Poderes.radioCollitPCarafe + Jefe.radioCollitionQuad)
                        {
                            positionUltimoDisparo.X = Nave.poderCarafe[i].getPosition().X;
                            positionUltimoDisparo.Y = Nave.poderCarafe[i].getPosition().Y;
                            confirmaEliminaPoderCarafe(i);
                            return true;
                        }
                    }
                }
            }
            return false;
        }


        //*********Colision Poder Helix a Quad
        public bool colisionPHelixAQuad(Jefe jefe, Jefe.tipo tipoJefe)
        {
            if (Nave.poderHelix.Count > 0)
            {
                for (i = 0; i < Nave.poderHelix.Count; i++)
                {
                    for (j = 0; j < 4; j++)
                    {
                        distancia = (float)Math.Sqrt(
                          Math.Pow(Nave.poderHelix[i].Position.X -
                          (jefe.Position.X + Jefe.collitionQuadX[j]), 2) +
                          Math.Pow(Nave.poderHelix[i].Position.Y -
                          (jefe.Position.Y + Jefe.collitionQuadY[j]), 2)
                          );

                        if (distancia <= Poderes.radioCollitPHelix + Jefe.radioCollitionQuad)
                        {
                            positionUltimoDisparo.X = Nave.poderHelix[i].getPosition().X;
                            positionUltimoDisparo.Y = Nave.poderHelix[i].getPosition().Y;
                            Nave.poderHelixTotal.Add(Nave.poderHelix[i]);
                            Nave.poderHelix.RemoveAt(i);
                            return true;
                        }
                    }
                }
            }
            return false;
        }


        //*********Colision Poder Pisces a Quad
        public bool colisionPPiscesAQuad(Jefe jefe, Jefe.tipo tipoJefe)
        {
            if (Nave.poderPisces.Count > 0)
            {
                for (i = 0; i < Nave.poderPisces.Count; i++)
                {
                    for (j = 0; j < 4; j++)
                    {
                        distancia = (float)Math.Sqrt(
                          Math.Pow(Nave.poderPisces[i].Position.X -
                          (jefe.Position.X + Jefe.collitionQuadX[j]), 2) +
                          Math.Pow(Nave.poderPisces[i].Position.Y -
                          (jefe.Position.Y + Jefe.collitionQuadY[j]), 2)
                          );

                        if (distancia <= Poderes.radioCollitPPisces + Jefe.radioCollitionQuad)
                        {
                            positionUltimoDisparo.X = Nave.poderPisces[i].getPosition().X;
                            positionUltimoDisparo.Y = Nave.poderPisces[i].getPosition().Y;
                            Nave.poderPiscesTotal.Add(Nave.poderPisces[i]);
                            Nave.poderPisces.RemoveAt(i);
                            return true;
                        }
                    }
                }
            }
            return false;
        }


        //*********Colision disparo a Martillo Quad
        public bool colisionDisparoAMartilloQuad(Vector2 PosMartillo)
        {
            if (Nave.disparos.Count > 0)
            {
                for (i = 0; i < Nave.disparos.Count; i++)
                {
                    for (j = 0; j < 4; j++)
                    {
                        distancia = (float)Math.Sqrt(
                          Math.Pow(Nave.disparos[i].Position.X -
                          (PosMartillo.X + Jefe.collitionMartilloQuadX[j]), 2) +
                          Math.Pow(Nave.disparos[i].Position.Y -
                          (PosMartillo.Y + Jefe.collitionMartilloQuadY[j]), 2)
                          );

                        if (distancia <= Nave.disparos[i].radioCollition +
                            Jefe.radioCollitionMartilloQuad)
                        {
                            positionUltimoDisparo.X = Nave.disparos[i].getPosition().X;
                            positionUltimoDisparo.Y = Nave.disparos[i].getPosition().Y;
                            if (j >= 1){ultimoTipoColision = 1;
                            }else { ultimoTipoColision = 0; }
                            confirmaEliminaDisparoNave(i);
                            return true;
                        }
                    }
                }
            }
            return false;
        }


        //*********Colision Poder Galactica a Martillo Quad
        public bool colisionPGalacticaAMartilloQuad(Vector2 PosMartillo)
        {
            if (Nave.poderGalactica.Count > 0)
            {
                for (i = 0; i < Nave.poderGalactica.Count; i++)
                {
                    for (j = 0; j < 4; j++)
                    {
                        distancia = (float)Math.Sqrt(
                          Math.Pow(Nave.poderGalactica[i].Position.X -
                          (PosMartillo.X + Jefe.collitionMartilloQuadX[j]), 2) +
                          Math.Pow(Nave.poderGalactica[i].Position.Y -
                          (PosMartillo.Y + Jefe.collitionMartilloQuadY[j]), 2)
                          );

                        if (distancia <= Poderes.radioCollitPGalactica +
                            Jefe.radioCollitionMartilloQuad)
                        {
                            positionUltimoDisparo.X = Nave.poderGalactica[i].getPosition().X;
                            positionUltimoDisparo.Y = Nave.poderGalactica[i].getPosition().Y;
                            if (j >= 1){ultimoTipoColision = 1;}
                            else { ultimoTipoColision = 0; }
                            Nave.poderGalacticaTotal.Add(Nave.poderGalactica[i]);
                            Nave.poderGalactica.RemoveAt(i);
                            return true;
                        }
                    }
                }
            }
            return false;
        }


        //*********Colision Poder Carafe a Martillo Quad
        public bool colisionPCarafeAMartilloQuad(Vector2 PosMartillo)
        {
            if (Nave.poderCarafe.Count > 0)
            {
                for (i = 0; i < Nave.poderCarafe.Count; i++)
                {
                    for (j = 0; j < 4; j++)
                    {
                        distancia = (float)Math.Sqrt(
                          Math.Pow(Nave.poderCarafe[i].Position.X -
                          (PosMartillo.X + Jefe.collitionMartilloQuadX[j]), 2) +
                          Math.Pow(Nave.poderCarafe[i].Position.Y -
                          (PosMartillo.Y + Jefe.collitionMartilloQuadY[j]), 2)
                          );

                        if (distancia <= Poderes.radioCollitPCarafe +
                            Jefe.radioCollitionMartilloQuad)
                        {
                            positionUltimoDisparo.X = Nave.poderCarafe[i].getPosition().X;
                            positionUltimoDisparo.Y = Nave.poderCarafe[i].getPosition().Y;
                            if (j >= 1) { ultimoTipoColision = 1; }
                            else { ultimoTipoColision = 0; }
                            confirmaEliminaPoderCarafe(i);
                            return true;
                        }
                    }
                }
            }
            return false;
        }


        //*********Colision Poder Helix a Martillo Quad
        public bool colisionPHelixAMartilloQuad(Vector2 PosMartillo)
        {
            if (Nave.poderHelix.Count > 0)
            {
                for (i = 0; i < Nave.poderHelix.Count; i++)
                {
                    for (j = 0; j < 4; j++)
                    {
                        distancia = (float)Math.Sqrt(
                          Math.Pow(Nave.poderHelix[i].Position.X -
                          (PosMartillo.X + Jefe.collitionMartilloQuadX[j]), 2) +
                          Math.Pow(Nave.poderHelix[i].Position.Y -
                          (PosMartillo.Y + Jefe.collitionMartilloQuadY[j]), 2)
                          );

                        if (distancia <= Poderes.radioCollitPHelix +
                            Jefe.radioCollitionMartilloQuad)
                        {
                            positionUltimoDisparo.X = Nave.poderHelix[i].getPosition().X;
                            positionUltimoDisparo.Y = Nave.poderHelix[i].getPosition().Y;
                            if (j >= 1) { ultimoTipoColision = 1; }
                            else { ultimoTipoColision = 0; }
                            Nave.poderHelixTotal.Add(Nave.poderHelix[i]);
                            Nave.poderHelix.RemoveAt(i);
                            return true;
                        }
                    }
                }
            }
            return false;
        }


        //*********Colision Poder Arp a Martillo Quad
        public bool colisionPArpAMartilloQuad(Vector2 PosMartillo)
        {
            if (Nave.poderArp.Count > 0)
            {
                for (i = 0; i < Nave.poderArp.Count; i++)
                {
                    for (j = 1; j < 4; j++)
                    {
                        for (w = 0; w < 6; w++)
                        {
                            distancia = (float)Math.Sqrt(
                              Math.Pow((Nave.poderArp[i].Position.X + Poderes.collitionPArpX[w]) -
                              (PosMartillo.X + Jefe.collitionMartilloQuadX[j]), 2) +
                              Math.Pow((Nave.poderArp[i].Position.Y + Poderes.collitionPArpY[w]) -
                              (PosMartillo.Y + Jefe.collitionMartilloQuadY[j]), 2)
                              );

                            if (distancia <= Poderes.radioCollitPArp +
                                Jefe.radioCollitionMartilloQuad)
                            {
                                positionUltimoDisparo.X = Nave.poderArp[i].getPosition().X;
                                positionUltimoDisparo.Y = Nave.poderArp[i].getPosition().Y;
                                if (j >= 1)
                                {
                                    ultimoTipoColision = 1; 
                                    Nave.poderArpTotal.Add(Nave.poderArp[i]);
                                    Nave.poderArp.RemoveAt(i);
                                }

                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }


        //*********Colision Poder Perseus a Martillo Quad
        public bool colisionPPerseusAMartilloQuad(Vector2 PosMartillo)
        {
            if (Nave.poderPerseus.Count > 0)
            {
                for (i = 0; i < Nave.poderPerseus.Count; i++)
                {
                    for (j = 1; j < 4; j++)
                    {
                        distancia = (float)Math.Sqrt(
                          Math.Pow(Nave.poderPerseus[i].Position.X -
                          (PosMartillo.X + Jefe.collitionMartilloQuadX[j]), 2) +
                          Math.Pow(Nave.poderPerseus[i].Position.Y -
                          (PosMartillo.Y + Jefe.collitionMartilloQuadY[j]), 2)
                          );

                        if (distancia <= Poderes.radioCollitPPerseus +
                            Jefe.radioCollitionMartilloQuad)
                        {
                            positionUltimoDisparo.X = PosMartillo.X;
                            positionUltimoDisparo.Y = PosMartillo.Y;
                            if (j >= 1)
                            {
                                ultimoTipoColision = 1;
                                Nave.poderPerseus[i].Escala = 0.1f;
                                Nave.poderPerseusTotal.Add(Nave.poderPerseus[i]);
                                Nave.poderPerseus.RemoveAt(i);
                            }
                            else { ultimoTipoColision = 0; }
                            return true;
                        }
                    }
                }
            }
            return false;
        }


        //*********Colision Poder Pisces a Martillo Quad
        public bool colisionPPiscesAMartilloQuad(Vector2 PosMartillo)
        {
            if (Nave.poderPisces.Count > 0)
            {
                for (i = 0; i < Nave.poderPisces.Count; i++)
                {
                    for (j = 0; j < 4; j++)
                    {
                        distancia = (float)Math.Sqrt(
                          Math.Pow(Nave.poderPisces[i].Position.X -
                          (PosMartillo.X + Jefe.collitionMartilloQuadX[j]), 2) +
                          Math.Pow(Nave.poderPisces[i].Position.Y -
                          (PosMartillo.Y + Jefe.collitionMartilloQuadY[j]), 2)
                          );

                        if (distancia <= Poderes.radioCollitPPisces +
                            Jefe.radioCollitionMartilloQuad)
                        {
                            positionUltimoDisparo.X = Nave.poderPisces[i].getPosition().X;
                            positionUltimoDisparo.Y = Nave.poderPisces[i].getPosition().Y;
                            if (j >= 1) { ultimoTipoColision = 1; }
                            else { ultimoTipoColision = 0; }
                            Nave.poderPiscesTotal.Add(Nave.poderPisces[i]);
                            Nave.poderPisces.RemoveAt(i);
                            return true;
                        }
                    }
                }
            }
            return false;
        }


        //*********Colision disparo a Rued
        public bool colisionDisparoARued(Jefe jefe, Jefe.tipo tipoJefe)
        {
            if (Nave.disparos.Count > 0)
            {
                for (i = 0; i < Nave.disparos.Count; i++)
                {
                    distancia = (float)Math.Sqrt(
                      Math.Pow(Nave.disparos[i].Position.X - jefe.Position.X, 2) +
                      Math.Pow(Nave.disparos[i].Position.Y - jefe.Position.Y, 2)
                      );

                    if (distancia <= Nave.disparos[i].radioCollition +
                        Jefe.radioCollitionRued)
                    {
                        positionUltimoDisparo.X = Nave.disparos[i].getPosition().X;
                        positionUltimoDisparo.Y = Nave.disparos[i].getPosition().Y;
                        confirmaEliminaDisparoNave(i);
                        return true;
                    }
                }
            }

            return false;
        }


        //*********Colision disparo a Flecha Rued
        public bool colisionDisparoAFlechaRued(Vector2 PosFlecha)
        {
            if (Nave.disparos.Count > 0)
            {
                for (i = 0; i < Nave.disparos.Count; i++)
                {
                        distancia = (float)Math.Sqrt(
                          Math.Pow(Nave.disparos[i].Position.X -PosFlecha.X, 2) +
                          Math.Pow(Nave.disparos[i].Position.Y -PosFlecha.Y, 2)
                          );

                        if (distancia <= Nave.disparos[i].radioCollition +
                            Jefe.radioCollitionFlechaRued)
                        {
                            positionUltimoDisparo.X = Nave.disparos[i].getPosition().X;
                            positionUltimoDisparo.Y = Nave.disparos[i].getPosition().Y;
                            confirmaEliminaDisparoNave(i);
                            return true;
                        }
                 }
            }
            return false;
        }

        //*********Colision Poder Glactica a Rued
        public bool colisionPGalacticaARued(Jefe jefe, Jefe.tipo tipoJefe)
        {
            if (Nave.poderGalactica.Count > 0)
            {
                for (i = 0; i < Nave.poderGalactica.Count; i++)
                {
                    distancia = (float)Math.Sqrt(
                      Math.Pow(Nave.poderGalactica[i].Position.X - jefe.Position.X, 2) +
                      Math.Pow(Nave.poderGalactica[i].Position.Y - jefe.Position.Y, 2)
                      );

                    if (distancia <= Poderes.radioCollitPGalactica +
                        Jefe.radioCollitionRued)
                    {
                        positionUltimoDisparo.X = Nave.poderGalactica[i].getPosition().X;
                        positionUltimoDisparo.Y = Nave.poderGalactica[i].getPosition().Y;
                        Nave.poderGalacticaTotal.Add(Nave.poderGalactica[i]);
                        Nave.poderGalactica.RemoveAt(i);
                        return true;
                    }
                }
            }

            return false;
        }


        //*********Colision Poder Carafe a Rued
        public bool colisionPCarafeARued(Jefe jefe, Jefe.tipo tipoJefe)
        {
            if (Nave.poderCarafe.Count > 0)
            {
                for (i = 0; i < Nave.poderCarafe.Count; i++)
                {
                    distancia = (float)Math.Sqrt(
                      Math.Pow(Nave.poderCarafe[i].Position.X - jefe.Position.X, 2) +
                      Math.Pow(Nave.poderCarafe[i].Position.Y - jefe.Position.Y, 2)
                      );

                    if (distancia <= Poderes.radioCollitPCarafe +
                        Jefe.radioCollitionRued)
                    {
                        positionUltimoDisparo.X = Nave.poderCarafe[i].getPosition().X;
                        positionUltimoDisparo.Y = Nave.poderCarafe[i].getPosition().Y;
                        confirmaEliminaPoderCarafe(i);
                        return true;
                    }
                }
            }

            return false;
        }

        //*********Colision Poder Helix a Rued
        public bool colisionPHelixARued(Jefe jefe, Jefe.tipo tipoJefe)
        {
            if (Nave.poderHelix.Count > 0)
            {
                for (i = 0; i < Nave.poderHelix.Count; i++)
                {
                    distancia = (float)Math.Sqrt(
                      Math.Pow(Nave.poderHelix[i].Position.X - jefe.Position.X, 2) +
                      Math.Pow(Nave.poderHelix[i].Position.Y - jefe.Position.Y, 2)
                      );

                    if (distancia <= Poderes.radioCollitPHelix +
                        Jefe.radioCollitionRued)
                    {
                        positionUltimoDisparo.X = Nave.poderHelix[i].getPosition().X;
                        positionUltimoDisparo.Y = Nave.poderHelix[i].getPosition().Y;
                        Nave.poderHelixTotal.Add(Nave.poderHelix[i]);
                        Nave.poderHelix.RemoveAt(i);
                        return true;
                    }
                }
            }

            return false;
        }


        //*********Colision Poder Pisces a Rued
        public bool colisionPPiscesARued(Jefe jefe, Jefe.tipo tipoJefe)
        {
            if (Nave.poderPisces.Count > 0)
            {
                for (i = 0; i < Nave.poderPisces.Count; i++)
                {
                    distancia = (float)Math.Sqrt(
                      Math.Pow(Nave.poderPisces[i].Position.X - jefe.Position.X, 2) +
                      Math.Pow(Nave.poderPisces[i].Position.Y - jefe.Position.Y, 2)
                      );

                    if (distancia <= Poderes.radioCollitPPisces +
                        Jefe.radioCollitionRued)
                    {
                        positionUltimoDisparo.X = Nave.poderPisces[i].getPosition().X;
                        positionUltimoDisparo.Y = Nave.poderPisces[i].getPosition().Y;
                        Nave.poderPiscesTotal.Add(Nave.poderPisces[i]);
                        Nave.poderPisces.RemoveAt(i);
                        return true;
                    }
                }
            }

            return false;
        }


        //*********Colision Poder Glactica a Flecha Rued
        public bool colisionPGalacticaAFlechaRued(Vector2 PosFlecha)
        {
            if (Nave.poderGalactica.Count > 0)
            {
                for (i = 0; i < Nave.poderGalactica.Count; i++)
                {
                    distancia = (float)Math.Sqrt(
                      Math.Pow(Nave.poderGalactica[i].Position.X - PosFlecha.X, 2) +
                      Math.Pow(Nave.poderGalactica[i].Position.Y - PosFlecha.Y, 2)
                      );

                    if (distancia <= Poderes.radioCollitPGalactica +
                        Jefe.radioCollitionFlechaRued)
                    {
                        positionUltimoDisparo.X = Nave.poderGalactica[i].getPosition().X;
                        positionUltimoDisparo.Y = Nave.poderGalactica[i].getPosition().Y;
                        Nave.poderGalacticaTotal.Add(Nave.poderGalactica[i]);
                        Nave.poderGalactica.RemoveAt(i);
                        return true;
                    }
                }
            }

            return false;
        }


        //*********Colision Poder Carafe a Flecha Rued
        public bool colisionPCarafeAFlechaRued(Vector2 PosFlecha)
        {
            if (Nave.poderCarafe.Count > 0)
            {
                for (i = 0; i < Nave.poderCarafe.Count; i++)
                {
                    distancia = (float)Math.Sqrt(
                      Math.Pow(Nave.poderCarafe[i].Position.X - PosFlecha.X, 2) +
                      Math.Pow(Nave.poderCarafe[i].Position.Y - PosFlecha.Y, 2)
                      );

                    if (distancia <= Poderes.radioCollitPCarafe +
                        Jefe.radioCollitionFlechaRued)
                    {
                        positionUltimoDisparo.X = Nave.poderCarafe[i].getPosition().X;
                        positionUltimoDisparo.Y = Nave.poderCarafe[i].getPosition().Y;
                        confirmaEliminaPoderCarafe(i);
                        return true;
                    }
                }
            }

            return false;
        }


        //*********Colision Poder Helix a Flecha Rued
        public bool colisionPHelixAFlechaRued(Vector2 PosFlecha)
        {
            if (Nave.poderHelix.Count > 0)
            {
                for (i = 0; i < Nave.poderHelix.Count; i++)
                {
                    distancia = (float)Math.Sqrt(
                      Math.Pow(Nave.poderHelix[i].Position.X - PosFlecha.X, 2) +
                      Math.Pow(Nave.poderHelix[i].Position.Y - PosFlecha.Y, 2)
                      );

                    if (distancia <= Poderes.radioCollitPHelix +
                        Jefe.radioCollitionFlechaRued)
                    {
                        positionUltimoDisparo.X = Nave.poderHelix[i].getPosition().X;
                        positionUltimoDisparo.Y = Nave.poderHelix[i].getPosition().Y;
                        Nave.poderHelixTotal.Add(Nave.poderHelix[i]);
                        Nave.poderHelix.RemoveAt(i);
                        return true;
                    }
                }
            }

            return false;
        }


        //*********Colision Poder Arp a Flecha Rued
        public bool colisionPArpAFlechaRued(Vector2 PosFlecha)
        {
            if (Nave.poderArp.Count > 0)
            {
                for (i = 0; i < Nave.poderArp.Count; i++)
                {
                    for (w = 0; w < 6; w++)
                    {
                        distancia = (float)Math.Sqrt(
                          Math.Pow((Nave.poderArp[i].Position.X + Poderes.collitionPArpX[w]) 
                          - PosFlecha.X, 2) +
                          Math.Pow((Nave.poderArp[i].Position.Y + Poderes.collitionPArpY[w])
                          - PosFlecha.Y, 2)
                          );

                        if (distancia <= Poderes.radioCollitPArp +
                            Jefe.radioCollitionFlechaRued)
                        {
                            positionUltimoDisparo.X = Nave.poderArp[i].getPosition().X;
                            positionUltimoDisparo.Y = Nave.poderArp[i].getPosition().Y;
                            Nave.poderArpTotal.Add(Nave.poderArp[i]);
                            Nave.poderArp.RemoveAt(i);
                            return true;
                        }
                    }
                }
            }

            return false;
        }


        //*********Colision Poder Perseus a Flecha Rued
        public bool colisionPPerseusAFlechaRued(Vector2 PosFlecha)
        {
            if (Nave.poderPerseus.Count > 0)
            {
                for (i = 0; i < Nave.poderPerseus.Count; i++)
                {
                        distancia = (float)Math.Sqrt(
                          Math.Pow(Nave.poderPerseus[i].Position.X- PosFlecha.X, 2) +
                          Math.Pow(Nave.poderPerseus[i].Position.Y- PosFlecha.Y, 2)
                          );

                        if (distancia <= Poderes.radioCollitPPerseus +
                            Jefe.radioCollitionFlechaRued)
                        {
                            positionUltimoDisparo.X = PosFlecha.X;
                            positionUltimoDisparo.Y = PosFlecha.Y;
                            Nave.poderPerseus[i].Escala = 0.1f;
                            Nave.poderPerseusTotal.Add(Nave.poderPerseus[i]);
                            Nave.poderPerseus.RemoveAt(i);
                            return true;
                        }
                  
                }
            }

            return false;
        }


        //*********Colision Poder Pisces a Flecha Rued
        public bool colisionPPiscesAFlechaRued(Vector2 PosFlecha)
        {
            if (Nave.poderPisces.Count > 0)
            {
                for (i = 0; i < Nave.poderPisces.Count; i++)
                {
                    distancia = (float)Math.Sqrt(
                      Math.Pow(Nave.poderPisces[i].Position.X - PosFlecha.X, 2) +
                      Math.Pow(Nave.poderPisces[i].Position.Y - PosFlecha.Y, 2)
                      );

                    if (distancia <= Poderes.radioCollitPPisces +
                        Jefe.radioCollitionFlechaRued)
                    {
                        positionUltimoDisparo.X = Nave.poderPisces[i].getPosition().X;
                        positionUltimoDisparo.Y = Nave.poderPisces[i].getPosition().Y;
                        Nave.poderPiscesTotal.Add(Nave.poderPisces[i]);
                        Nave.poderPisces.RemoveAt(i);
                        return true;
                    }

                }
            }

            return false;
        }


        //*****Nave a Jefe
        public bool colisionNaveALaxo(Jefe jefe)
        {
            for (int i = 0; i < 11; i++)
            {
                distancia = (float)Math.Sqrt(
                     Math.Pow(jefe.Position.X - 
                     (Nave.PositionNav.X + Nave.collitionX[i]), 2) +
                     Math.Pow(jefe.Position.Y - 
                     (Nave.PositionNav.Y + Nave.collitionY[i]), 2)
                     );

                if (distancia <= Jefe.radioCollitionLaxo + Nave.radioCollitionN)
                {
                    return true;
                }
            }
            for (int i = 0; i < 11; i++)
            {
                for (j = 0; j < 3; j++)
                {
                    distancia = (float)Math.Sqrt(
                         Math.Pow( (jefe.Position.X+Jefe.collitionLaxoX[j]) -
                         (Nave.PositionNav.X + Nave.collitionX[i]), 2) +
                         Math.Pow((jefe.Position.Y + Jefe.collitionLaxoY[j]) -
                         (Nave.PositionNav.Y + Nave.collitionY[i]), 2)
                         );

                    if (distancia <= Jefe.radioCollitionDanoLaxo + Nave.radioCollitionN)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        //*****Nave a Esp
        public bool colisionNaveAEsp(Jefe jefe)
        {
            for (int i = 0; i < 11; i++)
            {
                for (j = 0; j < 7; j++)
                {
                    distancia = (float)Math.Sqrt(
                         Math.Pow((jefe.Position.X + Jefe.collitionEspX[j]) -
                         (Nave.PositionNav.X + Nave.collitionX[i]), 2) +
                         Math.Pow((jefe.Position.Y + Jefe.collitionEspY[j]) -
                         (Nave.PositionNav.Y + Nave.collitionY[i]), 2)
                         );

                    if (distancia <= Jefe.radioCollitionEsp + Nave.radioCollitionN)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        //*****Nave a Brazo 1 Esp
        public bool colisionNaveABrazo1Esp(Vector2 PosBrazo,float Rotacion)
        {
            for (int i = 0; i < 11; i++)
            {
                for (j = 0; j < 9; j++)
                    {
                        X1p = (Jefe.collitionBrazoEspX[j]);
                        Y1p = (Jefe.collitionBrazoEspY[j]);

                        X1 = (PosBrazo.X) + ((X1p * Math.Cos(MathHelper.ToRadians(Rotacion))) -
                              (Y1p * Math.Sin(MathHelper.ToRadians(Rotacion))));
                        Y1 = (PosBrazo.Y) + ((X1p * Math.Sin(MathHelper.ToRadians(Rotacion))) +
                              (Y1p * Math.Cos(MathHelper.ToRadians(Rotacion))));

                        distancia = (float)Math.Sqrt(
                          Math.Pow((Nave.PositionNav.X + Nave.collitionX[i]) - X1, 2) +
                          Math.Pow((Nave.PositionNav.Y + Nave.collitionY[i]) - Y1, 2)
                        );

                        if (distancia <= Nave.radioCollitionN +
                            Jefe.radioCollitionBrazoEsp)
                        {
                            return true;
                        }
                    }
                
            }
            return false;
        }

        //*****Nave a Brazo 1 Esp
        public bool colisionNaveABrazo2Esp(Vector2 PosBrazo, float Rotacion)
        {
            for (int i = 0; i < 11; i++)
            {
                for (j = 9; j < 18; j++)
                {
                    X1p = (Jefe.collitionBrazoEspX[j]);
                    Y1p = (Jefe.collitionBrazoEspY[j]);

                    X1 = (PosBrazo.X) + ((X1p * Math.Cos(MathHelper.ToRadians(Rotacion))) -
                          (Y1p * Math.Sin(MathHelper.ToRadians(Rotacion))));
                    Y1 = (PosBrazo.Y) + ((X1p * Math.Sin(MathHelper.ToRadians(Rotacion))) +
                          (Y1p * Math.Cos(MathHelper.ToRadians(Rotacion))));

                    distancia = (float)Math.Sqrt(
                      Math.Pow((Nave.PositionNav.X + Nave.collitionX[i]) - X1, 2) +
                      Math.Pow((Nave.PositionNav.Y + Nave.collitionY[i]) - Y1, 2)
                    );

                    if (distancia <= Nave.radioCollitionN +
                        Jefe.radioCollitionBrazoEsp)
                    {
                        return true;
                    }
                }

            }
            return false;
        }


        //*****Nave a Quad
        public bool colisionNaveAQuad(Jefe jefe)
        {
            for (int i = 0; i < 11; i++)
            {
                for (j = 0; j < 4; j++)
                {
                    distancia = (float)Math.Sqrt(
                         Math.Pow((jefe.Position.X + Jefe.collitionQuadX[j]) -
                         (Nave.PositionNav.X + Nave.collitionX[i]), 2) +
                         Math.Pow((jefe.Position.Y + Jefe.collitionQuadY[j]) -
                         (Nave.PositionNav.Y + Nave.collitionY[i]), 2)
                         );

                    if (distancia <= Jefe.radioCollitionQuad + Nave.radioCollitionN)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        //*****Nave a Martillo Quad 
        public bool colisionNaveAMartilloQuad(Vector2 PosMartillo)
        {

            for (int i = 0; i < 11; i++)
            {
                for (j = 0; j < 4; j++)
                {
                    distancia = (float)Math.Sqrt(
                         Math.Pow((PosMartillo.X + Jefe.collitionMartilloQuadX[j]) -
                         (Nave.PositionNav.X + Nave.collitionX[i]), 2) +
                         Math.Pow((PosMartillo.Y + Jefe.collitionMartilloQuadY[j]) -
                         (Nave.PositionNav.Y + Nave.collitionY[i]), 2)
                         );

                    if (distancia <= Jefe.radioCollitionMartilloQuad + Nave.radioCollitionN)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        //*****Nave a Rued
        public bool colisionNaveARued(Jefe jefe)
        {
            for (int i = 0; i < 11; i++)
            {
                    distancia = (float)Math.Sqrt(
                         Math.Pow(jefe.Position.X -
                         (Nave.PositionNav.X + Nave.collitionX[i]), 2) +
                         Math.Pow(jefe.Position.Y -
                         (Nave.PositionNav.Y + Nave.collitionY[i]), 2)
                         );

                    if (distancia <= Jefe.radioCollitionRued + Nave.radioCollitionN)
                    {
                        return true;
                    }
            }
            return false;
        }

        //*****Nave a Flecha Rued
        public bool colisionNaveAFlechaRued(Vector2 PosFlecha)
        {
            for (int i = 0; i < 11; i++)
            {
                distancia = (float)Math.Sqrt(
                     Math.Pow(PosFlecha.X -
                     (Nave.PositionNav.X + Nave.collitionX[i]), 2) +
                     Math.Pow(PosFlecha.Y -
                     (Nave.PositionNav.Y + Nave.collitionY[i]), 2)
                     );

                if (distancia <= Jefe.radioCollitionFlechaRued + Nave.radioCollitionN)
                {
                    return true;
                }
            }
            return false;
        }


        //Comprobacion de poder carafe al colisionar con enemigos
        public void confirmaEliminaPoderCarafe(int indice)
        {
            if (Nave.poderCarafe[indice].tipoPoderCarafeActual == Poderes.tipoPoderCarafe.pMedio)
            {
                Nave.poderCarafeMedioTotal.Add(Nave.poderCarafe[indice]);
            }
            else
                if (Nave.poderCarafe[indice].tipoPoderCarafeActual == Poderes.tipoPoderCarafe.pIzquierda)
                {
                    Nave.poderCarafeIzquierdaTotal.Add(Nave.poderCarafe[indice]);
                }
                else
                    if (Nave.poderCarafe[indice].tipoPoderCarafeActual == Poderes.tipoPoderCarafe.pDerecha)
                    {
                        Nave.poderCarafeDerechaTotal.Add(Nave.poderCarafe[indice]);
                    }
            Nave.poderCarafe.RemoveAt(indice);
        }

        // Comprobacion de disparos de la nave al colisionar con enemigos
        public void confirmaEliminaDisparoNave(int indice)
        {
            if (Nave.disparos[indice].tipoActual == Disparo.tipo.pequed ||
                Nave.disparos[indice].tipoActual == Disparo.tipo.pequei
             )
            {
                ultimaColisionTipoDisparo = Nave.disparos[indice].tipoActual;
                Nave.disparosTotales.Add(Nave.disparos[indice]);
                Nave.disparos.Remove(Nave.disparos[indice]);
            }else
            if (Nave.disparos[indice].tipoActual == Disparo.tipo.diagD)
            {
                ultimaColisionTipoDisparo = Nave.disparos[indice].tipoActual;
                Nave.disparosDiagDerechaTotales.Add(Nave.disparos[indice]);
                Nave.disparos.Remove(Nave.disparos[indice]);
            }else
            if (Nave.disparos[indice].tipoActual == Disparo.tipo.diagI)
            {
                ultimaColisionTipoDisparo = Nave.disparos[indice].tipoActual;
                Nave.disparosDiagIzquierdaTotales.Add(Nave.disparos[indice]);
                Nave.disparos.Remove(Nave.disparos[indice]);
            }else
            if (Nave.disparos[indice].tipoActual == Disparo.tipo.super)
            {
                ultimaColisionTipoDisparo = Nave.disparos[indice].tipoActual;
                Nave.disparosSuperTotales.Add(Nave.disparos[indice]);
                Nave.disparos.Remove(Nave.disparos[indice]);
            }

        }


    }
}
