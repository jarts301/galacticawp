using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input.Touch;

namespace Galactica
{
    class QuickMode:GameComun
    {
        public enum Estado {Game,Pause,GameOver,Menu}
        public static Estado estadoActual;
        public QuickModePause pause = new QuickModePause();
        public QuickModeMenu menu = new QuickModeMenu();
        public QuickModeGOver gOver = new QuickModeGOver();

        public QuickMode()
        {
            estadoActual = Estado.Menu;
        }

        public void update(GameTime t)
        {
            switch(estadoActual)
            {
                case Estado.Game:
                    QuickModeGame.update(t);
                    break;
                case Estado.Pause:
                    pause.update(t);
                    break;
                case Estado.Menu:
                    menu.update(t);
                    break;
                case Estado.GameOver:
                    gOver.update(t);
                    break;
            }
        }

        public void draw(SpriteBatch sb)
        {
            if (estadoActual == Estado.Game || 
                estadoActual == Estado.Pause ||
                estadoActual == Estado.GameOver)
            {
                QuickModeGame.draw(sb);
            }
            if (estadoActual == Estado.Pause)
            {
                pause.draw(sb);
            }
            if (estadoActual == Estado.Menu)
            {
                menu.draw(sb);
            }
            if (estadoActual == Estado.GameOver)
            {
                gOver.draw(sb);
            }
        }

    }
}
