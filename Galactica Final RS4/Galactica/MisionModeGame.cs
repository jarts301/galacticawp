using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Galactica
{
    class MisionModeGame:GameComun
    {
        public static int numMision;

        public MisionModeGame()
        {
            numMision = 0;
        }

        public void update(GameTime time)
        {
            switch (numMision)
            {
                case 0:
                    break;
                case 1:
                    Mision1.update(time);
                    break;
                case 2:
                    Mision2.update(time);
                    break;
                case 3:
                    Mision3.update(time);
                    break;
                case 4:
                    Mision4.update(time);
                    break;
                case 5:
                    Mision5.update(time);
                    break;
                case 6:
                    Mision6.update(time);
                    break;
                case 7:
                    Mision7.update(time);
                    break;
                case 8:
                    Mision8.update(time);
                    break;
                case 9:
                    Mision9.update(time);
                    break;
                case 10:
                    Mision10.update(time);
                    break;
                case 11:
                    Mision11.update(time);
                    break;
                case 12:
                    Mision12.update(time);
                    break;
                case 13:
                    Mision13.update(time);
                    break;
                case 14:
                    Mision14.update(time);
                    break;
                case 15:
                    Mision15.update(time);
                    break;
                case 16:
                    Mision16.update(time);
                    break;
                case 17:
                    Mision17.update(time);
                    break;
                case 18:
                    Mision18.update(time);
                    break;
                case 19:
                    Mision19.update(time);
                    break;
                case 20:
                    Mision20.update(time);
                    break;
            }
            
            
        }

        public void draw(SpriteBatch sb)
        {
            switch (numMision)
            {
                case 0:
                    break;
                case 1:
                    Mision1.draw(sb);
                    break;
                case 2:
                    Mision2.draw(sb);
                    break;
                case 3:
                    Mision3.draw(sb);
                    break;
                case 4:
                    Mision4.draw(sb);
                    break;
                case 5:
                    Mision5.draw(sb);
                    break;
                case 6:
                    Mision6.draw(sb);
                    break;
                case 7:
                    Mision7.draw(sb);
                    break;
                case 8:
                    Mision8.draw(sb);
                    break;
                case 9:
                    Mision9.draw(sb);
                    break;
                case 10:
                    Mision10.draw(sb);
                    break;
                case 11:
                    Mision11.draw(sb);
                    break;
                case 12:
                    Mision12.draw(sb);
                    break;
                case 13:
                    Mision13.draw(sb);
                    break;
                case 14:
                    Mision14.draw(sb);
                    break;
                case 15:
                    Mision15.draw(sb);
                    break;
                case 16:
                    Mision16.draw(sb);
                    break;
                case 17:
                    Mision17.draw(sb);
                    break;
                case 18:
                    Mision18.draw(sb);
                    break;
                case 19:
                    Mision19.draw(sb);
                    break;
                case 20:
                    Mision20.draw(sb);
                    break;
            }

        }

    }
}
