using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Info;

namespace Galactica
{
    public class GalacticaMain : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        public enum Estado {MainMenu,QuickGame,MisionMode,Options,
                            Help,About,SavedGame,Buy}
        public static Estado estadoActual;
        private Textura Tex;
        private Audio audio;
        private Acelerometro acelerometro;
        private SpriteFont fuente;
        private Random random;
        private MainMenu mainMenu;
        private QuickMode quickGame;
        private MisionMode misionMode;
        private Opciones opciones;
        private Ayuda ayuda;
        private AcercaDe acercaDe;
        private SavedGame savedGame;
        private Buy buy;
        private static double checkTrialModeDelay = 1000.0;
        public static bool isTrialMode = true;

        private double cont_draw=0,frames=0,ultima_actua=0;
        private double updates = 0, contUpdates = 0;
        public static int BESTSCORE=0;

        //Object dat = DeviceExtendedProperties.GetValue("DeviceTotalMemory");
        //Object dat = DeviceExtendedProperties.GetValue("ApplicationCurrentMemoryUsage");
        public GalacticaMain()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            TargetElapsedTime = TimeSpan.FromSeconds(1.0f/30);

            graphics.PreferredBackBufferWidth = 480;
            graphics.PreferredBackBufferHeight = 800;

            // Allow portrait
            graphics.SupportedOrientations = DisplayOrientation.Portrait;
            graphics.IsFullScreen = true;

            //Guide.SimulateTrialMode = true;
        }

        protected override void Initialize()
        {
            base.Initialize();
        }
        
        protected override void LoadContent()
        {
            estadoActual = Estado.MainMenu;
            spriteBatch = new SpriteBatch(GraphicsDevice);
            random = new Random();
            acelerometro = new Acelerometro();
            fuente = Content.Load<SpriteFont>("Moire");
            cargarTexturas();
            cargarAudio();
            //almacenamiento.salvar();

            /*Lo que debe cargar antes que todo*/
            Poderes.load(Tex); Animation.load(Tex); BarraVida.load(Tex); Boton.load(Tex); Onda.load(Tex);
            Disparo.load(Tex); DisparoEnemigo.load(Tex); Enemigo.load(Tex); Plus.load(Tex);
            Estrella.load(Tex); GameComun.loadEstrellas();MainMenu.load(Tex);Nave.load(Tex, fuente); DisparoJefe.load(Tex);
            Jefe.load(Tex); QuickModeGame.load(Tex, fuente); QuickModePause.load(Tex); NMini.load(Tex);
            QuickModeMenu.load(Tex,fuente);QuickModeGOver.load(Tex,fuente); MisionModeMenu.load(Tex,fuente);
            MisionModeInfo.load(Tex, fuente); MisionModePause.load(Tex); MisionModeGOver.load(Tex,fuente);
            Control.load(Tex); Opciones.load(Tex); Ayuda.load(Tex); AcercaDe.load(Tex); SavedGame.load(Tex);
            Buy.load(Tex,fuente);


            mainMenu = new MainMenu();
            quickGame = new QuickMode();
            misionMode = new MisionMode();
            opciones = new Opciones();
            ayuda = new Ayuda();
            acercaDe = new AcercaDe();
            savedGame = new SavedGame();
            buy=new Buy();


            //Almacenamiento.borrar();
            Almacenamiento.crearDatos();
            Almacenamiento.cargarOpciones();
            Almacenamiento.cargarScore();

        }

        protected override void UnloadContent()
        {

        }

        //*********** UPDATE ************
        protected override void Update(GameTime gameTime)
        {
            contUpdates++;
            TouchControl.updateTouches();
            acelerometro.update();

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
            {
                procesaBotonBack(gameTime);
            }
                

            switch (estadoActual)
            {
                case Estado.MainMenu:
                    mainMenu.update(gameTime,this);
                    break;
                case Estado.QuickGame:
                    quickGame.update(gameTime);
                    break;
                case Estado.MisionMode:
                    misionMode.update(gameTime);
                    break;
                case Estado.Options:
                    opciones.update(gameTime);
                    break;
                case Estado.Help:
                    ayuda.update(gameTime);
                    break;
                case Estado.About:
                    acercaDe.update(gameTime);
                    break;
                case Estado.SavedGame:
                    savedGame.update(gameTime);
                    break;
                case Estado.Buy:
                    buy.update(gameTime);
                    break;

            }

            //Trial mode comprobacion
            if (isTrialMode && checkTrialModeDelay >= 0)
            {
                checkTrialModeDelay -=
                gameTime.ElapsedGameTime.TotalMilliseconds;
                if (checkTrialModeDelay <= 0)
                {
                    isTrialMode = Guide.IsTrialMode;
                }
            }


            //Fotogramas por segundo o FPS
            if (gameTime.TotalGameTime.TotalMilliseconds > ultima_actua + 1000)
            {
                frames = cont_draw;
                updates = contUpdates;
                cont_draw = 0;
                contUpdates = 0;

                ultima_actua = gameTime.TotalGameTime.TotalMilliseconds;
            }

            base.Update(gameTime);

        }

        // ************** DRAW ****************
        protected override void Draw(GameTime gameTime)
        {
            cont_draw++;
            GraphicsDevice.Clear(Color.Black);

            spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend);

            switch (estadoActual)
            {

                case Estado.MainMenu:
                    spriteBatch.DrawString(fuente, "Best Score: " + BESTSCORE, new Vector2(10, 10), Color.Gold);
                    mainMenu.draw(spriteBatch);
                    break;
                case Estado.QuickGame:
                    quickGame.draw(spriteBatch);
                    break;
                case Estado.MisionMode:
                    misionMode.draw(spriteBatch);
                    break;
                case Estado.Options:
                    opciones.draw(spriteBatch);
                    break;
                case Estado.Help:
                    ayuda.draw(spriteBatch);
                    break;
                case Estado.About:
                    acercaDe.draw(spriteBatch);
                    break;
                case Estado.SavedGame:
                    savedGame.draw(spriteBatch);
                    break;
                case Estado.Buy:
                    buy.draw(spriteBatch);
                    break;

            }

            //spriteBatch.DrawString(fuente, "FPS: "+Microsoft.Phone.Info.DeviceStatus.ApplicationPeakMemoryUsage.ToString()/*+frames+"-"+almacenamiento.leer()*/,new Vector2(10,770),Color.White);
            //spriteBatch.DrawString(fuente, "UPS: " + updates, new Vector2(130, 770), Color.White);
            spriteBatch.End();

            base.Draw(gameTime);
        }

        protected override void OnActivated(object sender, EventArgs args)
        {
                isTrialMode = true;
                checkTrialModeDelay = 1000.0f;
            GalacticaMain.estadoActual = GalacticaMain.Estado.MainMenu;
            QuickMode.estadoActual = QuickMode.Estado.Menu;
        }

        protected override void OnExiting(object sender, EventArgs args)
        {
            if (QuickMode.estadoActual == QuickMode.Estado.Game)
            {
                if (GalacticaMain.estadoActual == GalacticaMain.Estado.Buy ||
                   GalacticaMain.estadoActual == GalacticaMain.Estado.QuickGame)
                {
                    Almacenamiento.salvarInGame();
                }
            }
        }

        protected override void OnDeactivated(object sender, EventArgs args)
        {
            if (QuickMode.estadoActual == QuickMode.Estado.Game)
            {
                if (GalacticaMain.estadoActual == GalacticaMain.Estado.Buy ||
                   GalacticaMain.estadoActual == GalacticaMain.Estado.QuickGame)
                {
                    Almacenamiento.salvarInGame();
                }
            }
        }


        //Boton back procesamiento
        public void procesaBotonBack(GameTime gameTime)
        {
            (Audio.getEfecto("presionarBoton")).Play();

            if (GalacticaMain.estadoActual == GalacticaMain.Estado.MainMenu)
            {
                this.Exit();
            }else
            if (QuickMode.estadoActual == QuickMode.Estado.Menu &&
                GalacticaMain.estadoActual == GalacticaMain.Estado.QuickGame)
            {
                GalacticaMain.estadoActual = GalacticaMain.Estado.MainMenu;
            }else
            if (GalacticaMain.estadoActual == GalacticaMain.Estado.Options)
            {
                Almacenamiento.salvarOpciones();
                GalacticaMain.estadoActual = GalacticaMain.Estado.MainMenu;
            }else
            if (GalacticaMain.estadoActual == GalacticaMain.Estado.Help)
            {
                GalacticaMain.estadoActual = GalacticaMain.Estado.MainMenu;
            }
            else
            if (GalacticaMain.estadoActual == GalacticaMain.Estado.About)
            {
                GalacticaMain.estadoActual = GalacticaMain.Estado.MainMenu;
            }
            else
            if (GalacticaMain.estadoActual == GalacticaMain.Estado.SavedGame)
            {
                GalacticaMain.estadoActual = GalacticaMain.Estado.MainMenu;
            }
            else
            if (MisionMode.estadoActual == MisionMode.Estado.Menu &&
                GalacticaMain.estadoActual == GalacticaMain.Estado.MisionMode)
            {
                GalacticaMain.estadoActual = GalacticaMain.Estado.MainMenu;
            }
            else
            if (MisionMode.estadoActual == MisionMode.Estado.Info &&
                GalacticaMain.estadoActual == GalacticaMain.Estado.MisionMode)
            {
                MisionMode.estadoActual = MisionMode.Estado.Menu;
                MisionModeMenu.cargarMisiones();
            }
            else
            if (QuickMode.estadoActual == QuickMode.Estado.Game &&
                GalacticaMain.estadoActual == GalacticaMain.Estado.QuickGame)
            {
                if (MediaPlayer.GameHasControl && Audio.musicaActiva)
                {
                    MediaPlayer.Pause();
                }
                QuickMode.estadoActual = QuickMode.Estado.Pause;
            }
            else
            if (MisionMode.estadoActual == MisionMode.Estado.Game &&
                GalacticaMain.estadoActual == GalacticaMain.Estado.MisionMode)
            {
                if (MediaPlayer.GameHasControl && Audio.musicaActiva)
                {
                    MediaPlayer.Pause();
                }
                MisionMode.estadoActual = MisionMode.Estado.Pause;
            }else
            if (QuickMode.estadoActual == QuickMode.Estado.Pause &&
                GalacticaMain.estadoActual == GalacticaMain.Estado.QuickGame)
            {
                if (MediaPlayer.GameHasControl && Audio.musicaActiva)
                {
                    MediaPlayer.Resume();
                }
                QuickModeGame.ultimoTPlus = gameTime.TotalGameTime.TotalMilliseconds;
                QuickMode.estadoActual = QuickMode.Estado.Game;
            }else
            if (MisionMode.estadoActual == MisionMode.Estado.Pause &&
                GalacticaMain.estadoActual == GalacticaMain.Estado.MisionMode)
            {
                if (MediaPlayer.GameHasControl && Audio.musicaActiva)
                {
                    MediaPlayer.Resume();
                }
                MisionMode.estadoActual = MisionMode.Estado.Game;
            }else
            if (QuickMode.estadoActual == QuickMode.Estado.GameOver &&
                GalacticaMain.estadoActual == GalacticaMain.Estado.QuickGame)
            {
                MainMenu.playMusic = true;

                QuickMode.estadoActual = QuickMode.Estado.Menu;
                GalacticaMain.estadoActual = GalacticaMain.Estado.MainMenu;
                Nave.reiniciar();
                QuickModeGame.reiniciar();
            }else
            if (MisionMode.estadoActual == MisionMode.Estado.GameOver &&
                GalacticaMain.estadoActual == GalacticaMain.Estado.MisionMode)
            {
                MainMenu.playMusic = true;

                MisionMode.estadoActual = MisionMode.Estado.Menu;
                GalacticaMain.estadoActual = GalacticaMain.Estado.MainMenu;
                Boton.reiniciarMision();
                
            }else
            if (GalacticaMain.estadoActual == GalacticaMain.Estado.Buy)
            {
                if (QuickMode.estadoActual == QuickMode.Estado.Game)
                {
                    GalacticaMain.estadoActual = GalacticaMain.Estado.MainMenu;
                    QuickMode.estadoActual = QuickMode.Estado.Menu;
                    Nave.reiniciar();
                    QuickModeGame.reiniciar();
                }
                else
                {
                    GalacticaMain.estadoActual = GalacticaMain.Estado.MisionMode;
                    MisionMode.estadoActual = MisionMode.Estado.Menu;
                    MisionModeMenu.cargarMisiones();
                }
            }

        }

        //*** Cargar Audio
        public void cargarAudio()
        {
            audio = new Audio();
            audio.addEfecto(this.Content.Load<SoundEffect>("Audio/disparo"), "disparo");
            audio.addEfecto(this.Content.Load<SoundEffect>("Audio/EstallidoEnemigo"), "EstallidoEnemigo");
            audio.addEfecto(this.Content.Load<SoundEffect>("Audio/inicioPoder"), "inicioPoder");
            audio.addEfecto(this.Content.Load<SoundEffect>("Audio/EstallidoJefe"), "EstallidoJefe");
            audio.addEfecto(this.Content.Load<SoundEffect>("Audio/presionarBoton"), "presionarBoton");
            audio.addEfecto(this.Content.Load<SoundEffect>("Audio/Estallido"), "Estallido");
            audio.addEfecto(this.Content.Load<SoundEffect>("Audio/Estrella"), "Estrella");
            audio.addEfecto(this.Content.Load<SoundEffect>("Audio/Error"), "Error");
            Audio.silencio=this.Content.Load<SoundEffect>("Audio/silencio");

            audio.addMusica(this.Content.Load<Song>("Audio/Galactic"),"Galactic");
            audio.addMusica(this.Content.Load<Song>("Audio/GameOver"), "GameOver");
            audio.addMusica(this.Content.Load<Song>("Audio/mainMenu"), "mainMenu");
        }


        /**********Carga Texturas***************************/
        public void cargarTexturas()
        {
            Tex = new Textura();
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/NGalactica"), "NGalactica");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/NCarafe"), "NCarafe");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/NHelix"), "NHelix");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/NArp"), "NArp");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/NPerseus"), "NPerseus");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/NPisces"), "NPisces");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/PoderGalactica"), "PoderGalactica");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/PoderCarafe"), "PoderCarafe");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/PoderHelix"), "PoderHelix");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/PoderArp"), "PoderArp");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/PoderPerseus"), "PoderPerseus");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/PoderPisces"), "PoderPisces");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/ENor"), "ENor");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/EKami"), "EKami");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/ESid"), "ESid");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/Disparo"), "Disparo");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/SuperDisparo"), "SuperDisparo");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/Titulo"), "Titulo");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/DisparoSid"), "DisparoSid");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/DisparoNor"), "DisparoNor");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/Estrella"), "Estrella");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/animationDispANave"), "animationDispANave");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/animationDispAEnemigo"), "animationDispAEnemigo");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/animationPoder"), "animationPoder");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/EstallidoEnemigo"), "EstallidoEnemigo");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/EstallidoJefe"), "EstallidoJefe");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/EstallidoJefe"), "EstallidoJefe");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/EstallidoNave"), "EstallidoNave");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/PauseBoton"), "PauseBoton");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/TituloPause"), "TituloPause");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/TituloPauseMision"), "TituloPauseMision");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/TituloQuickGame"), "TituloQuickGame");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/TituloQuickGame"), "TituloQuickGame");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/TituloGameOver"), "TituloGameOver");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/TituloMisionMode"), "TituloMisionMode");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/Plus"), "Plus");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BarraVida"), "BarraVida");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BarraVidaMarco"), "BarraVidaMarco");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BQuickMode"), "BQuickMode");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BOpciones"), "BOpciones");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BResumeGame"), "BResumeGame");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BExitGame"), "BExitGame");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BFlecha"), "BFlecha");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BPlay"), "BPlay");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BAudio"), "BAudio");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BMision"), "BMision");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BMisionMode"), "BMisionMode");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BPower"), "BPower");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BContinue"), "BContinue");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BBack"), "BBack");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BNextMision"), "BNextMision");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BRestart"), "BRestart");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BSalir"), "BSalir");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BHelp"), "BHelp");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BAbout"), "BAbout");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BTilting"), "BTilting");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BButtons"), "BButtons");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BMedium"), "BMedium");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BLarge"), "BLarge");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BContinueGame"), "BContinueGame");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BNewGame"), "BNewGame");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BFacebook"), "BFacebook");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BComent"), "BComent");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BGoBuy"), "BGoBuy");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BNotNow"), "BNotNow");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/TEscogerNave"), "TEscogerNave");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/TResultados"), "TResultados");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/TInfo"), "TInfo");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/TRecompensa"), "TRecompensa");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/TituloWin"), "TituloWin");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/TituloLose"), "TituloLose");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/TituloOpciones"), "TituloOpciones");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/TituloSonidosMusica"), "TituloSonidosMusica");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/TituloControlMode"), "TituloControlMode");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/TituloButtonSize"), "TituloButtonSize");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/TituloSavedGame"), "TituloSavedGame");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/TituloBuyGame"), "TituloBuyGame");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/Separador"), "Separador");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/JLaxo"), "JLaxo");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/JEsp"), "JEsp");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/JEspBrazos"), "JEspBrazos");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/JQuad"), "JQuad");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/JQuadMartillo"), "JQuadMartillo");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/JRued"), "JRued");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/JRuedFlecha"), "JRuedFlecha");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/JefeDisparoMax"), "JefeDisparoMax");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/JefeDisparoNormal"), "JefeDisparoNormal");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/Capitana"), "Capitana");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/NMini"), "NMini");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/Onda"), "Onda");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/Control"), "Control");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/Help"), "Help");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/About"), "About");
            //Fondos
            Tex.addTexturaFondo(this.Content.Load<Texture2D>("Sprites/FondoBlack"));//0
            Tex.addTexturaFondo(this.Content.Load<Texture2D>("Sprites/FondoEstadisticas"));//1
            Tex.addTexturaFondo(this.Content.Load<Texture2D>("Sprites/FondoPause"));//2
        }


    }
}
