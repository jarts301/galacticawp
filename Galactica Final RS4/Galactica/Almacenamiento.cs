using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.IsolatedStorage;
using System.IO;

namespace Galactica
{
    class Almacenamiento
    {
        const string directoryName = "EstadoJuego";
        const string misionFile = "Misions.dat";
        const string bonusFile = "Bonus.dat";
        const string gameFile = "InGame.dat";
        const string scoreFile = "Score.dat";
        const string optionsFile = "Opciones.dat";

        public enum tipoBonus{sumarAsalud,disparoSuper,disparoDiag,
                              naves,tEsperaPoder,poderDisp,recuperacion}


        //public static EstadoMisiones misiones = new EstadoMisiones();
        //public static Bonus bonus = new Bonus();
        //public static InGame inGame = new InGame();
        //public static Opciones opciones = new Opciones();

        public class datScore
        {
            public static byte[] score = new byte[4];
        }
        
        public class datMisiones
        {
            public static byte[] activas = new byte[4];
            public static byte[] premio = new byte[4];
        }

        public class datBonus
        {
            public static byte[] sumarASaludNave = new byte[4];
            public static byte[] disparoSuperActivo = new byte[4];
            public static byte[] disparoDiagActivo = new byte[4];
            public static byte[] naves = new byte[4];
            public static byte[] tiempoEsperaPoder = new byte[4];
            public static byte[] sumaPoderDisparos = new byte[4];
            public static byte[] recuperacion = new byte[4];
        }

        public class datInGame
        {
            public static byte[] puntos = new byte[4];
            public static byte[] vidaNave = new byte[4];
            public static byte[] texturaNaveActual = new byte[4];
            public static byte[] maxEnemigos = new byte[4];
            public static byte[] enemigosDestruidos = new byte[4];
            public static byte[] disparoDiagActivo = new byte[4];
            public static byte[] disparoSuperActivo = new byte[4];
            public static byte[] tiempoEsperaPlus = new byte[4];
            public static byte[] aumentoVidaJefe = new byte[4];
            public static byte[] aumentoVidaEnemigo = new byte[4];
            public static byte[] adicionDanoJefe = new byte[4];
            public static byte[] adicionDanoDispJefe = new byte[4];
            public static byte[] adicionDanoEnemigo = new byte[4];
            public static byte[] adicionDanoDispEnemigo = new byte[4];
            public static byte[] ultimaAdJefe = new byte[4];
            public static byte[] ultimoAumento = new byte[4];
            public static byte[] hayJefe = new byte[4];
            public static byte[] tipoJefe = new byte[4];
            public static byte[] vidaJefe = new byte[4];
        }


        public class datOpciones
        {
            public static byte[] audioActivo = new byte[4];
            public static byte[] controlMode = new byte[4];
            public static byte[] buttonSize = new byte[4];
        }
        //****************************************************************

        public static void borrar()
        {
            using (IsolatedStorageFile storage =
                  IsolatedStorageFile.GetUserStoreForApplication())
            {
                string rutaMisionFile = Path.Combine(directoryName, misionFile);
                string rutaBonusFile = Path.Combine(directoryName, bonusFile);
                string rutaOptionsFile = Path.Combine(directoryName, optionsFile);
                string rutaInGameFile = Path.Combine(directoryName, gameFile);
                string rutaScoreFile = Path.Combine(directoryName, scoreFile);

                if (storage.FileExists(rutaMisionFile))
                {
                    storage.DeleteFile(rutaMisionFile);
                }
                if (storage.FileExists(rutaBonusFile))
                {
                    storage.DeleteFile(rutaBonusFile);
                }
                if (storage.FileExists(rutaOptionsFile))
                {
                    storage.DeleteFile(rutaOptionsFile);
                }
                if (storage.FileExists(rutaInGameFile))
                {
                    storage.DeleteFile(rutaInGameFile);
                }
                if (storage.FileExists(rutaScoreFile))
                {
                    storage.DeleteFile(rutaScoreFile);
                }
            }
        }

        //Salvar datos
        public static void crearDatos()
        {
            using (IsolatedStorageFile storage=
                   IsolatedStorageFile.GetUserStoreForApplication())
            {

                    if (!storage.DirectoryExists(directoryName))
                    {
                        storage.CreateDirectory(directoryName);
                    }

                string rutaMisionFile = Path.Combine(directoryName,misionFile);
                string rutaBonusFile = Path.Combine(directoryName, bonusFile);
                string rutaOptionsFile = Path.Combine(directoryName, optionsFile);
                string rutaScoreFile = Path.Combine(directoryName, scoreFile);

                if (!storage.FileExists(rutaMisionFile))
                {
                    //Datos iniciales
                    //Misiones
                    datMisiones.activas = BitConverter.GetBytes(1);//1
                    datMisiones.premio = BitConverter.GetBytes(0);//0
                    using (IsolatedStorageFileStream stream = storage.CreateFile(rutaMisionFile))
                    {
                        stream.Write(datMisiones.activas, 0, 4);
                        stream.Write(datMisiones.premio, 0, 4);
                        stream.Close();
                    }
                }
                if (!storage.FileExists(rutaBonusFile))
                {
                    //Datos iniciales
                    //Bonus o extras
                    datBonus.sumarASaludNave = BitConverter.GetBytes(100);
                    datBonus.disparoSuperActivo = BitConverter.GetBytes(0);
                    datBonus.disparoDiagActivo = BitConverter.GetBytes(0);
                    datBonus.naves = BitConverter.GetBytes(1);//1
                    datBonus.tiempoEsperaPoder = BitConverter.GetBytes(8000);//8000
                    datBonus.sumaPoderDisparos = BitConverter.GetBytes(0);
                    datBonus.recuperacion = BitConverter.GetBytes(50);
                    using (IsolatedStorageFileStream stream = storage.CreateFile(rutaBonusFile))
                    {
                        stream.Write(datBonus.sumarASaludNave, 0, 4);
                        stream.Write(datBonus.disparoSuperActivo, 0, 4);
                        stream.Write(datBonus.disparoDiagActivo, 0, 4);
                        stream.Write(datBonus.naves, 0, 4);
                        stream.Write(datBonus.tiempoEsperaPoder, 0, 4);
                        stream.Write(datBonus.sumaPoderDisparos, 0, 4);
                        stream.Write(datBonus.recuperacion, 0, 4);
                        stream.Close();
                    }
                }
                if (!storage.FileExists(rutaOptionsFile))
                {
                    //Datos iniciales
                    //Opciones
                    datOpciones.audioActivo = BitConverter.GetBytes(1);
                    datOpciones.buttonSize = BitConverter.GetBytes(1);
                    datOpciones.controlMode = BitConverter.GetBytes(0);
                    using (IsolatedStorageFileStream stream = storage.CreateFile(rutaOptionsFile))
                    {
                        stream.Write(datOpciones.audioActivo, 0, 4);
                        stream.Write(datOpciones.buttonSize, 0, 4);
                        stream.Write(datOpciones.controlMode, 0, 4);
                        stream.Close();
                    }
                }

                if (!storage.FileExists(rutaScoreFile))
                {
                    //Datos iniciales
                    //Opciones
                    datScore.score = BitConverter.GetBytes(0);
                    using (IsolatedStorageFileStream stream = storage.CreateFile(rutaScoreFile))
                    {
                        stream.Write(datScore.score, 0, 4);
                        stream.Close();
                    }
                }

            }
        }


        public static void cargarScore()
        {
            using (IsolatedStorageFile storage =
                   IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (storage.DirectoryExists(directoryName))
                {
                    string rutaScoreFile = Path.Combine(directoryName, scoreFile);

                    if (storage.FileExists(rutaScoreFile))
                    {
                        using (IsolatedStorageFileStream stream =
                            storage.OpenFile(rutaScoreFile,
                                             FileMode.Open,
                                             FileAccess.Read))
                        {
                            stream.Read(datScore.score, 0, 4);

                            GalacticaMain.BESTSCORE = 
                                BitConverter.ToInt32(datScore.score, 0);
                            stream.Close();
                        }
                    }
                }
            }
        }

        public static void salvarScore()
        {
            using (IsolatedStorageFile storage =
                   IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (storage.DirectoryExists(directoryName))
                {
                    string rutaScoreFile = Path.Combine(directoryName, scoreFile);

                    if (storage.FileExists(rutaScoreFile))
                    {
                        using (IsolatedStorageFileStream stream =
                            storage.OpenFile(rutaScoreFile,
                                             FileMode.Open,
                                             FileAccess.Write))
                        {
                            datScore.score  =
                                BitConverter.GetBytes(GalacticaMain.BESTSCORE);

                            stream.Write(datScore.score, 0, 4);
                            stream.Close();
                        }
                    }
                }
            }
        }


        public static void cargarMisiones()
        {
            using (IsolatedStorageFile storage =
                   IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (storage.DirectoryExists(directoryName))
                {
                    string rutaMisionFile = Path.Combine(directoryName, misionFile);

                    if (storage.FileExists(rutaMisionFile))
                    {
                        using (IsolatedStorageFileStream stream =
                            storage.OpenFile(rutaMisionFile,
                                             FileMode.Open,
                                             FileAccess.Read))
                        {
                            stream.Read(datMisiones.activas, 0, 4);
                            stream.Read(datMisiones.premio, 0, 4);
                            stream.Close();
                        }
                    }

                }
            }
        }


        public static void salvarMisiones()
        {
            using (IsolatedStorageFile storage =
                   IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (storage.DirectoryExists(directoryName))
                {
                    string rutaMisionFile = Path.Combine(directoryName, misionFile);

                    if (storage.FileExists(rutaMisionFile))
                    {
                        using (IsolatedStorageFileStream stream =
                            storage.OpenFile(rutaMisionFile,
                                             FileMode.Open,
                                             FileAccess.Read))
                        {
                            stream.Read(datMisiones.activas, 0, 4);
                            stream.Read(datMisiones.premio, 0, 4);
                            stream.Close();
                        }
                    }

                    if (storage.FileExists(rutaMisionFile))
                    {
                        using (IsolatedStorageFileStream stream =
                            storage.OpenFile(rutaMisionFile,
                                             FileMode.Open,
                                             FileAccess.Write))
                        {
                            datMisiones.activas = BitConverter.GetBytes(
                               BitConverter.ToInt32(datMisiones.activas, 0) + 1);
                            datMisiones.premio = BitConverter.GetBytes(
                               BitConverter.ToInt32(datMisiones.premio, 0) + 1);

                            stream.Write(datMisiones.activas, 0, 4);
                            stream.Write(datMisiones.premio, 0, 4);
                            stream.Close();
                        }
                    }

                }
            }
        }


        //Cargar Bonus
        public static void cargarBonus()
        {
            using (IsolatedStorageFile storage =
                   IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (storage.DirectoryExists(directoryName))
                {
                    string rutaBonusFile = Path.Combine(directoryName, bonusFile);

                    if (storage.FileExists(rutaBonusFile))
                    {
                        using (IsolatedStorageFileStream stream =
                            storage.OpenFile(rutaBonusFile,
                                             FileMode.Open,
                                             FileAccess.Read))
                        {
                            stream.Read(datBonus.sumarASaludNave, 0, 4);
                            stream.Read(datBonus.disparoSuperActivo, 0, 4);
                            stream.Read(datBonus.disparoDiagActivo, 0, 4);
                            stream.Read(datBonus.naves, 0, 4);
                            stream.Read(datBonus.tiempoEsperaPoder, 0, 4);
                            stream.Read(datBonus.sumaPoderDisparos, 0, 4);
                            stream.Read(datBonus.recuperacion, 0, 4);

                            Nave.MaxVida = BitConverter.ToInt32(datBonus.sumarASaludNave,0);
                            Boton.navesActivas = BitConverter.ToInt32(datBonus.naves, 0);
                            Boton.tiempoEsperaPoder = BitConverter.ToInt32(datBonus.tiempoEsperaPoder, 0);
                            Nave.masPoderDisp = BitConverter.ToInt32(datBonus.sumaPoderDisparos, 0);
                            Plus.recuperacion = BitConverter.ToInt32(datBonus.recuperacion, 0);

                            if (BitConverter.ToInt32(datBonus.disparoSuperActivo, 0) == 1)
                            {
                                Nave.sePuedeDispSuper = true;
                            }
                            else
                            {
                                Nave.sePuedeDispSuper = false;
                            }
                            if (BitConverter.ToInt32(datBonus.disparoDiagActivo, 0) == 1)
                            {
                                Nave.sePuedeDispDiag = true;
                            }
                            else
                            {
                                Nave.sePuedeDispDiag = false;
                            }

                            QuickModeMenu.rescatadas = BitConverter.ToInt32(datBonus.naves, 0);

                            stream.Close();
                        }
                    }

                }
            }
        }


        //Salvar Bonus
        public static void salvarBonus(int datoASalvar,tipoBonus tipoB)
        {
            using (IsolatedStorageFile storage =
                   IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (storage.DirectoryExists(directoryName))
                {
                    string rutaBonusFile = Path.Combine(directoryName, bonusFile);

                    if (storage.FileExists(rutaBonusFile))
                    {
                        using (IsolatedStorageFileStream stream =
                            storage.OpenFile(rutaBonusFile,
                                             FileMode.Open,
                                             FileAccess.Read))
                        {
                            stream.Read(datBonus.sumarASaludNave, 0, 4);
                            stream.Read(datBonus.disparoSuperActivo, 0, 4);
                            stream.Read(datBonus.disparoDiagActivo, 0, 4);
                            stream.Read(datBonus.naves, 0, 4);
                            stream.Read(datBonus.tiempoEsperaPoder, 0, 4);
                            stream.Read(datBonus.sumaPoderDisparos, 0, 4);
                            stream.Read(datBonus.recuperacion, 0, 4);
                            stream.Close();
                        }
                    }

                    if (storage.FileExists(rutaBonusFile))
                    {
                        using (IsolatedStorageFileStream stream =
                            storage.OpenFile(rutaBonusFile,
                                             FileMode.Open,
                                             FileAccess.Write))
                        {
                            switch (tipoB)
                            {
                                case tipoBonus.sumarAsalud:
                                    datBonus.sumarASaludNave =
                                        BitConverter.GetBytes(datoASalvar);
                                    break;
                                case tipoBonus.disparoSuper:
                                    datBonus.disparoSuperActivo =
                                        BitConverter.GetBytes(datoASalvar);
                                    break;
                                case tipoBonus.disparoDiag:
                                    datBonus.disparoDiagActivo =
                                        BitConverter.GetBytes(datoASalvar);
                                    break;
                                case tipoBonus.naves:
                                    datBonus.naves = 
                                        BitConverter.GetBytes(datoASalvar);
                                    break;
                                case tipoBonus.tEsperaPoder:
                                    datBonus.tiempoEsperaPoder = 
                                        BitConverter.GetBytes(datoASalvar);
                                    break;
                                case tipoBonus.poderDisp:
                                    datBonus.sumaPoderDisparos =
                                        BitConverter.GetBytes(datoASalvar);
                                    break;
                                case tipoBonus.recuperacion:
                                    datBonus.recuperacion =
                                        BitConverter.GetBytes(datoASalvar);
                                    break;
                            }

                            stream.Write(datBonus.sumarASaludNave, 0, 4);
                            stream.Write(datBonus.disparoSuperActivo, 0, 4);
                            stream.Write(datBonus.disparoDiagActivo, 0, 4);
                            stream.Write(datBonus.naves, 0, 4);
                            stream.Write(datBonus.tiempoEsperaPoder, 0, 4);
                            stream.Write(datBonus.sumaPoderDisparos, 0, 4);
                            stream.Write(datBonus.recuperacion, 0, 4);
                            stream.Close();
                        }
                    }

                }
            }
        }


        //Salvar opciones
        public static void salvarOpciones()
        {
            using (IsolatedStorageFile storage =
                   IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (storage.DirectoryExists(directoryName))
                {
                    string rutaOptionsFile = Path.Combine(directoryName, optionsFile);

                    if (storage.FileExists(rutaOptionsFile))
                    {
                        using (IsolatedStorageFileStream stream =
                            storage.OpenFile(rutaOptionsFile,
                                             FileMode.Open,
                                             FileAccess.Write))
                        {
                            if (Audio.musicaActiva && Audio.efectosActivos)
                            {datOpciones.audioActivo = BitConverter.GetBytes(1);}
                            else{datOpciones.audioActivo = BitConverter.GetBytes(0);}

                            if (!Control.tamanoGrande)
                            { datOpciones.buttonSize = BitConverter.GetBytes(0); }
                            else { datOpciones.buttonSize = BitConverter.GetBytes(1); }

                            if (!Control.controlActivo)
                            { datOpciones.controlMode = BitConverter.GetBytes(0); }
                            else { datOpciones.controlMode = BitConverter.GetBytes(1); }

                            stream.Write(datOpciones.audioActivo, 0, 4);
                            stream.Write(datOpciones.buttonSize, 0, 4);
                            stream.Write(datOpciones.controlMode, 0, 4);

                            stream.Close();
                        }
                    }
                }
            }
        }

        //Cargar opciones
        public static void cargarOpciones()
        {
            using (IsolatedStorageFile storage =
                   IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (storage.DirectoryExists(directoryName))
                {
                    string rutaOptionsFile = Path.Combine(directoryName, optionsFile);

                    if (storage.FileExists(rutaOptionsFile))
                    {
                        using (IsolatedStorageFileStream stream =
                            storage.OpenFile(rutaOptionsFile,
                                             FileMode.Open,
                                             FileAccess.Read))
                        {
                            stream.Read(datOpciones.audioActivo, 0, 4);
                            stream.Read(datOpciones.buttonSize, 0, 4);
                            stream.Read(datOpciones.controlMode, 0, 4);

                            if (BitConverter.ToInt32(datOpciones.audioActivo, 0) == 1)
                            {
                                Audio.musicaActiva = true;
                                Audio.efectosActivos = true;
                            }
                            else
                            {
                                Audio.musicaActiva = false;
                                Audio.efectosActivos = false;
                            }

                            if (BitConverter.ToInt32(datOpciones.buttonSize, 0) == 1)
                            {
                                Control.tamanoGrande = true;
                            }
                            else
                            {
                                Control.tamanoGrande = false;
                            }

                            if (BitConverter.ToInt32(datOpciones.controlMode, 0) == 1)
                            {
                                Control.controlActivo = true;
                                Acelerometro.acelActivo = false;
                            }
                            else
                            {
                                Control.controlActivo = false;
                                Acelerometro.acelActivo = true;
                            }

                            stream.Close();
                        }
                    }
                }
            }
        }



        //Salvar In Game
        public static void salvarInGame()
        {
            string rutaInGameFile = Path.Combine(directoryName, gameFile);

            using (IsolatedStorageFile storage =
                   IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (storage.DirectoryExists(directoryName))
                {
                    if (!storage.FileExists(rutaInGameFile))
                    {
                        using (IsolatedStorageFileStream stream =
                            storage.CreateFile(rutaInGameFile))
                        {
                            datosASalvarInGame(stream);
                        }
                    }else
                    {
                        using (IsolatedStorageFileStream stream =
                            storage.OpenFile(rutaInGameFile,
                                             FileMode.Open,
                                             FileAccess.Write))
                        {

                            datosASalvarInGame(stream);
                        }

                    }
                }
            }
        }


        //Borrar In Game
        public static void borrarInGame()
        {
            string rutaInGameFile = Path.Combine(directoryName, gameFile);

            using (IsolatedStorageFile storage =
                   IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (storage.DirectoryExists(directoryName))
                {
                    if (storage.FileExists(rutaInGameFile))
                    {
                        storage.DeleteFile(rutaInGameFile);
                    }
                }
            }
        }


        //Cargar In Game
        public static void cargarInGame()
        {
            using (IsolatedStorageFile storage =
                   IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (storage.DirectoryExists(directoryName))
                {
                    string rutaInGameFile = Path.Combine(directoryName, gameFile);

                    if (storage.FileExists(rutaInGameFile))
                    {
                        using (IsolatedStorageFileStream stream =
                            storage.OpenFile(rutaInGameFile,
                                             FileMode.Open,
                                             FileAccess.Read))
                        {

                            stream.Read(datInGame.puntos, 0, 4);
                            stream.Read(datInGame.vidaNave, 0, 4);
                            stream.Read(datInGame.texturaNaveActual, 0, 4);
                            stream.Read(datInGame.maxEnemigos, 0, 4);
                            stream.Read(datInGame.enemigosDestruidos, 0, 4);
                            stream.Read(datInGame.disparoDiagActivo, 0, 4);
                            stream.Read(datInGame.disparoSuperActivo, 0, 4);
                            stream.Read(datInGame.tiempoEsperaPlus, 0, 4);
                            stream.Read(datInGame.aumentoVidaJefe, 0, 4);
                            stream.Read(datInGame.aumentoVidaEnemigo, 0, 4);
                            stream.Read(datInGame.adicionDanoJefe, 0, 4);
                            stream.Read(datInGame.adicionDanoDispJefe, 0, 4);
                            stream.Read(datInGame.adicionDanoEnemigo, 0, 4);
                            stream.Read(datInGame.adicionDanoDispEnemigo, 0, 4);
                            stream.Read(datInGame.ultimaAdJefe, 0, 4);
                            stream.Read(datInGame.ultimoAumento, 0, 4);
                            stream.Read(datInGame.hayJefe, 0, 4);
                            stream.Read(datInGame.tipoJefe, 0, 4);
                            stream.Read(datInGame.vidaJefe, 0, 4);

                            Nave.Puntos = BitConverter.ToInt32(datInGame.puntos,0);
                            Nave.Vida = BitConverter.ToInt32(datInGame.vidaNave, 0);
                            Nave.TexturaNave = BitConverter.ToInt32(datInGame.texturaNaveActual, 0);
                            QuickModeGame.maxEnemigos = BitConverter.ToInt32(datInGame.maxEnemigos, 0);
                            Nave.Destruidos = BitConverter.ToInt32(datInGame.enemigosDestruidos, 0);
                            if ( BitConverter.ToInt32(datInGame.disparoDiagActivo,0)==1)
                            { Nave.disparosDiagActivos = true; }
                            else { Nave.disparosDiagActivos = false; }
                            if (BitConverter.ToInt32(datInGame.disparoSuperActivo, 0) == 1)
                            { Nave.disparoSuperActivo = true; }
                            else { Nave.disparoSuperActivo = false; }
                            QuickModeGame.tiempoEsperaPlus = BitConverter.ToInt32(datInGame.tiempoEsperaPlus, 0);
                            QuickModeGame.aumentoEnVidaJefe = BitConverter.ToInt32(datInGame.aumentoVidaJefe, 0);
                            QuickModeGame.aumentoEnVidaEnemigo = BitConverter.ToInt32(datInGame.aumentoVidaEnemigo, 0);
                            Jefe.adicionDanoNave = BitConverter.ToInt32(datInGame.adicionDanoJefe, 0);
                            DisparoJefe.adicionDanoDisNave = BitConverter.ToInt32(datInGame.adicionDanoDispJefe, 0);
                            Enemigo.adicionDanoNave = BitConverter.ToInt32(datInGame.adicionDanoEnemigo, 0);
                            DisparoEnemigo.adicionDanoDisNave = BitConverter.ToInt32(datInGame.adicionDanoDispEnemigo, 0);
                            QuickModeGame.ultimoAumento = BitConverter.ToInt32(datInGame.ultimoAumento, 0);

                            if (Nave.Puntos >= 1500) { QuickModeGame.llegaA1500 = true; }
                            else { QuickModeGame.llegaA1500 = false; }

                            QuickModeGame.ultimaAdicionJefe = BitConverter.ToInt32(datInGame.ultimaAdJefe, 0);
                            if (BitConverter.ToInt32(datInGame.hayJefe,0)==1)
                            {
                                switch (BitConverter.ToInt32(datInGame.tipoJefe,0))
                                {
                                    case 1:
                                        QuickModeGame.agregarJefe(Jefe.tipo.Laxo);
                                        break;
                                    case 2:
                                        QuickModeGame.agregarJefe(Jefe.tipo.Esp);
                                        break;
                                    case 3:
                                        QuickModeGame.agregarJefe(Jefe.tipo.Quad);
                                        break;
                                    case 4:
                                        QuickModeGame.agregarJefe(Jefe.tipo.Rued);
                                        break;
                                }
                                QuickModeGame.jefes[0].vida = 
                                    BitConverter.ToInt32(datInGame.vidaJefe, 0);
                            }

                            stream.Close();
                        }
                    }

                }
            }
        }





        //****
        public static int getMisionesActivas()
        {
            return BitConverter.ToInt32(datMisiones.activas, 0);
        }
        public static int getPremiosDados()
        {
            return BitConverter.ToInt32(datMisiones.premio, 0);
        }

        public static int getBestScore()
        {
            return BitConverter.ToInt32(datScore.score, 0);
        }

        //****************************************************************************

        public static void datosASalvarInGame(IsolatedStorageFileStream stream)
        {
            datInGame.puntos = BitConverter.GetBytes(Nave.Puntos);
            datInGame.vidaNave = BitConverter.GetBytes((int)Nave.Vida);
            datInGame.texturaNaveActual = BitConverter.GetBytes(Nave.TexturaNave);
            datInGame.maxEnemigos = BitConverter.GetBytes(QuickModeGame.maxEnemigos);
            datInGame.enemigosDestruidos = BitConverter.GetBytes(Nave.Destruidos);
            if (Nave.disparosDiagActivos)
            { datInGame.disparoDiagActivo = BitConverter.GetBytes(1); }
            else { datInGame.disparoDiagActivo = BitConverter.GetBytes(0); }
            if (Nave.disparoSuperActivo)
            { datInGame.disparoSuperActivo = BitConverter.GetBytes(1); }
            else { datInGame.disparoSuperActivo = BitConverter.GetBytes(0); }
            datInGame.tiempoEsperaPlus = BitConverter.GetBytes((int)QuickModeGame.tiempoEsperaPlus);
            datInGame.aumentoVidaJefe = BitConverter.GetBytes(QuickModeGame.aumentoEnVidaJefe);
            datInGame.aumentoVidaEnemigo = BitConverter.GetBytes(QuickModeGame.aumentoEnVidaEnemigo);
            datInGame.adicionDanoJefe = BitConverter.GetBytes(Jefe.adicionDanoNave);
            datInGame.adicionDanoDispJefe = BitConverter.GetBytes(DisparoJefe.adicionDanoDisNave);
            datInGame.adicionDanoEnemigo = BitConverter.GetBytes(Enemigo.adicionDanoNave);
            datInGame.adicionDanoDispEnemigo = BitConverter.GetBytes(DisparoEnemigo.adicionDanoDisNave);
            datInGame.ultimoAumento = BitConverter.GetBytes(QuickModeGame.ultimoAumento);

            datInGame.ultimaAdJefe = BitConverter.GetBytes(QuickModeGame.ultimaAdicionJefe);
            if (QuickModeGame.jefes.Count > 0)
            {
                datInGame.hayJefe = BitConverter.GetBytes(1);
                datInGame.vidaJefe =
                    BitConverter.GetBytes(QuickModeGame.jefes[0].vida);

                switch (QuickModeGame.jefes[0].tipoJefe)
                {
                    case Jefe.tipo.Laxo:
                        datInGame.tipoJefe =
                            BitConverter.GetBytes(1);
                        break;
                    case Jefe.tipo.Esp:
                        datInGame.tipoJefe =
                            BitConverter.GetBytes(2);
                        break;
                    case Jefe.tipo.Quad:
                        datInGame.tipoJefe =
                            BitConverter.GetBytes(3);
                        break;
                    case Jefe.tipo.Rued:
                        datInGame.tipoJefe =
                            BitConverter.GetBytes(4);
                        break;
                }

            }
            else
            {
                datInGame.hayJefe = BitConverter.GetBytes(0);
                datInGame.vidaJefe = BitConverter.GetBytes(0);
                datInGame.tipoJefe = BitConverter.GetBytes(0);
            }

            stream.Write(datInGame.puntos, 0, 4);
            stream.Write(datInGame.vidaNave, 0, 4);
            stream.Write(datInGame.texturaNaveActual, 0, 4);
            stream.Write(datInGame.maxEnemigos, 0, 4);
            stream.Write(datInGame.enemigosDestruidos, 0, 4);
            stream.Write(datInGame.disparoDiagActivo, 0, 4);
            stream.Write(datInGame.disparoSuperActivo, 0, 4);
            stream.Write(datInGame.tiempoEsperaPlus, 0, 4);
            stream.Write(datInGame.aumentoVidaJefe, 0, 4);
            stream.Write(datInGame.aumentoVidaEnemigo, 0, 4);
            stream.Write(datInGame.adicionDanoJefe, 0, 4);
            stream.Write(datInGame.adicionDanoDispJefe, 0, 4);
            stream.Write(datInGame.adicionDanoEnemigo, 0, 4);
            stream.Write(datInGame.adicionDanoDispEnemigo, 0, 4);
            stream.Write(datInGame.ultimaAdJefe, 0, 4);
            stream.Write(datInGame.ultimoAumento, 0, 4);
            stream.Write(datInGame.hayJefe, 0, 4);
            stream.Write(datInGame.tipoJefe, 0, 4);
            stream.Write(datInGame.vidaJefe, 0, 4);

            stream.Close();
        }


        public static bool hayJuegoSalvado()
        {
            using (IsolatedStorageFile storage =
                   IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (storage.DirectoryExists(directoryName))
                {
                    string rutaInGameFile = Path.Combine(directoryName, gameFile);

                    if (storage.FileExists(rutaInGameFile))
                    { return true; }
                    else { return false; }
                }
            }

            return false;
        }


    }
}
