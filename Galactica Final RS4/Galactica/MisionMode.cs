using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Galactica
{
    class MisionMode
    {
        public enum Estado {Game,Pause,GameOver,Menu,Info}
        public static Estado estadoActual;
        public MisionModeMenu menu=new MisionModeMenu();
        public MisionModeInfo info = new MisionModeInfo();
        public MisionModeGame game = new MisionModeGame();
        public MisionModePause pause = new MisionModePause();
        public MisionModeGOver gOver = new MisionModeGOver();

        public MisionMode()
        {
            estadoActual = Estado.Menu;
        }

        public void update(GameTime t)
        {
            Nave.TexturaNave = 0;
            switch(estadoActual)
            {
                case Estado.Info:
                    info.update(t);
                    break;
                case Estado.Game:
                    game.update(t);
                    break;
                case Estado.Menu:
                    menu.update(t);
                    break;
                case Estado.Pause:
                    pause.update(t);
                    break;
                case Estado.GameOver:
                    gOver.update(t);
                    break;
            }
        }

        public void draw(SpriteBatch sb)
        {
            if (estadoActual == Estado.Game ||
                estadoActual== Estado.Pause ||
                estadoActual==Estado.GameOver)
            {
                game.draw(sb);
            }
            if (estadoActual == Estado.Info)
            {
                info.draw(sb);
            }
            if (estadoActual == Estado.Menu)
            {
                menu.draw(sb);
            }
            if (estadoActual == Estado.Pause)
            {
                pause.draw(sb);
            }
            if (estadoActual == Estado.GameOver)
            {
                gOver.draw(sb);
            }
        }
    }
}
