using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Media;

namespace Galactica
{
    class Mision13:QuickModeGame
    {

        public static StringBuilder tituloMision = new StringBuilder("Mission 13");
        public static StringBuilder mensajeMision = new StringBuilder("Escape most fast as\npossible of the expansive\nwave. avoid collide or \nyou will lose.\nTip:Keep moving.");
        public static StringBuilder recompensa = new StringBuilder("Decreases power timeout.\nin Quick Game.");
        public static StringBuilder noRecompensa = new StringBuilder("No Reward...");
        public static int cuentaTiempo=100,cuentaGolpes=3;
        private static Vector2 positionTime;
        private static double ultimoTMinuto;
        private static Onda onda=new Onda();

        public Mision13()
        {
            
            
        }

        public static new void reiniciar()
        {
            eliminarTodos();
            eliminaAnimaciones();
            positionTime = new Vector2(340, 50);
            velocidadEnem = 4;
            velocidadEnemY = 4;
            velocidadHiEnem = 6;
            velocidadHiEnemY = 6;
            maxEnemigos = 3;
            cuentaTiempo = 100;
            cuentaGolpes = 3;
            aumentoEnVidaEnemigo = 0;
            ultimoTMinuto = 0;
            ultimoAumento = 0;
            eliminarTodos();
            onda.reiniciar();
            eliminaAnimaciones();
            Nave.puntoInicio(new Vector2(240,550));
            EscalaMensaje = escalaInicialMensaje = 1.5f;
            actualFrameMensaje = 0;
            tiempoTitileoMensaje = 0;
            mensajeInicialActivo = true;
            botones.Clear();
            botones.Add(new Boton(Boton.TipoBoton.BPause, 0, 0));
            botones.Add(new Boton(Boton.TipoBoton.BAudio, 430, 0));
        }


        public static new void load(Textura t,SpriteFont f)
        {
            fuente = f;
        }

        public static new void update(GameTime time)
        {
            nave.update(time);
            updateEstrellas(time);
            updateEnemigos(time);
            updateGame(time);
            updateAnimaciones(time);
            onda.update(time);
            Nave.disparosActivos = false;
            Nave.velocidadNave = 11;
            Nave.movEnY = false;
            for (i = 0; i < botones.Count; i++)
            {
                botones[i].update(time);
            }

            //Fotogramas por segundo o FPS
            if (time.TotalGameTime.TotalMilliseconds > ultimoTMinuto + 1000)
            {
                cuentaTiempo= cuentaTiempo-1;
                ultimoTMinuto = time.TotalGameTime.TotalMilliseconds;
            }

            updateMensajeInicial(time);
            barraVidaNave.update(time, Nave.Vida, Nave.MaxVida);
            if (Nave.Vida <= 0) { Animation.animacionesEstallidoNave.update(time); }
            gameOver();
            if (Control.controlActivo)
            {
                control.update(time);
            }

        }

        public static new void draw(SpriteBatch sb)
        {
            //CLEAR NEGRO
            sb.Draw(Textura.TexturasFondo[0], Vector2.Zero, null, Color.Black, 0.0f, Vector2.Zero,
                40.0f, SpriteEffects.None, 0.999f);

            nave.draw(sb);
            drawEstrellas(sb);
            drawEnemigos(sb);
            drawEstadNave(sb);
            drawAnimaciones(sb);
            drawMensajeInicial(sb);
            onda.draw(sb);
            if (Nave.Vida <= 0) { Animation.animacionesEstallidoNave.draw(sb); }

            for (i = 0; i < botones.Count; i++)
            {
                botones[i].draw(sb);
            }
            if (Control.controlActivo)
            {
                control.draw(sb);
            }
           // drawEstadisticas(sb);
        }

        /******************Estadisticas de la nave**************/
        public static new void drawEstadNave(SpriteBatch sb)
        {
            sb.Draw(Textura.TexturasFondo[1], Vector2.Zero, null,Color.Black, 0.0f, Vector2.Zero, 
                new Vector2(50.0f,2.3f),SpriteEffects.None, 0.401f);

            //** Vida de la nave
            sb.DrawString(fuente,Nave.Vida.ToString(),PositionSalud, Color.White, 0.0f,
                Vector2.Zero, 0.8f, SpriteEffects.None, 0.388f);
            barraVidaNave.draw(sb);

            //Tiempo restante
            sb.DrawString(fuente, "Time:" + cuentaTiempo, positionTime, Color.Gold, 0.0f,
                Vector2.Zero, 1.0f, SpriteEffects.None, 0.4f);

            sb.DrawString(fuente, "/", PositionLabelPuntos, Color.White, 0.0f,
                Vector2.Zero, 1.3f, SpriteEffects.None, 0.4f);
        }

        public static new void updateGame(GameTime gt)
        {
            if (cuentaTiempo == 70 && maxEnemigos < 4) { maxEnemigos = maxEnemigos + 1; }
            if (cuentaTiempo == 20 && maxEnemigos < 5) { maxEnemigos = maxEnemigos + 1; }
            if (Nave.Vida <= 70 && cuentaGolpes == 3) 
            {
                cuentaGolpes--; onda.Position.Y = 750;
            }
            if (Nave.Vida <= 40 && cuentaGolpes == 2)
            {
                cuentaGolpes--; onda.Position.Y = 700;
            }
            if (Nave.Vida <= 10 && cuentaGolpes == 1)
            {
                cuentaGolpes--; onda.Position.Y = 640;
                Nave.Vida = 0;
            }

            if (enemigos.Count < maxEnemigos)
            {
                insertaEnemigo();
            }
        }

        public static new void gameOver()
        {
            if (cuentaTiempo <= 0)
            {
                if (MediaPlayer.GameHasControl && Audio.musicaActiva)
                {
                    MediaPlayer.IsRepeating = false;
                    MediaPlayer.Play(Audio.getMusica("GameOver"));
                }
                    MisionModeGOver.gana = true;

                    Almacenamiento.cargarMisiones();
                    if (Almacenamiento.getPremiosDados() == 12)
                    {
                        Almacenamiento.salvarBonus(4000, Almacenamiento.tipoBonus.tEsperaPoder);
                        MisionModeGOver.cosasGanadas = recompensa;
                        Almacenamiento.salvarMisiones();
                    }
                    else
                    {
                        MisionModeGOver.cosasGanadas = Mision1.yaRecompensa;
                    }

                    MisionMode.estadoActual = MisionMode.Estado.GameOver;
            }

            if (Nave.Vida <= 0)
            {
                Nave.Vida = 0;
                Nave.EscalaN = 0;
                Animation.animacionesEstallidoNave.setPosition(Nave.PositionNav);
                MisionModeGOver.gana = false;
                MisionModeGOver.cosasGanadas = noRecompensa;
                if (Animation.animacionesEstallidoNave.animacionFinalizada)
                {
                    if (MediaPlayer.GameHasControl && Audio.musicaActiva)
                    {
                        MediaPlayer.IsRepeating = false;
                        MediaPlayer.Play(Audio.getMusica("GameOver"));
                    }
                    MisionMode.estadoActual = MisionMode.Estado.GameOver;
                }
            }
        }

        //********Dibujo Mensaje Inicial
        public static new void drawMensajeInicial(SpriteBatch sb)
        {
            if (mensajeInicialActivo)
            {
                sb.DrawString(fuente,tituloMision+" Start!!", new Vector2(40, 320), 
                    Color.LightGreen, 0 ,Vector2.Zero, EscalaMensaje, SpriteEffects.None,0.4f);
            }
        }


        public static new void insertaEnemigo()
        {
            num2 = 0;
            num = random.Next(1, 3);

            if (num == 1)
            {
                num2 = random.Next(1, 3);
                if (num2 == 1)
                {
                    if (Enemigo.KamiTotal.Count > 0)
                    {
                        Enemigo.KamiTotal[Enemigo.KamiTotal.Count - 1].reinicia((-1) * random.Next(100, 500),
                                   random.Next(80, 350),
                                   velocidadEnem, 0, aumentoEnVidaEnemigo);
                        enemigos.Add(Enemigo.KamiTotal[Enemigo.KamiTotal.Count - 1]);
                        Enemigo.KamiTotal.RemoveAt(Enemigo.KamiTotal.Count - 1);
                    }
                }
                if (num2 == 2)
                {
                    if (Enemigo.KamiTotal.Count > 0)
                    {
                        Enemigo.KamiTotal[Enemigo.KamiTotal.Count - 1].reinicia(random.Next(1000, 1500),
                                  random.Next(80, 350),
                                  velocidadEnem, 0, aumentoEnVidaEnemigo);
                        enemigos.Add(Enemigo.KamiTotal[Enemigo.KamiTotal.Count - 1]);
                        Enemigo.KamiTotal.RemoveAt(Enemigo.KamiTotal.Count - 1);
                    }
                }
            }
            if (num == 2)
            {
                num2 = random.Next(1, 3);
                if (num2 == 1)
                {
                    if (Enemigo.HiKamiTotal.Count > 0)
                    {
                        Enemigo.HiKamiTotal[Enemigo.HiKamiTotal.Count - 1].reinicia((-1) * random.Next(100, 500),
                                                 random.Next(80, 350),
                                                 velocidadHiEnem, 0, aumentoEnVidaEnemigo);
                        enemigos.Add(Enemigo.HiKamiTotal[Enemigo.HiKamiTotal.Count - 1]);
                        Enemigo.HiKamiTotal.RemoveAt(Enemigo.HiKamiTotal.Count - 1);
                    }
                }
                if (num2 == 2)
                {
                    if (Enemigo.HiKamiTotal.Count > 0)
                    {
                        Enemigo.HiKamiTotal[Enemigo.HiKamiTotal.Count - 1].reinicia(random.Next(1000, 1500),
                                                 random.Next(80, 350),
                                                 velocidadHiEnem, 0, aumentoEnVidaEnemigo);
                        enemigos.Add(Enemigo.HiKamiTotal[Enemigo.HiKamiTotal.Count - 1]);
                        Enemigo.HiKamiTotal.RemoveAt(Enemigo.HiKamiTotal.Count - 1);
                    }
                }
            }
        }

    }
}
