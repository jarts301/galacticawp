using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Media;

namespace Galactica
{
    class Mision12:QuickModeGame
    {

        public static StringBuilder tituloMision = new StringBuilder("Mission 12");
        public static StringBuilder mensajeMision = new StringBuilder("Protects the small\nspaceship of enemies,\nand avoid that them \ndestroy it, until the \ntime finish.\nTip:Do not let the\nenemy get too close.");
        public static StringBuilder recompensa = new StringBuilder("Maximum energy increased\nby 200, in Quick Game.");
        public static StringBuilder noRecompensa = new StringBuilder("No Reward...");
        public static int cuentaTiempo=90;
        private static Vector2 positionMiniS, positionTime;
        private static double ultimoTMinuto;
        private static NMini mini=mini = new NMini();
        private static BarraVida barraVidaMini = new BarraVida(new Vector2(6, 50), BarraVida.tipo.mini);

        public Mision12()
        {

        }


        public static new void reiniciar()
        {
            eliminarTodos();
            eliminaAnimaciones();
            positionMiniS = new Vector2(6, 50);
            positionTime = new Vector2(340, 50);
            velocidadEnem = 4;
            velocidadEnemY = 4;
            velocidadHiEnem = 6;
            velocidadHiEnemY = 6;
            maxEnemigos = 4;
            cuentaTiempo = 90;
            aumentoEnVidaEnemigo = 0;
            ultimoTMinuto = 0;
            ultimoAumento = 0;
            mini.reiniciar();
            eliminarTodos();
            eliminaAnimaciones();
            EscalaMensaje = escalaInicialMensaje = 1.5f;
            actualFrameMensaje = 0;
            tiempoTitileoMensaje = 0;
            mensajeInicialActivo = true;
            Nave.disparosDiagActivos = true;
            botones.Clear();
            botones.Add(new Boton(Boton.TipoBoton.BPause, 0, 0));
            botones.Add(new Boton(Boton.TipoBoton.BAudio, 430, 0));
            botones.Add(new Boton(Boton.TipoBoton.BPower,
                                  (int)(480 - (100 * 0.8f)),
                                  (int)(800 - (100 * 0.8f))));
        }


        public static new void load(Textura t,SpriteFont f)
        {
            fuente = f;
        }

        public static new void update(GameTime time)
        {
            nave.update(time);
            updateEstrellas(time);
            updateGame(time);
            updateEnemigos(time);
            updateAnimaciones(time);
            mini.update(time);
            for (i = 0; i < botones.Count; i++)
            {
                botones[i].update(time);
            }
 
            for (i = 0; i < enemigos.Count; i++)
            {
                for (j = 0; j < enemigos[i].disparosSid.Count; j++)
                {
                    if (colision.colisionDispAMini(mini, enemigos[i].disparosSid[j])
                        && enemigos[i].tipoEnemigo== Enemigo.tipo.Sid)
                    {
                        enemigos[i].disparosSid[j].estaEnColision = false;
                        mini.vida = mini.vida - 3;
                        Enemigo.disparosSidTotal.Add(enemigos[i].disparosSid[j]);
                        enemigos[i].disparosSid.Remove(enemigos[i].disparosSid[j]);
                    }
                }

            }

            for (i = 0; i < enemigos.Count; i++)
            {
                if (colision.colisionMiniAEnemigo(mini, enemigos[i]))
                {
                    mini.vida = mini.vida - 20;
                    enemigos[i].estaEnColisionNave = true;
                    enemigos[i].vida = 0;
                }
            }

            //Cuenta de tiempo
            if (time.TotalGameTime.TotalMilliseconds > ultimoTMinuto + 1000)
            {
                cuentaTiempo= cuentaTiempo-1;
                ultimoTMinuto = time.TotalGameTime.TotalMilliseconds;
            }

            updateMensajeInicial(time);
            barraVidaNave.update(time, Nave.Vida, Nave.MaxVida);
            barraVidaMini.update(time,mini.vida,mini.maxVida);
            if (Nave.Vida <= 0 || mini.vida<=0) { Animation.animacionesEstallidoNave.update(time); }
            gameOver();

            if (Control.controlActivo)
            {
                control.update(time);
            }

        }

        public static new void draw(SpriteBatch sb)
        {
            //CLEAR NEGRO
            sb.Draw(Textura.TexturasFondo[0], Vector2.Zero, null, Color.Black, 0.0f, Vector2.Zero,
                40.0f, SpriteEffects.None, 0.999f);

            nave.draw(sb);
            drawEstrellas(sb);
            drawEnemigos(sb);
            drawEstadNave(sb);
            drawAnimaciones(sb);
            drawMensajeInicial(sb);
            mini.draw(sb);
            if (Nave.Vida <= 0 || mini.vida <= 0) { Animation.animacionesEstallidoNave.draw(sb); }

            for (i = 0; i < botones.Count; i++){botones[i].draw(sb);}
            if (Control.controlActivo)
            {
                control.draw(sb);
            }
            //drawEstadisticas(sb);
        }

        /******************Estadisticas de la nave**************/
        public static new void drawEstadNave(SpriteBatch sb)
        {
            sb.Draw(Textura.TexturasFondo[1], Vector2.Zero, null,Color.Black, 0.0f, Vector2.Zero, 
                new Vector2(50.0f,2.3f),SpriteEffects.None, 0.401f);

            //** Vida de la nave
            sb.DrawString(fuente,Nave.Vida.ToString(),PositionSalud, Color.White, 0.0f,
                Vector2.Zero, 0.8f, SpriteEffects.None, 0.388f);
            barraVidaNave.draw(sb);
            //****Puntos a vencer
            sb.DrawString(fuente, "Mini S", positionMiniS, Color.White, 0.0f,
                Vector2.Zero, 0.75f, SpriteEffects.None, 0.388f);
            barraVidaMini.draw(sb);

            //Tiempo restante
            sb.DrawString(fuente, "Time:" + cuentaTiempo, positionTime, Color.Gold, 0.0f,
                Vector2.Zero, 1.0f, SpriteEffects.None, 0.4f);
            sb.DrawString(fuente, "/", PositionLabelPuntos, Color.White, 0.0f,
                Vector2.Zero, 1.3f, SpriteEffects.None, 0.4f);
        }

        public static new void updateGame(GameTime gt)
        {
            if (cuentaTiempo == 20 && maxEnemigos<=4) { maxEnemigos = maxEnemigos + 3; }

            if (enemigos.Count < maxEnemigos)
            {
                insertaEnemigo();
            }
        }

        public static new void gameOver()
        {
            if (cuentaTiempo <= 0)
            {
                if (MediaPlayer.GameHasControl && Audio.musicaActiva)
                {
                    MediaPlayer.IsRepeating = false;
                    MediaPlayer.Play(Audio.getMusica("GameOver"));
                }
                MisionModeGOver.gana = true;

                Almacenamiento.cargarMisiones();
                if (Almacenamiento.getPremiosDados() == 11)
                {
                    Almacenamiento.salvarBonus(600, Almacenamiento.tipoBonus.sumarAsalud);
                    MisionModeGOver.cosasGanadas = recompensa;
                    Almacenamiento.salvarMisiones();
                }
                else
                {
                    MisionModeGOver.cosasGanadas = Mision1.yaRecompensa;
                }

                MisionMode.estadoActual = MisionMode.Estado.GameOver;
            }

            if (Nave.Vida <= 0)
            {
                Nave.Vida = 0;
                Nave.EscalaN = 0;
                Animation.animacionesEstallidoNave.setPosition(Nave.PositionNav);
                MisionModeGOver.gana = false;
                MisionModeGOver.cosasGanadas = noRecompensa;
                if (Animation.animacionesEstallidoNave.animacionFinalizada)
                {
                    if (MediaPlayer.GameHasControl && Audio.musicaActiva)
                    {
                        MediaPlayer.IsRepeating = false;
                        MediaPlayer.Play(Audio.getMusica("GameOver"));
                    }
                    MisionMode.estadoActual = MisionMode.Estado.GameOver;
                }
            }else
            if (mini.vida <= 0)
            {
                mini.vida = 0;
                mini.Escala = 0;
                Animation.animacionesEstallidoNave.setPosition(mini.getPosition());
                MisionModeGOver.gana = false;
                MisionModeGOver.cosasGanadas = noRecompensa;
                if (Animation.animacionesEstallidoNave.animacionFinalizada)
                {
                    if (MediaPlayer.GameHasControl && Audio.musicaActiva)
                    {
                        MediaPlayer.IsRepeating = false;
                        MediaPlayer.Play(Audio.getMusica("GameOver"));
                    }
                    MisionMode.estadoActual = MisionMode.Estado.GameOver;
                }
            }
        }

        //********Dibujo Mensaje Inicial
        public static new void drawMensajeInicial(SpriteBatch sb)
        {
            if (mensajeInicialActivo)
            {
                sb.DrawString(fuente,tituloMision+" Start!!", new Vector2(40, 320), 
                    Color.LightGreen, 0 ,Vector2.Zero, EscalaMensaje, SpriteEffects.None,0.4f);
            }
        }

        public static new void insertaEnemigo()
        {
            num = 0;
            num2 = 0;
            num = random.Next(1, 5);

            if (num == 1)
            {
                num2 = random.Next(1, 3);
                if (num2 == 1)
                {
                    if (Enemigo.KamiTotal.Count > 0)
                    {
                        Enemigo.KamiTotal[Enemigo.KamiTotal.Count - 1].reinicia((-1) * random.Next(100, 500),
                                   random.Next(80, 350),
                                   velocidadEnem, 0, aumentoEnVidaEnemigo);
                        enemigos.Add(Enemigo.KamiTotal[Enemigo.KamiTotal.Count - 1]);
                        Enemigo.KamiTotal.RemoveAt(Enemigo.KamiTotal.Count - 1);
                    }
                }
                if (num2 == 2)
                {
                    if (Enemigo.KamiTotal.Count > 0)
                    {
                        Enemigo.KamiTotal[Enemigo.KamiTotal.Count - 1].reinicia(random.Next(1000, 1500),
                                  random.Next(80, 350),
                                  velocidadEnem, 0, aumentoEnVidaEnemigo);
                        enemigos.Add(Enemigo.KamiTotal[Enemigo.KamiTotal.Count - 1]);
                        Enemigo.KamiTotal.RemoveAt(Enemigo.KamiTotal.Count - 1);
                    }
                }
            }
            if (num == 2)
            {
                num2 = random.Next(1, 3);
                if (num2 == 1)
                {
                    if (Enemigo.SidTotal.Count > 0)
                    {
                        Enemigo.SidTotal[Enemigo.SidTotal.Count - 1].reinicia((-1) * random.Next(100, 500),
                                  random.Next(80, 280),
                                  velocidadEnem, velocidadEnemY, aumentoEnVidaEnemigo);
                        enemigos.Add(Enemigo.SidTotal[Enemigo.SidTotal.Count - 1]);
                        Enemigo.SidTotal.RemoveAt(Enemigo.SidTotal.Count - 1);
                    }
                }
                if (num2 == 2)
                {
                    if (Enemigo.SidTotal.Count > 0)
                    {
                        Enemigo.SidTotal[Enemigo.SidTotal.Count - 1].reinicia(random.Next(1000, 1500),
                                                  random.Next(80, 280),
                                                  velocidadEnem, velocidadEnemY, aumentoEnVidaEnemigo);
                        enemigos.Add(Enemigo.SidTotal[Enemigo.SidTotal.Count - 1]);
                        Enemigo.SidTotal.RemoveAt(Enemigo.SidTotal.Count - 1);
                    }
                }
            }
            if (num == 3)
            {
                num2 = random.Next(1, 3);
                if (num2 == 1)
                {
                    if (Enemigo.HiKamiTotal.Count > 0)
                    {
                        Enemigo.HiKamiTotal[Enemigo.HiKamiTotal.Count - 1].reinicia((-1) * random.Next(100, 500),
                                                 random.Next(80, 350),
                                                 velocidadHiEnem, 0, aumentoEnVidaEnemigo);
                        enemigos.Add(Enemigo.HiKamiTotal[Enemigo.HiKamiTotal.Count - 1]);
                        Enemigo.HiKamiTotal.RemoveAt(Enemigo.HiKamiTotal.Count - 1);
                    }
                }
                if (num2 == 2)
                {
                    if (Enemigo.HiKamiTotal.Count > 0)
                    {
                        Enemigo.HiKamiTotal[Enemigo.HiKamiTotal.Count - 1].reinicia(random.Next(1000, 1500),
                                                 random.Next(80, 350),
                                                 velocidadHiEnem, 0, aumentoEnVidaEnemigo);
                        enemigos.Add(Enemigo.HiKamiTotal[Enemigo.HiKamiTotal.Count - 1]);
                        Enemigo.HiKamiTotal.RemoveAt(Enemigo.HiKamiTotal.Count - 1);
                    }
                }
            }
            if (num == 4)
            {
                num2 = random.Next(1, 3);
                if (num2 == 1)
                {
                    if (Enemigo.HiSidTotal.Count > 0)
                    {
                        Enemigo.HiSidTotal[Enemigo.HiSidTotal.Count - 1].reinicia((-1) * random.Next(100, 500),
                                                 random.Next(80, 280),
                                                 velocidadHiEnem, velocidadHiEnemY, aumentoEnVidaEnemigo);
                        enemigos.Add(Enemigo.HiSidTotal[Enemigo.HiSidTotal.Count - 1]);
                        Enemigo.HiSidTotal.RemoveAt(Enemigo.HiSidTotal.Count - 1);
                    }
                }
                if (num2 == 2)
                {
                    if (Enemigo.HiSidTotal.Count > 0)
                    {
                        Enemigo.HiSidTotal[Enemigo.HiSidTotal.Count - 1].reinicia(random.Next(1000, 1500),
                                                  random.Next(80, 280),
                                                  velocidadHiEnem, velocidadHiEnemY, aumentoEnVidaEnemigo);
                        enemigos.Add(Enemigo.HiSidTotal[Enemigo.HiSidTotal.Count - 1]);
                        Enemigo.HiSidTotal.RemoveAt(Enemigo.HiSidTotal.Count - 1);
                    }
                }
            }

        }

    }
}
