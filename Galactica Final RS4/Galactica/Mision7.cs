using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Media;

namespace Galactica
{
    class Mision7:QuickModeGame
    {

        public static StringBuilder tituloMision = new StringBuilder("Mission 7");
        public static StringBuilder mensajeMision = new StringBuilder("Shoot to the enemy all\nthe time, dont let to\ndo it or he will reload\nall his energy and\nyou will lose.\nTip:Shoot as fast as\nyou can.");
        public static StringBuilder recompensa = new StringBuilder("Maximum energy increased\nby 50, in Quick Game.");
        public static StringBuilder noRecompensa = new StringBuilder("No Reward...");
        public static int cuentaTiempo=60;
        private static Vector2 positionToBeat;
        private static double ultimoTMinuto;
        private static Vector2 positionEnemigoS, positionTime;
        private static Enemigo elEnemigo = new Enemigo(Enemigo.tipo.Nor,200,200,0,0);
        private static BarraVida barraVidaEnemigo = new BarraVida(new Vector2(6, 50), BarraVida.tipo.mini);

        public Mision7()
        {

            
        }

        public static new void reiniciar()
        {
            eliminarTodos();
            eliminaAnimaciones();
            positionToBeat = new Vector2(6, 50);
            positionTime = new Vector2(340, 50);
            positionEnemigoS = new Vector2(6, 50);
            botones.Clear();
            botones.Add(new Boton(Boton.TipoBoton.BPause, 0, 0));
            botones.Add(new Boton(Boton.TipoBoton.BAudio, 430, 0));
            botones.Add(new Boton(Boton.TipoBoton.BPower,
                                  (int)(480 - (100 * 0.8f)),
                                  (int)(800 - (100 * 0.8f))));

            velocidadEnem = 4;
            velocidadEnemY = 4;
            velocidadHiEnem = 6;
            velocidadHiEnemY = 6;
            maxEnemigos = 5;
            cuentaTiempo = 60;
            elEnemigo.movimientoLoco = true;
            elEnemigo.setPosition(new Vector2(200, 200));
            elEnemigo.maxVida = 200;
            elEnemigo.vida = 30;
            elEnemigo.velMinLoco = 5; elEnemigo.velMaxLoco = 10;
            elEnemigo.locoAbajo = 450; elEnemigo.locoArriba = 50;
            elEnemigo.locoDerecha = 480; elEnemigo.locoIzquierda = 0;
            aumentoEnVidaEnemigo = 0;
            ultimoTMinuto = 0;
            ultimoAumento = 0;
            eliminarTodos();
            eliminaAnimaciones();
            EscalaMensaje = escalaInicialMensaje = 1.5f;
            actualFrameMensaje = 0;
            tiempoTitileoMensaje = 0;
            mensajeInicialActivo = true;
            Nave.puntoInicio(new Vector2(240, 700));
        }

        public static new void load(Textura t,SpriteFont f)
        {
            fuente = f;
        }

        public static new void update(GameTime time)
        {
            nave.update(time);
            updateEstrellas(time);
            Nave.movEnY = false;
            elEnemigo.update(time);
            if (elEnemigo.estaEnColisionDisp)
            {
                elEnemigo.estaEnColisionDisp = false;
                if (Animation.animacionesDispAEnemigoTotal.Count > 0)
                {
                    Animation.animacionesDispAEnemigoTotal.ElementAt(Animation.animacionesDispAEnemigoTotal.Count - 1)
                        .setPosition(elEnemigo.Position);
                    animacionesQ.Add(Animation.animacionesDispAEnemigoTotal[Animation.animacionesDispAEnemigoTotal.Count - 1]);
                    Animation.animacionesDispAEnemigoTotal.RemoveAt(Animation.animacionesDispAEnemigoTotal.Count - 1);
                }
            }

            updateAnimaciones(time);
            for (i = 0; i < botones.Count; i++)
            {
                botones[i].update(time);
            }

            if (time.TotalGameTime.TotalMilliseconds > ultimoTMinuto + 1000)
            {
                cuentaTiempo= cuentaTiempo-1;
                ultimoTMinuto = time.TotalGameTime.TotalMilliseconds;
                elEnemigo.vida = elEnemigo.vida + 10;
            }
            if (elEnemigo.vida <= 0) { elEnemigo.vida = 20; }

            updateMensajeInicial(time);
            barraVidaNave.update(time, Nave.Vida, Nave.MaxVida);
            barraVidaEnemigo.update(time, elEnemigo.vida, elEnemigo.maxVida);
            if (Nave.Vida <= 0) { Animation.animacionesEstallidoNave.update(time); }
            gameOver();
            if (Control.controlActivo)
            {
                control.update(time);
            }
        }

        public static new void draw(SpriteBatch sb)
        {
            //CLEAR NEGRO
            sb.Draw(Textura.TexturasFondo[0], Vector2.Zero, null, Color.Black, 0.0f, Vector2.Zero,
                40.0f, SpriteEffects.None, 0.999f);

            nave.draw(sb);
            drawEstrellas(sb);
            drawEstadNave(sb);
            elEnemigo.draw(sb);
            drawAnimaciones(sb);
            drawMensajeInicial(sb);
            if (Nave.Vida <= 0) { Animation.animacionesEstallidoNave.draw(sb); }

            for (i = 0; i < botones.Count; i++)
            {
                botones[i].draw(sb);
            }
            if (Control.controlActivo)
            {
                control.draw(sb);
            }
           // drawEstadisticas(sb);
        }

        /******************Estadisticas de la nave**************/
        public static new void drawEstadNave(SpriteBatch sb)
        {
            sb.Draw(Textura.TexturasFondo[1], Vector2.Zero, null,Color.Black, 0.0f, Vector2.Zero, 
                new Vector2(50.0f,2.3f),SpriteEffects.None, 0.401f);

            //** Vida de la nave
            sb.DrawString(fuente,Nave.Vida.ToString(),PositionSalud, Color.White, 0.0f,
                Vector2.Zero, 0.8f, SpriteEffects.None, 0.388f);
            barraVidaNave.draw(sb);
            //Tiempo restante
            sb.DrawString(fuente, "Time:" + cuentaTiempo, positionTime, Color.Gold, 0.0f,
                Vector2.Zero, 1.0f, SpriteEffects.None, 0.4f);

            sb.DrawString(fuente, "Enemy", positionEnemigoS, Color.White, 0.0f,
                Vector2.Zero, 0.75f, SpriteEffects.None, 0.388f);
            barraVidaEnemigo.draw(sb);

            //**Puntos
            sb.DrawString(fuente, "/", PositionLabelPuntos, Color.White, 0.0f,
                Vector2.Zero, 1.3f, SpriteEffects.None, 0.4f);
        }

        public static new void gameOver()
        {
            if (cuentaTiempo <= 0)
            {
                if (MediaPlayer.GameHasControl && Audio.musicaActiva)
                {
                    MediaPlayer.IsRepeating = false;
                    MediaPlayer.Play(Audio.getMusica("GameOver"));
                }
                MisionModeGOver.gana = true;

                Almacenamiento.cargarMisiones();
                if (Almacenamiento.getPremiosDados() == 6)
                {
                    Almacenamiento.salvarBonus(300, Almacenamiento.tipoBonus.sumarAsalud);
                    MisionModeGOver.cosasGanadas = recompensa;
                    Almacenamiento.salvarMisiones();
                }
                else
                {
                    MisionModeGOver.cosasGanadas = Mision1.yaRecompensa;
                }

                MisionMode.estadoActual = MisionMode.Estado.GameOver;
            }

            if (Nave.Vida <= 0 || elEnemigo.vida>=elEnemigo.maxVida)
            {
                Nave.Vida = 0;
                elEnemigo.vida = elEnemigo.maxVida;
                Nave.EscalaN = 0;
                Animation.animacionesEstallidoNave.setPosition(Nave.PositionNav);
                MisionModeGOver.gana = false;
                MisionModeGOver.cosasGanadas = noRecompensa;
                if (Animation.animacionesEstallidoNave.animacionFinalizada)
                {
                    if (MediaPlayer.GameHasControl && Audio.musicaActiva)
                    {
                        MediaPlayer.IsRepeating = false;
                        MediaPlayer.Play(Audio.getMusica("GameOver"));
                    }
                    MisionMode.estadoActual = MisionMode.Estado.GameOver;
                }
            }
        }

        //********Dibujo Mensaje Inicial
        public static new void drawMensajeInicial(SpriteBatch sb)
        {
            if (mensajeInicialActivo)
            {
                sb.DrawString(fuente,tituloMision+" Start!!", new Vector2(40, 320), 
                    Color.LightGreen, 0 ,Vector2.Zero, EscalaMensaje, SpriteEffects.None,0.4f);
            }
        }
    }
}
