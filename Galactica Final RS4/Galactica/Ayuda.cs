using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Galactica
{
    class Ayuda:GameComun
    {

        private static List<Texture2D> textura;
        private static Vector2 positionTitulo,origenTitulo;
        private List<Boton> botones;
        private int i;

        public Ayuda()
        {
            botones = new List<Boton>();
            botones.Add(new Boton(Boton.TipoBoton.BBack, 10, 730));
        }

        public static void load(Textura t)
        {
            setTexturas(t);
            loadTitulo();
        }

        public void update(GameTime time)
        {
            updateEstrellas(time);
            for (i = 0; i < botones.Count; i++)
            {
                botones[i].update(time);
            }
        }

        public void draw(SpriteBatch sb)
        {
            //CLEAR NEGRO
            sb.Draw(Textura.TexturasFondo[0], Vector2.Zero, null, Color.Black, 0.0f, Vector2.Zero,
                100.0f, SpriteEffects.None, 0.999f);

            drawEstrellas(sb);
            drawTitulo(sb);
            for (i = 0; i < botones.Count; i++)
            {
                botones[i].draw(sb);
            }
        }

        /*******************************************/

        /******TITULO************/
        public static void loadTitulo()
        {
            positionTitulo = Vector2.Zero;
            origenTitulo = Vector2.Zero;
        }

        public void drawTitulo(SpriteBatch sb)
        {
            sb.Draw(textura[0],Vector2.Zero, null, Color.White, 0.0f, origenTitulo,
                2.0f, SpriteEffects.None, 0.5f);
        }

        /*********Texturas****************/
        public static void setTexturas(Textura t)
        {
            textura = new List<Texture2D>();
            textura.Add(t.getTextura("Help"));//0
        }

    }
}
