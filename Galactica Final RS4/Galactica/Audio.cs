using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

namespace Galactica
{
    class Audio
    {
        private static List<SoundEffect> Efectos=new List<SoundEffect>();
        private static List<string> nombreEfectos=new List<string>();
        private static List<Song> Musica=new List<Song>();
        private static List<string> nombreMusica=new List<string>();
        public static SoundEffect silencio;
        private static int i;
        public static bool efectosActivos,musicaActiva;

        public Audio()
        {
            efectosActivos = true;
            musicaActiva = true;            
        }

        public void addEfecto(SoundEffect efect,string nom)
        {
            Efectos.Add(efect);
            nombreEfectos.Add(nom);
        }

        public void addMusica(Song song, string nom)
        {
            Musica.Add(song);
            nombreMusica.Add(nom);
        }

        public static SoundEffect getEfecto(string nombre)
        {
            if (efectosActivos)
            {
                for (i = 0; i < Efectos.Count; i++)
                {
                    if (nombreEfectos.ElementAt(i).Equals(nombre))
                    {
                        return Efectos.ElementAt(i);
                    }
                }
            }

            return silencio;
        }

        public static Song getMusica(string nombre)
        {
           for (i = 0; i < Musica.Count; i++)
           {
               if (nombreMusica.ElementAt(i).Equals(nombre))
               {
                   return Musica.ElementAt(i);
               }
           }
            return null;
        }


    }
}
