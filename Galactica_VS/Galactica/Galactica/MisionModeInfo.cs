using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Galactica
{
    class MisionModeInfo:GameComun
    {
        private static List<Texture2D> textura;
        private static SpriteFont Letra;
        private Vector2 positionTitulo,origenTitulo,positionImagen;
        private Vector2 positionSubT, origenSubT,positionInfo,positionTMision;
        private List<Boton> botones;
        public static StringBuilder TituloMision = new StringBuilder();
        public static StringBuilder InfoMision=new StringBuilder();
        private int i;

        public MisionModeInfo()
        {
            loadTitulo();
            botones = new List<Boton>();
            botones.Add(new Boton(Boton.TipoBoton.BPlay, 200, 700));
            botones.Add(new Boton(Boton.TipoBoton.BBack, 10, 730));
        }

        public static void load(Textura t, SpriteFont f)
        {
            setTexturas(t);
            Letra = f;
        }

        public void update(GameTime time)
        {
            updateEstrellas(time);
            for (i = 0; i < botones.Count; i++)
            {
                botones[i].update(time);
            }
        }

        public void draw(SpriteBatch sb)
        {
            //CLEAR NEGRO
            sb.Draw(Textura.TexturasFondo[0], Vector2.Zero, null, Color.Black, 0.0f, Vector2.Zero,
                    40.0f, SpriteEffects.None, 0.999f);

            drawEstrellas(sb);
            drawTitulo(sb);
            for (i = 0; i < botones.Count; i++)
            {
                botones[i].draw(sb);
            }

            sb.DrawString(Letra,InfoMision, positionInfo, Color.White,0.0f,Vector2.Zero, 1.2f, 
                SpriteEffects.None, 0.01f);
            sb.DrawString(Letra, TituloMision, positionTMision, Color.LightGreen, 0.0f, 
                Vector2.Zero, 1.8f,SpriteEffects.None, 0.01f);

        }

        /*******************************************/

        /******TITULO************/
        public void loadTitulo()
        {
            positionTitulo = new Vector2(480/2,70);
            origenTitulo = new Vector2(textura[0].Width/2,textura[0].Height/2);
            positionSubT = new Vector2(480 / 2, 480);
            origenSubT = new Vector2(textura[1].Width / 2, textura[1].Height / 2);
            positionInfo = new Vector2(8,330);
            positionImagen = new Vector2(15,100);
            positionTMision = new Vector2(165,150);
        }

        public void drawTitulo(SpriteBatch sb)
        {
            sb.Draw(textura[0], positionTitulo, null, Color.White, 0.0f, origenTitulo,
                1.2f, SpriteEffects.None, 0.1f);
            sb.Draw(textura[1], positionSubT, null, Color.White, 0.0f, origenSubT,
                new Vector2(1.5f,1.5f), SpriteEffects.None, 0.1f);
            sb.Draw(textura[2], positionImagen, null, Color.White, 0.0f, Vector2.Zero,
                0.62f, SpriteEffects.None, 0.09f);
        }

        /*********Texturas****************/
        public static void setTexturas(Textura t)
        {
            textura = new List<Texture2D>();
            textura.Add(t.getTextura("TituloMisionMode"));//0
            textura.Add(t.getTextura("TInfo"));//1
            textura.Add(t.getTextura("Capitana"));//2
        }
    }
}
