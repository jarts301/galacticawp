using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Galactica
{
    class Enemigo : GameComun
    {
        /***************Valores importantes**********/
        public static List<Enemigo> SidTotal;
        public static List<Enemigo> NorTotal;
        public static List<Enemigo> KamiTotal;
        public static List<Enemigo> HiSidTotal;
        public static List<Enemigo> HiNorTotal;
        public static List<Enemigo> HiKamiTotal;
        public enum tipo { Nor, Kami, Sid, HiNor, HiKami, HiSid }
        private static List<Texture2D> texEnemigos;
        public static List<DisparoEnemigo> disparosSidTotal;//Sid
        public static List<DisparoEnemigo> disparosNorTotal;//Nor

        //public static List<Animation> animacionesDispANaveTotal;

        /****Para colisiones de enemigos****/
        public static int radioCollitionNor;
        public static int radioCollitionKami;
        public static int radioCollitionSid;
        public static int radioCollitionHiNor;
        public static int radioCollitionHiKami;
        public static int radioCollitionHiSid;

        public static List<int> collitionXSid=new List<int>();
        public static List<int> collitionYSid=new List<int>();
        public static List<int> collitionXHiSid = new List<int>();
        public static List<int> collitionYHiSid = new List<int>();

        public bool estaEnColisionDisp;
        public bool estaEnColisionNave;
        public bool estaEnColisionPoder;

        /*************Miembros de Enemigo**********/
        public int vida,maxVida;

        private int texturaAnim;
        private bool KamiPasivo;//Kami
        private bool SidMovHorizontal,dispararSid,SidMovVertical;//Sid
        private Vector2 posNave;
        public tipo tipoEnemigo;
        private int velocidadX,velocidadY,velocidadXini,velocidadYini;
        private int auxKamiPos;//kami
        private int contadorDisparosSid;//Sid
        private double ultima_actua;//Nor
        private bool goDerecha/*Todos*/,goArriba/*Kami*/;
        private float x1, y1,Tiempo;
        private int i;
        private float Rotacion;
        private Color colorEnemigo;
        private int velocidadCambioColor,tonoAzul,tonoRojo;
        private double ultimoTDisparo;
        public static int adicionDanoNave;
        public static int num=0;
        public bool movimientoLoco,cambiarMov,movAposition;
        public int velMinLoco=0, velMaxLoco=0,numero=0;
        public int locoAbajo=0, locoArriba=0, locoDerecha=0, locoIzquierda=0;
        public Vector2 positionAMoverse=new Vector2();

        
        /*********Constructores de Enemigo*************/
        public Enemigo()
        {
        }

        public Enemigo(tipo t, int x, int y, int vx,int vy)
        {
            movimientoLoco = false;
            cambiarMov = true;
            movAposition = false;
            num = 0;
            tipoEnemigo = t;
            defineDatosIniciales();
            Position.X = x;
            Position.Y = y;
            velocidadX = vx;
            velocidadCambioColor = 5;
            tonoAzul = 151;
            tonoRojo = 151;
            velocidadY = vy;
            velocidadXini = vx;
            if (t == tipo.Sid && t==tipo.HiSid) { velocidadYini = vy; }
            ultima_actua = 0;
            auxKamiPos = 0;
            KamiPasivo = true;
            SidMovHorizontal = true;
            SidMovVertical = false;
            goArriba = true;
            dispararSid = false;
            estaEnColisionDisp = false;
            estaEnColisionNave = false;
            estaEnColisionPoder = false;
            actualframe = 0;
            contadorDisparosSid = 0;
            Rotacion = 0;
            ultimoTDisparo = 0;
            if (Position.X >= 450) { goDerecha = false; }
            if (Position.X <= 30) { goDerecha = true; }
            if (Position.X >= 450 && (t == tipo.Sid || t == tipo.HiSid)) { Rotacion = 180; }
        }

        /**************Metodos principales de Enemigo*******/

        public void reinicia(int x, int y, int vx, int vy,int aumentoVida)
        {
            movimientoLoco = false;
            cambiarMov = true;
            num = 0;
            defineDatosIniciales();
            setAumentoVida(aumentoVida);
            Position.X = x;
            Position.Y = y;
            velocidadX = vx;
            velocidadCambioColor = 5;
            tonoAzul = 151;
            tonoRojo = 151;
            velocidadY = vy;
            velocidadXini = vx;
            if (tipoEnemigo == tipo.Sid && tipoEnemigo == tipo.HiSid) { velocidadYini = vy; }
            ultima_actua = 0;
            auxKamiPos = 0;
            KamiPasivo = true;
            SidMovHorizontal = true;
            SidMovVertical = false;
            goArriba = true;
            dispararSid = false;
            estaEnColisionDisp = false;
            estaEnColisionNave = false;
            estaEnColisionPoder = false;
            actualframe = 0;
            contadorDisparosSid = 0;
            Rotacion = 0;
            ultimoTDisparo = 0;
            if (Position.X >= 450) { goDerecha = false; }
            if (Position.X <= 30) { goDerecha = true; }
            if (Position.X >= 450 && (tipoEnemigo == tipo.Sid || tipoEnemigo == tipo.HiSid)) { Rotacion = 180; }
        }

        //Metodo load de enemigo 
        public static void load(Textura t)
        {
            adicionDanoNave = 0;
            setTextura(t);
            defineListasDeDisparos();
            radioCollitionNor = (int)(45 * 0.65f);
            radioCollitionHiNor = (int)(45 * 1.3f);
            radioCollitionKami = (int)(30*0.85f);
            radioCollitionHiKami = (int)(30 * 1.7f);
            defineCollitionBoxSid();
            defineCollitionBoxHiSid();
            loadListasEnemigos();
        }

        //metodo update de enemigo
        public void update(GameTime gameTime)
        {
            defineDatosAnimacion(gameTime);
            velocidadAnimacion(gameTime);
            updateDisparosSid(gameTime);
            updateDisparosNor(gameTime);
            updateAnimaciones(gameTime);
        }

        //metodo draw de enemigo
        public void draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texEnemigos[texturaAnim], Position, TrozoAnim, colorEnemigo,
                    MathHelper.ToRadians(Rotacion), Origen, Escala, SpriteEffects.None, 0.43f);
            drawDisparosSid(spriteBatch);
            drawDisparosNor(spriteBatch);
            drawAnimaciones(spriteBatch);
        }
        /**************FIN Metodos principales de Enemigo*******/

        /*******************Definicion de metodos agregados*********************/

        public void setAumentoVida(int v)
        {
            vida = vida+v;
            maxVida = maxVida+v;
        }

        public void setPosition(Vector2 pos)
        {
            Position.X = pos.X;
            Position.Y = pos.Y;
        }


        public static void loadListasEnemigos()
        {
            SidTotal = new List<Enemigo>(); NorTotal = new List<Enemigo>();
            KamiTotal = new List<Enemigo>(); HiSidTotal = new List<Enemigo>();
            HiNorTotal = new List<Enemigo>(); HiKamiTotal = new List<Enemigo>();

            for (int i = 0; i < 300; i++)
            {
                NorTotal.Add(new Enemigo(Enemigo.tipo.Nor,0,0,0,0));
                KamiTotal.Add(new Enemigo(Enemigo.tipo.Kami, 0, 0, 0, 0));
                SidTotal.Add(new Enemigo(Enemigo.tipo.Sid, 0, 0, 0, 0));
                HiNorTotal.Add(new Enemigo(Enemigo.tipo.HiNor, 0, 0, 0, 0));
                HiKamiTotal.Add(new Enemigo(Enemigo.tipo.HiKami, 0, 0, 0, 0));
                HiSidTotal.Add(new Enemigo(Enemigo.tipo.HiSid, 0, 0, 0, 0));
            }
        }

        //velocidad de la animacion
        public void velocidadAnimacion(GameTime gameTime)
        {
            
            dato += gameTime.ElapsedGameTime;
            if (dato.TotalMilliseconds >=
                ((gameTime.ElapsedGameTime.Milliseconds) * velocidad))
            {
                actualframe = actualframe + 1;
                dato = TimeSpan.Zero;
            }

            if (actualframe >= numeroCuadros) { actualframe = 0; }
        }

        //define los datos de cada enemigo
        public void defineDatosAnimacion(GameTime gameTime)
        {
            if (tipoEnemigo == tipo.Nor || tipoEnemigo==tipo.HiNor)
            {
                defineTrozoAnim(100*actualframe,0,100,100);
                defineOrigen();
                movimientoNor(gameTime);
            }else
            if (tipoEnemigo == tipo.Kami || tipoEnemigo==tipo.HiKami)
            {
                defineTrozoAnim(100 * actualframe, 0, 100, 100);
                defineOrigen();
                movimientoKami();
            }else
            if (tipoEnemigo == tipo.Sid || tipoEnemigo==tipo.HiSid)
            {
                defineTrozoAnim(100 * actualframe, 0, 100, 75);
                defineOrigen();
                movimientoSid(gameTime);
            }

            if (tipoEnemigo == tipo.HiNor || tipoEnemigo==tipo.HiKami || tipoEnemigo==tipo.HiSid)
            {
                tonoAzul = tonoAzul + velocidadCambioColor;
                tonoRojo = tonoRojo + velocidadCambioColor;

                if (tonoAzul <= 150 || tonoAzul >= 250)
                {
                    velocidadCambioColor = velocidadCambioColor * (-1);
                }

                colorEnemigo.B = (byte)tonoAzul;
                colorEnemigo.R = (byte)tonoRojo;
            }
        }

        //Datos iniciales de cada enemigo
        public void defineDatosIniciales()
        {
            switch(tipoEnemigo)
            {
                case tipo.Nor:
                       velocidad = 2;
                       numeroCuadros = 8;
                       texturaAnim = 0;
                       Escala = 0.65f;
                       colorEnemigo = Color.White;
                       vida =maxVida= 15;
                    break;

                case tipo.Kami:
                       velocidad = 2;
                       numeroCuadros = 8;
                       texturaAnim = 1;
                       Escala = 0.85f;
                       colorEnemigo = Color.White;
                       vida = maxVida = 15;
                    break;

                case tipo.Sid:
                       velocidad = 2;
                       numeroCuadros = 7;
                       texturaAnim = 2;
                       Escala = 0.85f;
                       colorEnemigo = Color.White;
                       vida = maxVida = 15; 
                    break;

                case tipo.HiNor:
                    velocidad = 2;
                    numeroCuadros = 8;
                    texturaAnim = 0;
                    Escala = 1.0f;
                    colorEnemigo = Color.White;
                    vida = maxVida = 30;
                    break;

                case tipo.HiKami:
                    velocidad = 2;
                    numeroCuadros = 8;
                    texturaAnim = 1;
                    Escala = 1.2f;
                    colorEnemigo = Color.White;
                    vida = maxVida = 30;
                    break;

                case tipo.HiSid:
                    velocidad = 2;
                    numeroCuadros = 7;
                    texturaAnim = 2;
                    Escala = 1.2f;
                    colorEnemigo = Color.White;
                    vida = maxVida = 30;
                    break;
            }
        }

        //define el cuandro de la animacion actual
        public void defineTrozoAnim(int x,int y,int width,int height)
        {
            TrozoAnim.X = x;
            TrozoAnim.Y = y;
            TrozoAnim.Width = width;
            TrozoAnim.Height = height;
        }

        //define el origen de coordenadas de la animacion
        public void defineOrigen()
        {
            Origen.X = TrozoAnim.Width / 2;
            Origen.Y = TrozoAnim.Height / 2;
        }

        /**************Inteligencia de Nor******************/
        public void movimientoNor(GameTime gameTime)
        {
            if (!movimientoLoco)
            {
                if (goDerecha)
                {
                    Position.X = Position.X + (velocidadX - velocidadY);
                    if (Position.X >= 400 && (velocidadX - velocidadY) != 0) { velocidadY++; }
                    if ((velocidadX - velocidadY) == 0 || Position.X >= 450)
                    { goDerecha = false; velocidadY = 0; }
                }
                else
                    if (!goDerecha)
                    {
                        Position.X = Position.X - (velocidadX - velocidadY);
                        if (Position.X <= 80 && (velocidadX - velocidadY) != 0) { velocidadY++; }
                        if ((velocidadX - velocidadY) == 0 || Position.X <= 30)
                        { goDerecha = true; velocidadY = 0; }
                    }

                if ((gameTime.TotalGameTime.TotalMilliseconds > ultima_actua + 5000)
                    && (Position.X > 0 && Position.X < 480))
                {
                    if (disparosNorTotal.Count > 0)
                    {
                        disparosNorTotal.ElementAt(disparosNorTotal.Count - 1).setPosition(Position);
                        disparosNorTotal.ElementAt(disparosNorTotal.Count - 1).Disparar = true;
                        disparosNor.Add(disparosNorTotal.ElementAt(disparosNorTotal.Count - 1));
                        disparosNorTotal.Remove(disparosNorTotal.ElementAt(disparosNorTotal.Count - 1));
                    }

                    ultima_actua = gameTime.TotalGameTime.TotalMilliseconds;
                }
            }
            else
            {
                if (!movAposition)
                {
                    if (cambiarMov)
                    {
                        velocidadX = random.Next(velMinLoco, velMaxLoco);
                        velocidadY = random.Next(velMinLoco, velMaxLoco);
                        numero = random.Next(1, 5);
                        switch (numero)
                        {
                            case 1:
                                velocidadX = velocidadX * (1);
                                velocidadY = velocidadY * (1);
                                break;
                            case 2:
                                velocidadX = velocidadX * (-1);
                                velocidadY = velocidadY * (1);
                                break;
                            case 3:
                                velocidadX = velocidadX * (1);
                                velocidadY = velocidadY * (-1);
                                break;
                            case 4:
                                velocidadX = velocidadX * (-1);
                                velocidadY = velocidadY * (-1);
                                break;

                        }
                        cambiarMov = false;
                    }

                    if (Position.Y >= locoAbajo && velocidadY > 0)
                    {
                        velocidadY = velocidadY * (-1);
                        //cambiarMov = true;
                    }
                    if (Position.Y <= locoArriba && velocidadY < 0)
                    {
                        velocidadY = velocidadY * (-1);
                        cambiarMov = true;
                    }
                    if (Position.X >= locoDerecha && velocidadX > 0)
                    {
                        velocidadX = velocidadX * (-1);
                        //cambiarMov = true;
                    }
                    if (Position.X <= locoIzquierda && velocidadX < 0)
                    {
                        velocidadX = velocidadX * (-1);
                        cambiarMov = true;
                    }

                    Position.X = Position.X + velocidadX;
                    Position.Y = Position.Y + velocidadY;
                }
                else
                {
                        if (positionAMoverse.X != Position.X && Position.X > positionAMoverse.X)
                        {
                            Position.X = Position.X - 1;
                        }
                        if (positionAMoverse.X != Position.X && Position.X < positionAMoverse.X)
                        {
                            Position.X = Position.X + 1;
                        }
                        if (positionAMoverse.Y != Position.Y && Position.Y > positionAMoverse.Y)
                        {
                            Position.Y = Position.Y - 1;
                        }
                        if (positionAMoverse.Y != Position.Y && Position.Y < positionAMoverse.Y)
                        {
                            Position.Y = Position.Y + 1;
                        }
                }
            }

            confirmaColisiones(20);

        }

        //update de disparos de Nor
        public void updateDisparosNor(GameTime gt)
        {
            if (disparosNor.Count > 0)
            {
                for (i = 0; i < disparosNor.Count; i++)
                {
                    disparosNor[i].update(gt);
                }


                for (i = 0; i < disparosNor.Count; i++)
                {
                    if (disparosNor[i].estaEnColision)
                    {
                        disparosNor[i].estaEnColision = false;

                        if (Animation.animacionesDispANaveTotal.Count > 0)
                        {
                            Animation.animacionesDispANaveTotal.ElementAt(Animation.animacionesDispANaveTotal.Count - 1)
                                .setPosition(disparosNor[i].getPosition());
                            animaciones.Add(Animation.animacionesDispANaveTotal[Animation.animacionesDispANaveTotal.Count - 1]);
                            Animation.animacionesDispANaveTotal.RemoveAt(Animation.animacionesDispANaveTotal.Count - 1);
                        }

                        disparosNor[i].postDisparo = false;
                        disparosNorTotal.Add(disparosNor[i]);
                        disparosNor.Remove(disparosNor[i]);
                    }
                }

                for (i = 0; i < disparosNor.Count; i++)
                {
                    if (disparosNor[i].getPosition().X > 480 ||
                     disparosNor[i].getPosition().X < 0 ||
                     disparosNor[i].getPosition().Y < 0 ||
                     disparosNor[i].getPosition().Y > 800)
                    {
                        disparosNor[i].postDisparo = false;
                        disparosNorTotal.Add(disparosNor[i]);
                        disparosNor.Remove(disparosNor[i]);
                    }
                }
            }
        }

        //draw disparos de Nor
        public void drawDisparosNor(SpriteBatch sb)
        {
            for (i = 0; i < disparosNor.Count; i++)
            {
                disparosNor[i].draw(sb);
            }
        }
        /**************Fin Inteligencia de Nor******************/

        /**************Inteligencia de Kami******************/
        public void movimientoKami()
        {
            if (KamiPasivo)
            {
                   if (goDerecha)
                   {
                        Position.X = Position.X + velocidadX;
                        Position.Y = Position.Y + velocidadY;
                        if (goArriba) { velocidadY=velocidadY-3; }
                        if (!goArriba) { velocidadY=velocidadY+3; }
                        if (velocidadY > 10 || velocidadY < -10) { goArriba = !goArriba; }
                        if (Position.X >= 240) 
                        {
                             Tiempo = 0;
                             posNave = Nave.PositionNav;
                             x1 = Position.X;
                             y1 = Position.Y;
                             velocidadX = (int)((posNave.X - x1)) * 1 / 20;
                             velocidadY = (int)((posNave.Y - y1)) * 1 / 20;
                             KamiPasivo = false;
                        }
                    }

                    if (!goDerecha)
                    {
                        Position.X = Position.X - velocidadX;
                        Position.Y = Position.Y + velocidadY;
                        if (goArriba) { velocidadY = velocidadY - 3;}
                        if (!goArriba) { velocidadY = velocidadY + 3; }
                        if (velocidadY > 10 || velocidadY < -10) { goArriba = !goArriba; }
                        if (Position.X <= 240)
                        {
                             Tiempo = 0;
                             posNave = Nave.PositionNav; 
                             x1 = Position.X;
                             y1 = Position.Y;
                             velocidadX = (int)((posNave.X - x1)) * 1 / 25;
                             velocidadY = (int)((posNave.Y - y1)) * 1 / 25;
                             KamiPasivo = false;
                        }
                    }
            }

            if(!KamiPasivo)
            {
                    Tiempo++;
                    Position.X =  x1+velocidadX * Tiempo;
                    Position.Y =  y1+velocidadY * Tiempo;
                    if ((Position.X > 600 || Position.X < -200) || 
                        (Position.Y > 1000 || Position.Y < -200)  )
                    {
                        auxKamiPos = random.Next(0,2);
                        if (auxKamiPos == 0)
                        { 
                            Position.X = random.Next(600,1500);
                            goDerecha = false;
                        }else
                        if(auxKamiPos == 1)
                        { 
                            Position.X = random.Next(-1000,-200);
                            goDerecha = true;
                        }
                        Position.Y = random.Next(10, 300);
                        velocidadX = velocidadXini;
                        velocidadY = 0;
                        KamiPasivo = true;
                    }
            }

            confirmaColisiones(30);

        }
        /**************Fin Inteligencia de Kami******************/
        /**************Inteligencia de Sid******************/
        public void movimientoSid(GameTime gt)
        {
            if (SidMovHorizontal)
            {
                if (goDerecha)
                {
                    if (Position.X < 20)
                    { Position.X = Position.X + velocidadX; }
                    else
                    {
                        posNave = Nave.PositionNav;
                        SidMovHorizontal = false;
                        SidMovVertical = true;
                    }
                }
                if (!goDerecha)
                {
                    if (Position.X > 460)
                    { Position.X = Position.X - velocidadX; }
                    else 
                    {
                        posNave = Nave.PositionNav;
                        SidMovHorizontal = false;
                        SidMovVertical = true;
                    }
                }
            }
            if (SidMovVertical)
            {
                if (Position.Y < posNave.Y - 15)
                {
                    Position.Y = Position.Y + velocidadY;
                    ultimoTDisparo = gt.TotalGameTime.TotalMilliseconds;
                }
                else
                {
                    if (Position.Y > posNave.Y + 15)
                    {
                        Position.Y = Position.Y - velocidadY;
                        ultimoTDisparo = gt.TotalGameTime.TotalMilliseconds;
                    }
                    else
                    {
                        if (gt.TotalGameTime.TotalMilliseconds > ultimoTDisparo + 500)
                        {
                            dispararSid = true;
                            SidMovVertical = false;
                        }
                    }
                }    
            }

            if (dispararSid)
            {
                if (disparosSidTotal.Count > 0)
                {
                    disparosSidTotal.ElementAt(disparosSidTotal.Count - 1).setLadoMovimiento(goDerecha);
                    disparosSidTotal.ElementAt(disparosSidTotal.Count - 1).setPosition(Position);
                    disparosSid.Add(disparosSidTotal[disparosSidTotal.Count - 1]);
                    disparosSidTotal.RemoveAt(disparosSidTotal.Count - 1);
                    contadorDisparosSid++;
                }
            }

            if (contadorDisparosSid > 5)
            {
                    contadorDisparosSid = 0;
                    dispararSid = false;
                    posNave = Nave.PositionNav;
                    SidMovVertical = true;
            }

            confirmaColisiones(20);

        }
        //update de disparos de Sid
        public void updateDisparosSid(GameTime gt)
        {
            if (disparosSid.Count != 0)
            {
                for (i = 0; i < disparosSid.Count; i++)
                {
                    disparosSid[i].update(gt);
                }
            }

            /**********************************/
            for (i = 0; i < disparosSid.Count; i++)
            {
                if (disparosSid[i].estaEnColision)
                {
                    disparosSid[i].estaEnColision = false;
                    if (Animation.animacionesDispANaveTotal.Count > 0)
                    {
                        Animation.animacionesDispANaveTotal.ElementAt(Animation.animacionesDispANaveTotal.Count - 1)
                            .setPosition(disparosSid[i].getPosition());
                        animaciones.Add(Animation.animacionesDispANaveTotal[Animation.animacionesDispANaveTotal.Count - 1]);
                        Animation.animacionesDispANaveTotal.RemoveAt(Animation.animacionesDispANaveTotal.Count - 1);
                    }

                    disparosSidTotal.Add(disparosSid[i]);
                    disparosSid.Remove(disparosSid[i]);
                }
            }
            /*************************************/

            for (i = 0; i < disparosSid.Count; i++)
            {
                if (disparosSid[i].getPosition().X > 480 || disparosSid[i].getPosition().X < 0)
                {
                    disparosSidTotal.Add(disparosSid[i]);
                    disparosSid.Remove(disparosSid[i]);
                }
            }

        }

        //draw disparos de Sid
        public void drawDisparosSid(SpriteBatch sb)
        {
            for (i = 0; i < disparosSid.Count; i++)
            {
                disparosSid[i].draw(sb);
            }
        }

        public static void defineCollitionBoxSid()
        {
            radioCollitionSid = (int)(16 * 0.85f);
            collitionXSid = new List<int>();
            collitionYSid = new List<int>();

            collitionXSid.Add((int)(-25 * 0.85f)); collitionYSid.Add((int)(-11 * 0.85f));
            collitionXSid.Add((int)(4 * 0.85f)); collitionYSid.Add((int)(-11 * 0.85f));
            collitionXSid.Add((int)(-25 * 0.85f)); collitionYSid.Add((int)(15 * 0.85f));
            collitionXSid.Add((int)(4 * 0.85f)); collitionYSid.Add((int)(14 * 0.85f));
            collitionXSid.Add((int)(31 * 0.85f)); collitionYSid.Add((int)(1 * 0.85f));
        }

        public static void defineCollitionBoxHiSid()
        {
            radioCollitionHiSid = (int)(16 * 1.2f);
            collitionXHiSid = new List<int>();
            collitionYHiSid = new List<int>();

            collitionXHiSid.Add((int)(-25 * 1.2f)); collitionYHiSid.Add((int)(-11 * 1.2f));
            collitionXHiSid.Add((int)(4 * 1.2f)); collitionYHiSid.Add((int)(-11 * 1.2f));
            collitionXHiSid.Add((int)(-25 * 1.2f)); collitionYHiSid.Add((int)(15 * 1.2f));
            collitionXHiSid.Add((int)(4 * 1.2f)); collitionYHiSid.Add((int)(14 * 1.2f));
            collitionXHiSid.Add((int)(31 * 1.2f)); collitionYHiSid.Add((int)(1 * 1.2f));
        }

        /**************Fin Inteligencia de sid******************/

        /********** Update Animaciones ************* */

        public void updateAnimaciones(GameTime gt)
        {
            if (animaciones.Count > 0)
            {
                for (i = 0; i < animaciones.Count; i++)
                {
                    animaciones[i].update(gt);
                }
            }
            if (animaciones.Count > 0)
            {
                for (i = 0; i < animaciones.Count; i++)
                {
                    if (animaciones[i].animacionFinalizada &&
                        animaciones[i].tipoActual==Animation.tipoAnimacion.DisparoANave)
                    {
                        animaciones[i].animacionFinalizada = false;
                        Animation.animacionesDispANaveTotal.Add(animaciones[i]);
                        animaciones.Remove(animaciones[i]);
                    }
                }
            }
        }

        /***************** Draw animaciones *********** */

        public void drawAnimaciones(SpriteBatch sb)
        {
            if (animaciones.Count > 0)
            {
                for (i = 0; i < animaciones.Count; i++)
                {
                    animaciones[i].draw(sb);
                }
            }
        }

        /*Definicion de las texturas de los enemigos*/
        public static void setTextura(Textura t)
        {
            texEnemigos = new List<Texture2D>();
            texEnemigos.Add(t.getTextura("ENor"));//0
            texEnemigos.Add(t.getTextura("EKami"));//1
            texEnemigos.Add(t.getTextura("ESid"));//2
        }

        /*Definicion listas de disparos disparos*/
        public static void defineListasDeDisparos()
        {
            /**disparos para todos los enemigos tipoSid*/
            disparosSidTotal = new List<DisparoEnemigo>();
            //disparos para todos los enemigos Tipo Nor
            disparosNorTotal = new List<DisparoEnemigo>();
            for (int i = 0; i < 300; i++)
            {
                disparosSidTotal.Add(new DisparoEnemigo(Vector2.Zero,
                    DisparoEnemigo.tipo.dispSid, true));
                disparosNorTotal.Add(new DisparoEnemigo(Vector2.Zero,
                    DisparoEnemigo.tipo.dispNor, true));
            }
        }

        // Confirma colisiones
        public void confirmaColisiones(int danoNave)
        {
            if (colision.colisionDispAEnemigo(this, tipoEnemigo))
            {
                estaEnColisionDisp = true;
                if (Collition.ultimaColisionTipoDisparo != Disparo.tipo.super)
                {
                    vida = vida - (5+Nave.masPoderDisp);
                }
                else
                {
                    vida = vida - (10+Nave.masPoderDisp);
                }
            }

            if (colision.colisionNaveAEnemigo(this))
            {
                estaEnColisionNave = true;
                Nave.Vida = Nave.Vida - (danoNave + adicionDanoNave);
                vida = 0;
            }

            switch (Nave.TexturaNave)
            {
                case 0:
                    if (colision.colisionPoderGalacticaAEnemigo(this, tipoEnemigo))
                    {
                        estaEnColisionPoder = true;
                        vida = vida - 50;
                    }
                    break;
                case 1:
                    if (colision.colisionPoderCarafeAEnemigo(this, tipoEnemigo))
                    {
                        estaEnColisionPoder = true;
                        vida = vida - 30;
                    }
                    break;
                case 2:
                    if (colision.colisionPoderHelixAEnemigo(this, tipoEnemigo))
                    {
                        estaEnColisionPoder = true;
                        vida = vida - 1500;
                    }
                    break;
                case 3:
                    if (colision.colisionPoderArpAEnemigo(this, tipoEnemigo))
                    {
                        estaEnColisionPoder = true;
                        vida = vida - 8000;
                    }
                    break;
                case 4:
                    if (colision.colisionPoderPerseusAEnemigo(this, tipoEnemigo))
                    {
                        estaEnColisionPoder = true;
                        vida = vida - random.Next(1000, 5000);
                    }
                    break;
                case 5:
                    if (colision.colisionPoderPiscesAEnemigo(this, tipoEnemigo))
                    {
                        estaEnColisionPoder = true;
                        vida = vida - 80;
                    }
                    break;
            }

        }

    }
}
