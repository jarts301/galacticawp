using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Media;

namespace Galactica
{
    class Mision17:QuickModeGame
    {

        public static StringBuilder tituloMision = new StringBuilder("Mission 17");
        public static StringBuilder mensajeMision = new StringBuilder("Look attentively which\nof the enemies hide\nthe star (plus) and\ndestroy him, dont fail\nmore than three times\nor you will lose.\nTip:Keep your eye on\nthe target.");
        public static StringBuilder recompensa = new StringBuilder("Recharge, the energy in\n500, to catch the star\n in Quick Game.");
        public static StringBuilder noRecompensa = new StringBuilder("No Reward...");
        private static Vector2 positionEnemigoS, positionTime, positionToBeat, positionAyuda1, positionAyuda2;
        private static List<Enemigo> elEnemigo = new List<Enemigo>();
        private static new Random random=new Random();
        private static Plus estrella=new Plus(Vector2.Zero);
        private static int escogido=0,enemSeleccionado=20,aciertos=0;
        public static bool periodoMuestra,periodoEscoger;
        public static double tiempoEsperaMuestra=0, tiempoMovLoco=0;

        public Mision17()
        {
            
        }

        public static new void reiniciar()
        {
            eliminaAnimaciones();
            positionTime = new Vector2(340, 50);
            positionEnemigoS = new Vector2(6, 50);
            positionToBeat = new Vector2(6, 50);
            positionAyuda1 = new Vector2(90, 150);
            positionAyuda2 = new Vector2(50, 150);
            elEnemigo.Clear();
            for (i = 0; i < 4; i++)
            {
                elEnemigo.Add(new Enemigo(Enemigo.tipo.Nor, 200, 200, 0, 0));
            }
            botones.Clear();
            botones.Add(new Boton(Boton.TipoBoton.BPause, 0, 0));
            botones.Add(new Boton(Boton.TipoBoton.BAudio, 430, 0));

            velocidadEnem = 4;
            velocidadEnemY = 4;
            velocidadHiEnem = 6;
            velocidadHiEnemY = 6;
            maxEnemigos = 5;
            estrella.muestra=true;
            periodoMuestra = true;
            periodoEscoger = false;
            enemSeleccionado = 20;
            aciertos = 0;
            for (int i = 0; i < elEnemigo.Count; i++)
            {
                elEnemigo[i].movimientoLoco = true;
                elEnemigo[i].setPosition(new Vector2(random.Next(20, 400), random.Next(300, 400)));
                elEnemigo[i].maxVida = 200;
                elEnemigo[i].vida = 200;
                elEnemigo[i].velMinLoco = 4; elEnemigo[i].velMaxLoco = 10;
                elEnemigo[i].locoAbajo = 450; elEnemigo[i].locoArriba = 250;
                elEnemigo[i].locoDerecha = 460; elEnemigo[i].locoIzquierda = 20;
                elEnemigo[i].movAposition = true;
            }
            elEnemigo[0].positionAMoverse = new Vector2(60, 400);
            elEnemigo[1].positionAMoverse = new Vector2(180, 400);
            elEnemigo[2].positionAMoverse = new Vector2(300, 400);
            elEnemigo[3].positionAMoverse = new Vector2(420, 400);
            aumentoEnVidaEnemigo = 0;
            ultimoAumento = 0;
            EscalaMensaje = escalaInicialMensaje = 1.5f;
            actualFrameMensaje = 0;
            tiempoTitileoMensaje = 0;
            mensajeInicialActivo = true;
            Nave.puntoInicio(new Vector2(240, 700));
        }

        public static new void load(Textura t,SpriteFont f)
        {
            fuente = f;
        }

        public static new void update(GameTime time)
        {
            nave.update(time);
            updateEstrellas(time);
            updateGame(time);
            updateAnimaciones(time);
            Nave.movEnY = false;
            for (i = 0; i < elEnemigo.Count; i++)
            {
                elEnemigo[i].update(time);
            }

            for (i = 0; i < elEnemigo.Count; i++)
            {
                if (elEnemigo[i].estaEnColisionDisp && !periodoEscoger)
                {
                    elEnemigo[i].estaEnColisionDisp = false;
                    if (Animation.animacionesDispAEnemigoTotal.Count > 0)
                    {
                        Animation.animacionesDispAEnemigoTotal.ElementAt(Animation.animacionesDispAEnemigoTotal.Count - 1)
                              .setPosition(elEnemigo[i].Position);
                        animacionesQ.Add(Animation.animacionesDispAEnemigoTotal[Animation.animacionesDispAEnemigoTotal.Count - 1]);
                        Animation.animacionesDispAEnemigoTotal.RemoveAt(Animation.animacionesDispAEnemigoTotal.Count - 1);
                    }
                }
            }

            for (i = 0; i < botones.Count; i++)
            {
                botones[i].update(time);
            }
            updateMensajeInicial(time);
            barraVidaNave.update(time, Nave.Vida, Nave.MaxVida);
            if (Nave.Vida <= 0) { Animation.animacionesEstallidoNave.update(time); }
            gameOver();
            if (Control.controlActivo)
            {
                control.update(time);
            }
        }

        public static new void draw(SpriteBatch sb)
        {
            //CLEAR NEGRO
            sb.Draw(Textura.TexturasFondo[0], Vector2.Zero, null, Color.Black, 0.0f, Vector2.Zero,
                40.0f, SpriteEffects.None, 0.999f);

            nave.draw(sb);
            drawEstrellas(sb);
            drawEstadNave(sb);
            drawAnimaciones(sb);
            drawMensajeInicial(sb);

            for (i = 0; i < elEnemigo.Count; i++)
            {
                elEnemigo[i].draw(sb);
            }

            if (Nave.Vida <= 0) { Animation.animacionesEstallidoNave.draw(sb); }

            for (i = 0; i < botones.Count; i++)
            {
                botones[i].draw(sb);
            }

            if (elEnemigo[0].positionAMoverse == elEnemigo[0].Position &&
                elEnemigo[1].positionAMoverse == elEnemigo[1].Position &&
                elEnemigo[2].positionAMoverse == elEnemigo[2].Position &&
                elEnemigo[3].positionAMoverse == elEnemigo[3].Position &&
                periodoMuestra)
            {
                estrella.draw(sb);
            }
            if (Control.controlActivo)
            {
                control.draw(sb);
            }
           // drawEstadisticas(sb);
        }

        /******************Estadisticas de la nave**************/
        public static new void drawEstadNave(SpriteBatch sb)
        {
            sb.Draw(Textura.TexturasFondo[1], Vector2.Zero, null,Color.Black, 0.0f, Vector2.Zero, 
                new Vector2(50.0f,2.3f),SpriteEffects.None, 0.401f);

            //** Vida de la nave
            sb.DrawString(fuente,Nave.Vida.ToString(),PositionSalud, Color.White, 0.0f,
                Vector2.Zero, 0.8f, SpriteEffects.None, 0.388f);
            barraVidaNave.draw(sb);

            sb.DrawString(fuente, "Successes:" + aciertos.ToString()+"/3", positionToBeat, Color.Gold, 0.0f,
                Vector2.Zero, 1.0f, SpriteEffects.None, 0.4f);

            if (!periodoEscoger)
            {
                sb.DrawString(fuente, "Pay attention!", positionAyuda1, Color.Gold, 0.0f, Vector2.Zero,
                    1.3f, SpriteEffects.None, 0.4f);
            }
            if (periodoEscoger &&
                (elEnemigo[0].positionAMoverse == elEnemigo[0].Position &&
                elEnemigo[1].positionAMoverse == elEnemigo[1].Position &&
                elEnemigo[2].positionAMoverse == elEnemigo[2].Position &&
                elEnemigo[3].positionAMoverse == elEnemigo[3].Position))
            {
                sb.DrawString(fuente, "Shoot! to the enemy\n   with the star.", positionAyuda2, Color.Gold, 0.0f, Vector2.Zero,
                    1.3f, SpriteEffects.None, 0.4f);
            }

            //**Puntos
            sb.DrawString(fuente, "/", PositionLabelPuntos, Color.White, 0.0f,
                Vector2.Zero, 1.3f, SpriteEffects.None, 0.4f);
        }

        //Update Game
        public static new void updateGame(GameTime gt)
        {

            if (gt.TotalGameTime.TotalMilliseconds > tiempoMovLoco + 10000 &&
                !periodoMuestra && !periodoEscoger)
            {
                periodoEscoger = true;
                for (i = 0; i < elEnemigo.Count; i++)
                {
                    elEnemigo[i].movAposition = true;
                }
            }

            if (!periodoEscoger && !periodoMuestra)
            {
                Nave.disparosActivos = false;
            }

            if (periodoMuestra)
            {
                Nave.disparosActivos = false;
                if (elEnemigo[0].movAposition &&
                    elEnemigo[1].movAposition &&
                    elEnemigo[2].movAposition &&
                    elEnemigo[3].movAposition)
                {
                    if (elEnemigo[0].positionAMoverse == elEnemigo[0].Position &&
                        elEnemigo[1].positionAMoverse == elEnemigo[1].Position &&
                        elEnemigo[2].positionAMoverse == elEnemigo[2].Position &&
                        elEnemigo[3].positionAMoverse == elEnemigo[3].Position)
                    {

                        if (gt.TotalGameTime.TotalMilliseconds > tiempoEsperaMuestra + 3000)
                        {
                            periodoMuestra = false;
                            num2 = random.Next(1, 9);
                            switch (num2)
                            {
                                case 1:
                                    elEnemigo[0].positionAMoverse = new Vector2(60, 400);
                                    elEnemigo[1].positionAMoverse = new Vector2(180, 400);
                                    elEnemigo[2].positionAMoverse = new Vector2(300, 400);
                                    elEnemigo[3].positionAMoverse = new Vector2(420, 400);
                                    break;
                                case 2:
                                    elEnemigo[0].positionAMoverse = new Vector2(180, 400);
                                    elEnemigo[1].positionAMoverse = new Vector2(60, 400);
                                    elEnemigo[2].positionAMoverse = new Vector2(300, 400);
                                    elEnemigo[3].positionAMoverse = new Vector2(420, 400);
                                    break;
                                case 3:
                                    elEnemigo[0].positionAMoverse = new Vector2(180, 400);
                                    elEnemigo[1].positionAMoverse = new Vector2(60, 400);
                                    elEnemigo[2].positionAMoverse = new Vector2(420, 400);
                                    elEnemigo[3].positionAMoverse = new Vector2(300, 400);
                                    break;
                                case 4: 
                                    elEnemigo[0].positionAMoverse = new Vector2(180, 400);
                                    elEnemigo[1].positionAMoverse = new Vector2(420, 400);
                                    elEnemigo[2].positionAMoverse = new Vector2(60, 400);
                                    elEnemigo[3].positionAMoverse = new Vector2(300, 400);
                                    break;
                                case 5:
                                    elEnemigo[0].positionAMoverse = new Vector2(180, 400);
                                    elEnemigo[1].positionAMoverse = new Vector2(420, 400);
                                    elEnemigo[2].positionAMoverse = new Vector2(300, 400);
                                    elEnemigo[3].positionAMoverse = new Vector2(60, 400);
                                    break;
                                case 6:
                                    elEnemigo[0].positionAMoverse = new Vector2(300, 400);
                                    elEnemigo[1].positionAMoverse = new Vector2(420, 400);
                                    elEnemigo[2].positionAMoverse = new Vector2(180, 400);
                                    elEnemigo[3].positionAMoverse = new Vector2(60, 400);
                                    break;
                                case 7:
                                    elEnemigo[0].positionAMoverse = new Vector2(420, 400);
                                    elEnemigo[1].positionAMoverse = new Vector2(180, 400);
                                    elEnemigo[2].positionAMoverse = new Vector2(300, 400);
                                    elEnemigo[3].positionAMoverse = new Vector2(60, 400);
                                    break;
                                case 8:
                                    elEnemigo[0].positionAMoverse = new Vector2(60, 400);
                                    elEnemigo[1].positionAMoverse = new Vector2(420, 400);
                                    elEnemigo[2].positionAMoverse = new Vector2(300, 400);
                                    elEnemigo[3].positionAMoverse = new Vector2(180, 400);
                                    break;
                            }
                            for (i = 0; i < elEnemigo.Count; i++)
                            {
                                elEnemigo[i].movAposition = false;
                            }
                        }
                        tiempoMovLoco = gt.TotalGameTime.TotalMilliseconds;
                        switch (num)
                        {
                            case 1:
                                estrella.Position.X = elEnemigo[0].positionAMoverse.X;
                                estrella.Position.Y = elEnemigo[0].positionAMoverse.Y;
                                escogido = 0;
                                break;
                            case 2:
                                estrella.Position.X = elEnemigo[1].positionAMoverse.X;
                                estrella.Position.Y = elEnemigo[1].positionAMoverse.Y;
                                escogido = 1;
                                break;
                            case 3:
                                estrella.Position.X = elEnemigo[2].positionAMoverse.X;
                                estrella.Position.Y = elEnemigo[2].positionAMoverse.Y;
                                escogido = 2;
                                break;
                            case 4:
                                estrella.Position.X = elEnemigo[3].positionAMoverse.X;
                                estrella.Position.Y = elEnemigo[3].positionAMoverse.Y;
                                escogido = 3;
                                break;
                        }
                    }
                    else
                    {
                        tiempoEsperaMuestra = gt.TotalGameTime.TotalMilliseconds;
                        num = random.Next(1, 5); 
                    }
                }
            }

            if (periodoEscoger)
            {
                if (elEnemigo[0].movAposition &&
                    elEnemigo[1].movAposition &&
                    elEnemigo[2].movAposition &&
                    elEnemigo[3].movAposition)
                {
                    if (elEnemigo[0].positionAMoverse == elEnemigo[0].Position &&
                        elEnemigo[1].positionAMoverse == elEnemigo[1].Position &&
                        elEnemigo[2].positionAMoverse == elEnemigo[2].Position &&
                        elEnemigo[3].positionAMoverse == elEnemigo[3].Position)
                    {
                        Nave.disparosActivos = true;
                        for (i = 0; i < elEnemigo.Count; i++)
                        {
                            if (elEnemigo[i].estaEnColisionDisp)
                            {
                                elEnemigo[i].estaEnColisionDisp = false;
                                enemSeleccionado = i;
                                periodoMuestra = true;
                                periodoEscoger = false;
                                tiempoEsperaMuestra = gt.TotalGameTime.TotalMilliseconds;
                                if (enemSeleccionado == escogido)
                                {
                                    aciertos = aciertos + 1;
                                    (Audio.getEfecto("Estrella")).Play();
                                }
                                else
                                {
                                    (Audio.getEfecto("Error")).Play();
                                    Nave.Vida = Nave.Vida - 35;
                                }
                                for (j = 0; j < elEnemigo.Count; j++)
                                {
                                    if (elEnemigo[j].estaEnColisionDisp)
                                    {
                                        elEnemigo[j].estaEnColisionDisp = false;
                                    }
                                }
                                break;
                            }
                        }

                    }
                    else
                    {
                        tiempoEsperaMuestra = gt.TotalGameTime.TotalMilliseconds;
                        num = random.Next(1, 5);
                        Nave.disparosActivos = false;
                    }
                }
            }

        }


        public static new void gameOver()
        {
            if (aciertos == 3)
            {
                if (MediaPlayer.GameHasControl && Audio.musicaActiva)
                {
                    MediaPlayer.IsRepeating = false;
                    MediaPlayer.Play(Audio.getMusica("GameOver"));
                }
                MisionModeGOver.gana = true;

                Almacenamiento.cargarMisiones();
                if (Almacenamiento.getPremiosDados() == 16)
                {
                    Almacenamiento.salvarBonus(500, Almacenamiento.tipoBonus.recuperacion);
                    MisionModeGOver.cosasGanadas = recompensa;
                    Almacenamiento.salvarMisiones();
                }
                else
                {
                    MisionModeGOver.cosasGanadas = Mision1.yaRecompensa;
                }

                MisionMode.estadoActual = MisionMode.Estado.GameOver;
            }

            if (Nave.Vida <= 0)
            {
                Nave.Vida = 0;
                Nave.EscalaN = 0;
                Animation.animacionesEstallidoNave.setPosition(Nave.PositionNav);
                MisionModeGOver.gana = false;
                MisionModeGOver.cosasGanadas = noRecompensa;
                if (Animation.animacionesEstallidoNave.animacionFinalizada)
                {
                    if (MediaPlayer.GameHasControl && Audio.musicaActiva)
                    {
                        MediaPlayer.IsRepeating = false;
                        MediaPlayer.Play(Audio.getMusica("GameOver"));
                    }
                    MisionMode.estadoActual = MisionMode.Estado.GameOver;
                }
            }
        }

        //********Dibujo Mensaje Inicial
        public static new void drawMensajeInicial(SpriteBatch sb)
        {
            if (mensajeInicialActivo)
            {
                sb.DrawString(fuente,tituloMision+" Start!!", new Vector2(40, 320), 
                    Color.LightGreen, 0 ,Vector2.Zero, EscalaMensaje, SpriteEffects.None,0.4f);
            }
        }

       
    }
}
