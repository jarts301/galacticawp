using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Galactica
{
    class BarraVida:GameComun
    {
        private static Texture2D texturaBarra;
        private static Texture2D texturaMarco;
        public enum tipo {jefe,nave,mini}
        private float Rotacion;
        private Color Tinte;
        private Vector2 escala,escalaMarco;
        private float factorEscala;
        private float escalaInicialX, escalaInicialY;

        public BarraVida(Vector2 Pos,tipo tipoBarra)
        {
            Position.X = Pos.X;
            Position.Y = Pos.Y;
            Tinte = Color.LightGreen;
            Rotacion = 0;
            factorEscala = 0;
            setOrigen();
            if (tipoBarra == tipo.jefe)
            {
                escalaInicialX = 4.6f; escalaInicialY = 0.5f;
            }
            if (tipoBarra == tipo.nave)
            {
                escalaInicialX = 1.42f; escalaInicialY = 1.3f;
            }
            if (tipoBarra == tipo.mini)
            {
                escalaInicialX = 1.2f; escalaInicialY = 1.0f;
            }

            escala = new Vector2(escalaInicialX,escalaInicialY);
            escalaMarco = new Vector2(escalaInicialX,escalaInicialY);
        }

        public static void load(Textura tex)
        {
            texturaBarra = tex.getTextura("BarraVida");
            texturaMarco = tex.getTextura("BarraVidaMarco");
        }

        public void update(GameTime gt,float vida,float maxVida)
        {
            if (vida > ((maxVida * 3) / 8)) { Tinte = Color.LightGreen; }
            if (vida <= ((maxVida * 3) / 8)) { Tinte = Color.Gold; }
            if (vida <= ((maxVida * 1) / 8)) { Tinte = Color.Red; }

            if (maxVida != 0)
            {
                factorEscala = vida / maxVida;
                escala.X = factorEscala*escalaInicialX;
            }
        }

        public void draw(SpriteBatch sb)
        {
            sb.Draw(texturaBarra, Position, null, Tinte,MathHelper.ToRadians(Rotacion),
                Origen, escala, SpriteEffects.None, 0.4f);
            sb.Draw(texturaMarco, Position, null, Color.White, MathHelper.ToRadians(Rotacion),
                Origen, escalaMarco, SpriteEffects.None, 0.39f);
        }
        //***********************************************************

        public void reiniciaColor()
        {
            Tinte = Color.LightGreen;
        }

        public void setOrigen()
        {
            Origen.X = 0;
            Origen.Y = 0;
        }

        public void setPosition(Vector2 pos)
        {
            Position.X = pos.X;
            Position.Y = pos.Y;
        }

    }
}
