using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Devices.Sensors;
using Microsoft.Xna.Framework.Input.Touch;

namespace Galactica
{
    class Disparo : GameComun
    {
        private static List<Texture2D> texDisparo;
        private int texturaActual;
        public  enum tipo{pequei,pequed,diagI,diagD,super}
        public tipo tipoActual;
        public float Rotacion;

        public Disparo()
        {
        }

        public Disparo(Vector2 posN,tipo t)
        {
            tipoActual = t;
            Rotacion=0;
            Escala = 0.85f;
            radioCollition = (int)(7 * Escala);

            if (t == tipo.pequed || t == tipo.diagD)
            {
                setPositionPequed(posN);
                setOrigen(0);
                texturaActual = 0;
                if (t == tipo.diagD) { Rotacion = 35; }
            }
            if (t == tipo.pequei || t == tipo.diagI)
            {
                setPositionPequei(posN);
                setOrigen(0);
                texturaActual = 0;
                if (t == tipo.diagI) { Rotacion = -35; }
            }
            if (t == tipo.super)
            {
                setPositionSuper(posN);
                setOrigen(1);
                texturaActual = 1;
                Escala = 0.7f;
                radioCollition = (int)(37 * Escala);
            }
        }

        public static void load(Textura t)
        {
            setTextura(t);
        }

        public void update(GameTime gameTime)
        {
            if (tipoActual==tipo.pequed || tipoActual==tipo.pequei)
            {
                movimientoDisparo();
            }
            if (tipoActual==tipo.diagD)
            {
                movimientoDisparoDiagDerecha();
            }
            if (tipoActual == tipo.diagI)
            {
                movimientoDisparoDiagIzquierda();
            }
            if (tipoActual == tipo.super)
            {
                movimientoDisparoSuper();
            }
        }

        public void draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texDisparo[texturaActual],Position,null,Color.White,
                MathHelper.ToRadians(Rotacion),Origen,Escala,SpriteEffects.None,0.6f);
        }

        /***********************************/

        public static void setTextura(Textura t)
        {
            texDisparo = new List<Texture2D>();
            texDisparo.Add(t.getTextura("Disparo"));
            texDisparo.Add(t.getTextura("SuperDisparo"));
        }

        public void setPositionPequed(Vector2 posNave)
        {
            Position.X = posNave.X+25;
            Position.Y = posNave.Y+3;
        }
        public void setPositionPequei(Vector2 posNave)
        {
            Position.X = posNave.X-25;
            Position.Y = posNave.Y+3;
        }

        public void setPositionSuper(Vector2 posNave)
        {
            Position.X = posNave.X;
            Position.Y = posNave.Y - 25;
        }

        public Vector2 getPosition()
        {
            return Position;
        }

        public void movimientoDisparo()
        {
            Position.Y = Position.Y-25;
        }

        public void movimientoDisparoDiagDerecha()
        {
            Position.X = Position.X + 15;
            Position.Y = Position.Y - 25;
        }

        public void movimientoDisparoDiagIzquierda()
        {
            Position.X = Position.X - 15;
            Position.Y = Position.Y - 25;
        }

        public void movimientoDisparoSuper()
        {
            Position.Y = Position.Y - 30;
        }

        public void setOrigen(int textura)
        {
            Origen.X = texDisparo[textura].Width / 2;
            Origen.Y = texDisparo[textura].Height / 2;
        }

    }
}
