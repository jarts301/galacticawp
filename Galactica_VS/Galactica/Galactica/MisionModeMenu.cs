using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Galactica
{
    class MisionModeMenu:GameComun
    {
        private static List<Texture2D> textura;
        private static SpriteFont Letra;
        public static List<int> numeroMision;
        public static List<bool> estaActivo;
        public static List<Vector2> idMision;
        private Vector2 positionTitulo,origenTitulo;
        public static List<Boton> botones;
        private int i,j,posx,posy,cont;

        public MisionModeMenu()
        {
            loadTitulo();
            posx = 10; posy = 140;
            cont = 0;
            botones = new List<Boton>();
            idMision = new List<Vector2>();
            numeroMision = new List<int>();
            estaActivo = new List<bool>();
            for (i = 0; i < 5; i++)
            {
                for (j = 0; j < 4; j++)
                {
                    cont++;
                    botones.Add(new Boton(Boton.TipoBoton.BMision, posx, posy));
                    idMision.Add(new Vector2(posx,posy));
                    numeroMision.Add(cont);
                    estaActivo.Add(false);
                    posx = posx + 120;
                }
                posx = 10;
                posy = posy + 120;
            }
            //estaActivo[0] = true;
            botones.Add(new Boton(Boton.TipoBoton.BBack, 10, 730));
            
        }

        //Carga las misiones para saber cuales activar
        public static void cargarMisiones()
        {
            Almacenamiento.cargarMisiones();
            for (int i = 0; i < Almacenamiento.getMisionesActivas(); i++)
            {
                estaActivo[i] = true;
            }

            Nave.MaxVida = 100;
            Nave.sePuedeDispDiag = true;
            Nave.sePuedeDispSuper = true;
            Boton.tiempoEsperaPoder = 6000;
            Nave.masPoderDisp = 0;
            Plus.recuperacion = 50;
            Nave.Puntos = 0;
            Nave.Vida = 100;
            Nave.TexturaNave = 0;
            QuickModeGame.maxEnemigos = 0;
            Nave.Destruidos = 0;
            QuickModeGame.tiempoEsperaPlus = 30000;
            QuickModeGame.aumentoEnVidaJefe = 0;
            QuickModeGame.aumentoEnVidaEnemigo = 0;
            Jefe.adicionDanoNave = 0;
            DisparoJefe.adicionDanoDisNave = 0;
            Enemigo.adicionDanoNave = 0;
            DisparoEnemigo.adicionDanoDisNave = 0;
            QuickModeGame.ultimaAdicionJefe = 0;      
        }

        public static void load(Textura t, SpriteFont f)
        {
            setTexturas(t);
            Letra = f;
        }

        public void update(GameTime time)
        {
            updateEstrellas(time);
            for (i = 0; i < botones.Count; i++)
            {
                botones[i].update(time);
            }
            verificaBotonMision();
        }

        public void draw(SpriteBatch sb)
        {
            //CLEAR NEGRO
            sb.Draw(Textura.TexturasFondo[0], Vector2.Zero, null, Color.Black, 0.0f, Vector2.Zero,
                    40.0f, SpriteEffects.None, 0.999f);

            drawEstrellas(sb);
            drawTitulo(sb);
            for (i = 0; i < botones.Count; i++)
            {
                botones[i].draw(sb);
            }

            for (i = 0; i < numeroMision.Count; i++)
            {
                sb.DrawString(Letra,numeroMision[i].ToString(), idMision[i], 
                    Color.White,0.0f,Vector2.Zero, 1.7f, SpriteEffects.None, 0.01f);
            }
        }

        /*******************************************/

        /******TITULO************/
        public void loadTitulo()
        {
            positionTitulo = new Vector2(480/2,70);
            origenTitulo = new Vector2(textura[0].Width/2,textura[0].Height/2);
        }

        public void drawTitulo(SpriteBatch sb)
        {
            sb.Draw(textura[0], positionTitulo, null, Color.White, 0.0f, origenTitulo,
                1.2f, SpriteEffects.None, 0.1f);
        }

        //**Verifica si boton activo
        public void verificaBotonMision()
        {
            for (i = 0; i < estaActivo.Count; i++)
            {
                if (estaActivo[i])
                {
                    botones[i].actualframeB = 0;
                }
            }
        }

        /*********Texturas****************/
        public static void setTexturas(Textura t)
        {
            textura = new List<Texture2D>();
            textura.Add(t.getTextura("TituloMisionMode"));//0
        }

    }
}
