using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Media;

namespace Galactica
{
    class Mision18:QuickModeGame
    {
        public static StringBuilder tituloMision = new StringBuilder("Mission 18");
        public static StringBuilder mensajeMision = new StringBuilder("Eliminate only red color\nenemies, if you fail\nyou will lose.\nTip:If no red enemies,\nwait some time,\nthey will appear.");
        public static StringBuilder recompensa = new StringBuilder("Increases the shooting\npower,in Quick Game.");
        public static StringBuilder noRecompensa = new StringBuilder("No Reward...");
        public static int destruidos=0;
        private static Vector2 positionToBeat, positionTime;
        private static double ultimoTEliminar = 0;

        public Mision18()
        {
            
        }

        public static new void reiniciar()
        {
            eliminarTodos();
            eliminaAnimaciones();
            positionToBeat = new Vector2(6, 50);
            positionTime = new Vector2(340, 50);
            botones.Clear();
            botones.Add(new Boton(Boton.TipoBoton.BPause, 0, 0));
            botones.Add(new Boton(Boton.TipoBoton.BAudio, 430, 0));
            botones.Add(new Boton(Boton.TipoBoton.BPower,
                                  (int)(480 - (100 * 0.8f)),
                                  (int)(800 - (100 * 0.8f))));

            velocidadEnem = 4;
            velocidadEnemY = 4;
            velocidadHiEnem = 6;
            velocidadHiEnemY = 6;
            maxEnemigos = 5;
            destruidos = 0;
            aumentoEnVidaEnemigo = 0;
            ultimoAumento = 0;
            ultimoTEliminar = 0;
            eliminarTodos();
            eliminaAnimaciones();
            EscalaMensaje = escalaInicialMensaje = 1.5f;
            actualFrameMensaje = 0;
            tiempoTitileoMensaje = 0;
            mensajeInicialActivo = true;
            Nave.disparoSuperActivo = true;
        }

        public static new void load(Textura t,SpriteFont f)
        {
            fuente = f;
        }

        public static new void update(GameTime time)
        {
            nave.update(time);
            updateEstrellas(time);
            updateEnemigos(time);
            updateGame(time);
            updateAnimaciones(time);
            for (i = 0; i < botones.Count; i++)
            {
                botones[i].update(time);
            }

            updateMensajeInicial(time);
            barraVidaNave.update(time, Nave.Vida, Nave.MaxVida);
            if (Nave.Vida <= 0) { Animation.animacionesEstallidoNave.update(time); }
            gameOver();
            if (Control.controlActivo)
            {
                control.update(time);
            }

        }

        public static new void draw(SpriteBatch sb)
        {
            //CLEAR NEGRO
            sb.Draw(Textura.TexturasFondo[0], Vector2.Zero, null, Color.Black, 0.0f, Vector2.Zero,
                40.0f, SpriteEffects.None, 0.999f);

            nave.draw(sb);
            drawEstrellas(sb);
            drawEnemigos(sb);
            drawEstadNave(sb);
            drawAnimaciones(sb);
            drawMensajeInicial(sb);
            if (Nave.Vida <= 0) { Animation.animacionesEstallidoNave.draw(sb); }

            for (i = 0; i < botones.Count; i++)
            {
                botones[i].draw(sb);
            }
            if (Control.controlActivo)
            {
                control.draw(sb);
            }
           // drawEstadisticas(sb);
        }

        /******************Estadisticas de la nave**************/
        public static new void drawEstadNave(SpriteBatch sb)
        {
            sb.Draw(Textura.TexturasFondo[1], Vector2.Zero, null,Color.Black, 0.0f, Vector2.Zero, 
                new Vector2(50.0f,2.3f),SpriteEffects.None, 0.401f);

            //** Vida de la nave
            sb.DrawString(fuente,Nave.Vida.ToString(),PositionSalud, Color.White, 0.0f,
                Vector2.Zero, 0.8f, SpriteEffects.None, 0.388f);
            barraVidaNave.draw(sb);
            //****Puntos a vencer
            sb.DrawString(fuente, "Destroyed red:" + destruidos +"/20", positionToBeat, Color.Gold, 0.0f,
                Vector2.Zero, 1.0f, SpriteEffects.None, 0.4f);

            //**Puntos
            sb.DrawString(fuente, "/", PositionLabelPuntos, Color.White, 0.0f,
                Vector2.Zero, 1.3f, SpriteEffects.None, 0.4f);
        }

        public static new void updateGame(GameTime gt)
        {
            if (gt.TotalGameTime.TotalMilliseconds > ultimoTEliminar + 15000)
            {
                eliminarTodosLosEnemigos();
                ultimoTEliminar = gt.TotalGameTime.TotalMilliseconds;
            }

            if (enemigos.Count < maxEnemigos)
            {
                insertaEnemigo();
            }
        }

        public static new void gameOver()
        {
            if (destruidos >= 20)
            {
                if (MediaPlayer.GameHasControl && Audio.musicaActiva)
                {
                    MediaPlayer.IsRepeating = false;
                    MediaPlayer.Play(Audio.getMusica("GameOver"));
                }
                MisionModeGOver.gana = true;

                Almacenamiento.cargarMisiones();
                if (Almacenamiento.getPremiosDados() == 17)
                {
                    Almacenamiento.salvarBonus(20, Almacenamiento.tipoBonus.poderDisp);
                    MisionModeGOver.cosasGanadas = recompensa;
                    Almacenamiento.salvarMisiones();
                }
                else
                {
                    MisionModeGOver.cosasGanadas = Mision1.yaRecompensa;
                }

                MisionMode.estadoActual = MisionMode.Estado.GameOver;
            }

            if (Nave.Vida <= 0)
            {
                Nave.Vida = 0;
                Nave.EscalaN = 0;
                Animation.animacionesEstallidoNave.setPosition(Nave.PositionNav);
                MisionModeGOver.gana = false;
                MisionModeGOver.cosasGanadas = noRecompensa;
                if (Animation.animacionesEstallidoNave.animacionFinalizada)
                {
                    if (MediaPlayer.GameHasControl && Audio.musicaActiva)
                    {
                        MediaPlayer.IsRepeating = false;
                        MediaPlayer.Play(Audio.getMusica("GameOver"));
                    }
                    MisionMode.estadoActual = MisionMode.Estado.GameOver;
                }
            }
        }

        //********Dibujo Mensaje Inicial
        public static new void drawMensajeInicial(SpriteBatch sb)
        {
            if (mensajeInicialActivo)
            {
                sb.DrawString(fuente,tituloMision+" Start!!", new Vector2(40, 320), 
                    Color.LightGreen, 0 ,Vector2.Zero, EscalaMensaje, SpriteEffects.None,0.4f);
            }
        }


        /*Actualiza enemigos*/
        public static new void updateEnemigos(GameTime gt)
        {
            if (enemigos.Count > 0)
            {
                for (i = 0; i < enemigos.Count; i++)
                {
                    enemigos[i].update(gt);

                    if (enemigos[i].estaEnColisionDisp ||
                        enemigos[i].estaEnColisionNave ||
                        enemigos[i].estaEnColisionPoder)
                    {
                        enemigos[i].estaEnColisionDisp = false;
                        enemigos[i].estaEnColisionNave = false;
                        enemigos[i].estaEnColisionPoder = false;

                        if (Animation.animacionesDispAEnemigoTotal.Count > 0)
                        {
                            Animation.animacionesDispAEnemigoTotal.ElementAt(Animation.animacionesDispAEnemigoTotal.Count - 1)
                                .setPosition(enemigos[i].Position);
                            animacionesQ.Add(Animation.animacionesDispAEnemigoTotal[Animation.animacionesDispAEnemigoTotal.Count - 1]);
                            Animation.animacionesDispAEnemigoTotal.RemoveAt(Animation.animacionesDispAEnemigoTotal.Count - 1);
                        }

                        if (enemigos[i].vida <= 0 || enemigos[i].estaEnColisionNave)
                        {
                            (Audio.getEfecto("EstallidoEnemigo")).Play();

                            Nave.Destruidos = Nave.Destruidos + 1;
                            enemigos[i].estaEnColisionNave = false;

                            if (Animation.animacionesEstallidoEnemigoTotal.Count > 0)
                            {
                                Animation.animacionesEstallidoEnemigoTotal.ElementAt(Animation.animacionesEstallidoEnemigoTotal.Count - 1)
                                    .setPosition(enemigos[i].Position);
                                animacionesQ.Add(Animation.animacionesEstallidoEnemigoTotal[Animation.animacionesEstallidoEnemigoTotal.Count - 1]);
                                Animation.animacionesEstallidoEnemigoTotal.RemoveAt(Animation.animacionesEstallidoEnemigoTotal.Count - 1);
                            }

                            switch (enemigos[i].tipoEnemigo)
                            {
                                case Enemigo.tipo.Nor:
                                    confirmaEliminarNor(i);
                                    destruidos++;
                                    break;
                                case Enemigo.tipo.Kami:
                                    EliminarYRetornoEnemigoTotal(i);
                                    Nave.Vida = Nave.Vida - 15;
                                    break;
                                case Enemigo.tipo.Sid:
                                    confirmaEliminarSid(i);
                                    Nave.Vida = Nave.Vida - 15;
                                    break;
                                case Enemigo.tipo.HiNor:
                                    confirmaEliminarNor(i);
                                    destruidos++;
                                    break;
                                case Enemigo.tipo.HiKami:
                                    EliminarYRetornoEnemigoTotal(i);
                                    Nave.Vida = Nave.Vida - 15;
                                    break;
                                case Enemigo.tipo.HiSid:
                                    confirmaEliminarSid(i);
                                    Nave.Vida = Nave.Vida - 15;
                                    break;
                            }

                        }//si murio el enemigo

                    }//si esta wne colision

                }//for mirando cada enemigo

            }//si enemigos count >0

        }

        public static new void insertaEnemigo()
        {
            num = 0;
            num2 = 0;
            num = random.Next(1, 21);

            if (num == 1 || num == 2 || num == 3 || num == 4 || num == 5 || num == 6)
            {
                num2 = random.Next(1, 3);
                if (num2 == 1)
                {
                    if (Enemigo.NorTotal.Count > 0)
                    {
                        Enemigo.NorTotal[Enemigo.NorTotal.Count - 1].reinicia((-1) * random.Next(100, 500),
                                  random.Next(80, 350),
                                  velocidadEnem, 0, aumentoEnVidaEnemigo);
                        enemigos.Add(Enemigo.NorTotal[Enemigo.NorTotal.Count - 1]);
                        Enemigo.NorTotal.RemoveAt(Enemigo.NorTotal.Count - 1);
                    }
                }
                if (num2 == 2)
                {
                    if (Enemigo.NorTotal.Count > 0)
                    {
                        Enemigo.NorTotal[Enemigo.NorTotal.Count - 1].reinicia(random.Next(1000, 1500),
                                 random.Next(80, 350),
                                 velocidadEnem, 0, aumentoEnVidaEnemigo);
                        enemigos.Add(Enemigo.NorTotal[Enemigo.NorTotal.Count - 1]);
                        Enemigo.NorTotal.RemoveAt(Enemigo.NorTotal.Count - 1);
                    }
                }
            }
            if (num == 7 || num == 8 || num == 9 || num == 10 || num == 11)
            {
                num2 = random.Next(1, 3);
                if (num2 == 1)
                {
                    if (Enemigo.KamiTotal.Count > 0)
                    {
                        Enemigo.KamiTotal[Enemigo.KamiTotal.Count - 1].reinicia((-1) * random.Next(100, 500),
                                   random.Next(80, 350),
                                   velocidadEnem, 0, aumentoEnVidaEnemigo);
                        enemigos.Add(Enemigo.KamiTotal[Enemigo.KamiTotal.Count - 1]);
                        Enemigo.KamiTotal.RemoveAt(Enemigo.KamiTotal.Count - 1);
                    }
                }
                if (num2 == 2)
                {
                    if (Enemigo.KamiTotal.Count > 0)
                    {
                        Enemigo.KamiTotal[Enemigo.KamiTotal.Count - 1].reinicia(random.Next(1000, 1500),
                                  random.Next(80, 350),
                                  velocidadEnem, 0, aumentoEnVidaEnemigo);
                        enemigos.Add(Enemigo.KamiTotal[Enemigo.KamiTotal.Count - 1]);
                        Enemigo.KamiTotal.RemoveAt(Enemigo.KamiTotal.Count - 1);
                    }
                }
            }
            if (num == 12 || num == 13 || num == 14 || num == 15)
            {
                num2 = random.Next(1, 3);
                if (num2 == 1)
                {
                    if (Enemigo.SidTotal.Count > 0)
                    {
                        Enemigo.SidTotal[Enemigo.SidTotal.Count - 1].reinicia((-1) * random.Next(100, 500),
                                  random.Next(80, 280),
                                  velocidadEnem, velocidadEnemY, aumentoEnVidaEnemigo);
                        enemigos.Add(Enemigo.SidTotal[Enemigo.SidTotal.Count - 1]);
                        Enemigo.SidTotal.RemoveAt(Enemigo.SidTotal.Count - 1);
                    }
                }
                if (num2 == 2)
                {
                    if (Enemigo.SidTotal.Count > 0)
                    {
                        Enemigo.SidTotal[Enemigo.SidTotal.Count - 1].reinicia(random.Next(1000, 1500),
                                                  random.Next(80, 280),
                                                  velocidadEnem, velocidadEnemY, aumentoEnVidaEnemigo);
                        enemigos.Add(Enemigo.SidTotal[Enemigo.SidTotal.Count - 1]);
                        Enemigo.SidTotal.RemoveAt(Enemigo.SidTotal.Count - 1);
                    }
                }
            }
            if (num == 16 || num == 17)
            {
                num2 = random.Next(1, 3);
                if (num2 == 1)
                {
                    if (Enemigo.HiNorTotal.Count > 0)
                    {
                        Enemigo.HiNorTotal[Enemigo.HiNorTotal.Count - 1].reinicia((-1) * random.Next(100, 500),
                                                random.Next(80, 350),
                                                velocidadHiEnem, 0, aumentoEnVidaEnemigo);
                        enemigos.Add(Enemigo.HiNorTotal[Enemigo.HiNorTotal.Count - 1]);
                        Enemigo.HiNorTotal.RemoveAt(Enemigo.HiNorTotal.Count - 1);
                    }
                }
                if (num2 == 2)
                {
                    if (Enemigo.HiNorTotal.Count > 0)
                    {
                        Enemigo.HiNorTotal[Enemigo.HiNorTotal.Count - 1].reinicia(random.Next(1000, 1500),
                                                 random.Next(80, 350),
                                                 velocidadHiEnem, 0, aumentoEnVidaEnemigo);
                        enemigos.Add(Enemigo.HiNorTotal[Enemigo.HiNorTotal.Count - 1]);
                        Enemigo.HiNorTotal.RemoveAt(Enemigo.HiNorTotal.Count - 1);
                    }
                }
            }
            if (num == 18 || num == 19)
            {
                num2 = random.Next(1, 3);
                if (num2 == 1)
                {
                    if (Enemigo.HiKamiTotal.Count > 0)
                    {
                        Enemigo.HiKamiTotal[Enemigo.HiKamiTotal.Count - 1].reinicia((-1) * random.Next(100, 500),
                                                 random.Next(80, 350),
                                                 velocidadHiEnem, 0, aumentoEnVidaEnemigo);
                        enemigos.Add(Enemigo.HiKamiTotal[Enemigo.HiKamiTotal.Count - 1]);
                        Enemigo.HiKamiTotal.RemoveAt(Enemigo.HiKamiTotal.Count - 1);
                    }
                }
                if (num2 == 2)
                {
                    if (Enemigo.HiKamiTotal.Count > 0)
                    {
                        Enemigo.HiKamiTotal[Enemigo.HiKamiTotal.Count - 1].reinicia(random.Next(1000, 1500),
                                                 random.Next(80, 350),
                                                 velocidadHiEnem, 0, aumentoEnVidaEnemigo);
                        enemigos.Add(Enemigo.HiKamiTotal[Enemigo.HiKamiTotal.Count - 1]);
                        Enemigo.HiKamiTotal.RemoveAt(Enemigo.HiKamiTotal.Count - 1);
                    }
                }
            }
            if (num == 20)
            {
                num2 = random.Next(1, 3);
                if (num2 == 1)
                {
                    if (Enemigo.HiSidTotal.Count > 0)
                    {
                        Enemigo.HiSidTotal[Enemigo.HiSidTotal.Count - 1].reinicia((-1) * random.Next(100, 500),
                                                 random.Next(80, 280),
                                                 velocidadHiEnem, velocidadHiEnemY, aumentoEnVidaEnemigo);
                        enemigos.Add(Enemigo.HiSidTotal[Enemigo.HiSidTotal.Count - 1]);
                        Enemigo.HiSidTotal.RemoveAt(Enemigo.HiSidTotal.Count - 1);
                    }
                }
                if (num2 == 2)
                {
                    if (Enemigo.HiSidTotal.Count > 0)
                    {
                        Enemigo.HiSidTotal[Enemigo.HiSidTotal.Count - 1].reinicia(random.Next(1000, 1500),
                                                  random.Next(80, 280),
                                                  velocidadHiEnem, velocidadHiEnemY, aumentoEnVidaEnemigo);
                        enemigos.Add(Enemigo.HiSidTotal[Enemigo.HiSidTotal.Count - 1]);
                        Enemigo.HiSidTotal.RemoveAt(Enemigo.HiSidTotal.Count - 1);
                    }
                }
            }
        }

    }
}
