using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Galactica
{
    class Plus:GameComun
    {
        private static List<Texture2D> texPlus;
        private int texturaActual;
        public  enum tipo{vida,disparo}
        public tipo tipoPlus;
        public float Rotacion;
        public bool plusActivo;
        public bool muestra;
        private int i;
        public static int recuperacion;

        public Plus()
        {
        }

        public Plus(Vector2 pos)
        {
            Rotacion=0;
            plusActivo = false;
            Escala = 0.4f;
            radioCollition = (int)(75 * Escala);
            setPosition(pos);
            setOrigen(0);
            texturaActual = 0;
        }

        public void reiniciar(Vector2 pos)
        {
            Rotacion = 0;
            setPosition(pos);
            i = random.Next(1, 3);
            if (i==1)
            {
                tipoPlus = tipo.vida;
            }else
            if(i==2)
            {
                tipoPlus = tipo.disparo;
            }
        }

        public static void load(Textura t)
        {
            setTextura(t);
        }

        public void update(GameTime gameTime)
        {
            if (!muestra)
            {
                Position.Y = Position.Y + 8;

                Rotacion = Rotacion = Rotacion + 6;
                if (Rotacion >= 360)
                {
                    Rotacion = 0;
                }

                if (colision.colisionPlusANave(this))
                {
                    if (tipoPlus == tipo.vida)
                    {
                        if (Nave.MaxVida == Nave.Vida)
                        {
                            if (!Nave.disparosDiagActivos && Nave.sePuedeDispDiag)
                            {
                                Nave.disparosDiagActivos = true;
                            }
                            else
                            {
                                if (!Nave.disparoSuperActivo && Nave.sePuedeDispSuper)
                                {
                                    Nave.disparoSuperActivo = true;
                                }
                            }
                        }
                        else
                        {
                            Nave.Vida = Nave.Vida + recuperacion;
                            if (Nave.Vida > Nave.MaxVida)
                            {
                                Nave.Vida = Nave.MaxVida;
                            }
                        }
                    }
                    if (tipoPlus == tipo.disparo)
                    {
                        if (!Nave.disparosDiagActivos && Nave.sePuedeDispDiag && 
                            Nave.Vida > ((Nave.MaxVida * 1) / 8))
                        {
                            Nave.disparosDiagActivos = true;
                        }
                        else
                        {
                            if (!Nave.disparoSuperActivo && Nave.sePuedeDispSuper && 
                                Nave.Vida > ((Nave.MaxVida * 2) / 8))
                            {
                                Nave.disparoSuperActivo = true;
                            }
                            else
                            {
                                Nave.Vida = Nave.Vida + recuperacion;
                                if (Nave.Vida > Nave.MaxVida)
                                {
                                    Nave.Vida = Nave.MaxVida;
                                }
                            }
                        }
                    }
                    plusActivo = false;
                }
            }
        }

        public void draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texPlus[texturaActual],Position,null,Color.White,
                MathHelper.ToRadians(Rotacion),Origen,Escala,SpriteEffects.None,0.3999f);
        }

        /***********************************/

        public static void setTextura(Textura t)
        {
            texPlus = new List<Texture2D>();
            texPlus.Add(t.getTextura("Plus"));
        }

        public void setPosition(Vector2 v)
        {
            Position.X = v.X;
            Position.Y = v.Y;
        }

        public Vector2 getPosition()
        {
            return Position;
        }

        public void setOrigen(int textura)
        {
            Origen.X = texPlus[textura].Width / 2;
            Origen.Y = texPlus[textura].Height / 2;
        }

    }
}
