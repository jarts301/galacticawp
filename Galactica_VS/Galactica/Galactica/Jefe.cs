using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Galactica
{
    class Jefe:GameComun
    {

        /***************Valores importantes**********/
        public enum tipo { Laxo, Esp, Quad, Rued }
        private static List<Texture2D> texJefes;
        private static List<Texture2D> texPartesJefe;
        public static List<Jefe> jefesTotales;
        public List<DisparoJefe> disparos=new List<DisparoJefe>();
        public static List<DisparoJefe> disparosMaxTotal;
        public static List<DisparoJefe> disparosNormalTotal;
        public int vida,maxVida,limiteLlegada;
        public CollitionJefe colisionJefe=new CollitionJefe();
        public int tiempoEsperaDisparo,tiempoEsperaAtaque;
        public static int adicionDanoNave;
        private int i;

        /****** Brazos Esp *******/
        private Vector2 PositionParte1=new Vector2();
        private Vector2 OrigenParte1 = new Vector2();
        private Rectangle TrozoAnimParte1=new Rectangle();
        private Vector2 PositionParte2=new Vector2();
        private Vector2 OrigenParte2=new Vector2();
        private Rectangle TrozoAnimParte2 = new Rectangle();
        private float RotacionParte1, RotacionParte2;
        private float EscalaParte;

        //************Colisiones
        public static int radioCollitionDanoLaxo,radioCollitionLaxo;
        public static int radioCollitionEsp, radioCollitionBrazoEsp;
        public static int radioCollitionQuad, radioCollitionMartilloQuad;
        public static int radioCollitionRued, radioCollitionFlechaRued;
        public static List<int> collitionLaxoX, collitionLaxoY;
        public static List<int> collitionEspX, collitionEspY, collitionBrazoEspX, collitionBrazoEspY;
        public static List<int> collitionQuadX, collitionQuadY,collitionMartilloQuadX, collitionMartilloQuadY;
        public bool estaEnColisionDisp,estaEnColisionPoder;
        public static double rotacionActual;

        //*************Miembros de Jefe*********
        private int texturaAnim, lineaAnimacion;
        public tipo tipoJefe;
        private float Rotacion;
        private double ultimoTDisparoNormal, ultimoTDisparoMax, ultimoTDisparoNormalAux, ultimoTDisparoNormalAux2;
        private double ultimoTParteAtaca, tiempoDetencion;
        private bool modoIndefenso;
        private bool parteAtaca, detenerUnTiempo,devolverParte,parteReposo;
        private int numeroAleatorio;
        private Color colorParte1,colorParte2;
        private int velocidadCambioColor, tonoAzul, tonoVerde;
        private bool llegadaFinalizada;
        private Vector2 PositionParte3=new Vector2();
        private Rectangle TrozoAnimParte=new Rectangle();
        private Color colorParte3;
        private int RotacionParte3;
        private Vector2 OrigenParte=new Vector2();
        private int actualframeParte, lineaAnimacionParte;
        private bool Avance1, Avance2, Avance3, Avance4;

        public Jefe(tipo t, int x, int y)
        {
            tipoJefe = t;
            Position.X = x;
            Position.Y = y;
            estaEnColisionDisp = false;
            estaEnColisionPoder = false;
            tiempoEsperaDisparo = 9000;
            tiempoEsperaAtaque = 8000;

            switch(t)
            {
                case tipo.Laxo:
                       velocidad = 3;
                       vida = maxVida = 20000;
                       Escala = 1.1f;
                       texturaAnim = 0;
                       actualframe = 0;
                       lineaAnimacion = 0;
                       ultimoTDisparoNormal = 0;
                       ultimoTDisparoNormalAux = 0;
                       ultimoTDisparoMax = 0;
                       modoIndefenso = false;
                       radioCollitionLaxo = (int)(160 * Escala);
                       radioCollitionDanoLaxo = (int)(37 * Escala);
                       Rotacion = 0;
                    break;

                case tipo.Esp:
                       velocidad = 3;
                       vida = maxVida = 20000;
                       Escala = 1.2f;
                       texturaAnim = 1;
                       actualframe = 0;
                       lineaAnimacion = 0;
                       ultimoTDisparoNormal = 0;
                       ultimoTDisparoNormalAux = 0;
                       Rotacion = 0;
                       radioCollitionEsp= (int)(42 * Escala);
                       radioCollitionBrazoEsp = (int)(25 * 1.2f);
                       numeroAleatorio = 0;
                       inicializaBrazosEsp();
                    break;

                case tipo.Quad:
                       velocidad = 3;
                       vida = maxVida = 30000;
                       Escala = 1.1f;
                       texturaAnim = 2;
                       actualframe = 0;
                       lineaAnimacion = 0;
                       ultimoTDisparoNormal = 0;
                       ultimoTDisparoNormalAux = 0;
                       ultimoTDisparoNormalAux2 = 0;
                       Rotacion = 0;
                       radioCollitionQuad= (int)(50 * Escala);
                       radioCollitionMartilloQuad = (int)(50 * 1.2f);
                       inicializaMartillosQuad();
                    break;

                case tipo.Rued:
                       velocidad = 3;
                       limiteLlegada = -50;
                       vida =maxVida= 50000;
                       Escala = 1.1f;
                       Avance1 = true;
                       Avance2 = Avance3 = Avance4 = false;
                       texturaAnim = 3;
                       actualframe = 0;
                       lineaAnimacion = 0;
                       ultimoTDisparoNormal = 0;
                       Rotacion = 0;
                       radioCollitionRued= (int)(200 * Escala);
                       radioCollitionFlechaRued = (int)(94 * 0.5f);
                       inicializaFlechasRued();
                    break;
            }
        }


        public void reinicia(int x, int y,int aumentoVida)
        {
            Position.X = x;
            Position.Y = y;
            estaEnColisionDisp = false;
            estaEnColisionPoder = false;
            tiempoEsperaDisparo = 9000;
            tiempoEsperaAtaque = 8000;

            switch (tipoJefe)
            {
                case tipo.Laxo:
                    velocidad = 3;
                    vida = maxVida = 20000;
                    Escala = 1.1f;
                    texturaAnim = 0;
                    actualframe = 0;
                    lineaAnimacion = 0;
                    ultimoTDisparoNormal = 0;
                    ultimoTDisparoNormalAux = 0;
                    ultimoTDisparoMax = 0;
                    modoIndefenso = false;
                    radioCollitionLaxo = (int)(160 * Escala);
                    radioCollitionDanoLaxo = (int)(37 * Escala);
                    Rotacion = 0;
                    break;

                case tipo.Esp:
                    velocidad = 3;
                    vida = maxVida = 20000;
                    Escala = 1.2f;
                    texturaAnim = 1;
                    actualframe = 0;
                    lineaAnimacion = 0;
                    ultimoTDisparoNormal = 0;
                    ultimoTDisparoNormalAux = 0;
                    Rotacion = 0;
                    radioCollitionEsp = (int)(42 * Escala);
                    radioCollitionBrazoEsp = (int)(25 * 1.2f);
                    numeroAleatorio = 0;
                    inicializaBrazosEsp();
                    break;

                case tipo.Quad:
                    velocidad = 3;
                    vida = maxVida = 30000;
                    Escala = 1.1f;
                    texturaAnim = 2;
                    actualframe = 0;
                    lineaAnimacion = 0;
                    ultimoTDisparoNormal = 0;
                    ultimoTDisparoNormalAux = 0;
                    ultimoTDisparoNormalAux2 = 0;
                    Rotacion = 0;
                    radioCollitionQuad = (int)(50 * Escala);
                    radioCollitionMartilloQuad = (int)(50 * 1.2f);
                    inicializaMartillosQuad();
                    break;

                case tipo.Rued:
                    velocidad = 3;
                    limiteLlegada = -50;
                    vida = maxVida = 50000;
                    Escala = 1.1f;
                    Avance1 = true;
                    Avance2 = Avance3 = Avance4 = false;
                    texturaAnim = 3;
                    actualframe = 0;
                    lineaAnimacion = 0;
                    ultimoTDisparoNormal = 0;
                    Rotacion = 0;
                    radioCollitionRued = (int)(200 * Escala);
                    radioCollitionFlechaRued = (int)(94 * 0.5f);
                    inicializaFlechasRued();
                    break;
            }
            setAumentoVida(aumentoVida);
        }


        public static void load(Textura t)
        {
            adicionDanoNave = 0;
            setTextura(t);
            cargaListaDisparos();
            defineCollitionBox();
            jefesTotales = new List<Jefe>();
            jefesTotales.Add(new Jefe(Jefe.tipo.Laxo, 240, -800));
            jefesTotales.Add(new Jefe(Jefe.tipo.Esp, 240, -800));
            jefesTotales.Add(new Jefe(Jefe.tipo.Quad, 240, -800));
            jefesTotales.Add(new Jefe(Jefe.tipo.Rued, 240, -800));
        }

        //metodo update de enemigo
        public void update(GameTime gameTime)
        {
            defineDatosAnimacion(gameTime);
            velocidadAnimacion(gameTime);
            updateDisparos(gameTime);
            updateAnimaciones(gameTime);
            updateTiemposEspera();
        }

        //metodo draw de enemigo
        public void draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texJefes[texturaAnim], Position, TrozoAnim, Color.White,
                    MathHelper.ToRadians(Rotacion), Origen, Escala, SpriteEffects.None, 0.44f);
            drawDisparosLaxo(spriteBatch);
            drawAnimaciones(spriteBatch);
            drawBrazosEsp(spriteBatch);
            drawMartillosQuad(spriteBatch);
            drawFlechasRued(spriteBatch);
        }

        /*******************Definicion de metodos agregados*********************/

        public void setAumentoVida(int v)
        {
            vida = vida + v;
            maxVida = maxVida + v;
        }

        //velocidad de la animacion
        public void velocidadAnimacion(GameTime gameTime)
        {
            dato += gameTime.ElapsedGameTime;
            if (dato.TotalMilliseconds >=
                ((gameTime.ElapsedGameTime.Milliseconds) * velocidad))
            {
                actualframe = actualframe + 1;
                dato = TimeSpan.Zero;
            }

            if (actualframe >= 5)
            {
                actualframe = 0;
                lineaAnimacion++;
                if (tipoJefe != tipo.Laxo)
                {
                    if (lineaAnimacion >= 2) { lineaAnimacion = 0; }
                }
                else
                {
                    if (modoIndefenso)
                    {
                        if (lineaAnimacion >= 4) { lineaAnimacion = 2; }
                    }
                    else
                    {
                        if (lineaAnimacion >= 2) { lineaAnimacion = 0; }
                    }
                }
            }
        }

        //Cargar lista disparos
        public static void cargaListaDisparos()
        {
               disparosMaxTotal=new List<DisparoJefe>();
               for(int i=0;i<50;i++)
               {
                   disparosMaxTotal.Add(new DisparoJefe(Vector2.Zero,DisparoJefe.tipo.maximo));
               }
               
               disparosNormalTotal=new List<DisparoJefe>();
               for (int i = 0; i < 100; i++)
               {
                   disparosNormalTotal.Add(new DisparoJefe(Vector2.Zero, DisparoJefe.tipo.normal));
               }
        }

        //update disparos laxo
        public void updateDisparos(GameTime gt)
        {
            if (disparos.Count > 0)
            {
                for (i = 0; i < disparos.Count; i++)
                {
                    disparos[i].update(gt);
                }

                for (i = 0; i < disparos.Count; i++)
                {
                    if (disparos[i].estaEnColision)
                    {
                        disparos[i].estaEnColision = false;

                        if (Animation.animacionesDispANaveTotal.Count > 0)
                        {
                            Animation.animacionesDispANaveTotal.ElementAt(Animation.animacionesDispANaveTotal.Count - 1)
                                .setPosition(disparos[i].getPosition());
                            animaciones.Add(Animation.animacionesDispANaveTotal[Animation.animacionesDispANaveTotal.Count - 1]);
                            Animation.animacionesDispANaveTotal.RemoveAt(Animation.animacionesDispANaveTotal.Count - 1);
                        }

                        if (disparos[i].tipoActual.Equals(DisparoJefe.tipo.maximo))
                        {
                            disparos[i].postDisparo = false;
                            disparosMaxTotal.Add(disparos[i]);
                            disparos.Remove(disparos[i]);
                        }
                        else
                        {
                            disparos[i].postDisparo = false;
                            disparosNormalTotal.Add(disparos[i]);
                            disparos.Remove(disparos[i]);
                        }
                    }
                }

                for (i = 0; i < disparos.Count; i++)
                {
                    if (disparos[i].getPosition().X > 480 ||
                     disparos[i].getPosition().X < 0 ||
                     disparos[i].getPosition().Y < 0 ||
                     disparos[i].getPosition().Y > 800)
                    {
                        if (disparos[i].tipoActual.Equals(DisparoJefe.tipo.maximo))
                        {
                            disparos[i].postDisparo = false;
                            disparosMaxTotal.Add(disparos[i]);
                            disparos.Remove(disparos[i]);
                        }
                        else
                        {
                            disparos[i].postDisparo = false;
                            disparosNormalTotal.Add(disparos[i]);
                            disparos.Remove(disparos[i]);
                        }
                    }
                }//End for
            }//End if
        }

        //Draw disparos Laxo
        public void drawDisparosLaxo(SpriteBatch sb)
        {
            for (i = 0; i < disparos.Count; i++)
            {
                disparos[i].draw(sb);
            }
        }

        //define los datos de cada enemigo
        public void defineDatosAnimacion(GameTime gameTime)
        {
            switch(tipoJefe)
            {
                case tipo.Laxo:
                         defineTrozoAnim(actualframe*400, lineaAnimacion*400, 400, 400);
                         setOrigen();
                         movimientoLaxo(gameTime);
                    break;

                case tipo.Esp:
                         defineTrozoAnim(actualframe * 400, lineaAnimacion * 400, 400, 400);
                         setOrigen();
                         movimientoEsp(gameTime);
                    break;

                case tipo.Quad:
                         defineTrozoAnim(actualframe * 400, lineaAnimacion * 311, 400, 311);
                         setOrigen();
                         movimientoQuad(gameTime);
                    break;

                case tipo.Rued:
                         defineTrozoAnim(actualframe * 400, lineaAnimacion * 400, 400, 400);
                         setOrigen();
                         movimientoRued(gameTime);
                    break;
            }
        }

        //Define movimiento de Lax
        public void movimientoLaxo(GameTime gt)
        {
            if (Position.Y <= 100)
            {
                Position.Y = Position.Y + 7;
            }

            if (gt.TotalGameTime.TotalMilliseconds > ultimoTDisparoNormal + tiempoEsperaDisparo)
            {
                if (disparosNormalTotal.Count > 0)
                {
                    disparosNormalTotal.ElementAt(disparosNormalTotal.Count - 1).setPosition(Position, true);
                    disparosNormalTotal.ElementAt(disparosNormalTotal.Count - 1).Disparar = true;
                    disparos.Add(disparosNormalTotal.ElementAt(disparosNormalTotal.Count - 1));
                    disparosNormalTotal.Remove(disparosNormalTotal.ElementAt(disparosNormalTotal.Count - 1));
                }

                ultimoTDisparoNormal = gt.TotalGameTime.TotalMilliseconds;
            }

            if (gt.TotalGameTime.TotalMilliseconds > ultimoTDisparoNormalAux + 
                (tiempoEsperaDisparo-1000))
            {
                if (disparosNormalTotal.Count > 0)
                {
                    disparosNormalTotal.ElementAt(disparosNormalTotal.Count - 1).setPosition(Position, false);
                    disparosNormalTotal.ElementAt(disparosNormalTotal.Count - 1).Disparar = true;
                    disparos.Add(disparosNormalTotal.ElementAt(disparosNormalTotal.Count - 1));
                    disparosNormalTotal.Remove(disparosNormalTotal.ElementAt(disparosNormalTotal.Count - 1));
                }
                    ultimoTDisparoNormalAux = gt.TotalGameTime.TotalMilliseconds;
            }

            if (gt.TotalGameTime.TotalMilliseconds > ultimoTDisparoMax + 6000)
            {
                if (disparosMaxTotal.Count > 0)
                {
                    disparosMaxTotal.ElementAt(disparosMaxTotal.Count - 1).setPosition(Position);
                    disparosMaxTotal.ElementAt(disparosMaxTotal.Count - 1).Disparar = true;
                    disparos.Add(disparosMaxTotal.ElementAt(disparosMaxTotal.Count - 1));
                    disparosMaxTotal.Remove(disparosMaxTotal.ElementAt(disparosMaxTotal.Count - 1));
                }

                if (modoIndefenso)
                {
                    modoIndefenso = false;
                }
                else
                {
                    lineaAnimacion = 2;
                    modoIndefenso = true;
                }
                ultimoTDisparoMax = gt.TotalGameTime.TotalMilliseconds;
            }

            confirmaColisionesLaxo();
        }

        /*****Movimiento Esp ******/
        public void movimientoEsp(GameTime gt)
        {
            if (Position.Y <= 130)
            {
                Position.Y = Position.Y + 10;
            }
            else
            {
                if (llegadaFinalizada)
                {
                    ultimoTParteAtaca = gt.TotalGameTime.TotalMilliseconds;
                    llegadaFinalizada = false;
                }
            }

            if (gt.TotalGameTime.TotalMilliseconds > ultimoTDisparoNormal + tiempoEsperaDisparo)
            {
                if (disparosNormalTotal.Count > 0)
                {
                    disparosNormalTotal.ElementAt(disparosNormalTotal.Count - 1).setPosition(Position, true);
                    disparosNormalTotal.ElementAt(disparosNormalTotal.Count - 1).Disparar = true;
                    disparos.Add(disparosNormalTotal.ElementAt(disparosNormalTotal.Count - 1));
                    disparosNormalTotal.Remove(disparosNormalTotal.ElementAt(disparosNormalTotal.Count - 1));
                }

                ultimoTDisparoNormal = gt.TotalGameTime.TotalMilliseconds;
            }

            if (gt.TotalGameTime.TotalMilliseconds > ultimoTDisparoNormalAux + 
                (tiempoEsperaDisparo-1000))
            {
                if (disparosNormalTotal.Count > 0)
                {
                    disparosNormalTotal.ElementAt(disparosNormalTotal.Count - 1).setPosition(Position, false);
                    disparosNormalTotal.ElementAt(disparosNormalTotal.Count - 1).Disparar = true;
                    disparos.Add(disparosNormalTotal.ElementAt(disparosNormalTotal.Count - 1));
                    disparosNormalTotal.Remove(disparosNormalTotal.ElementAt(disparosNormalTotal.Count - 1));
                }

                ultimoTDisparoNormalAux = gt.TotalGameTime.TotalMilliseconds;
            }

            updateBrazosEsp(gt);
            confirmaColisionesEsp();
        }

        //Movimeinto Quad
        public void movimientoQuad(GameTime gt)
        {
            if (Position.Y <= 100)
            {
                Position.Y = Position.Y + 10;
            }
            else
            {
                if (llegadaFinalizada)
                {
                    ultimoTParteAtaca = gt.TotalGameTime.TotalMilliseconds;
                    llegadaFinalizada = false;
                }
            }

            /*if (gt.TotalGameTime.TotalMilliseconds > ultimoTDisparoNormal + 5000)
            {
                disparosNormalTotal.ElementAt(0).setPosition(Position, true);
                disparosNormalTotal.ElementAt(0).Disparar = true;
                disparos.Add(disparosNormalTotal.ElementAt(0));
                disparosNormalTotal.Remove(disparosNormalTotal.ElementAt(0));

                ultimoTDisparoNormal = gt.TotalGameTime.TotalMilliseconds;
            }

            if (gt.TotalGameTime.TotalMilliseconds > ultimoTDisparoNormalAux + 4000)
            {
                disparosNormalTotal.ElementAt(0).setPosition(Position, false);
                disparosNormalTotal.ElementAt(0).Disparar = true;
                disparos.Add(disparosNormalTotal.ElementAt(0));
                disparosNormalTotal.Remove(disparosNormalTotal.ElementAt(0));

                ultimoTDisparoNormalAux = gt.TotalGameTime.TotalMilliseconds;
            }*/

            if (gt.TotalGameTime.TotalMilliseconds > ultimoTDisparoNormalAux2 +
                (tiempoEsperaDisparo-1000))
            {
                if (disparosNormalTotal.Count > 0)
                {
                    disparosNormalTotal.ElementAt(disparosNormalTotal.Count - 1).setPosition(Position);
                    disparosNormalTotal.ElementAt(disparosNormalTotal.Count - 1).Disparar = true;
                    disparos.Add(disparosNormalTotal.ElementAt(disparosNormalTotal.Count - 1));
                    disparosNormalTotal.Remove(disparosNormalTotal.ElementAt(disparosNormalTotal.Count - 1));
                }

                ultimoTDisparoNormalAux2 = gt.TotalGameTime.TotalMilliseconds;
            }

            updateMartillosQuad(gt);
            confirmaColisionesQuad();
        }

        //Movimiento Rued
        public void movimientoRued(GameTime gt)
        {
            if (vida <= ((maxVida * 3) / 4) && Avance1)
            { limiteLlegada = limiteLlegada + 150; Avance1 = false; Avance2 = true; }
            if (vida <= ((maxVida * 2) / 4) && Avance2)
            { limiteLlegada = limiteLlegada + 150; Avance2 = false; Avance3 = true; }
            if (vida <= ((maxVida * 1) / 4) && Avance3)
            { limiteLlegada = limiteLlegada + 100; Avance3 = false; Avance4 = true; }
            if (vida <= 100 && Avance4)
            { limiteLlegada = limiteLlegada + 80; Avance4 = false; }

            if(Position.Y <= limiteLlegada)
            {
                if (vida > 100) { Position.Y = Position.Y + 10; }
                else { Position.Y = Position.Y + 1; }
            }
            else
            {
                if (llegadaFinalizada)
                {
                    ultimoTParteAtaca = gt.TotalGameTime.TotalMilliseconds;
                    llegadaFinalizada = false;
                }
            }

            if (gt.TotalGameTime.TotalMilliseconds > ultimoTDisparoNormalAux2 + 
                (tiempoEsperaDisparo-1000))
            {
                if (disparosNormalTotal.Count > 0)
                {
                    disparosNormalTotal.ElementAt(disparosNormalTotal.Count - 1).setPosition(Position);
                    disparosNormalTotal.ElementAt(disparosNormalTotal.Count - 1).Disparar = true;
                    disparos.Add(disparosNormalTotal.ElementAt(disparosNormalTotal.Count - 1));
                    disparosNormalTotal.Remove(disparosNormalTotal.ElementAt(disparosNormalTotal.Count - 1));
                }

                ultimoTDisparoNormalAux2 = gt.TotalGameTime.TotalMilliseconds;
            }

            updateFlechasRued(gt);
        }

        /* ********* Update Animaciones ************* */

        public void updateAnimaciones(GameTime gt)
        {
            if (animaciones.Count > 0)
            {
                for (i = 0; i < animaciones.Count; i++)
                {
                    animaciones[i].update(gt);
                }
            }
            if (animaciones.Count > 0)
            {
                for (i = 0; i < animaciones.Count; i++)
                {
                    if (animaciones[i].animacionFinalizada &&
                        animaciones[i].tipoActual==Animation.tipoAnimacion.DisparoANave)
                    {
                        animaciones[i].animacionFinalizada = false;
                        Animation.animacionesDispANaveTotal.Add(animaciones[i]);
                        animaciones.Remove(animaciones[i]);
                    }
                }
            }
        }

        /***************** Draw animaciones *********** */

        public void drawAnimaciones(SpriteBatch sb)
        {
            if (animaciones.Count > 0)
            {
                for (i = 0; i < animaciones.Count; i++)
                {
                    animaciones[i].draw(sb);
                }
            }
        }

        //define el cuandro de la animacion actual
        public void defineTrozoAnim(int x, int y, int width, int height)
        {
            TrozoAnim.X = x;
            TrozoAnim.Y = y;
            TrozoAnim.Width = width;
            TrozoAnim.Height = height;
        }

        public void updateTiemposEspera()
        {
            if (vida <= ((maxVida * 6) / 8) && vida > ((maxVida * 5) / 8))
            { tiempoEsperaDisparo = 7000; tiempoEsperaAtaque = 6000; }
            if (vida <= ((maxVida * 5) / 8) && vida > ((maxVida * 2) / 8))
            { tiempoEsperaDisparo = 5000; tiempoEsperaAtaque = 5000; }
            if (vida <= ((maxVida * 2) / 8) && vida > ((maxVida * 1) / 8))
            { tiempoEsperaDisparo = 3000; tiempoEsperaAtaque = 4000; }
            if (vida <= ((maxVida * 1) / 8))
            { tiempoEsperaDisparo = 2000; tiempoEsperaAtaque = 3000; }
        }

        //Inicializa Brazos Esp
        public void inicializaBrazosEsp()
        {
            EscalaParte = 1.1f;
            ultimoTParteAtaca = 0;
            parteAtaca = false;
            detenerUnTiempo = false;
            tiempoDetencion = 0;
            devolverParte = false;
            parteReposo = true;
            colorParte1 = Color.White;
            colorParte2 = Color.White;
            velocidadCambioColor = 35;
            tonoAzul = 101;
            tonoVerde = 101;
            numeroAleatorio = random.Next(1, 4);
            llegadaFinalizada = true;

            PositionParte1.X=-500;PositionParte1.Y=190;
            TrozoAnimParte1.X=0;TrozoAnimParte1.Y=0;TrozoAnimParte1.Width=500;TrozoAnimParte1.Height=62;
            OrigenParte1.X=10;OrigenParte1.Y=31;
            RotacionParte1 = 50;

            PositionParte2.X=1000;PositionParte2.Y=190;
            TrozoAnimParte2.X = 0; TrozoAnimParte2.Y = 62; TrozoAnimParte2.Width = 500; TrozoAnimParte2.Height = 62;
            OrigenParte2.X=490;OrigenParte2.Y=31;
            RotacionParte2 = -50;
        }

        // Inicializa Martillos Quad
        public void inicializaMartillosQuad()
        {
            EscalaParte = 1.2f;
            ultimoTParteAtaca = 0;
            parteAtaca = false;
            detenerUnTiempo = false;
            tiempoDetencion = 0;
            devolverParte = false;
            parteReposo = true;
            colorParte1 = colorParte2 = colorParte3 = Color.White;
            velocidadCambioColor = 35;
            tonoAzul = 101;
            tonoVerde = 101;
            numeroAleatorio = 0;
            llegadaFinalizada = true;

            PositionParte1.X = 800; PositionParte1.Y = 380;
            PositionParte2.X = 800; PositionParte2.Y = 550;
            PositionParte3.X = 800; PositionParte3.Y = 720;

            RotacionParte1 = 0; RotacionParte2 = 0; RotacionParte3 = 0;
            actualframeParte = 0; lineaAnimacionParte = 0;
        }

        // Inicializa Flechas Rued
        public void inicializaFlechasRued()
        {
            EscalaParte = 0.5f;
            velocidadCambioColor = 35;
            tonoAzul = 101;
            tonoVerde = 101;
            numeroAleatorio = random.Next(1, 4);
            llegadaFinalizada = true;
            PositionParte1.X = 70; PositionParte1.Y = Position.Y + 250;
            PositionParte2.X = 240; PositionParte2.Y = Position.Y + 250;
            PositionParte3.X = 410; PositionParte3.Y = Position.Y + 250;
            colorParte1 = colorParte2 = colorParte3 = Color.White;
            RotacionParte1 = 180; RotacionParte2 = 180; RotacionParte3 = 180;
            actualframeParte = 0; lineaAnimacionParte = 0;
            parteAtaca = false;
            parteReposo = true;
            devolverParte = false;
            detenerUnTiempo = false;
            ultimoTParteAtaca = 0;
            tiempoDetencion = 0;
        }

        //Actualiza brazos de Esp
        public void updateBrazosEsp(GameTime gt)
        {
            if(PositionParte1.X<0){PositionParte1.X = PositionParte1.X + 3;}
            if (PositionParte2.X > 480){PositionParte2.X = PositionParte2.X - 3;}
            if (PositionParte1.X >= 0 && PositionParte2.X <= 480 && parteReposo)
            {
                if (((ultimoTParteAtaca + tiempoEsperaAtaque) - gt.TotalGameTime.TotalMilliseconds) > 2000)
                {
                    numeroAleatorio = random.Next(1, 4);
                }

                if ((ultimoTParteAtaca + tiempoEsperaAtaque) - gt.TotalGameTime.TotalMilliseconds <= 2000)
                {
                    tonoAzul = tonoAzul + velocidadCambioColor;
                    tonoVerde = tonoVerde + velocidadCambioColor;

                    if (tonoAzul <= 100 || tonoAzul >= 235)
                    {
                        velocidadCambioColor = velocidadCambioColor * (-1);
                    }

                    switch (numeroAleatorio)
                    {
                        case 1:
                              colorParte1.B = (byte)tonoAzul;
                              colorParte1.G = (byte)tonoVerde;
                            break;
                        case 2:
                              colorParte2.B = (byte)tonoAzul;
                              colorParte2.G = (byte)tonoVerde;
                            break;
                        case 3:
                              colorParte1.B = (byte)tonoAzul;
                              colorParte1.G = (byte)tonoVerde;
                              colorParte2.B = (byte)tonoAzul;
                              colorParte2.G = (byte)tonoVerde;
                            break;
                    }
                }

                if (gt.TotalGameTime.TotalMilliseconds > (ultimoTParteAtaca + tiempoEsperaAtaque))
                {
                    parteAtaca = true;
                    parteReposo = false;
                    colorParte1.B = (byte)254;
                    colorParte1.G = (byte)254;
                    colorParte2.B = (byte)254;
                    colorParte2.G = (byte)254;
                }
            }

            if (parteAtaca)
            {
                switch (numeroAleatorio)
                {
                    case 1:
                        if (RotacionParte1 < 90){RotacionParte1 = RotacionParte1 + 5;
                        }else{
                            detenerUnTiempo=true;
                            parteAtaca = false;
                            tiempoDetencion = gt.TotalGameTime.TotalMilliseconds;
                        }
                    break;

                    case 2:
                       if (RotacionParte2 > (-90)){RotacionParte2 = RotacionParte2 - 5;
                       }else{
                            detenerUnTiempo=true;
                            parteAtaca = false;
                            tiempoDetencion = gt.TotalGameTime.TotalMilliseconds;
                        }
                    break;

                    case 3:
                    if (RotacionParte1 < 90 && RotacionParte2 > (-90)){
                           RotacionParte1 = RotacionParte1 + 5;
                           RotacionParte2 = RotacionParte2 - 5;
                       }else{
                            detenerUnTiempo=true;
                            parteAtaca = false;
                            tiempoDetencion = gt.TotalGameTime.TotalMilliseconds;
                       }
                    break;
                }
            }

            if (detenerUnTiempo)
            {
                if (gt.TotalGameTime.TotalMilliseconds > tiempoDetencion + 5000)
                {
                    devolverParte = true;
                    detenerUnTiempo=false;
                }
            }

            if (devolverParte)
            {
                if (RotacionParte1 > 50)
                {
                    RotacionParte1 = RotacionParte1 - 0.3f;
                }
                if (RotacionParte2 < -50)
                {
                    RotacionParte2 = RotacionParte2 + 0.3f;
                }
                if(RotacionParte1<=50 && RotacionParte2>=(-50))
                {
                    devolverParte = false;
                    parteReposo = true;
                    ultimoTParteAtaca = gt.TotalGameTime.TotalMilliseconds;
                }
            }
        }

        //Actualiza martillos Quad
        public void updateMartillosQuad(GameTime gt)
        {
            if (PositionParte1.X > 430){PositionParte1.X = PositionParte1.X - 3;}
            if (PositionParte2.X > 430) { PositionParte2.X = PositionParte2.X - 3; }
            if (PositionParte3.X > 430) { PositionParte3.X = PositionParte3.X - 3; }
            defineTrozoAnimMartillo(actualframeParte*300,lineaAnimacionParte*122,300,122);
            setOrigenMartillo();

            if (PositionParte1.X <= 430 && PositionParte2.X <= 430 &&
                PositionParte3.X <= 430 && parteReposo)
            {
                if (((ultimoTParteAtaca + tiempoEsperaAtaque) - gt.TotalGameTime.TotalMilliseconds) > 2000)
                {
                    numeroAleatorio = random.Next(1, 7);
                }

                if ((ultimoTParteAtaca + tiempoEsperaAtaque) - gt.TotalGameTime.TotalMilliseconds <= 2000)
                {
                    tonoAzul = tonoAzul + velocidadCambioColor;
                    tonoVerde = tonoVerde + velocidadCambioColor;

                    if (tonoAzul <= 100 || tonoAzul >= 235)
                    {
                        velocidadCambioColor = velocidadCambioColor * (-1);
                    }

                    switch (numeroAleatorio)
                    {
                        case 1:
                            colorParte1.B = (byte)tonoAzul;
                            colorParte1.G = (byte)tonoVerde;
                            break;
                        case 2:
                            colorParte2.B = (byte)tonoAzul;
                            colorParte2.G = (byte)tonoVerde;
                            break;
                        case 3:
                            colorParte3.B = (byte)tonoAzul;
                            colorParte3.G = (byte)tonoVerde;
                            break;
                        case 4:
                            colorParte1.B = (byte)tonoAzul;
                            colorParte1.G = (byte)tonoVerde;
                            colorParte2.B = (byte)tonoAzul;
                            colorParte2.G = (byte)tonoVerde;
                            break;
                        case 5:
                            colorParte1.B = (byte)tonoAzul;
                            colorParte1.G = (byte)tonoVerde;
                            colorParte3.B = (byte)tonoAzul;
                            colorParte3.G = (byte)tonoVerde;
                            break;
                        case 6:
                            colorParte2.B = (byte)tonoAzul;
                            colorParte2.G = (byte)tonoVerde;
                            colorParte3.B = (byte)tonoAzul;
                            colorParte3.G = (byte)tonoVerde;
                            break;
                    }
                }

                if (gt.TotalGameTime.TotalMilliseconds > (ultimoTParteAtaca + tiempoEsperaAtaque))
                {
                    parteAtaca = true;
                    parteReposo = false;
                    colorParte1.B = (byte)254;
                    colorParte1.G = (byte)254;
                    colorParte2.B = (byte)254;
                    colorParte2.G = (byte)254;
                    colorParte3.B = (byte)254;
                    colorParte3.G = (byte)254;
                }
            }
            
            if (parteAtaca)
            {
                switch (numeroAleatorio)
                {
                    case 1:
                        if (PositionParte1.X >0) { PositionParte1.X = PositionParte1.X - 20; }
                        else{detenerUnTiempo = true;parteAtaca = false;
                            tiempoDetencion = gt.TotalGameTime.TotalMilliseconds;}
                        break;

                    case 2:
                        if (PositionParte2.X > 0){PositionParte2.X = PositionParte2.X - 20;
                        }else{detenerUnTiempo = true; parteAtaca = false;
                            tiempoDetencion = gt.TotalGameTime.TotalMilliseconds;}
                        break;

                    case 3:
                        if (PositionParte3.X > 0) { PositionParte3.X = PositionParte3.X - 20; }
                        else{detenerUnTiempo = true;parteAtaca = false;
                            tiempoDetencion = gt.TotalGameTime.TotalMilliseconds;}
                        break;

                    case 4:
                        if (PositionParte1.X > 0 && PositionParte2.X > 0){ 
                            PositionParte1.X = PositionParte1.X - 20;
                            PositionParte2.X = PositionParte2.X - 20;}
                        else{detenerUnTiempo = true; parteAtaca = false;
                            tiempoDetencion = gt.TotalGameTime.TotalMilliseconds;}
                        break;

                    case 5:
                        if (PositionParte1.X > 0 && PositionParte3.X > 0){ 
                            PositionParte1.X = PositionParte1.X - 20;
                            PositionParte3.X = PositionParte3.X - 20;}
                        else{detenerUnTiempo = true; parteAtaca = false;
                            tiempoDetencion = gt.TotalGameTime.TotalMilliseconds;}
                        break;

                    case 6:
                        if (PositionParte3.X > 0 && PositionParte2.X > 0){ 
                            PositionParte3.X = PositionParte3.X - 20;
                            PositionParte2.X = PositionParte2.X - 20;}
                        else{detenerUnTiempo = true; parteAtaca = false;
                            tiempoDetencion = gt.TotalGameTime.TotalMilliseconds;}
                        break;
                }
            }

            if (detenerUnTiempo)
            {
                if (gt.TotalGameTime.TotalMilliseconds > tiempoDetencion + 4000)
                {
                    devolverParte = true;
                    detenerUnTiempo = false;
                }
            }

            if (devolverParte)
            {
                if (PositionParte1.X < 430) { PositionParte1.X = PositionParte1.X + 10; }
                if (PositionParte2.X < 430){PositionParte2.X = PositionParte2.X + 10;}
                if (PositionParte3.X < 430) { PositionParte3.X = PositionParte3.X + 10; }
                if (PositionParte1.X >= 430 && PositionParte2.X >= 430 && PositionParte3.X >= 430)
                {
                    devolverParte = false;
                    parteReposo = true;
                    ultimoTParteAtaca = gt.TotalGameTime.TotalMilliseconds;
                }
            }

            velocidadAnimacionMartillo(gt);
        }


        //Actualiza flechas Rued
        public void updateFlechasRued(GameTime gt)
        {
            if (PositionParte1.Y < Position.Y + 250) 
            {
                if (vida > 100){PositionParte1.Y = PositionParte1.Y + 10;}
                else { PositionParte1.Y = PositionParte1.Y + 1; }
            }
            if (PositionParte2.Y < Position.Y + 250) 
            {
                if (vida > 100) { PositionParte2.Y = PositionParte2.Y + 10; }
                else { PositionParte2.Y = PositionParte2.Y + 1; }
            }
            if (PositionParte3.Y < Position.Y + 250) 
            {
                if (vida > 100) { PositionParte3.Y = PositionParte3.Y + 10; }
                else { PositionParte3.Y = PositionParte3.Y + 1; }
            }

            defineTrozoAnimMartillo(actualframeParte * 200, 0, 200, 188);
            setOrigenFlecha();

            if (PositionParte1.Y >= Position.Y + 250 && PositionParte2.Y >= Position.Y + 250 &&
                PositionParte3.Y >= Position.Y + 250 && parteReposo)
            {
                if (((ultimoTParteAtaca + tiempoEsperaAtaque) - gt.TotalGameTime.TotalMilliseconds) > 2000)
                {
                    numeroAleatorio = random.Next(1, 7);
                }

                if ((ultimoTParteAtaca + tiempoEsperaAtaque) - gt.TotalGameTime.TotalMilliseconds <= 2000)
                {
                    tonoAzul = tonoAzul + velocidadCambioColor;
                    tonoVerde = tonoVerde + velocidadCambioColor;

                    if (tonoAzul <= 100 || tonoAzul >= 235)
                    {
                        velocidadCambioColor = velocidadCambioColor * (-1);
                    }

                    switch (numeroAleatorio)
                    {
                        case 1:
                            colorParte1.B = (byte)tonoAzul;
                            colorParte1.G = (byte)tonoVerde;
                            break;
                        case 2:
                            colorParte2.B = (byte)tonoAzul;
                            colorParte2.G = (byte)tonoVerde;
                            break;
                        case 3:
                            colorParte3.B = (byte)tonoAzul;
                            colorParte3.G = (byte)tonoVerde;
                            break;
                        case 4:
                            colorParte1.B = (byte)tonoAzul;
                            colorParte1.G = (byte)tonoVerde;
                            colorParte2.B = (byte)tonoAzul;
                            colorParte2.G = (byte)tonoVerde;
                            break;
                        case 5:
                            colorParte1.B = (byte)tonoAzul;
                            colorParte1.G = (byte)tonoVerde;
                            colorParte3.B = (byte)tonoAzul;
                            colorParte3.G = (byte)tonoVerde;
                            break;
                        case 6:
                            colorParte2.B = (byte)tonoAzul;
                            colorParte2.G = (byte)tonoVerde;
                            colorParte3.B = (byte)tonoAzul;
                            colorParte3.G = (byte)tonoVerde;
                            break;
                    }
                }

                if (gt.TotalGameTime.TotalMilliseconds > (ultimoTParteAtaca + tiempoEsperaAtaque))
                {
                    parteAtaca = true;
                    parteReposo = false;
                    colorParte1.B = (byte)254;
                    colorParte1.G = (byte)254;
                    colorParte2.B = (byte)254;
                    colorParte2.G = (byte)254;
                    colorParte3.B = (byte)254;
                    colorParte3.G = (byte)254;
                }
            }

            if (parteAtaca)
            {
                switch (numeroAleatorio)
                {
                    case 1:
                        if (PositionParte1.Y < 800) { PositionParte1.Y = PositionParte1.Y + 20; }
                        else{detenerUnTiempo = true; parteAtaca = false;
                            tiempoDetencion = gt.TotalGameTime.TotalMilliseconds;}
                        break;

                    case 2:
                        if (PositionParte2.Y < 800){PositionParte2.Y = PositionParte2.Y + 20;}
                        else{detenerUnTiempo = true; parteAtaca = false;
                            tiempoDetencion = gt.TotalGameTime.TotalMilliseconds;}
                        break;

                    case 3:
                        if (PositionParte3.Y < 800) { PositionParte3.Y = PositionParte3.Y + 20; }
                        else{detenerUnTiempo = true; parteAtaca = false;
                            tiempoDetencion = gt.TotalGameTime.TotalMilliseconds;}
                        break;

                    case 4:
                        if (PositionParte1.Y < 800 && PositionParte2.Y < 800)
                        {
                            PositionParte1.Y = PositionParte1.Y + 20;
                            PositionParte2.Y = PositionParte2.Y + 20;
                        }
                        else
                        {
                            detenerUnTiempo = true; parteAtaca = false;
                            tiempoDetencion = gt.TotalGameTime.TotalMilliseconds;
                        }
                        break;

                    case 5:
                        if (PositionParte1.Y < 800 && PositionParte3.Y < 800)
                        {
                            PositionParte1.Y = PositionParte1.Y + 20;
                            PositionParte3.Y = PositionParte3.Y + 20;
                        }
                        else
                        {
                            detenerUnTiempo = true; parteAtaca = false;
                            tiempoDetencion = gt.TotalGameTime.TotalMilliseconds;
                        }
                        break;

                    case 6:
                        if (PositionParte3.Y < 800 && PositionParte2.Y < 800)
                        {
                            PositionParte3.Y = PositionParte3.Y + 20;
                            PositionParte2.Y = PositionParte2.Y + 20;
                        }
                        else
                        {
                            detenerUnTiempo = true; parteAtaca = false;
                            tiempoDetencion = gt.TotalGameTime.TotalMilliseconds;
                        }
                        break;
                }
            }

            if (detenerUnTiempo)
            {
                if (gt.TotalGameTime.TotalMilliseconds > tiempoDetencion + 2000)
                {
                    devolverParte = true;
                    detenerUnTiempo = false;
                }
            }

            if (devolverParte)
            {
                if (PositionParte1.Y >= Position.Y + 250) { PositionParte1.Y = PositionParte1.Y - 10; }
                if (PositionParte2.Y >= Position.Y + 250) { PositionParte2.Y = PositionParte2.Y - 10; }
                if (PositionParte3.Y >= Position.Y + 250) { PositionParte3.Y = PositionParte3.Y - 10; }
                if (PositionParte1.Y < Position.Y + 250 &&
                    PositionParte2.Y < Position.Y + 250 &&
                    PositionParte3.Y < Position.Y + 250)
                {
                    devolverParte = false;
                    parteReposo = true;
                    ultimoTParteAtaca = gt.TotalGameTime.TotalMilliseconds;
                }
            }

            velocidadAnimacionFlecha(gt);
            confirmaColisionesRued();
        }

        //Dibuja brazos Esp
        public void drawBrazosEsp(SpriteBatch sb)
        {
            if (tipoJefe == tipo.Esp)
            {
                sb.Draw(texPartesJefe[0], PositionParte1, TrozoAnimParte1, colorParte1,
                MathHelper.ToRadians(RotacionParte1), OrigenParte1, EscalaParte, SpriteEffects.None, 0.442f);
                sb.Draw(texPartesJefe[0], PositionParte2, TrozoAnimParte2, colorParte2,
                MathHelper.ToRadians(RotacionParte2), OrigenParte2, EscalaParte, SpriteEffects.None, 0.441f);
            }
        }

        //Dibuja Martillo Quad
        public void drawMartillosQuad(SpriteBatch sb)
        {
            if (tipoJefe == tipo.Quad)
            {
                sb.Draw(texPartesJefe[1], PositionParte1, TrozoAnimParte, colorParte1,
                   MathHelper.ToRadians(RotacionParte1), OrigenParte, EscalaParte, SpriteEffects.None, 0.442f);
                sb.Draw(texPartesJefe[1], PositionParte2, TrozoAnimParte, colorParte2,
                   MathHelper.ToRadians(RotacionParte2), OrigenParte, EscalaParte, SpriteEffects.None, 0.442f);
                sb.Draw(texPartesJefe[1], PositionParte3, TrozoAnimParte, colorParte3,
                   MathHelper.ToRadians(RotacionParte3), OrigenParte, EscalaParte, SpriteEffects.None, 0.442f);
            }
        }

        //Dibuja Flechas Rued
        public void drawFlechasRued(SpriteBatch sb)
        {
            if (tipoJefe == tipo.Rued)
            {
                sb.Draw(texPartesJefe[2], PositionParte1, TrozoAnimParte, colorParte1,
                   MathHelper.ToRadians(RotacionParte1), OrigenParte, EscalaParte, SpriteEffects.None, 0.442f);
                sb.Draw(texPartesJefe[2], PositionParte2, TrozoAnimParte, colorParte2,
                   MathHelper.ToRadians(RotacionParte2), OrigenParte, EscalaParte, SpriteEffects.None, 0.442f);
                sb.Draw(texPartesJefe[2], PositionParte3, TrozoAnimParte, colorParte3,
                   MathHelper.ToRadians(RotacionParte3), OrigenParte, EscalaParte, SpriteEffects.None, 0.442f);
            }
        }

        //Velocidad animacion martillo
        public void velocidadAnimacionMartillo(GameTime gameTime)
        {
            dato += gameTime.ElapsedGameTime;
            if (dato.TotalMilliseconds >=
                ((gameTime.ElapsedGameTime.Milliseconds) * 3))
            {
                actualframeParte = actualframeParte + 1;
                dato = TimeSpan.Zero;
            }

            if (actualframeParte >= 5)
            {
                actualframeParte = 0;
                lineaAnimacionParte++;
                if (lineaAnimacionParte >= 2) { lineaAnimacionParte = 0; }
            }
        }

        //Velocidad animacion flecha
        public void velocidadAnimacionFlecha(GameTime gameTime)
        {
            dato += gameTime.ElapsedGameTime;
            if (dato.TotalMilliseconds >=
                ((gameTime.ElapsedGameTime.Milliseconds) * 3))
            {
                actualframeParte = actualframeParte + 1;
                dato = TimeSpan.Zero;
            }
            if (actualframeParte >= 10){actualframeParte = 0;}
        }

        //trozo animacion martillo
        public void defineTrozoAnimMartillo(int x, int y, int width, int height)
        {
            TrozoAnimParte.X = x;
            TrozoAnimParte.Y = y;
            TrozoAnimParte.Width = width;
            TrozoAnimParte.Height = height;
        }

        public void setOrigenMartillo()
        {
            OrigenParte.X = 0;
            OrigenParte.Y = TrozoAnimParte.Height / 2;
        }

        public void setOrigenFlecha()
        {
            OrigenParte.X = TrozoAnimParte.Width / 2;
            OrigenParte.Y = TrozoAnimParte.Height / 2;
        }

        //define el origen de coordenadas de la animacion
        public void setOrigen()
        {
            Origen.X = TrozoAnim.Width / 2;
            Origen.Y = TrozoAnim.Height / 2;
        }

        /*Definicion de las texturas de los Jefes*/
        public static void setTextura(Textura t)
        {
            texJefes = new List<Texture2D>();
            texJefes.Add(t.getTextura("JLaxo"));//0
            texJefes.Add(t.getTextura("JEsp"));//1
            texJefes.Add(t.getTextura("JQuad"));//2
            texJefes.Add(t.getTextura("JRued"));//3

            texPartesJefe=new List<Texture2D>();
            texPartesJefe.Add(t.getTextura("JEspBrazos"));//0
            texPartesJefe.Add(t.getTextura("JQuadMartillo"));//1
            texPartesJefe.Add(t.getTextura("JRuedFlecha"));//2
        }

        public static void defineCollitionBox()
        {
            //COLLITION LAXO
            collitionLaxoX = new List<int>();
            collitionLaxoY = new List<int>();

            collitionLaxoX.Add((int)(-34 * 1.1f)); collitionLaxoY.Add((int)(168 * 1.1f));
            collitionLaxoX.Add((int)(0 * 1.1f)); collitionLaxoY.Add((int)(168 * 1.1f));
            collitionLaxoX.Add((int)(40 * 1.1f)); collitionLaxoY.Add((int)(168 * 1.1f));

            //COLLITION ESP
            collitionEspX = new List<int>();
            collitionEspY = new List<int>();
            collitionBrazoEspX = new List<int>();
            collitionBrazoEspY = new List<int>();
            collitionEspX.Add((int)(-148 * 1.2f)); collitionEspY.Add((int)(7 * 1.2f));
            collitionEspX.Add((int)(-103 * 1.2f)); collitionEspY.Add((int)(52 * 1.2f));
            collitionEspX.Add((int)(-54 * 1.2f)); collitionEspY.Add((int)(101 * 1.2f));
            collitionEspX.Add((int)(1 * 1.2f)); collitionEspY.Add((int)(152 * 1.2f));
            collitionEspX.Add((int)(55 * 1.2f)); collitionEspY.Add((int)(101 * 1.2f));
            collitionEspX.Add((int)(108 * 1.2f)); collitionEspY.Add((int)(52 * 1.2f));
            collitionEspX.Add((int)(146 * 1.2f)); collitionEspY.Add((int)(7 * 1.2f));
            //Brazo1
            collitionBrazoEspX.Add((int)(30 * 1.1f)); collitionBrazoEspY.Add((int)(6 * 1.1f));
            collitionBrazoEspX.Add((int)(80 * 1.1f)); collitionBrazoEspY.Add((int)(6 * 1.1f));
            collitionBrazoEspX.Add((int)(130 * 1.1f)); collitionBrazoEspY.Add((int)(6 * 1.1f));
            collitionBrazoEspX.Add((int)(180 * 1.1f)); collitionBrazoEspY.Add((int)(6 * 1.1f));
            collitionBrazoEspX.Add((int)(230 * 1.1f)); collitionBrazoEspY.Add((int)(6 * 1.1f));
            collitionBrazoEspX.Add((int)(280 * 1.1f)); collitionBrazoEspY.Add((int)(6 * 1.1f));
            collitionBrazoEspX.Add((int)(330 * 1.1f)); collitionBrazoEspY.Add((int)(6 * 1.1f));
            collitionBrazoEspX.Add((int)(380 * 1.1f)); collitionBrazoEspY.Add((int)(6 * 1.1f));
            collitionBrazoEspX.Add((int)(430 * 1.1f)); collitionBrazoEspY.Add((int)(6 * 1.1f));
            //Brazo2
            collitionBrazoEspX.Add((int)(-432 * 1.1f)); collitionBrazoEspY.Add((int)(6 * 1.1f));
            collitionBrazoEspX.Add((int)(-382 * 1.1f)); collitionBrazoEspY.Add((int)(6 * 1.1f));
            collitionBrazoEspX.Add((int)(-332 * 1.1f)); collitionBrazoEspY.Add((int)(6 * 1.1f));
            collitionBrazoEspX.Add((int)(-282 * 1.1f)); collitionBrazoEspY.Add((int)(6 * 1.1f));
            collitionBrazoEspX.Add((int)(-232 * 1.1f)); collitionBrazoEspY.Add((int)(6 * 1.1f));
            collitionBrazoEspX.Add((int)(-182 * 1.1f)); collitionBrazoEspY.Add((int)(6 * 1.1f));
            collitionBrazoEspX.Add((int)(-132 * 1.1f)); collitionBrazoEspY.Add((int)(6 * 1.1f));
            collitionBrazoEspX.Add((int)(-82 * 1.1f)); collitionBrazoEspY.Add((int)(6 * 1.1f));
            collitionBrazoEspX.Add((int)(-32 * 1.1f)); collitionBrazoEspY.Add((int)(6 * 1.1f));

            //COLLITION QUAD
            collitionQuadX = new List<int>();
            collitionQuadY = new List<int>();
            collitionMartilloQuadX = new List<int>();
            collitionMartilloQuadY = new List<int>();
            collitionQuadX.Add((int)(-150 * 1.1f)); collitionQuadY.Add((int)(120 * 1.1f));
            collitionQuadX.Add((int)(-49 * 1.1f)); collitionQuadY.Add((int)(120 * 1.1f));
            collitionQuadX.Add((int)(51 * 1.1f)); collitionQuadY.Add((int)(120 * 1.1f));
            collitionQuadX.Add((int)(151 * 1.1f)); collitionQuadY.Add((int)(120 * 1.1f));   
            //Martillo
            collitionMartilloQuadX.Add((int)(32 * 1.2f)); collitionMartilloQuadY.Add((int)(0 * 1.2f));
            collitionMartilloQuadX.Add((int)(109 * 1.2f)); collitionMartilloQuadY.Add((int)(0 * 1.2f));
            collitionMartilloQuadX.Add((int)(182 * 1.2f)); collitionMartilloQuadY.Add((int)(0 * 1.2f));
            collitionMartilloQuadX.Add((int)(250 * 1.2f)); collitionMartilloQuadY.Add((int)(0 * 1.2f));  
        }

        // Confirma colisiones Laxo
        public void confirmaColisionesLaxo()
        {
            rotacionActual = RotacionParte1;

            if (colisionJefe.colisionDisparoALaxo(this, tipoJefe) && tipoJefe==tipo.Laxo)
            {
                estaEnColisionDisp = true;
                if (CollitionJefe.ultimoTipoColision == 1 && lineaAnimacion>=2)
                {
                    if (CollitionJefe.ultimaColisionTipoDisparo != Disparo.tipo.super)
                    {
                        vida = vida - 5;
                    }else
                    {
                        vida = vida - 10;
                    }
                }
            }

            if (colisionJefe.colisionNaveALaxo(this))
            {
                if (Nave.permiteColisionesJefe)
                {
                    Nave.Vida = Nave.Vida - (30+adicionDanoNave);
                    Nave.permiteColisionesJefe = false;
                }
            }

            switch(Nave.TexturaNave)
            {
                case 0:
                    for (i = 0; i < 4; i++)
                    {
                        if (colisionJefe.colisionPoderGalacticaALaxo(this, tipoJefe))
                        {
                            estaEnColisionPoder = true;
                            if (CollitionJefe.ultimoTipoColision == 1 && lineaAnimacion >= 2)
                            {
                                vida = vida - 50;
                            }
                        }
                    }
                    break;
                case 1:
                    for (i = 0; i < 5; i++)
                    {
                        if (colisionJefe.colisionPoderCarafeALaxo(this, tipoJefe))
                        {
                            estaEnColisionPoder = true;
                            if (CollitionJefe.ultimoTipoColision == 1 && lineaAnimacion >= 2)
                            {
                                vida = vida - 30;
                            }
                        }
                    }
                    break;
                case 2:
                    if (colisionJefe.colisionPoderHelixALaxo(this, tipoJefe))
                    {
                        estaEnColisionPoder = true;
                        if (CollitionJefe.ultimoTipoColision == 1 && lineaAnimacion >= 2)
                        {
                            vida = vida - 1500;
                        }
                    }
                    break;
                case 3:
                    if (colisionJefe.colisionPoderArpALaxo(this, tipoJefe))
                    {
                        estaEnColisionPoder = true;
                        if (CollitionJefe.ultimoTipoColision == 1 && lineaAnimacion >= 2)
                        {
                            vida = vida - 8000;
                        }
                    }
                    break;
                case 4:
                    if (colisionJefe.colisionPoderPerseusALaxo(this, tipoJefe))
                    {
                        estaEnColisionPoder = true;
                        if (CollitionJefe.ultimoTipoColision == 1 && lineaAnimacion >= 2)
                        {
                            vida = vida - random.Next(1000, 5000);
                        }
                    }
                    break;
                case 5:
                    if (colisionJefe.colisionPoderPiscesALaxo(this, tipoJefe))
                    {
                        estaEnColisionPoder = true;
                        if (CollitionJefe.ultimoTipoColision == 1 && lineaAnimacion >= 2)
                        {
                            vida = vida - 80;
                        }
                    }
                    break;
            }

        }

        //Confirma colisiones Esp
        public void confirmaColisionesEsp()
        {
            if (colisionJefe.colisionDisparoAEsp(this, tipoJefe))
            {
                estaEnColisionDisp = true;
                if (CollitionJefe.ultimoTipoColision == 1)
                {
                    if (CollitionJefe.ultimaColisionTipoDisparo != Disparo.tipo.super)
                    {
                        vida = vida - 5;
                    }
                    else
                    {
                        vida = vida - 10;
                    }
                }
            }

            if (colisionJefe.colisionDisparoABrazo1Esp(PositionParte1, RotacionParte1) )
            {
                estaEnColisionDisp = true;
            }

            if (colisionJefe.colisionDisparoABrazo2Esp(PositionParte2, RotacionParte2))
            {
                estaEnColisionDisp = true;
            }

            if (colisionJefe.colisionNaveAEsp(this))
            {
                if (Nave.permiteColisionesJefe)
                {
                    Nave.Vida = Nave.Vida - (30+adicionDanoNave);
                    Nave.permiteColisionesJefe = false;
                }
            }
            if (colisionJefe.colisionNaveABrazo1Esp(PositionParte1,RotacionParte1))
            {
                if (Nave.permiteColisionesJefe)
                {
                    Nave.Vida = Nave.Vida - (25 + adicionDanoNave);
                    Nave.permiteColisionesJefe = false;
                }
            }
            if (colisionJefe.colisionNaveABrazo2Esp(PositionParte2, RotacionParte2))
            {
                if (Nave.permiteColisionesJefe)
                {
                    Nave.Vida = Nave.Vida - (25 + adicionDanoNave);
                    Nave.permiteColisionesJefe = false;
                }
            }

            switch (Nave.TexturaNave)
            {
                case 0:
                    for (i = 0; i < 4; i++)
                    {
                        if (colisionJefe.colisionPGalacticaAEsp(this, tipoJefe))
                        {
                            estaEnColisionPoder = true;
                            if (CollitionJefe.ultimoTipoColision == 1)
                            {
                                vida = vida - 50;
                            }
                        }
                        if (colisionJefe.colisionPGalacticaABrazo1(PositionParte1, RotacionParte1))
                        {
                            estaEnColisionPoder = true;
                        }
                        if (colisionJefe.colisionPGalacticaABrazo2(PositionParte2, RotacionParte2))
                        {
                            estaEnColisionPoder = true;
                        }
                    }
                    break;
                case 1:
                    for (i = 0; i < 5; i++)
                    {
                        if (colisionJefe.colisionPCarafeAEsp(this, tipoJefe))
                        {
                            estaEnColisionPoder = true;
                            if (CollitionJefe.ultimoTipoColision == 1)
                            {
                                vida = vida - 30;
                            }
                        }
                        if (colisionJefe.colisionPCarafeABrazo1(PositionParte1, RotacionParte1))
                        {
                            estaEnColisionPoder = true;
                        }
                        if (colisionJefe.colisionPCarafeABrazo2(PositionParte2, RotacionParte2))
                        {
                            estaEnColisionPoder = true;
                        }
                    }
                    break;
                case 2:
                    if (colisionJefe.colisionPHelixAEsp(this, tipoJefe))
                    {
                        estaEnColisionPoder = true;
                        if (CollitionJefe.ultimoTipoColision == 1)
                        {
                            vida = vida - 1500;
                        }
                    }
                    if (colisionJefe.colisionPHelixABrazo1(PositionParte1, RotacionParte1))
                    {
                        estaEnColisionPoder = true;
                    }
                    if (colisionJefe.colisionPHelixABrazo2(PositionParte2, RotacionParte2))
                    {
                        estaEnColisionPoder = true;
                    }
                    break;
                case 3:
                    if (colisionJefe.colisionPArpAEsp(this, tipoJefe))
                    {
                        estaEnColisionPoder = true;
                        if (CollitionJefe.ultimoTipoColision == 1)
                        {
                            vida = vida - 8000;
                        }
                    }
                    break;
                case 4:
                    if (colisionJefe.colisionPPerseusAEsp(this, tipoJefe))
                    {
                        estaEnColisionPoder = true;
                        if (CollitionJefe.ultimoTipoColision == 1)
                        {
                            vida = vida - random.Next(1000, 5000);
                        }
                    }
                    break;
                case 5:
                    if (colisionJefe.colisionPPiscesAEsp(this, tipoJefe))
                    {
                        estaEnColisionPoder = true;
                        if (CollitionJefe.ultimoTipoColision == 1)
                        {
                            vida = vida - 80;
                        }
                    }
                    if (colisionJefe.colisionPPiscesABrazo1(PositionParte1, RotacionParte1))
                    {
                        estaEnColisionPoder = true;
                    }
                    if (colisionJefe.colisionPPiscesABrazo2(PositionParte2, RotacionParte2))
                    {
                        estaEnColisionPoder = true;
                    }
                    break;
            }


        }


        // Confirma colisiones Quad
        public void confirmaColisionesQuad()
        {
            if (colisionJefe.colisionDisparoAQuad(this, tipoJefe))
            {
                estaEnColisionDisp = true;
            }

            if (colisionJefe.colisionNaveAQuad(this))
            {
                if (Nave.permiteColisionesJefe)
                {
                    Nave.Vida = Nave.Vida - (30+adicionDanoNave);
                    Nave.permiteColisionesJefe = false;
                }
            }

            if (colisionJefe.colisionNaveAMartilloQuad(PositionParte1) ||
                colisionJefe.colisionNaveAMartilloQuad(PositionParte2) ||
                colisionJefe.colisionNaveAMartilloQuad(PositionParte3) )
            {
                if (Nave.permiteColisionesJefe)
                {
                    Nave.Vida = Nave.Vida - (25 + adicionDanoNave);
                    Nave.permiteColisionesJefe = false;
                }
            }

            if (colisionJefe.colisionDisparoAMartilloQuad(PositionParte1) ||
                colisionJefe.colisionDisparoAMartilloQuad(PositionParte2) ||
                colisionJefe.colisionDisparoAMartilloQuad(PositionParte3))
            {
                estaEnColisionDisp = true;
                if (CollitionJefe.ultimoTipoColision == 1)
                {
                    if (CollitionJefe.ultimaColisionTipoDisparo != Disparo.tipo.super)
                    {
                        vida = vida - 5;
                    }
                    else
                    {
                        vida = vida - 10;
                    }
                }
            }

            if (colisionJefe.colisionPGalacticaAQuad(this, tipoJefe))
            {
                estaEnColisionPoder = true;
            }
            if (colisionJefe.colisionPGalacticaAMartilloQuad(PositionParte1) ||
                colisionJefe.colisionPGalacticaAMartilloQuad(PositionParte2) ||
                colisionJefe.colisionPGalacticaAMartilloQuad(PositionParte3))
            {
                estaEnColisionPoder = true;
                if (CollitionJefe.ultimoTipoColision == 1)
                {
                    vida = vida - 50;
                }
            }


            switch(Nave.TexturaNave)
            {
                case 0:
                    for (i = 0; i < 4; i++)
                    {
                        if (colisionJefe.colisionPGalacticaAQuad(this, tipoJefe))
                        {
                            estaEnColisionPoder = true;
                        }
                        if (colisionJefe.colisionPGalacticaAMartilloQuad(PositionParte1) ||
                            colisionJefe.colisionPGalacticaAMartilloQuad(PositionParte2) ||
                            colisionJefe.colisionPGalacticaAMartilloQuad(PositionParte3))
                        {
                            estaEnColisionPoder = true;
                            if (CollitionJefe.ultimoTipoColision == 1)
                            {
                                vida = vida - 50;
                            }
                        }
                    }
                    break;
                case 1:
                    for (i = 0; i < 5; i++)
                    {
                        if (colisionJefe.colisionPCarafeAQuad(this, tipoJefe))
                        {
                            estaEnColisionPoder = true;
                        }
                        if (colisionJefe.colisionPCarafeAMartilloQuad(PositionParte1) ||
                            colisionJefe.colisionPCarafeAMartilloQuad(PositionParte2) ||
                            colisionJefe.colisionPCarafeAMartilloQuad(PositionParte3))
                        {
                            estaEnColisionPoder = true;
                            if (CollitionJefe.ultimoTipoColision == 1)
                            {
                                vida = vida - 30;
                            }
                        }
                    }
                    break;
                case 2:
                    if (colisionJefe.colisionPHelixAQuad(this, tipoJefe))
                    {
                        estaEnColisionPoder = true;
                    }
                    if (colisionJefe.colisionPHelixAMartilloQuad(PositionParte1) ||
                        colisionJefe.colisionPHelixAMartilloQuad(PositionParte2) ||
                        colisionJefe.colisionPHelixAMartilloQuad(PositionParte3))
                    {
                        estaEnColisionPoder = true;
                        if (CollitionJefe.ultimoTipoColision == 1)
                        {
                            vida = vida - 1500;
                        }
                    }
                    break;
                case 3:
                    if (colisionJefe.colisionPArpAMartilloQuad(PositionParte1) ||
                        colisionJefe.colisionPArpAMartilloQuad(PositionParte2) ||
                        colisionJefe.colisionPArpAMartilloQuad(PositionParte3))
                    {
                        estaEnColisionPoder = true;
                        if (CollitionJefe.ultimoTipoColision == 1)
                        {
                            vida = vida - 8000;
                        }
                    }
                    break;
                case 4:
                    if (colisionJefe.colisionPPerseusAMartilloQuad(PositionParte1) ||
                        colisionJefe.colisionPPerseusAMartilloQuad(PositionParte2) ||
                        colisionJefe.colisionPPerseusAMartilloQuad(PositionParte3))
                    {
                        estaEnColisionPoder = true;
                        if (CollitionJefe.ultimoTipoColision == 1)
                        {
                            vida = vida - random.Next(1000, 5000);
                        }
                    }
                    break;
                case 5:
                    if (colisionJefe.colisionPPiscesAQuad(this, tipoJefe))
                    {
                        estaEnColisionPoder = true;
                    }
                    if (colisionJefe.colisionPPiscesAMartilloQuad(PositionParte1) ||
                        colisionJefe.colisionPPiscesAMartilloQuad(PositionParte2) ||
                        colisionJefe.colisionPPiscesAMartilloQuad(PositionParte3))
                    {
                        estaEnColisionPoder = true;
                        if (CollitionJefe.ultimoTipoColision == 1)
                        {
                            vida = vida - 80;
                        }
                    }
                    break;
            }

        }


        // Confirma colisiones Rued
        public void confirmaColisionesRued()
        {
            if (colisionJefe.colisionDisparoARued(this, tipoJefe))
            {
                estaEnColisionDisp = true;
            }

            if (colisionJefe.colisionNaveAFlechaRued(PositionParte1) ||
                colisionJefe.colisionNaveAFlechaRued(PositionParte2) ||
                colisionJefe.colisionNaveAFlechaRued(PositionParte3))
            {
                if (Nave.permiteColisionesJefe)
                {
                    Nave.Vida = Nave.Vida - (25+adicionDanoNave);
                    Nave.permiteColisionesJefe = false;
                }
            }

            if (colisionJefe.colisionNaveARued(this))
            {
                if (Nave.permiteColisionesJefe)
                {
                    Nave.Vida = Nave.Vida - (30+adicionDanoNave);
                    Nave.permiteColisionesJefe = false;
                }
            }

            if (colisionJefe.colisionDisparoAFlechaRued(PositionParte1) ||
                colisionJefe.colisionDisparoAFlechaRued(PositionParte2) ||
                colisionJefe.colisionDisparoAFlechaRued(PositionParte3))
            {
                estaEnColisionDisp = true;
                if (CollitionJefe.ultimaColisionTipoDisparo != Disparo.tipo.super)
                {
                    vida = vida - 5;
                }
                else
                {
                    vida = vida - 10;
                }
            }

            if (colisionJefe.colisionPGalacticaARued(this, tipoJefe))
            {
                estaEnColisionPoder = true;
            }
            if (colisionJefe.colisionPGalacticaAFlechaRued(PositionParte1) ||
                colisionJefe.colisionPGalacticaAFlechaRued(PositionParte2) ||
                colisionJefe.colisionPGalacticaAFlechaRued(PositionParte3))
            {
                estaEnColisionPoder = true;
                vida = vida - 50;
            }


            switch (Nave.TexturaNave)
            {
                case 0:
                    for (i = 0; i < 4; i++)
                    {
                        if (colisionJefe.colisionPGalacticaARued(this, tipoJefe))
                        {
                            estaEnColisionPoder = true;
                        }
                        if (colisionJefe.colisionPGalacticaAFlechaRued(PositionParte1) ||
                            colisionJefe.colisionPGalacticaAFlechaRued(PositionParte2) ||
                            colisionJefe.colisionPGalacticaAFlechaRued(PositionParte3))
                        {
                            estaEnColisionPoder = true;
                            vida = vida - 50;
                        }
                    }
                    break;
                case 1:
                    for (i = 0; i < 5; i++)
                    {
                        if (colisionJefe.colisionPCarafeARued(this, tipoJefe))
                        {
                            estaEnColisionPoder = true;
                        }
                        if (colisionJefe.colisionPCarafeAFlechaRued(PositionParte1) ||
                            colisionJefe.colisionPCarafeAFlechaRued(PositionParte2) ||
                            colisionJefe.colisionPCarafeAFlechaRued(PositionParte3))
                        {
                            estaEnColisionPoder = true;
                            vida = vida - 30;
                        }
                    }
                    break;
                case 2:
                    if (colisionJefe.colisionPHelixARued(this, tipoJefe))
                    {
                        estaEnColisionPoder = true;
                    }
                    if (colisionJefe.colisionPHelixAFlechaRued(PositionParte1) ||
                        colisionJefe.colisionPHelixAFlechaRued(PositionParte2) ||
                        colisionJefe.colisionPHelixAFlechaRued(PositionParte3))
                    {
                        estaEnColisionPoder = true;
                        vida = vida - 1500;
                    }
                    break;
                case 3:
                    if (colisionJefe.colisionPArpAFlechaRued(PositionParte1) ||
                        colisionJefe.colisionPArpAFlechaRued(PositionParte2) ||
                        colisionJefe.colisionPArpAFlechaRued(PositionParte3))
                    {
                        estaEnColisionPoder = true;
                        vida = vida - 8000;
                    }
                    break;
                case 4:
                    if (colisionJefe.colisionPPerseusAFlechaRued(PositionParte1) ||
                        colisionJefe.colisionPPerseusAFlechaRued(PositionParte2) ||
                        colisionJefe.colisionPPerseusAFlechaRued(PositionParte3))
                    {
                        estaEnColisionPoder = true;
                        vida = vida - random.Next(1000, 5000);
                    }
                    break;
                case 5:
                    if (colisionJefe.colisionPPiscesARued(this, tipoJefe))
                    {
                        estaEnColisionPoder = true;
                    }
                    if (colisionJefe.colisionPPiscesAFlechaRued(PositionParte1) ||
                        colisionJefe.colisionPPiscesAFlechaRued(PositionParte2) ||
                        colisionJefe.colisionPPiscesAFlechaRued(PositionParte3))
                    {
                        estaEnColisionPoder = true;
                        vida = vida - 80;
                    }
                    break;
            }


        }

    }
}
