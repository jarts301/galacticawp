using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input.Touch;

namespace Galactica
{
    class Control:GameComun
    {
        public static Texture2D texControl;
        public static Vector2 centro1=new Vector2(90,700),centro2=new Vector2(110,680),elCentro;
        public float Rotacion;
        public Color color;
        public static bool controlActivo,tamanoGrande;
        public float distancia;
        private TouchCollection touches;
        private int i;
        private int regNeutra;
        private float resX, resY;
        private float angulo;

        public Control()
        {
            
        }

        public Control(Vector2 pos)
        {
            Position = pos;
            color = Color.White;
            Rotacion = 0.0f;
            Escala=0.75f;
            controlActivo = false;
            tamanoGrande = false;
            actualframe = 0;
        }

        public void reiniciar()
        {
        }

        public static void load(Textura tex)
        {
            texControl = tex.getTextura("Control");
        }

        public void update(GameTime gt)
        {
            touches = TouchControl.touches;
            if (tamanoGrande)
            {
                elCentro = centro2;
                regNeutra = 28;
                Escala = 0.9f;
                setPosition(Control.centro2);
            }
            else
            {
                elCentro = centro1;
                regNeutra = 23;
                Escala = 0.75f;
                setPosition(Control.centro1);
            }

            if (estaSobre())
            {
                actualframe = 0;
                defineTrozoAnim(200 * actualframe, 0, 200, 200);
                setOrigen();

                procesaMovimiento();
            }
            else
            {
                actualframe = 1;
                defineTrozoAnim(200 * actualframe, 0, 200, 200);
                setOrigen();
            }

        }

        public void draw(SpriteBatch sb)
        {
            sb.Draw(texControl, Position, TrozoAnim, color,
                MathHelper.ToRadians(Rotacion), Origen, Escala, SpriteEffects.None, 0.29f);
        }

        //*****************************************************************************
        public void defineTrozoAnim(int x, int y, int width, int height)
        {
            TrozoAnim.X = x;
            TrozoAnim.Y = y;
            TrozoAnim.Width = width;
            TrozoAnim.Height = height;
        }

        public void setOrigen()
        {
            Origen.X = TrozoAnim.Width / 2;
            Origen.Y = TrozoAnim.Height / 2;
        }

        public void setPosition(Vector2 pos)
        {
            Position = pos;
        }

        public bool estaSobre()
        {
            
            if (touches.Count > 0)
            {
                for (i = 0; i < touches.Count; i++)
                {
                    distancia = (float)Math.Sqrt(
                              Math.Pow(touches[i].Position.X - elCentro.X, 2) +
                              Math.Pow(touches[i].Position.Y - elCentro.Y, 2)
                            );
                }
            }
            else { return false; }

            if (distancia <= (100*Escala))
            {
                return true;
            }
            else { return false; }

        }


        public void procesaMovimiento()
        {
            Nave.limiteShot = 240;

            if (touches.Count > 0)
            {
                for (i = 0; i < touches.Count; i++)
                {
                    resX=touches[i].Position.X - elCentro.X;
                    resY=touches[i].Position.Y - elCentro.Y;

                    if (resX > 0 && resY > 0)
                    {
                        angulo=MathHelper.ToDegrees((float)Math.Atan(resY/resX));
                    }
                    if (resX < 0 && resY > 0)
                    {
                        angulo = 180 + MathHelper.ToDegrees((float)Math.Atan(resY / resX));
                    }
                    if (resX < 0 && resY < 0)
                    {
                        angulo = 180 + MathHelper.ToDegrees((float)Math.Atan(resY / resX));
                    }
                    if (resX > 0 && resY < 0)
                    {
                        angulo = 360 + MathHelper.ToDegrees((float)Math.Atan(resY / resX));
                    }
                }
            }

            if (distancia > regNeutra)
            {
                if (angulo < 22.5f || angulo > 337.5)
                { Nave.auxPosition.X = Nave.auxPosition.X + Nave.velocidadNave; }
                if (angulo > 22.5f && angulo < 67.5f && Nave.movEnY)
                {
                    Nave.auxPosition.X = Nave.auxPosition.X + Nave.velocidadNave;
                    Nave.auxPosition.Y = Nave.auxPosition.Y + Nave.velocidadNave;
                }
                if (angulo > 67.5f && angulo < 112.5f && Nave.movEnY)
                { Nave.auxPosition.Y = Nave.auxPosition.Y + Nave.velocidadNave; }
                if (angulo > 112.5f && angulo < 157.5f && Nave.movEnY)
                {
                    Nave.auxPosition.X = Nave.auxPosition.X - Nave.velocidadNave;
                    Nave.auxPosition.Y = Nave.auxPosition.Y + Nave.velocidadNave;
                }
                if (angulo > 157.5f && angulo < 202.5f)
                { Nave.auxPosition.X = Nave.auxPosition.X - Nave.velocidadNave; }
                if (angulo > 202.5f && angulo < 247.5f && Nave.movEnY)
                {
                    Nave.auxPosition.X = Nave.auxPosition.X - Nave.velocidadNave;
                    Nave.auxPosition.Y = Nave.auxPosition.Y - Nave.velocidadNave;
                }
                if (angulo > 247.5f && angulo < 292.5f && Nave.movEnY)
                { Nave.auxPosition.Y = Nave.auxPosition.Y - Nave.velocidadNave; }
                if (angulo > 292.5f && angulo < 337.5f && Nave.movEnY)
                {
                    Nave.auxPosition.X = Nave.auxPosition.X + Nave.velocidadNave;
                    Nave.auxPosition.Y = Nave.auxPosition.Y - Nave.velocidadNave;
                }

                Nave.setPosition(Nave.auxPosition);
            }

        }

    }
}
