using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Galactica
{
    class Onda:GameComun
    {
        private static List<Texture2D> texDisparo;
        private int texturaActual;
        public float Rotacion;

        public Onda()
        {
            Rotacion=0;
            Escala = 1.5f;
            Position.X=240;
            Position.Y=820;
            setOrigen();
            texturaActual = 0;
        }

        public void reiniciar()
        {
            Rotacion=0;
            Escala = 1.5f;
            Position.X=240;
            Position.Y=820;
            setOrigen();
            texturaActual = 0;
        }

        public static void load(Textura t)
        {
            setTextura(t);
        }

        public void update(GameTime gameTime)
        {
            velocidadAnimacion(gameTime,3);
            setTrozoAnimacion();
            setOrigen();
            movimiento(gameTime);
        }

        public void draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texDisparo[texturaActual], Position, TrozoAnim, Color.White,
                MathHelper.ToRadians(Rotacion),Origen,Escala,SpriteEffects.None,0.42f);
        }

        /***********************************/

        public static void setTextura(Textura t)
        {
            texDisparo = new List<Texture2D>();
            texDisparo.Add(t.getTextura("Onda"));
        }

        public void setPosition(Vector2 posNave)
        {
            Position.X = posNave.X;
            Position.Y = posNave.Y;
        }

        public Vector2 getPosition()
        {
            return Position;
        }

        public void setOrigen()
        {
            Origen.X = TrozoAnim.Width / 2;
            Origen.Y = TrozoAnim.Height / 2;
        }

        //define la velocidad de la animacion de la nave
        public void velocidadAnimacion(GameTime gameTime, int velocidad)
        {
            dato += gameTime.ElapsedGameTime;
            if (dato.TotalMilliseconds >=
                ((gameTime.ElapsedGameTime.Milliseconds) * velocidad))
            {
                actualframe = actualframe + 1;
                dato = TimeSpan.Zero;
            }
            if (actualframe >= 7) { actualframe = 0; }
        }

        //el cuadro que debe mostrar en el siguiente draw
        public void setTrozoAnimacion()
        {
            TrozoAnim.X = 0;
            TrozoAnim.Y = actualframe*100;
            TrozoAnim.Width = 400;
            TrozoAnim.Height = 100;
        }

        //Movimiento nave mini
        public void movimiento(GameTime gt)
        {

    
        }

    }
}
