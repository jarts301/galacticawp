using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Galactica
{
    class QuickModeGOver:GameComun
    {
         private static List<Texture2D> textura;
        private static SpriteFont fuente;
        private Vector2 positionTitulo, origenTitulo, positionPuntos, positionDestruidos, positionResultados;
        private List<Boton> botones;
        public static double tEsperaActivaBoton=1200.0;
        private int i;

        public QuickModeGOver()
        {
            loadTitulo();
            botones = new List<Boton>();
            positionPuntos=new Vector2(90,345);
            positionDestruidos = new Vector2(90, 385);
            positionResultados = new Vector2(-8, 290);
            botones.Add(new Boton(Boton.TipoBoton.BContinue, 245, 700));
            botones.Add(new Boton(Boton.TipoBoton.BRestart, 5, 700));
            //botones.Add(new Boton(Boton.TipoBoton.BComent, 20, 500));
            //botones.Add(new Boton(Boton.TipoBoton.BFacebook, 190, 500));
        }

        public static void load(Textura t,SpriteFont f)
        {
            fuente=f;
            setTexturas(t);
        }

        public void update(GameTime time)
        {
            updateEstrellas(time);
            if (tEsperaActivaBoton > 0)
            {
                tEsperaActivaBoton -=
                time.ElapsedGameTime.TotalMilliseconds;
            }
            else
            {
                for (i = 0; i < botones.Count; i++)
                {
                    botones[i].update(time);
                }
            }
            //botones[2].escala = 1.5f;
        }

        public void draw(SpriteBatch sb)
        {
            //CLEAR NEGRO
            sb.Draw(Textura.TexturasFondo[2], Vector2.Zero, null, Color.MediumVioletRed, 0.0f, Vector2.Zero,
                2.0f, SpriteEffects.None, 0.2f);

            drawEstrellas(sb);
            drawTitulo(sb);
            for (i = 0; i < botones.Count; i++)
            {
                botones[i].draw(sb);
            }

            sb.Draw(textura[1],positionResultados, null, Color.White, 0.0f, Vector2.Zero,
                1.3f, SpriteEffects.None, 0.19f);
            sb.DrawString(fuente,"Score: "+Nave.Puntos.ToString(), positionPuntos, Color.Gold, 0.0f,
                Vector2.Zero, 1.3f, SpriteEffects.None, 0.18f);
            sb.DrawString(fuente, "Destroyed: " + Nave.Destruidos.ToString(), positionDestruidos, Color.Gold, 0.0f,
                Vector2.Zero, 1.3f, SpriteEffects.None, 0.18f);
        }

        /*******************************************/

        //******TITULO***********
        public void loadTitulo()
        {
            positionTitulo = new Vector2(480/2,200);
            origenTitulo = new Vector2(textura[0].Width/2,textura[0].Height/2);
        }

        public void drawTitulo(SpriteBatch sb)
        {
            sb.Draw(textura[0], positionTitulo, null, Color.White, 0.0f, origenTitulo,
                1.1f, SpriteEffects.None, 0.1f);
        }

        //*********Texturas***************
        public static void setTexturas(Textura t)
        {
            textura = new List<Texture2D>();
            textura.Add(t.getTextura("TituloGameOver"));//0
            textura.Add(t.getTextura("TResultados"));//1
        }

    }
}
