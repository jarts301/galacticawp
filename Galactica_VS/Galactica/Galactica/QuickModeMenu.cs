using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.IsolatedStorage;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input.Touch;

namespace Galactica
{
    class QuickModeMenu:GameComun
    {
        private static List<Texture2D> textura;
        public static List<Texture2D> texturaNaves;
        private static List<String> nombresNave;
        private static SpriteFont Letra;
        private Vector2 positionTitulo,origenTitulo;
        private Vector2 positionSubT, origenSubT;
        private Vector2 positionNave,origenNave;
        private Vector2 positionNavesRescatadas,positionNombreNave;
        private List<Boton> botones;
        public static int texturaNaveActual,rescatadas;
        private int i;

        public QuickModeMenu()
        {
            loadTitulo();
            texturaNaveActual = 0;
            rescatadas = 1;
            positionNave = new Vector2(480/2,480);
            origenNave = new Vector2();
            botones = new List<Boton>();
            positionNavesRescatadas = new Vector2(5,230);
            positionNombreNave = new Vector2(5,258);
            botones.Add(new Boton(Boton.TipoBoton.BFlecha, 10, 450,false));
            botones.Add(new Boton(Boton.TipoBoton.BFlecha, 460, 530,true));
            botones.Add(new Boton(Boton.TipoBoton.BPlay, 200, 700));
            botones.Add(new Boton(Boton.TipoBoton.BBack, 10, 730));
        }

        public static void reiniciar()
        {
            texturaNaveActual = 0;
            Nave.TexturaNave = 0;
        }

        public static void load(Textura t, SpriteFont f)
        {
            setTexturas(t);
            Letra = f;
        }

        public void update(GameTime time)
        {
            updateEstrellas(time);
            updateNave(time);
            for (i = 0; i < botones.Count; i++)
            {
                botones[i].update(time);
            }
        }

        public void draw(SpriteBatch sb)
        {
            //CLEAR NEGRO
            sb.Draw(Textura.TexturasFondo[0], Vector2.Zero, null, Color.Black, 0.0f, Vector2.Zero,
                    40.0f, SpriteEffects.None, 0.999f);

            drawEstrellas(sb);
            drawTitulo(sb);
            for (i = 0; i < botones.Count; i++)
            {
                botones[i].draw(sb);
            }
            /*Naves*/
            sb.Draw(texturaNaves[texturaNaveActual], positionNave, TrozoAnim, Color.White, 0.0f, 
                origenNave,2.5f, SpriteEffects.None, 0.3f);

            sb.DrawString(Letra, "SPACESHIPS RESCUED: " +rescatadas+"/6", positionNavesRescatadas, Color.White,
                0.0f, Vector2.Zero, 1.0f, SpriteEffects.None, 0.01f);
            sb.DrawString(Letra,"NAME: "+nombresNave[texturaNaveActual], positionNombreNave, 
                Color.White,0.0f,Vector2.Zero, 1.0f, SpriteEffects.None, 0.01f);
        }

        /*******************************************/

        /******TITULO************/
        public void loadTitulo()
        {
            positionTitulo = new Vector2(480/2,70);
            origenTitulo = new Vector2(textura[0].Width/2,textura[0].Height/2);
            positionSubT = new Vector2(480 / 2, 220);
            origenSubT = new Vector2(textura[1].Width / 2, textura[1].Height / 2);
        }

        public void drawTitulo(SpriteBatch sb)
        {
            sb.Draw(textura[0], positionTitulo, null, Color.White, 0.0f, origenTitulo,
                1.2f, SpriteEffects.None, 0.1f);
            sb.Draw(textura[1], positionSubT, null, Color.White, 0.0f, origenSubT,
                1.5f, SpriteEffects.None, 0.1f);
        }

        /**Actualiza Nave**/
        public void updateNave(GameTime gameTime)
        {
            dato += gameTime.ElapsedGameTime;
            if (dato.TotalMilliseconds >=
                ((gameTime.ElapsedGameTime.Milliseconds) * 3))
            {
                actualframe = actualframe + 1;
                dato = TimeSpan.Zero;
            }

            if (actualframe >= 3) { actualframe = 0; }

            TrozoAnim.X = actualframe * 100;
            TrozoAnim.Y = 0;
            TrozoAnim.Width = 100;
            TrozoAnim.Height = 110;

            origenNave.X = TrozoAnim.Width / 2;
            origenNave.Y = TrozoAnim.Height / 2;
        }

        /*********Texturas****************/
        public static void setTexturas(Textura t)
        {
            textura = new List<Texture2D>();
            texturaNaves = new List<Texture2D>();
            nombresNave = new List<String>();

            textura.Add(t.getTextura("TituloQuickGame"));//0
            textura.Add(t.getTextura("TEscogerNave"));//1

            texturaNaves.Add(t.getTextura("NGalactica"));//0
            texturaNaves.Add(t.getTextura("NCarafe"));//1
            texturaNaves.Add(t.getTextura("NHelix"));//2
            texturaNaves.Add(t.getTextura("NArp"));//3
            texturaNaves.Add(t.getTextura("NPerseus"));//4
            texturaNaves.Add(t.getTextura("NPisces"));//5

            nombresNave.Add("GALACTICA");//0
            nombresNave.Add("CARAFE");//1
            nombresNave.Add("HELIX");//2
            nombresNave.Add("ARP");//3
            nombresNave.Add("PERSEUS");//4
            nombresNave.Add("PISCES");//5
        }
    }
}
