using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Galactica
{
    class Animation:GameComun
    {
        private static List<Texture2D> texturas;
        public enum tipoAnimacion { DisparoANave, 
                                    DisparoAEnemigo, 
                                    EstallidoEnemigo,
                                    EstallidoJefe,
                                    EstallidoNave,
                                    PoderGalactica,
                                    PoderCarafe,
                                    PoderHelix,
                                    PoderArp,
                                    PoderPerseus,
                                    PoderPisces}
        public tipoAnimacion tipoActual;
        private static int w;
        private int textura,maxFrames;
        public bool animacionFinalizada;
        public Color Tinte=new Color();
        public static List<Animation> animacionesDispANaveTotal;
        public static List<Animation> animacionesDispAEnemigoTotal;
        public static List<Animation> animacionesEstallidoEnemigoTotal;
        public static List<Animation> animacionesEstallidoJefeTotal;
        public static Animation animacionesEstallidoNave,animacionEstallidoJefe;

        public Animation()
        {
            
        }

        public Animation(tipoAnimacion t)
        {
            tipoActual = t;
            switch (t)
            {
                case tipoAnimacion.DisparoANave:
                        textura = 0;
                        actualframe = 0;
                        maxFrames = 2;
                        Escala = 1.0f;
                        velocidad = 3;
                        Tinte = Color.White;
                    break;
                case tipoAnimacion.DisparoAEnemigo:
                        textura = 1;
                        actualframe = 0;
                        maxFrames = 2;
                        Escala = 1.0f;
                        velocidad = 3;
                        Tinte = Color.White;
                    break;
                case tipoAnimacion.EstallidoEnemigo:
                        textura = 2;
                        actualframe = 0;
                        maxFrames = 4;
                        Escala = 1.0f;
                        velocidad = 3;
                        Tinte = Color.White;
                    break;
                case tipoAnimacion.EstallidoJefe:
                    textura = 4;
                    actualframe = 0;
                    maxFrames = 4;
                    Escala = 1.0f;
                    velocidad = 3;
                    Tinte = Color.White;
                    break;
                case tipoAnimacion.EstallidoNave:
                    textura = 5;
                    actualframe = 0;
                    maxFrames = 4;
                    Escala = 1.0f;
                    velocidad = 3;
                    Tinte = Color.White;
                    break;
                case tipoAnimacion.PoderGalactica:
                        textura = 3;
                        actualframe = 0;
                        maxFrames = 2;
                        Escala = 1.0f;
                        velocidad = 3;
                        Tinte = Color.White;
                    break;
                case tipoAnimacion.PoderCarafe:
                        textura = 3;
                        actualframe = 0;
                        maxFrames = 2;
                        Escala = 1.0f;
                        velocidad = 3;
                        Tinte = Color.Orange;
                    break;
                case tipoAnimacion.PoderHelix:
                        textura = 3;
                        actualframe = 0;
                        maxFrames = 2;
                        Escala = 1.0f;
                        velocidad = 3;
                        Tinte = Color.LightGreen;
                    break;
                case tipoAnimacion.PoderArp:
                    textura = 3;
                    actualframe = 0;
                    maxFrames = 2;
                    Escala = 1.0f;
                    velocidad = 3;
                    Tinte = Color.Yellow;
                    break;
                case tipoAnimacion.PoderPerseus:
                    textura = 3;
                    actualframe = 0;
                    maxFrames = 2;
                    Escala = 1.0f;
                    velocidad = 3;
                    Tinte = Color.Red;
                    break;
                case tipoAnimacion.PoderPisces:
                    textura = 3;
                    actualframe = 0;
                    maxFrames = 2;
                    Escala = 1.0f;
                    velocidad = 3;
                    Tinte = Color.Fuchsia;
                    break;
            }
            animacionFinalizada = false;
        }

        public static void load(Textura t)
        {
            setTextura(t);
            cargaListaAnimaciones();
        }

        public void update(GameTime gameTime)
        {

            switch (tipoActual)
            {
                case tipoAnimacion.DisparoANave:
                       updateDisparoANave(gameTime);
                    break;

                case tipoAnimacion.DisparoAEnemigo:
                       updateDisparoAEnemigo(gameTime);
                    break;

                case tipoAnimacion.EstallidoEnemigo:
                       updateEstallidoEnemigo(gameTime);
                    break;

                case tipoAnimacion.EstallidoJefe:
                    updateEstallidoJefe(gameTime);
                    break;
                case tipoAnimacion.EstallidoNave:
                    updateEstallidoNave(gameTime);
                    break;

                case tipoAnimacion.PoderGalactica:
                       updateAnimacionPoder(gameTime);
                    break;
                case tipoAnimacion.PoderCarafe:
                       updateAnimacionPoder(gameTime);
                    break;
                case tipoAnimacion.PoderHelix:
                    updateAnimacionPoder(gameTime);
                    break;
                case tipoAnimacion.PoderArp:
                    updateAnimacionPoder(gameTime);
                    break;
                case tipoAnimacion.PoderPerseus:
                    updateAnimacionPoder(gameTime);
                    break;
                case tipoAnimacion.PoderPisces:
                    updateAnimacionPoder(gameTime);
                    break;
            }

        }

        public void draw(SpriteBatch spriteBatch)
        {
            if (!animacionFinalizada)
            {
                spriteBatch.Draw(texturas[textura], Position, TrozoAnim, Tinte,
                    0.0f, Origen, Escala, SpriteEffects.None, 0.3f);
            }
        }


        ///*************************************///////

        public void setPosition(Vector2 p)
        {
            Position.X = p.X;
            Position.Y = p.Y;
        }
        public Vector2 getPosition()
        {
            return Position;
        }

        //Carga lista animaciones disparo a nave
        public static void cargaListaAnimaciones()
        {
            animacionesDispANaveTotal = new List<Animation>();
            animacionesDispAEnemigoTotal = new List<Animation>();
            animacionesEstallidoEnemigoTotal = new List<Animation>();
            animacionesEstallidoJefeTotal = new List<Animation>();
            animacionesEstallidoNave = new Animation(Animation.tipoAnimacion.EstallidoNave);
            animacionEstallidoJefe = new Animation(Animation.tipoAnimacion.EstallidoJefe);
            for (w = 0; w < 200; w++)
            {
                animacionesDispANaveTotal.Add(
                    new Animation(Animation.tipoAnimacion.DisparoANave));
            }
            for (w = 0; w < 150; w++)
            {
                animacionesDispAEnemigoTotal.Add(
                    new Animation(Animation.tipoAnimacion.DisparoAEnemigo));
                animacionesEstallidoEnemigoTotal.Add(
                    new Animation(Animation.tipoAnimacion.EstallidoEnemigo));
            }
            for (w = 0; w < 30; w++)
            {
                animacionesEstallidoJefeTotal.Add(
                    new Animation(Animation.tipoAnimacion.EstallidoJefe));
            } 
        }

        /*************************/
        //********** Disparo a Nave*************
        public void updateDisparoANave(GameTime gt)
        {
            dato += gt.ElapsedGameTime;
            if (dato.TotalMilliseconds >=
                ((gt.ElapsedGameTime.Milliseconds) * velocidad))
            {
                actualframe = actualframe + 1;
                dato = TimeSpan.Zero;
            }

            if (actualframe >= 2) 
            { 
                actualframe = 0;
                animacionFinalizada = true;
            }

            defineTrozoAnim(actualframe*24,0,24,24);

            setOrigen();
        }

        //*************Disparo a Enemigo***********
        public void updateDisparoAEnemigo(GameTime gt)
        {
            dato += gt.ElapsedGameTime;
            if (dato.TotalMilliseconds >=
                ((gt.ElapsedGameTime.Milliseconds) * velocidad))
            {
                actualframe = actualframe + 1;
                dato = TimeSpan.Zero;
            }

            if (actualframe >= 2)
            {
                actualframe = 0;
                animacionFinalizada = true;
            }

            defineTrozoAnim(actualframe * 24, 0, 24, 24);

            setOrigen();
        }

        //****************Estallido Enemigo*************
        public void updateEstallidoEnemigo(GameTime gt)
        {
            dato += gt.ElapsedGameTime;
            if (dato.TotalMilliseconds >=
                ((gt.ElapsedGameTime.Milliseconds) * velocidad))
            {
                actualframe = actualframe + 1;
                dato = TimeSpan.Zero;
            }

            if (actualframe >= 4)
            {
                actualframe = 0;
                animacionFinalizada = true;
            }

            defineTrozoAnim(actualframe * 100, 0, 100, 100);

            setOrigen();
        }

        //****************Estallido Jefe*************
        public void updateEstallidoJefe(GameTime gt)
        {
            dato += gt.ElapsedGameTime;
            if (dato.TotalMilliseconds >=
                ((gt.ElapsedGameTime.Milliseconds) * velocidad))
            {
                actualframe = actualframe + 1;
                dato = TimeSpan.Zero;
            }

            if (actualframe >= 4)
            {
                actualframe = 0;
                animacionFinalizada = true;
            }

            defineTrozoAnim(actualframe * 400, 0, 400, 400);

            setOrigen();
        }

        //****************Estallido Nave*************
        public void updateEstallidoNave(GameTime gt)
        {
            dato += gt.ElapsedGameTime;
            if (dato.TotalMilliseconds >=
                ((gt.ElapsedGameTime.Milliseconds) * velocidad))
            {
                actualframe = actualframe + 1;
                dato = TimeSpan.Zero;
            }

            if (actualframe >= 4)
            {
                actualframe = 0;
                animacionFinalizada = true;
            }

            defineTrozoAnim(actualframe * 200, 0, 200, 200);

            setOrigen();
        }

        // *******  actualiza animacion Poder
        public void updateAnimacionPoder(GameTime gt)
        {
            dato += gt.ElapsedGameTime;
            if (dato.TotalMilliseconds >=
                ((gt.ElapsedGameTime.Milliseconds) * velocidad))
            {
                actualframe = actualframe + 1;
                dato = TimeSpan.Zero;
            }

            if (actualframe >= 2)
            {
                actualframe = 0;
            }

            defineTrozoAnim(actualframe * 480, 0, 480, 800);
            setOrigen(Vector2.Zero);
        }

        /* Define si animacion termina o no*/
        public void TerminaAnimacion(bool siOno)
        {
            animacionFinalizada = siOno;
        }

        //**************************************
        public bool isFinAnimation()
        {
            if (actualframe >= maxFrames)
            {
                return true;
            }else{
                return false;
            }
        }

        public static void setTextura(Textura t)
        {
            texturas = new List<Texture2D>();
            texturas.Add(t.getTextura("animationDispANave"));//0
            texturas.Add(t.getTextura("animationDispAEnemigo"));//1
            texturas.Add(t.getTextura("EstallidoEnemigo"));//2
            texturas.Add(t.getTextura("animationPoder"));//3
            texturas.Add(t.getTextura("EstallidoJefe"));//4
            texturas.Add(t.getTextura("EstallidoNave"));//5
        }

        public void defineTrozoAnim(int x, int y, int width, int height)
        {
            TrozoAnim.X = x;
            TrozoAnim.Y = y;
            TrozoAnim.Width = width;
            TrozoAnim.Height = height;
        }

        public void setOrigen(Vector2 o)
        {
            Origen.X = o.X;
            Origen.Y = o.Y;
        }

        public void setOrigen()
        {
            Origen.X = TrozoAnim.Width / 2;
            Origen.Y = TrozoAnim.Height / 2;
        }
    }
}
