using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Galactica
{
    class Buy:GameComun
    {

        private static List<Texture2D> textura;
        private Vector2 positionTitulo,origenTitulo;
        private List<Boton> botones;
        private static SpriteFont Letra;
        private int i;
        private static StringBuilder mensaje=new StringBuilder("Enjoy all the missions,\nand the full Quick Game.\nRescue all spaceships,\nwith its incredible powers,\nand Do not lose the benefit\nof overcoming each mission,\nso you can go a lot further\nin Quick Game,and get many\nmore points.");

        public Buy()
        {
            loadTitulo();
            botones = new List<Boton>();
            botones.Add(new Boton(Boton.TipoBoton.BGoBuy, 80, 450));
            botones.Add(new Boton(Boton.TipoBoton.BNotNow, 80, 570));
        }

        public static void load(Textura t,SpriteFont fuente)
        {
            Letra = fuente;
            setTexturas(t);
        }

        public void update(GameTime time)
        {
            updateEstrellas(time);
            for (i = 0; i < botones.Count; i++)
            {
                botones[i].update(time);
            }
            Boton.ultimaActualizacion = time.TotalGameTime.TotalMilliseconds;
            Nave.ultimaActualizacion = time.TotalGameTime.TotalMilliseconds;
        }

        public void draw(SpriteBatch sb)
        {
            //CLEAR NEGRO
            sb.Draw(Textura.TexturasFondo[0], Vector2.Zero, null, Color.Black, 0.0f, Vector2.Zero,
                100.0f, SpriteEffects.None, 0.999f);

            drawEstrellas(sb);
            drawTitulo(sb);
            for (i = 0; i < botones.Count; i++)
            {
                botones[i].draw(sb);
            }

            sb.DrawString(Letra, mensaje, new Vector2(20,140),
                    Color.White, 0.0f, Vector2.Zero, 1.0f, SpriteEffects.None, 0.01f);

        }

        /*******************************************/

        /******TITULO************/
        public void loadTitulo()
        {
            positionTitulo = new Vector2(480/2,100);
            origenTitulo = new Vector2(textura[0].Width/2,textura[0].Height/2);
        }

        public void drawTitulo(SpriteBatch sb)
        {
            sb.Draw(textura[0], positionTitulo, null, Color.White, 0.0f, origenTitulo,
                1.2f, SpriteEffects.None, 0.1f);
        }

        /*********Texturas****************/
        public static void setTexturas(Textura t)
        {
            textura = new List<Texture2D>();
            textura.Add(t.getTextura("TituloBuyGame"));//0
        }

    }
}
