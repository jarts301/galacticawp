using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Galactica
{
    class MisionModeGOver:GameComun
    {
         private static List<Texture2D> textura;
        private static SpriteFont fuente;
        private Vector2 positionTitulo, origenTitulo, positionPuntos, positionDestruidos, positionResultados;
        private List<Boton> botones;
        public int texturaTitulo;
        public static bool gana;
        private int i;
        public static double tEsperaActivaBoton = 1200.0;
        public static StringBuilder cosasGanadas = new StringBuilder();

        public MisionModeGOver()
        {
            loadTitulo();
            gana = false;
            botones = new List<Boton>();
            positionPuntos=new Vector2(20,345);
            positionDestruidos = new Vector2(90, 385);
            positionResultados = new Vector2(-8, 290);
            botones.Add(new Boton(Boton.TipoBoton.BContinue, 245, 700));
            botones.Add(new Boton(Boton.TipoBoton.BRestart, 5, 700));
            botones.Add(new Boton(Boton.TipoBoton.BNextMision, 80, 550));
        }

        public static void load(Textura t,SpriteFont f)
        {
            fuente=f;
            setTexturas(t);
        }

        public void update(GameTime time)
        {
            updateEstrellas(time);
            updateTitulo(time);
            if (tEsperaActivaBoton > 0)
            {
                tEsperaActivaBoton -=
                time.ElapsedGameTime.TotalMilliseconds;
            }
            else
            {
                for (i = 0; i < 2; i++)
                {
                    botones[i].update(time);
                }
                if (gana && MisionModeGame.numMision != 20) { botones[2].update(time); }
            }
        }

        public void draw(SpriteBatch sb)
        {
            //CLEAR NEGRO
            sb.Draw(Textura.TexturasFondo[2], Vector2.Zero, null, Color.LightGreen, 0.0f, Vector2.Zero,
                2.0f, SpriteEffects.None, 0.2f);

            drawEstrellas(sb);
            drawTitulo(sb);
            for (i = 0; i < 2; i++)
            {
                botones[i].draw(sb);
            }

            if (gana && MisionModeGame.numMision != 20) { botones[2].draw(sb); }

            sb.Draw(textura[2],positionResultados, null, Color.White, 0.0f, Vector2.Zero,
                1.3f, SpriteEffects.None, 0.19f);
            sb.DrawString(fuente,cosasGanadas, positionPuntos, Color.White, 0.0f,
                Vector2.Zero, 1.0f, SpriteEffects.None, 0.18f);
        }
        /*******************************************/

        //******TITULO***********
        public void loadTitulo()
        {
            positionTitulo = new Vector2(480/2,200);
        }

        public void updateTitulo(GameTime gt)
        {
            if (gana) { texturaTitulo = 0; }
            else { texturaTitulo = 1; }
            SetOrigen(texturaTitulo);
        }

        public void drawTitulo(SpriteBatch sb)
        {
            sb.Draw(textura[texturaTitulo], positionTitulo, null, Color.White, 0.0f, origenTitulo,
                1.1f, SpriteEffects.None, 0.1f);
        }

        public void SetOrigen(int Tex)
        {
            origenTitulo = new Vector2(textura[Tex].Width / 2, textura[Tex].Height / 2);
        }

        //*********Texturas***************
        public static void setTexturas(Textura t)
        {
            textura = new List<Texture2D>();
            textura.Add(t.getTextura("TituloWin"));//0
            textura.Add(t.getTextura("TituloLose"));//1
            textura.Add(t.getTextura("TRecompensa"));//2
        }
    }
}
