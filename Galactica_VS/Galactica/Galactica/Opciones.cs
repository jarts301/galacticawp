using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Galactica
{
    class Opciones:GameComun
    {
        private static List<Texture2D> textura;
        private static Vector2 positionTitulo,origenTitulo;
        private List<Boton> botones;
        private int i;

        public Opciones()
        {
            botones = new List<Boton>();
            botones.Add(new Boton(Boton.TipoBoton.BBack, 10, 730));
            //botones.Add(new Boton(Boton.TipoBoton.BGuardar, 250, 720));
            botones.Add(new Boton(Boton.TipoBoton.BAudio, 210, 190));
            botones.Add(new Boton(Boton.TipoBoton.BTilting, 10, 340));
            botones.Add(new Boton(Boton.TipoBoton.BButtons, 260, 340));
            botones.Add(new Boton(Boton.TipoBoton.BMedium, 10, 510));
            botones.Add(new Boton(Boton.TipoBoton.BLarge, 260, 510));
        }

        public static void load(Textura t)
        {
            setTexturas(t);
            loadTitulo();
        }

        public void update(GameTime time)
        {
            updateEstrellas(time);
            for (i = 0; i < botones.Count-2; i++)
            {
                botones[i].update(time);
            }
            if (Control.controlActivo)
            {
                botones[4].update(time);
                botones[5].update(time);
            }
            botones[1].escala = 2.0f;
            Boton.ultimaActualizacion = time.TotalGameTime.TotalMilliseconds;
            Nave.ultimaActualizacion = time.TotalGameTime.TotalMilliseconds;
        }

        public void draw(SpriteBatch sb)
        {
            //CLEAR NEGRO
            sb.Draw(Textura.TexturasFondo[0], Vector2.Zero, null, Color.Black, 0.0f, Vector2.Zero,
                100.0f, SpriteEffects.None, 0.999f);

            drawEstrellas(sb);
            drawTitulo(sb);
            for (i = 0; i < botones.Count-2; i++)
            {
                botones[i].draw(sb);
            }
            if (Control.controlActivo)
            {
                botones[4].draw(sb);
                botones[5].draw(sb);
            }
        }

        /*******************************************/

        /******TITULO************/
        public static void loadTitulo()
        {
            positionTitulo = new Vector2(480/2,130);
            origenTitulo = new Vector2(textura[0].Width/2,textura[0].Height/2);
        }

        public void drawTitulo(SpriteBatch sb)
        {
            sb.Draw(textura[0], new Vector2(480/2,100), null, Color.White, 0.0f, origenTitulo,
                1.2f, SpriteEffects.None, 0.1f);
            sb.Draw(textura[1], new Vector2(25,150), null, Color.White, 0.0f, Vector2.Zero,
                0.86f, SpriteEffects.None, 0.1f);
            sb.Draw(textura[4], new Vector2(70, 280), null, Color.White, 0.0f, Vector2.Zero,
                1.0f, SpriteEffects.None, 0.1f);
            sb.Draw(textura[2], new Vector2(40, 295), null, Color.White, 0.0f, Vector2.Zero,
                1.0f, SpriteEffects.None, 0.1f);
            sb.Draw(textura[4], new Vector2(70, 440), null, Color.White, 0.0f, Vector2.Zero,
                1.0f, SpriteEffects.None, 0.1f);
            if (Control.controlActivo)
            {
                sb.Draw(textura[3], new Vector2(80, 460), null, Color.White, 0.0f, Vector2.Zero,
                    1.0f, SpriteEffects.None, 0.1f);
            }
        }

        /*********Texturas****************/
        public static void setTexturas(Textura t)
        {
            textura = new List<Texture2D>();
            textura.Add(t.getTextura("TituloOpciones"));//0
            textura.Add(t.getTextura("TituloSonidosMusica"));//1
            textura.Add(t.getTextura("TituloControlMode"));//2
            textura.Add(t.getTextura("TituloButtonSize"));//3
            textura.Add(t.getTextura("Separador"));//4
        }

    }
}
