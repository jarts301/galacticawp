﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework.Media;

namespace Galactica
{
    class QuickModeGame:InGameBase
    {

        public QuickModeGame()
        {
            
        }


        public static void reiniciar()
        {
            Jefe.adicionDanoNave = 0;
            DisparoJefe.adicionDanoDisNave = 0;
            Enemigo.adicionDanoNave = 0;
            DisparoEnemigo.adicionDanoDisNave = 0;
            llegaA1500 = false;
            tiempoEsperaPlus = 30000;
            ultimoTPlus = 0;
            velocidadEnem = 4;
            velocidadEnemY = 4;
            velocidadHiEnem = 6;
            velocidadHiEnemY = 6;
            maxEnemigos = 4;
            aumentoEnVidaEnemigo = 0;
            ultimoAumento = ultimaAdicionJefe = 0;
            eliminarTodos();
            eliminarJefes();
            eliminaAnimaciones();
            plus.plusActivo = false;
            plus.muestra = false;
            //Mensaje Inicial
            EscalaMensaje = escalaInicialMensaje = 1.5f;
            actualFrameMensaje = 0;
            tiempoTitileoMensaje = 0;
            mensajeInicialActivo = true;

            botones.Clear();
            botones.Add(new Boton(Boton.TipoBoton.BPause, 0, 0));
            botones.Add(new Boton(Boton.TipoBoton.BAudio, 430, 0));
            botones.Add(new Boton(Boton.TipoBoton.BPower,
                                  (int)(480 - (100 * 0.8f)),
                                  (int)(800 - (100 * 0.8f))));
        }


        public static void load(Textura t,SpriteFont f)
        {
            LoadEnemigos();
            loadEstrellas();
            fuente = f;
        }

        public static void update(GameTime time)
        {
            nave.update(time);
            updateEstrellas(time);
            updateEnemigos(time);
            updateGame(time);
            updateAnimaciones(time);
            for (i = 0; i < botones.Count; i++)
            {
                botones[i].update(time);
            }
            updateMensajeInicial(time);
            updateJefes(time);
            barraVidaNave.update(time, Nave.Vida, Nave.MaxVida);
            
            if (Nave.Vida <= 0) { Animation.animacionesEstallidoNave.update(time); }
            updatePlus(time);
            gameOver();

            if (Control.controlActivo)
            {
                control.update(time);
            }
        }

        public static void draw(SpriteBatch sb)
        {
            //CLEAR NEGRO
            sb.Draw(Textura.TexturasFondo[0], Vector2.Zero, null, Color.Black, 0.0f, Vector2.Zero,
                40.0f, SpriteEffects.None, 0.999f);

            nave.draw(sb);
            drawEstrellas(sb);
            drawEnemigos(sb);
            drawEstadNave(sb);
            drawAnimaciones(sb);
            drawJefes(sb);
            drawMensajeInicial(sb);
            if (Nave.Vida <= 0) { Animation.animacionesEstallidoNave.draw(sb); }

            if (plus.plusActivo)
            {
                plus.draw(sb);
            }

            for (i = 0; i < botones.Count; i++)
            {
                botones[i].draw(sb);
            }
            //drawEstadisticas(sb);

            if (Control.controlActivo)
            {
                control.draw(sb);
            }

        }
        /******************Estadisticas de la nave**************/
        public static void drawEstadNave(SpriteBatch sb)
        {
            sb.Draw(Textura.TexturasFondo[1], Vector2.Zero, null,Color.Black, 0.0f, Vector2.Zero, 
                new Vector2(50.0f,2.3f),SpriteEffects.None, 0.401f);

            //** Vida de la nave
            sb.DrawString(fuente, /*Nave.MaxVida.ToString()+"|"+*/Nave.Vida.ToString(),
                PositionSalud, Color.White, 0.0f,Vector2.Zero, 0.8f, SpriteEffects.None, 0.388f);
            /*sb.DrawString(fuente, Nave.Vida.ToString() + "/" + Nave.MaxVida.ToString(),
                PositionVistaSalud, Color.Black, 0.0f, Vector2.Zero, 0.8f, SpriteEffects.None, 0.389f);*/
            barraVidaNave.draw(sb);

            //**Puntos
            sb.DrawString(fuente, Nave.Puntos.ToString(), PositionPuntos, Color.Gold, 0.0f,
                Vector2.Zero, 1.3f, SpriteEffects.None, 0.4f);
            sb.DrawString(fuente, "/", PositionLabelPuntos, Color.White, 0.0f,
                Vector2.Zero, 1.3f, SpriteEffects.None, 0.4f);
        }

        public static void updatePlus(GameTime time)
        {
            if (time.TotalGameTime.TotalMilliseconds > ultimoTPlus + tiempoEsperaPlus)
            {
                plus.plusActivo = true;
                plus.reiniciar(new Vector2(random.Next(20, 460), -200));
                ultimoTPlus = time.TotalGameTime.TotalMilliseconds;
            }

            if (plus.plusActivo)
            {
                plus.update(time);
            }

            if (Nave.Vida <= ((Nave.MaxVida * 2) / 8)) { Nave.disparoSuperActivo = false; }
            if (Nave.Vida <= ((Nave.MaxVida * 1) / 8)) { Nave.disparosDiagActivos = false; }
        }

        //***Actualiza el juego
        public static void updateGame(GameTime gt)
        {
            if (Nave.Puntos >= 1500 && !llegaA1500)
            {
                maxEnemigos = maxEnemigos + 1;
                llegaA1500 = true;
            }

            if (Nave.Puntos > (ultimoAumento + 3000) && llegaA1500)
            {
                aumentoEnVidaEnemigo = aumentoEnVidaEnemigo + 3;
                tiempoEsperaPlus = tiempoEsperaPlus + 5000;
                aumentoEnVidaJefe = aumentoEnVidaJefe + 2000;
                Jefe.adicionDanoNave = Jefe.adicionDanoNave + 3;
                DisparoJefe.adicionDanoDisNave = DisparoJefe.adicionDanoDisNave + 3;
                Enemigo.adicionDanoNave = Enemigo.adicionDanoNave + 3;
                DisparoEnemigo.adicionDanoDisNave = DisparoEnemigo.adicionDanoDisNave + 3;
                maxEnemigos = maxEnemigos + 1;
                ultimoAumento = Nave.Puntos;
            }

            //***Saber si es trial o full version
            if (GalacticaMain.isTrialMode)
            {
                if (Nave.Puntos >= 5000)
                {
                    Almacenamiento.salvarInGame();
                    if (MediaPlayer.GameHasControl)
                    {
                        MediaPlayer.IsRepeating = true;
                        MediaPlayer.Play(Audio.getMusica("mainMenu"));
                    }
                    GalacticaMain.estadoActual = GalacticaMain.Estado.Buy;
                }
            }
            //**** fin saber si es trial o full //

            if (enemigos.Count < maxEnemigos && jefes.Count == 0)
            {
                insertaEnemigo();
            }

            /*if (gt.TotalGameTime.TotalMilliseconds > ultimoTSalvado + 30000)
            {
                Almacenamiento.salvarInGame();
                ultimoTSalvado = gt.TotalGameTime.TotalMilliseconds;
            }*/

        }

        /*Actualiza enemigos*/
        public static void updateEnemigos(GameTime gt)
        {
            if (enemigos.Count > 0)
            {
                for (i = 0; i < enemigos.Count; i++)
                {
                    enemigos[i].update(gt);

                    if (enemigos[i].estaEnColisionDisp ||
                        enemigos[i].estaEnColisionNave ||
                        enemigos[i].estaEnColisionPoder)
                    {
                        enemigos[i].estaEnColisionDisp = false;
                        enemigos[i].estaEnColisionNave = false;
                        enemigos[i].estaEnColisionPoder = false;

                        if (Animation.animacionesDispAEnemigoTotal.Count > 0)
                        {
                            Animation.animacionesDispAEnemigoTotal.ElementAt(Animation.animacionesDispAEnemigoTotal.Count - 1)
                                .setPosition(enemigos[i].Position);
                            animacionesQ.Add(Animation.animacionesDispAEnemigoTotal[Animation.animacionesDispAEnemigoTotal.Count - 1]);
                            Animation.animacionesDispAEnemigoTotal.RemoveAt(Animation.animacionesDispAEnemigoTotal.Count - 1);
                        }

                        if (enemigos[i].vida <= 0 || enemigos[i].estaEnColisionNave)
                        {
                            (Audio.getEfecto("EstallidoEnemigo")).Play();

                            Nave.Destruidos = Nave.Destruidos + 1;
                            enemigos[i].estaEnColisionNave = false;

                            if (Animation.animacionesEstallidoEnemigoTotal.Count > 0)
                            {
                                Animation.animacionesEstallidoEnemigoTotal.ElementAt(Animation.animacionesEstallidoEnemigoTotal.Count - 1)
                                    .setPosition(enemigos[i].Position);
                                animacionesQ.Add(Animation.animacionesEstallidoEnemigoTotal[Animation.animacionesEstallidoEnemigoTotal.Count - 1]);
                                Animation.animacionesEstallidoEnemigoTotal.RemoveAt(Animation.animacionesEstallidoEnemigoTotal.Count - 1);
                            }

                            switch (enemigos[i].tipoEnemigo)
                            {
                                case Enemigo.tipo.Nor:
                                        Nave.Puntos = Nave.Puntos + 10;
                                        confirmaEliminarNor(i);
                                    break;
                                case Enemigo.tipo.Kami:
                                        Nave.Puntos = Nave.Puntos + 20;
                                        EliminarYRetornoEnemigoTotal(i);
                                    break;
                                case Enemigo.tipo.Sid:
                                        Nave.Puntos = Nave.Puntos + 30;
                                        confirmaEliminarSid(i);
                                    break;
                                case Enemigo.tipo.HiNor:
                                        Nave.Puntos = Nave.Puntos + 50;
                                        confirmaEliminarNor(i);
                                    break;
                                case Enemigo.tipo.HiKami:
                                        Nave.Puntos = Nave.Puntos + 60;
                                        EliminarYRetornoEnemigoTotal(i);
                                    break;
                                case Enemigo.tipo.HiSid:
                                        Nave.Puntos = Nave.Puntos + 80;
                                        confirmaEliminarSid(i);
                                    break;
                            }

                        }//si murio el enemigo
                        
                    }//si esta wne colision

                }//for mirando cada enemigo

            }//si enemigos count >0


                /*if (Nave.Puntos >= 1000 && velocidadEnem <= 4 && velocidadEnemY <= 4)
                {
                    velocidadEnem=velocidadEnem + 1;
                    velocidadEnemY = velocidadEnemY + 1;
                    maxEnemigos = maxEnemigos + 1;
                }else
                if (Nave.Puntos >= 4000 && velocidadEnem <=5 && velocidadEnemY<=5)
                {
                    velocidadEnem = velocidadEnem + 1;
                    velocidadHiEnem = velocidadHiEnem + 1;
                    velocidadHiEnemY = velocidadHiEnemY + 1;
                    maxEnemigos = maxEnemigos + 1;
                }else
                if (Nave.Puntos >= 8000 && velocidadEnem <= 6 && velocidadEnemY <= 6)
                {
                    velocidadEnem = velocidadEnem + 1;
                    maxEnemigos = maxEnemigos + 1;
                }else
                if (Nave.Puntos >= 16000 && velocidadEnem <=6)
                {
                    velocidadHiEnem = velocidadHiEnem + 1;
                    velocidadHiEnemY = velocidadHiEnemY + 1;
                }*/
        }

        public static void gameOver()
        {
            if (Nave.Vida <= 0)
            {
                Nave.Vida = 0;
                Nave.EscalaN = 0;
                Animation.animacionesEstallidoNave.setPosition(Nave.PositionNav);
                if (Animation.animacionesEstallidoNave.animacionFinalizada)
                {
                    if (MediaPlayer.GameHasControl && Audio.musicaActiva)
                    {
                        MediaPlayer.IsRepeating = false;
                        MediaPlayer.Play(Audio.getMusica("GameOver"));
                    }
                    Almacenamiento.borrarInGame();

                    if (Nave.Puntos > GalacticaMain.BESTSCORE)
                    {
                        GalacticaMain.BESTSCORE = Nave.Puntos;
                        Almacenamiento.salvarScore();
                    }

                    QuickMode.estadoActual = QuickMode.Estado.GameOver;
                }
            }
        }

        /*******Dibuja enemigos******/
        public static void drawEnemigos(SpriteBatch sb)
        {
            if (enemigos.Count > 0)
            {
                for (i = 0; i < enemigos.Count; i++)
                { enemigos[i].draw(sb); }
            }
        }
        /********************Animaciones****************/
        public static void updateAnimaciones(GameTime time)
        {
            if (animacionesQ.Count > 0)
            {
                for (i = 0; i < animacionesQ.Count; i++)
                {
                    animacionesQ[i].update(time);
                }
            }

            if (animacionesQ.Count > 0)
            {
                for (i = 0; i < animacionesQ.Count; i++)
                {
                    if (animacionesQ[i].animacionFinalizada)
                    {
                        animacionesQ[i].animacionFinalizada = false;
                        switch(animacionesQ[i].tipoActual)
                        {
                            case Animation.tipoAnimacion.DisparoAEnemigo:
                                Animation.animacionesDispAEnemigoTotal.Add(animacionesQ[i]);
                                break;
                            case Animation.tipoAnimacion.EstallidoEnemigo:
                                Animation.animacionesEstallidoEnemigoTotal.Add(animacionesQ[i]);
                                break;
                            case Animation.tipoAnimacion.EstallidoJefe:
                                Animation.animacionesEstallidoJefeTotal.Add(animacionesQ[i]);
                                break;
                        }
                        animacionesQ.Remove(animacionesQ[i]);
                    }
                }
            }

        }

        /*******Dibuja animaciones************/
        public static void drawAnimaciones(SpriteBatch sb)
        {
            if (animacionesQ.Count > 0)
            {
                for (i = 0; i < animacionesQ.Count; i++)
                {
                    animacionesQ[i].draw(sb);
                }
            }
        }

        public static void eliminaAnimaciones()
        {
            int count=animacionesQ.Count;
            for (int i = count-1; i >= 0; i--)
            {
                animacionesQ[i].animacionFinalizada = false;
                switch (animacionesQ[i].tipoActual)
                {
                    case Animation.tipoAnimacion.DisparoAEnemigo:
                        Animation.animacionesDispAEnemigoTotal.Add(animacionesQ[i]);
                        break;
                    case Animation.tipoAnimacion.EstallidoEnemigo:
                        Animation.animacionesEstallidoEnemigoTotal.Add(animacionesQ[i]);
                        break;
                    case Animation.tipoAnimacion.EstallidoJefe:
                        Animation.animacionesEstallidoJefeTotal.Add(animacionesQ[i]);
                        break;
                }
                animacionesQ.Remove(animacionesQ[i]);
            }
        }

        /********************ENEMIGOS************************/
        //confirma solo estos porque los Kami no dispara
        public static void confirmaEliminarNor(int c)
        {
            if (enemigos[c].disparosNor.Count <= 0 &&
                enemigos[c].animaciones.Count <= 0)
            {
                EliminarYRetornoEnemigoTotal(c);
            }
            else
            {
                if (enemigos[c].disparosNor.Count > 0)
                {
                    for (int i = 0; i < enemigos[c].disparosNor.Count; i++)
                    {
                        Enemigo.disparosNorTotal.Add(enemigos[c].disparosNor[i]);
                        enemigos[c].disparosNor.RemoveAt(i);
                    }
                    enemigos[c].disparosNor.Clear();
                }
                if (enemigos[c].animaciones.Count > 0)
                {
                    for (int i = 0; i < enemigos[c].animaciones.Count; i++)
                    {
                        if (enemigos[c].animaciones[i].tipoActual ==
                            Animation.tipoAnimacion.DisparoANave)
                        {
                            Animation.animacionesDispANaveTotal.Add(enemigos[c].animaciones[i]);
                        }
                    }
                    enemigos[c].animaciones.Clear();
                }
                EliminarYRetornoEnemigoTotal(c);
            }
        }

        public static void confirmaEliminarSid(int c)
        {
            if (enemigos[c].disparosSid.Count <= 0 &&
                enemigos[c].animaciones.Count <= 0)
               {
                   EliminarYRetornoEnemigoTotal(c);
               }
               else
               {
                   if (enemigos[c].disparosSid.Count > 0)
                   {
                       for (int i = 0; i < enemigos[c].disparosSid.Count; i++)
                       {
                           Enemigo.disparosSidTotal.Add(enemigos[c].disparosSid[i]);
                       }
                       enemigos[c].disparosSid.Clear();
                   }

                   if (enemigos[c].animaciones.Count > 0)
                   {
                       for (int i = 0; i < enemigos[c].animaciones.Count; i++)
                       {
                           if (enemigos[c].animaciones[i].tipoActual ==
                               Animation.tipoAnimacion.DisparoANave)
                           {
                               Animation.animacionesDispANaveTotal.Add(enemigos[c].animaciones[i]);
                           }
                       }
                       enemigos[c].animaciones.Clear();
                   }

                   EliminarYRetornoEnemigoTotal(c);
                }
        }

        //Agregar jefe
        public static void agregarJefe()
        {
            num=random.Next(1, 5);
            switch(num)
            {
                case 1:
                    jefes.Add(Jefe.jefesTotales[0]);
                    Jefe.jefesTotales.RemoveAt(0);
                    break;
                case 2:
                    jefes.Add(Jefe.jefesTotales[1]);
                    Jefe.jefesTotales.RemoveAt(1);
                    break;
                case 3:
                    jefes.Add(Jefe.jefesTotales[2]);
                    Jefe.jefesTotales.RemoveAt(2);
                    break;
                case 4:
                    jefes.Add(Jefe.jefesTotales[3]);
                    Jefe.jefesTotales.RemoveAt(3);
                    break;
            }         
        }

        //Agregar jefe
        public static void agregarJefe(Jefe.tipo tipoJefe)
        {
            for (i = 0; i < Jefe.jefesTotales.Count; i++)
            {
                if (Jefe.jefesTotales[i].tipoJefe == tipoJefe)
                {
                    jefes.Add(Jefe.jefesTotales[i]);
                    Jefe.jefesTotales.RemoveAt(i);
                }
            }

        }


        //***Update Jefes
        public static void updateJefes(GameTime time)
        {
            for (i = 0; i < jefes.Count; i++)
            {
                jefes[i].update(time);
                if (jefes[i].estaEnColisionDisp)
                {
                    jefes[i].estaEnColisionDisp = false;
                    if (Animation.animacionesDispAEnemigoTotal.Count > 0)
                    {
                        Animation.animacionesDispAEnemigoTotal.ElementAt(Animation.animacionesDispAEnemigoTotal.Count - 1)
                            .setPosition(CollitionJefe.positionUltimoDisparo);
                        animacionesQ.Add(Animation.animacionesDispAEnemigoTotal[Animation.animacionesDispAEnemigoTotal.Count - 1]);
                        Animation.animacionesDispAEnemigoTotal.RemoveAt(Animation.animacionesDispAEnemigoTotal.Count - 1);
                    }
                }
                if (jefes[i].estaEnColisionPoder)
                {
                    jefes[i].estaEnColisionPoder = false;
                    if (Animation.animacionesEstallidoEnemigoTotal.Count > 0)
                    {
                        Animation.animacionesEstallidoEnemigoTotal.ElementAt(Animation.animacionesEstallidoEnemigoTotal.Count - 1)
                            .setPosition(CollitionJefe.positionUltimoDisparo);
                        animacionesQ.Add(Animation.animacionesEstallidoEnemigoTotal[Animation.animacionesEstallidoEnemigoTotal.Count - 1]);
                        Animation.animacionesEstallidoEnemigoTotal.RemoveAt(Animation.animacionesEstallidoEnemigoTotal.Count - 1);
                    }
                }
                if (jefes[i].vida <= 0)
                {
                    (Audio.getEfecto("EstallidoJefe")).Play();
                    if (jefes[i].animaciones.Count > 0)
                    {
                        for (j = 0; j < jefes[i].animaciones.Count; j++)
                        {
                            if (jefes[i].animaciones[j].tipoActual ==
                                Animation.tipoAnimacion.DisparoANave)
                            {
                                jefes[i].animaciones[j].animacionFinalizada = false;
                                Animation.animacionesDispANaveTotal.Add(jefes[i].animaciones[j]);
                                jefes[i].animaciones.Remove(jefes[i].animaciones[j]);
                            }
                        }
                    }

                    if (jefes[i].disparos.Count > 0)
                    {
                        for (j = 0; j < jefes[i].disparos.Count; j++)
                        {
                            if (jefes[i].disparos[j].tipoActual ==
                                DisparoJefe.tipo.normal)
                            {
                                Jefe.disparosNormalTotal.Add(jefes[i].disparos[j]);
                            }
                        }
                        for (j = 0; j < jefes[i].disparos.Count; j++)
                        {
                            if (jefes[i].disparos[j].tipoActual ==
                                DisparoJefe.tipo.maximo)
                            {
                                Jefe.disparosMaxTotal.Add(jefes[i].disparos[j]);
                            }
                        }
                    }
                    jefes[i].disparos.Clear();

                    if (Animation.animacionesEstallidoJefeTotal.Count > 0)
                    {
                        Animation.animacionesEstallidoJefeTotal.ElementAt(Animation.animacionesEstallidoJefeTotal.Count - 1)
                            .setPosition(jefes[i].Position);
                        animacionesQ.Add(Animation.animacionesEstallidoJefeTotal[Animation.animacionesEstallidoJefeTotal.Count - 1]);
                        Animation.animacionesEstallidoJefeTotal.RemoveAt(Animation.animacionesEstallidoJefeTotal.Count - 1);
                    }

                    Nave.Puntos = Nave.Puntos + 10000;
                    barraVidaJefe.reiniciaColor();

                    jefes[i].reinicia(240,-800,aumentoEnVidaJefe);
                    Jefe.jefesTotales.Add(jefes[i]);
                    jefes.RemoveAt(i);
                }
            }

            if (jefes.Count > 0)
            {
                barraVidaJefe.update(time,jefes[0].vida, jefes[0].maxVida);
                eliminarTodosLosEnemigos();
            }

            if (Nave.Puntos > (ultimaAdicionJefe + 8000))
            {
                agregarJefe();
                ultimaAdicionJefe = Nave.Puntos+(10000);
            }

        }

        //******Dibujar jefes
        public static void drawJefes(SpriteBatch sb)
        {
            for (i = 0; i < jefes.Count; i++)
            {
                jefes[i].draw(sb);
            }
            if (jefes.Count > 0)
            {
                barraVidaJefe.draw(sb);
            }
        }


        //***********************ESTRELLAS*********************/
        /****Carga estrellas**/
        public static void loadEstrellas()
        {
            for (i = 0; i < 50; i++)
            {
                estrellas.Add(new Estrella(new Vector2(random.Next(0, 479),
                                                       random.Next(0, 799))));
                estrellas[i].color = new Color(random.Next(200, 255), random.Next(200, 255),
                                               random.Next(200, 255));
                estrellas[i].escala = (float)(random.NextDouble() + 0.55);
                estrellas[i].vel = random.Next(6, 25);
            }
        }

        /****Actualiza estrellas***/
        public static void updateEstrellas(GameTime time)
        {
            for (i = 0; i < 50; i++)
            {
                estrellas[i].update(time);
            }
        }

        /****Dibuja estrellas**/
        public static void drawEstrellas(SpriteBatch sb)
        {
            for (i = 0; i < 50; i++)
            {
                estrellas[i].draw(sb);
            }
        }

        //******Actualiza Mensaje Inicial
        public static void updateMensajeInicial(GameTime gt)
        {
            if (mensajeInicialActivo)
            {
                EscalaMensaje = actualFrameMensaje;

                tiempoAyuda += gt.ElapsedGameTime;
                if (tiempoAyuda.TotalMilliseconds >=
                    ((gt.ElapsedGameTime.Milliseconds) * 10))
                {
                    actualFrameMensaje = actualFrameMensaje + escalaInicialMensaje;
                    tiempoAyuda = TimeSpan.Zero;
                }

                if (actualFrameMensaje > escalaInicialMensaje) { actualFrameMensaje = 0; }

                if (gt.TotalGameTime.TotalMilliseconds > tiempoTitileoMensaje + 2000)
                {
                    EscalaMensaje = escalaInicialMensaje;
                    mensajeInicialActivo = false;
                }
            }
        }

        //********Dibujo Mensaje Inicial
        public static void drawMensajeInicial(SpriteBatch sb)
        {
            if (mensajeInicialActivo)
            {
                sb.DrawString(fuente,"Quick Mode\n    Start!!", new Vector2(100, 320), 
                    Color.LightGreen, 0 ,Vector2.Zero, EscalaMensaje, SpriteEffects.None,0.4f);
            }
        }

        //********Eliminar todos los enemigos
        public static void eliminarTodosLosEnemigos()
        {
            if (enemigos.Count > 0)
            {
                for (i = enemigos.Count-1; i >= 0; i--)
                {
                    if (Animation.animacionesEstallidoEnemigoTotal.Count > 0)
                    {
                        Animation.animacionesEstallidoEnemigoTotal.ElementAt(Animation.animacionesEstallidoEnemigoTotal.Count - 1)
                            .setPosition(enemigos[i].Position);
                        animacionesQ.Add(Animation.animacionesEstallidoEnemigoTotal[Animation.animacionesEstallidoEnemigoTotal.Count - 1]);
                        Animation.animacionesEstallidoEnemigoTotal.RemoveAt(Animation.animacionesEstallidoEnemigoTotal.Count - 1);
                    }

                    switch (enemigos[i].tipoEnemigo)
                    {
                        case Enemigo.tipo.Nor:
                            Nave.Puntos = Nave.Puntos + 10;
                            confirmaEliminarNor(i);
                            break;
                        case Enemigo.tipo.Kami:
                            Nave.Puntos = Nave.Puntos + 20;
                            EliminarYRetornoEnemigoTotal(i);
                            break;
                        case Enemigo.tipo.Sid:
                            Nave.Puntos = Nave.Puntos + 30;
                            confirmaEliminarSid(i);
                            break;
                        case Enemigo.tipo.HiNor:
                            Nave.Puntos = Nave.Puntos + 50;
                            confirmaEliminarNor(i);
                            break;
                        case Enemigo.tipo.HiKami:
                            Nave.Puntos = Nave.Puntos + 60;
                            EliminarYRetornoEnemigoTotal(i);
                            break;
                        case Enemigo.tipo.HiSid:
                            Nave.Puntos = Nave.Puntos + 80;
                            confirmaEliminarSid(i);
                            break;
                    }
                    Nave.Destruidos = Nave.Destruidos + 1;
                }
            }
        }

        //********Eliminar todos los enemigos
        public static void eliminarTodos()
        {
                for (int i = enemigos.Count-1; i >= 0; i--)
                {
                    enemigos[i].estaEnColisionDisp = false;
                    enemigos[i].estaEnColisionNave = false;
                    enemigos[i].estaEnColisionPoder = false;
                    switch (enemigos[i].tipoEnemigo)
                    {
                        case Enemigo.tipo.Nor:
                            confirmaEliminarNor(i);
                            break;
                        case Enemigo.tipo.Kami:
                            EliminarYRetornoEnemigoTotal(i);
                            break;
                        case Enemigo.tipo.Sid:
                            confirmaEliminarSid(i);
                            break;
                        case Enemigo.tipo.HiNor:
                            confirmaEliminarNor(i);
                            break;
                        case Enemigo.tipo.HiKami:
                            EliminarYRetornoEnemigoTotal(i);
                            break;
                        case Enemigo.tipo.HiSid:
                            confirmaEliminarSid(i);
                            break;
                    }
                }
        }

        public static void EliminarYRetornoEnemigoTotal(int indice)
        {
            switch (enemigos[indice].tipoEnemigo)
            {
                case Enemigo.tipo.Nor:
                    Enemigo.NorTotal.Add(enemigos[indice]);
                    break;
                case Enemigo.tipo.Kami:
                    Enemigo.KamiTotal.Add(enemigos[indice]);
                    break;
                case Enemigo.tipo.Sid:
                    Enemigo.SidTotal.Add(enemigos[indice]);
                    break;
                case Enemigo.tipo.HiNor:
                    Enemigo.HiNorTotal.Add(enemigos[indice]);
                    break;
                case Enemigo.tipo.HiKami:
                    Enemigo.HiKamiTotal.Add(enemigos[indice]);
                    break;
                case Enemigo.tipo.HiSid:
                    Enemigo.HiSidTotal.Add(enemigos[indice]);
                    break;
            }
            enemigos.RemoveAt(indice);
        }

        //Eliminar Jefes
        public static void eliminarJefes()
        {

            if (jefes.Count > 0)
            {
                for (i = 0; i < jefes.Count; i++)
                {
                    if (jefes[i].animaciones.Count > 0)
                    {
                        for (j = 0; j < jefes[i].animaciones.Count; j++)
                        {
                            if (jefes[i].animaciones[j].tipoActual ==
                                Animation.tipoAnimacion.DisparoANave)
                            {
                                jefes[i].animaciones[j].animacionFinalizada = false;
                                Animation.animacionesDispANaveTotal.Add(jefes[i].animaciones[j]);
                                jefes[i].animaciones.Remove(jefes[i].animaciones[j]);
                            }
                        }
                    }

                    if (jefes[i].disparos.Count > 0)
                    {
                        for (j = 0; j < jefes[i].disparos.Count; j++)
                        {
                            if (jefes[i].disparos[j].tipoActual ==
                                DisparoJefe.tipo.normal)
                            {
                                Jefe.disparosNormalTotal.Add(jefes[i].disparos[j]);
                            }
                        }
                        for (j = 0; j < jefes[i].disparos.Count; j++)
                        {
                            if (jefes[i].disparos[j].tipoActual ==
                                DisparoJefe.tipo.maximo)
                            {
                                Jefe.disparosMaxTotal.Add(jefes[i].disparos[j]);
                            }
                        }
                    }
                    jefes[i].disparos.Clear();
                    barraVidaJefe.reiniciaColor();
                    jefes[i].reinicia(240, -800, aumentoEnVidaJefe);
                    Jefe.jefesTotales.Add(jefes[i]);
                    jefes.RemoveAt(i);
                }
            }
        }

        /******Dibuja Estadisticas*/
        public static void drawEstadisticas(SpriteBatch sb)
        {
            sb.DrawString(fuente, "DispSid: " + Enemigo.disparosSidTotal.Count,
                new Vector2(0, 25), Color.White);
            sb.DrawString(fuente, "DispNor: " + Enemigo.disparosNorTotal.Count,
                new Vector2(0, 50), Color.White);
            sb.DrawString(fuente, "AnimDisANav: " + Animation.animacionesDispANaveTotal.Count,
                new Vector2(0, 75), Color.White);
            sb.DrawString(fuente, "AnimDisAEne: " + Animation.animacionesDispAEnemigoTotal.Count,
                new Vector2(0, 100), Color.White);
            sb.DrawString(fuente, "DispNave/Iz/De: " + Nave.disparosTotales.Count
                +"/"+Nave.disparosDiagIzquierdaTotales.Count+"/"+Nave.disparosDiagDerechaTotales.Count,
                new Vector2(0, 125), Color.White);
            sb.DrawString(fuente, "AnimEstall: " + Animation.animacionesEstallidoEnemigoTotal.Count,
                new Vector2(0, 150), Color.White);
            sb.DrawString(fuente, "PoderGalact: " + Nave.poderGalacticaTotal.Count,
            new Vector2(0, 175), Color.White);
            sb.DrawString(fuente, "DisparosLax: " + Jefe.disparosMaxTotal.Count+";" +Jefe.disparosNormalTotal.Count,
            new Vector2(0, 200), Color.White);
            sb.DrawString(fuente, "DisparosSuperNav: " + Nave.disparosSuperTotales.Count,
            new Vector2(0, 225), Color.White);
            sb.DrawString(fuente, "PoderHelix: " + Nave.poderHelixTotal.Count,
            new Vector2(0, 250), Color.White);
            sb.DrawString(fuente, "PoderArp: " + Nave.poderArpTotal.Count,
            new Vector2(0, 275), Color.White);
            sb.DrawString(fuente, "PoderPerseus: " + Nave.poderPerseusTotal.Count,
            new Vector2(0, 300), Color.White);
            sb.DrawString(fuente, "PoderPisces: " + Nave.poderPiscesTotal.Count,
            new Vector2(0, 325), Color.White);
            sb.DrawString(fuente, "AnimEstallJefe: " + Animation.animacionesEstallidoJefeTotal.Count,
            new Vector2(0, 350), Color.White);
            if (jefes.Count > 0)
            {
                sb.DrawString(fuente, "VidaJefe: " + jefes[0].vida + "MVJ:" + jefes[0].maxVida, new Vector2(0, 375), Color.White);
            }
            sb.DrawString(fuente, "TotalJefes: " + Jefe.jefesTotales.Count, new Vector2(0, 400), Color.White);
            sb.DrawString(fuente, "PoderCarafe D/I/M: " + Nave.poderCarafeDerechaTotal.Count+"; \n" + 
                Nave.poderCarafeIzquierdaTotal.Count+"; "+Nave.poderCarafeMedioTotal.Count,
                new Vector2(0, 425), Color.White);
            sb.DrawString(fuente, "TlMal: " + Enemigo.NorTotal.Count + "/" 
                                            + Enemigo.KamiTotal.Count + "/"
                                            + Enemigo.SidTotal.Count + "/\n"
                                            + Enemigo.HiNorTotal.Count + "/"
                                            + Enemigo.HiKamiTotal.Count + "/"
                                            + Enemigo.HiSidTotal.Count + "/",
                new Vector2(0, 480), Color.White);
        }

        public static void insertaEnemigo()
        {
            num = 0;
            num2 = 0;

            if (Nave.Puntos > 1500)
            { num = random.Next(1, 21); }
            else
            {num = random.Next(1, 16);}

            if (num == 1 || num == 2 || num == 3 || num == 4 || num == 5 || num == 6)
            {
                num2 = random.Next(1, 3);
                if (num2 == 1)
                {
                    if (Enemigo.NorTotal.Count > 0)
                    {
                        Enemigo.NorTotal[Enemigo.NorTotal.Count - 1].reinicia((-1) * random.Next(100, 500),
                                  random.Next(80, 350),
                                  velocidadEnem, 0, aumentoEnVidaEnemigo);
                        enemigos.Add(Enemigo.NorTotal[Enemigo.NorTotal.Count - 1]);
                        Enemigo.NorTotal.RemoveAt(Enemigo.NorTotal.Count - 1);
                    }
                }
                if (num2 == 2)
                {
                    if (Enemigo.NorTotal.Count > 0)
                    {
                        Enemigo.NorTotal[Enemigo.NorTotal.Count - 1].reinicia(random.Next(1000, 1500),
                                 random.Next(80, 350),
                                 velocidadEnem, 0, aumentoEnVidaEnemigo);
                        enemigos.Add(Enemigo.NorTotal[Enemigo.NorTotal.Count - 1]);
                        Enemigo.NorTotal.RemoveAt(Enemigo.NorTotal.Count - 1);
                    }
                }
            }
            if (num == 7 || num == 8 || num == 9 || num == 10 || num == 11)
            {
                num2 = random.Next(1, 3);
                if (num2 == 1)
                {
                    if (Enemigo.KamiTotal.Count > 0)
                    {
                        Enemigo.KamiTotal[Enemigo.KamiTotal.Count - 1].reinicia((-1) * random.Next(100, 500),
                                   random.Next(80, 350),
                                   velocidadEnem, 0, aumentoEnVidaEnemigo);
                        enemigos.Add(Enemigo.KamiTotal[Enemigo.KamiTotal.Count - 1]);
                        Enemigo.KamiTotal.RemoveAt(Enemigo.KamiTotal.Count - 1);
                    }
                }
                if (num2 == 2)
                {
                    if (Enemigo.KamiTotal.Count > 0)
                    {
                        Enemigo.KamiTotal[Enemigo.KamiTotal.Count - 1].reinicia(random.Next(1000, 1500),
                                  random.Next(80, 350),
                                  velocidadEnem, 0, aumentoEnVidaEnemigo);
                        enemigos.Add(Enemigo.KamiTotal[Enemigo.KamiTotal.Count - 1]);
                        Enemigo.KamiTotal.RemoveAt(Enemigo.KamiTotal.Count - 1);
                    }
                }
            }
            if (num == 12 || num == 13 || num == 14 || num == 15)
            {
                num2 = random.Next(1, 3);
                if (num2 == 1)
                {
                    if (Enemigo.SidTotal.Count > 0)
                    {
                        Enemigo.SidTotal[Enemigo.SidTotal.Count - 1].reinicia((-1) * random.Next(100, 500),
                                  random.Next(80, 280),
                                  velocidadEnem, velocidadEnemY, aumentoEnVidaEnemigo);
                        enemigos.Add(Enemigo.SidTotal[Enemigo.SidTotal.Count - 1]);
                        Enemigo.SidTotal.RemoveAt(Enemigo.SidTotal.Count - 1);
                    }
                }
                if (num2 == 2)
                {
                    if (Enemigo.SidTotal.Count > 0)
                    {
                        Enemigo.SidTotal[Enemigo.SidTotal.Count - 1].reinicia(random.Next(1000, 1500),
                                                  random.Next(80, 280),
                                                  velocidadEnem, velocidadEnemY, aumentoEnVidaEnemigo);
                        enemigos.Add(Enemigo.SidTotal[Enemigo.SidTotal.Count - 1]);
                        Enemigo.SidTotal.RemoveAt(Enemigo.SidTotal.Count - 1);
                    }
                }
            }

            if (Nave.Puntos > 1500)
            {
                if (num == 16 || num == 17)
                {
                    num2 = random.Next(1, 3);
                    if (num2 == 1)
                    {
                        if (Enemigo.HiNorTotal.Count > 0)
                        {
                            Enemigo.HiNorTotal[Enemigo.HiNorTotal.Count - 1].reinicia((-1) * random.Next(100, 500),
                                                    random.Next(80, 350),
                                                    velocidadHiEnem, 0, aumentoEnVidaEnemigo);
                            enemigos.Add(Enemigo.HiNorTotal[Enemigo.HiNorTotal.Count - 1]);
                            Enemigo.HiNorTotal.RemoveAt(Enemigo.HiNorTotal.Count - 1);
                        }
                    }
                    if (num2 == 2)
                    {
                        if (Enemigo.HiNorTotal.Count > 0)
                        {
                            Enemigo.HiNorTotal[Enemigo.HiNorTotal.Count - 1].reinicia(random.Next(1000, 1500),
                                                     random.Next(80, 350),
                                                     velocidadHiEnem, 0, aumentoEnVidaEnemigo);
                            enemigos.Add(Enemigo.HiNorTotal[Enemigo.HiNorTotal.Count - 1]);
                            Enemigo.HiNorTotal.RemoveAt(Enemigo.HiNorTotal.Count - 1);
                        }
                    }
                }
                if (num == 18 || num == 19)
                {
                    num2 = random.Next(1, 3);
                    if (num2 == 1)
                    {
                        if (Enemigo.HiKamiTotal.Count > 0)
                        {
                            Enemigo.HiKamiTotal[Enemigo.HiKamiTotal.Count - 1].reinicia((-1) * random.Next(100, 500),
                                                     random.Next(80, 350),
                                                     velocidadHiEnem, 0, aumentoEnVidaEnemigo);
                            enemigos.Add(Enemigo.HiKamiTotal[Enemigo.HiKamiTotal.Count - 1]);
                            Enemigo.HiKamiTotal.RemoveAt(Enemigo.HiKamiTotal.Count - 1);
                        }
                    }
                    if (num2 == 2)
                    {
                        if (Enemigo.HiKamiTotal.Count > 0)
                        {
                            Enemigo.HiKamiTotal[Enemigo.HiKamiTotal.Count - 1].reinicia(random.Next(1000, 1500),
                                                     random.Next(80, 350),
                                                     velocidadHiEnem, 0, aumentoEnVidaEnemigo);
                            enemigos.Add(Enemigo.HiKamiTotal[Enemigo.HiKamiTotal.Count - 1]);
                            Enemigo.HiKamiTotal.RemoveAt(Enemigo.HiKamiTotal.Count - 1);
                        }
                    }
                }
                if (num == 20)
                {
                    num2 = random.Next(1, 3);
                    if (num2 == 1)
                    {
                        if (Enemigo.HiSidTotal.Count > 0)
                        {
                            Enemigo.HiSidTotal[Enemigo.HiSidTotal.Count - 1].reinicia((-1) * random.Next(100, 500),
                                                     random.Next(80, 280),
                                                     velocidadHiEnem, velocidadHiEnemY, aumentoEnVidaEnemigo);
                            enemigos.Add(Enemigo.HiSidTotal[Enemigo.HiSidTotal.Count - 1]);
                            Enemigo.HiSidTotal.RemoveAt(Enemigo.HiSidTotal.Count - 1);
                        }
                    }
                    if (num2 == 2)
                    {
                        if (Enemigo.HiSidTotal.Count > 0)
                        {
                            Enemigo.HiSidTotal[Enemigo.HiSidTotal.Count - 1].reinicia(random.Next(1000, 1500),
                                                      random.Next(80, 280),
                                                      velocidadHiEnem, velocidadHiEnemY, aumentoEnVidaEnemigo);
                            enemigos.Add(Enemigo.HiSidTotal[Enemigo.HiSidTotal.Count - 1]);
                            Enemigo.HiSidTotal.RemoveAt(Enemigo.HiSidTotal.Count - 1);
                        }
                    }
                }
            }
        }

        public static void LoadEnemigos()
        {
            int num = 0;
            int num2 = 0;
            for (int i = 0; i < 2; i++)
            {
                num = random.Next(1, 4);
                if (num == 1)
                {
                    num2 = random.Next(1, 3);
                    if (num2 == 1)
                    {
                        if (Enemigo.NorTotal.Count > 0)
                        {
                            Enemigo.NorTotal[Enemigo.NorTotal.Count - 1].reinicia((-1) * random.Next(100, 500),
                                                          random.Next(80, 350),
                                                          4, 0, aumentoEnVidaEnemigo);
                            enemigos.Add(Enemigo.NorTotal[Enemigo.NorTotal.Count - 1]);
                            Enemigo.NorTotal.RemoveAt(Enemigo.NorTotal.Count - 1);
                        }
                    }
                    if (num2 == 2)
                    {
                        if (Enemigo.NorTotal.Count > 0)
                        {
                            Enemigo.NorTotal[Enemigo.NorTotal.Count - 1].reinicia(random.Next(1000, 1500),
                                                         random.Next(80, 350),
                                                         4, 0, aumentoEnVidaEnemigo);
                            enemigos.Add(Enemigo.NorTotal[Enemigo.NorTotal.Count - 1]);
                            Enemigo.NorTotal.RemoveAt(Enemigo.NorTotal.Count - 1);
                        }
                    }
                }
                if (num == 2)
                {
                    num2 = random.Next(1, 3);
                    if (num2 == 1)
                    {
                        if (Enemigo.KamiTotal.Count > 0)
                        {
                            Enemigo.KamiTotal[Enemigo.KamiTotal.Count - 1].reinicia((-1) * random.Next(100, 500),
                                                           random.Next(80, 350),
                                                           4, 0, aumentoEnVidaEnemigo);
                            enemigos.Add(Enemigo.KamiTotal[Enemigo.KamiTotal.Count - 1]);
                            Enemigo.KamiTotal.RemoveAt(Enemigo.KamiTotal.Count - 1);
                        }
                    }
                    if (num2 == 2)
                    {
                        if (Enemigo.KamiTotal.Count > 0)
                        {
                            Enemigo.KamiTotal[Enemigo.KamiTotal.Count - 1].reinicia(random.Next(1000, 1500),
                                                          random.Next(80, 350),
                                                          4, 0, aumentoEnVidaEnemigo);
                            enemigos.Add(Enemigo.KamiTotal[Enemigo.KamiTotal.Count - 1]);
                            Enemigo.KamiTotal.RemoveAt(Enemigo.KamiTotal.Count - 1);
                        }
                    }
                }
                if (num == 3)
                {
                    num2 = random.Next(1, 3);
                    if (num2 == 1)
                    {
                        if (Enemigo.SidTotal.Count > 0)
                        {
                            Enemigo.SidTotal[Enemigo.SidTotal.Count - 1].reinicia((-1) * random.Next(100, 500),
                                                          random.Next(80, 280),
                                                          4, 4, aumentoEnVidaEnemigo);
                            enemigos.Add(Enemigo.SidTotal[Enemigo.SidTotal.Count - 1]);
                            Enemigo.SidTotal.RemoveAt(Enemigo.SidTotal.Count - 1);
                        }
                    }
                    if (num2 == 2)
                    {
                        if (Enemigo.SidTotal.Count > 0)
                        {
                            Enemigo.SidTotal[Enemigo.SidTotal.Count - 1].reinicia(random.Next(1000, 1500),
                                                         random.Next(80, 280),
                                                         4, 4, aumentoEnVidaEnemigo);
                            enemigos.Add(Enemigo.SidTotal[Enemigo.SidTotal.Count - 1]);
                            Enemigo.SidTotal.RemoveAt(Enemigo.SidTotal.Count - 1);
                        }
                    }
                }

            }
        }

    }
}


/*
 * if (Nave.Vida > Nave.MaxVida * 0.50) { colorLife = Color.LightGreen; }
 * if (Nave.Vida > Nave.MaxVida * 0.20 && Nave.Vida <= Nave.MaxVida * 0.50)
 * { colorLife = Color.Yellow; }
 * if (Nave.Vida <= Nave.MaxVida * 0.20) { colorLife = Color.Red; }
 * if (Nave.Vida <= 0) { Nave.Vida = 0; }*/