using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Devices.Sensors;
using Microsoft.Xna.Framework.Input.Touch;

namespace Galactica
{  
    class Nave : GameComun
    {
        private static List<Texture2D> texNaves;
        private float Rotacion;
        public static Vector2 auxPosition=new Vector2();
        public  static Vector2 PositionNav=new Vector2();
        public static List<Disparo> disparosTotales;
        public static List<Disparo> disparosDiagIzquierdaTotales;
        public static List<Disparo> disparosDiagDerechaTotales;
        public static List<Disparo> disparosSuperTotales;
        public static List<Disparo> disparos;
        public static List<int> collitionX=new List<int>();
        public static List<int> collitionY=new List<int>();
        public static int radioCollitionN;
        public static int TexturaNave;
        public static int MaxVida,Destruidos;
        public static float Vida;
        public static int Puntos;
        public static bool disparosActivos;
        public static bool poderActivo;
        public static List<Poderes> poderGalacticaTotal,poderGalactica,poderCarafeMedioTotal;
        public static List<Poderes> poderCarafeIzquierdaTotal,poderCarafeDerechaTotal,poderCarafe;
        public static List<Poderes> poderHelixTotal,poderHelix,poderArpTotal,poderArp;
        public static List<Poderes> poderPerseusTotal,poderPerseus, poderPiscesTotal,poderPisces;
        public static double ultimaActualizacion,ultimoTPoder;
        public static int contadorPoderGalactica,contadorPoderHelix,contadorPoderArp;
        public static int contadorPoderCarafe,contadorPoderPerseus,contadorPoderPisces;
        public static Animation animacionPoderGalactica,animacionPoderCarafe,animacionPoderHelix;
        public static Animation animacionPoderArp,animacionPoderPerseus,animacionPoderPisces;
        private int contadorToques;
        public static bool permiteColisionesJefe;
        public TouchCollection touches;
        public static double tiempoAyudaPorColision;
        public static float EscalaN;
        public static float escalaInicial, actualFrameAyuda;
        private int i,j;
        public static bool disparosDiagActivos, disparoSuperActivo;
        public static int velocidadNave;
        public static bool movEnY,estallaActivo;
        public static int limiteShot, masPoderDisp;
        public static bool sePuedeDispDiag, sePuedeDispSuper;

        public Nave()
        {
            iniciaAnimacionesPoder();
            poderActivo = false;
            movEnY = true;
            disparosDiagActivos = false;
            disparoSuperActivo = false;
            disparosActivos = true;
            sePuedeDispDiag = false;
            sePuedeDispSuper = false;
            setOrigen();
            velocidadNave = 8;
            tiempoAyudaPorColision = 0;
            actualFrameAyuda = 0;
            contadorPoderGalactica = 0;contadorPoderCarafe = 0;contadorPoderHelix = 0;
            contadorPoderArp = 0; contadorPoderPerseus = 0; contadorPoderPisces = 0;
            actualframe = 0;contadorToques=0;
            Rotacion = MathHelper.ToRadians(0);
            EscalaN = escalaInicial= 0.94f;
            radioCollitionN = (int)(10 * EscalaN);
            Vida = MaxVida;
            Puntos = 0;
            ultimoTPoder = 0;
            permiteColisionesJefe = true;
            puntoInicio(Vector2.Zero);
            defineCollitionBox();
        }

        public static void reiniciar()
        {
            poderActivo = false;
            disparosActivos = true;
            movEnY = true;
            disparosDiagActivos = false;
            disparoSuperActivo = false;
            tiempoAyudaPorColision = 0;
            actualFrameAyuda = 0;
            velocidadNave = 8;
            contadorPoderGalactica = 0; contadorPoderCarafe = 0; contadorPoderHelix = 0;
            contadorPoderArp = 0; contadorPoderPerseus = 0; contadorPoderPisces = 0;
            EscalaN = escalaInicial = 0.94f;
            radioCollitionN = (int)(10 * EscalaN);
            Vida = MaxVida;
            Puntos = 0;
            Destruidos = 0;
            ultimoTPoder = 0;
            permiteColisionesJefe = true;
            estallaActivo = true;
            puntoInicio(Vector2.Zero);
            reiniciaPoderes();
            reiniciarDisparos();
            Animation.animacionesEstallidoNave.animacionFinalizada = false;
        }

        public static void load(Textura t, SpriteFont f)
        {
            MaxVida = 100;
            Destruidos = 0;
            disparos = new List<Disparo>();
            disparosTotales = new List<Disparo>();
            loadDisparos();
            SetTexturas(t);
            loadPoder();
        }

        public void update(GameTime gameTime)
        {
            touches = TouchControl.touches;
            velocidadAnimacion(gameTime, 3);
            analisisPos();
            setTrozoAnimacion();
            setOrigen();
            cicloDisparos(gameTime);
            updatePoder(gameTime);
            animacionPoderGalactica.update(gameTime);
            animacionPoderCarafe.update(gameTime);
            animacionPoderHelix.update(gameTime);
            animacionPoderArp.update(gameTime);
            animacionPoderPerseus.update(gameTime);
            animacionPoderPisces.update(gameTime);
            updateColisionesJefe(gameTime);
            if (Vida <= 0 && estallaActivo) 
            { 
                (Audio.getEfecto("Estallido")).Play();
                estallaActivo = false;
            }
        }

        public void draw(SpriteBatch spriteBatch)
        {
            dibujaDisparo(spriteBatch);
            drawPoder(spriteBatch);
            animacionPoderGalactica.draw(spriteBatch);
            animacionPoderCarafe.draw(spriteBatch);
            animacionPoderHelix.draw(spriteBatch);
            animacionPoderArp.draw(spriteBatch);
            animacionPoderPerseus.draw(spriteBatch);
            animacionPoderPisces.draw(spriteBatch);

            spriteBatch.Draw(texNaves[TexturaNave],PositionNav,TrozoAnim,Color.White,Rotacion,
                Origen,EscalaN,SpriteEffects.None,0.5f);
        }

/********************************************************/
        //DETECTOR DE COLISIONES
        public void defineCollitionBox()
        {
            collitionX = new List<int>();
            collitionY = new List<int>();
            //Centros de los circulos de colision
            //suma necesaria a partir de la pos actual de la nave
            collitionX.Add((int)(-1 * EscalaN)); collitionY.Add((int)(-36 * EscalaN));
            collitionX.Add((int)(-10 * EscalaN)); collitionY.Add((int)(-18 * EscalaN));
            collitionX.Add((int)(-20 * EscalaN)); collitionY.Add((int)(-2 * EscalaN));
            collitionX.Add((int)(-26 * EscalaN)); collitionY.Add((int)(16 * EscalaN));
            collitionX.Add((int)(-34 * EscalaN)); collitionY.Add((int)(33 * EscalaN));
            collitionX.Add((int)(-9 * EscalaN)); collitionY.Add((int)(17 * EscalaN));
            collitionX.Add((int)(9 * EscalaN)); collitionY.Add((int)(17 * EscalaN));
            collitionX.Add((int)(34 * EscalaN)); collitionY.Add((int)(33 * EscalaN));
            collitionX.Add((int)(26 * EscalaN)); collitionY.Add((int)(16 * EscalaN));
            collitionX.Add((int)(18 * EscalaN)); collitionY.Add((int)(-2 * EscalaN));
            collitionX.Add((int)(8 * EscalaN)); collitionY.Add((int)(-18 * EscalaN));
        }

        // define la posicion
        public static void setPosition(Vector2 position)
        {
            PositionNav.X = position.X;
            PositionNav.Y = position.Y;
        }
        //obtiene la posicion
        public Vector2 getPosition()
        {
            return PositionNav;
        }
        //carga la textura de la nave
        public static void SetTexturas(Textura t)
        {
            texNaves=new List<Texture2D>();
            texNaves.Add(t.getTextura("NGalactica"));//0
            texNaves.Add(t.getTextura("NCarafe"));//1
            texNaves.Add(t.getTextura("NHelix"));//2
            texNaves.Add(t.getTextura("NArp"));//3
            texNaves.Add(t.getTextura("NPerseus"));//4
            texNaves.Add(t.getTextura("NPisces"));//5
        }
        //define el trozo de la animacion a mostrar
        //el cuadro que debe mostrar en el siguiente draw
        public void setTrozoAnimacion()
        {
            TrozoAnim.X = actualframe * 100;
            TrozoAnim.Y = 0;
            TrozoAnim.Width = 100;
            TrozoAnim.Height = 110;
        }
        //define el origen de coordenadas 
        //del cuadro de animacion actual
        public void setOrigen()
        {
            Origen.X =TrozoAnim.Width/2;
            Origen.Y =TrozoAnim.Height/2;
        }
        //define el punto de inicio de la nave
        public static void puntoInicio(Vector2 pos)
        {
            if (pos.X == 0 && pos.Y == 0)
            {
                auxPosition.X = 480 / 2;
                auxPosition.Y = 750;
            }
            else
            {
                auxPosition.X = pos.X;
                auxPosition.Y = pos.Y;
            }

            setPosition(auxPosition);
        }

        //**********************Reinicia Disparos***************************
        public static void reiniciarDisparos()
        {
            for (int i = 0; i < disparos.Count; i++)
            {
                if (disparos[i].tipoActual == Disparo.tipo.pequed ||
                    disparos[i].tipoActual == Disparo.tipo.pequei
                    )
                {
                    disparosTotales.Add(disparos[i]);
                }else
                if (disparos[i].tipoActual == Disparo.tipo.diagD)
                {
                    disparosDiagDerechaTotales.Add(disparos[i]);
                }else
                if (disparos[i].tipoActual == Disparo.tipo.diagI)
                {
                    disparosDiagIzquierdaTotales.Add(disparos[i]);
                }else
                if (disparos[i].tipoActual == Disparo.tipo.super)
                {
                    disparosSuperTotales.Add(disparos[i]);
                }
            }
            disparos.Clear();
        }

        //**********************Reinicia poderes****************************
        public static void reiniciaPoderes()
        {
            //Galactica
            for (int j = 0; j < poderGalactica.Count; j++)
            {
                poderGalacticaTotal.Add(poderGalactica[j]);
            }
            poderGalactica.Clear();
            poderActivo = false;
            animacionPoderGalactica.TerminaAnimacion(true);
            contadorPoderGalactica = 0;

            //Carafe
            for (int j = 0; j < poderCarafe.Count; j++)
            {
                poderCarafe[j].valAuxCarafe = 10;
                if (poderCarafe[j].tipoPoderCarafeActual == Poderes.tipoPoderCarafe.pMedio)
                {
                    poderCarafeMedioTotal.Add(poderCarafe[j]);
                }else
                if (poderCarafe[j].tipoPoderCarafeActual == Poderes.tipoPoderCarafe.pIzquierda)
                {
                    poderCarafeIzquierdaTotal.Add(poderCarafe[j]);
                }else
                if (poderCarafe[j].tipoPoderCarafeActual == Poderes.tipoPoderCarafe.pDerecha)
                {
                    poderCarafeDerechaTotal.Add(poderCarafe[j]);
                }
            }
            poderCarafe.Clear();
            animacionPoderCarafe.TerminaAnimacion(true);
            contadorPoderCarafe = 0;

            //Helix
            for (int j = 0; j < poderHelix.Count; j++)
            {
                poderHelixTotal.Add(poderHelix[j]);
            }
            poderHelix.Clear();
            animacionPoderHelix.TerminaAnimacion(true);
            contadorPoderHelix = 0;
            ultimoTPoder = 0;

            //Arp
            for (int j = 0; j < poderArp.Count; j++)
            {
                poderArpTotal.Add(poderArp[j]);
            }
            poderArp.Clear();
            animacionPoderArp.TerminaAnimacion(true);
            contadorPoderArp = 0;

            //Perseus
            for (int j = 0; j < poderPerseus.Count; j++)
            {
                poderPerseus[j].Escala = 0.1f;
                poderPerseusTotal.Add(poderPerseus[j]);
            }
            poderPerseus.Clear();
            animacionPoderPerseus.TerminaAnimacion(true);
            contadorPoderPerseus = 0;

            //Pisces
            for (int j = 0; j < poderPisces.Count; j++)
            {
                poderPiscesTotal.Add(poderPisces[j]);
            }
            poderPisces.Clear();
            animacionPoderPisces.TerminaAnimacion(true);
            contadorPoderPisces = 0;
            
        }

        //dibuja los disparos de la nave
        public void dibujaDisparo(SpriteBatch spriteBatch)
        {
            if (disparos.Count > 0)
            {
                for (i = 0; i < disparos.Count; i++)
                { disparos[i].draw(spriteBatch); }
            }
        }
        //define la velocidad de la animacion de la nave
        public void velocidadAnimacion(GameTime gameTime,int velocidad)
        {
            dato += gameTime.ElapsedGameTime;
            if (dato.TotalMilliseconds >= 
                ((gameTime.ElapsedGameTime.Milliseconds) * velocidad))
            {
                actualframe = actualframe + 1;
                dato = TimeSpan.Zero;
            }
            if (actualframe >= 3) { actualframe = 0; }
        }
        //hace todas las comprobaciones con disparos
        public void cicloDisparos(GameTime gameTime)
        {
            //touches = TouchPanel.GetState();
            if (touches.Count > 0)
            {
                for (i = 0; i < touches.Count; i++)
                {
                    if (TouchLocationState.Pressed.Equals(touches[i].State) && 
                        touches[i].Position.X>limiteShot && disparosActivos)
                    {
                        (Audio.getEfecto("disparo")).Play();

                        contadorToques++;
                        if (disparosTotales.Count > 0)
                        {
                            disparosTotales.ElementAt(disparosTotales.Count - 1).setPositionPequed(PositionNav);
                            disparos.Add(disparosTotales[disparosTotales.Count - 1]);
                            disparosTotales.RemoveAt(disparosTotales.Count - 1);

                            disparosTotales.ElementAt(disparosTotales.Count - 1).setPositionPequei(PositionNav);
                            disparos.Add(disparosTotales[disparosTotales.Count - 1]);
                            disparosTotales.RemoveAt(disparosTotales.Count - 1);
                        }

                        if (disparosDiagActivos)
                        {
                            if (disparosDiagDerechaTotales.Count > 0)
                            {
                                disparosDiagDerechaTotales.ElementAt(disparosDiagDerechaTotales.Count - 1)
                                    .setPositionPequed(PositionNav);
                                disparos.Add(disparosDiagDerechaTotales[disparosDiagDerechaTotales.Count - 1]);
                                disparosDiagDerechaTotales.RemoveAt(disparosDiagDerechaTotales.Count - 1);
                            }

                            if (disparosDiagIzquierdaTotales.Count > 0)
                            {
                                disparosDiagIzquierdaTotales.ElementAt(disparosDiagIzquierdaTotales.Count - 1)
                                    .setPositionPequei(PositionNav);
                                disparos.Add(disparosDiagIzquierdaTotales[disparosDiagIzquierdaTotales.Count - 1]);
                                disparosDiagIzquierdaTotales.RemoveAt(disparosDiagIzquierdaTotales.Count - 1);
                            }
                        }

                        if (contadorToques >= 3 && disparoSuperActivo)
                        {
                            if (disparosSuperTotales.Count > 0)
                            {
                                disparosSuperTotales.ElementAt(disparosSuperTotales.Count - 1)
                                    .setPositionSuper(PositionNav);
                                disparos.Add(disparosSuperTotales[disparosSuperTotales.Count - 1]);
                                disparosSuperTotales.RemoveAt(disparosSuperTotales.Count - 1);
                            }
                            contadorToques = 0;
                        }
                    }
                }
            }

            if (disparos.Count > 0)
            {
                for (i = 0; i < disparos.Count; i++)
                {
                    disparos[i].update(gameTime);
                }
            }

            if (disparos.Count > 0)
            {
                for (i = 0; i < disparos.Count; i++)
                {
                    if (disparos[i].getPosition().Y < 0 || 
                        disparos[i].getPosition().X <0 ||
                        disparos[i].getPosition().X >480
                        )
                    {
                        if(disparos[i].tipoActual== Disparo.tipo.pequed ||
                            disparos[i].tipoActual== Disparo.tipo.pequei
                           )
                        {
                            disparosTotales.Add(disparos[i]);
                            disparos.Remove(disparos[i]);
                        }else
                        if (disparos[i].tipoActual == Disparo.tipo.diagD)
                        {
                            disparosDiagDerechaTotales.Add(disparos[i]);
                            disparos.Remove(disparos[i]);
                        }else
                        if (disparos[i].tipoActual == Disparo.tipo.diagI)
                        {
                            disparosDiagIzquierdaTotales.Add(disparos[i]);
                            disparos.Remove(disparos[i]);
                        }else
                        if (disparos[i].tipoActual == Disparo.tipo.super)
                        {
                            disparosSuperTotales.Add(disparos[i]);
                            disparos.Remove(disparos[i]);
                        }

                    }
                }
            }
        }
        //analiza la pos de la nave y sus limites
        //tambien el movimiento de acuerdo al acelerometro
        public void analisisPos()
        {
            if (auxPosition.X >= 480)
            { auxPosition.X = 480; }
            if (auxPosition.X <= 0)
            { auxPosition.X = 0; }
            if (auxPosition.Y >= 770 )
            { auxPosition.Y = 770; }
            if (auxPosition.Y <= 0 )
            { auxPosition.Y = 0; }

            setPosition(auxPosition);
        }

        /************************Poder *******************/
        /* Carga Poder */
        public static void loadPoder()
        {
            poderGalacticaTotal = new List<Poderes>();
            poderGalactica = new List<Poderes>();
            poderCarafeMedioTotal = new List<Poderes>();
            poderCarafeIzquierdaTotal = new List<Poderes>();
            poderCarafeDerechaTotal = new List<Poderes>();
            poderCarafe=new List<Poderes>();
            poderHelixTotal = new List<Poderes>();
            poderHelix = new List<Poderes>();
            poderArpTotal = new List<Poderes>();
            poderArp = new List<Poderes>();
            poderPerseusTotal = new List<Poderes>();
            poderPerseus = new List<Poderes>();
            poderPiscesTotal = new List<Poderes>();
            poderPisces = new List<Poderes>();

            for (int i = 0; i < 300; i++)
            {
                poderGalacticaTotal.Add(new Poderes(Vector2.Zero,
                    Poderes.tipo.pGalactica,Poderes.tipoPoderCarafe.pNull));
                poderCarafeMedioTotal.Add(new Poderes(Vector2.Zero,
                    Poderes.tipo.pCarafe, Poderes.tipoPoderCarafe.pMedio));
                poderCarafeIzquierdaTotal.Add(new Poderes(Vector2.Zero,
                    Poderes.tipo.pCarafe, Poderes.tipoPoderCarafe.pIzquierda));
                poderCarafeDerechaTotal.Add(new Poderes(Vector2.Zero,
                    Poderes.tipo.pCarafe, Poderes.tipoPoderCarafe.pDerecha));
                poderHelixTotal.Add(new Poderes(Vector2.Zero,
                    Poderes.tipo.pHelix, Poderes.tipoPoderCarafe.pNull));
                poderArpTotal.Add(new Poderes(Vector2.Zero,
                    Poderes.tipo.pArp, Poderes.tipoPoderCarafe.pNull));
                poderPerseusTotal.Add(new Poderes(Vector2.Zero,
                    Poderes.tipo.pPerseus, Poderes.tipoPoderCarafe.pNull));
                poderPiscesTotal.Add(new Poderes(Vector2.Zero,
                    Poderes.tipo.pPisces, Poderes.tipoPoderCarafe.pNull));
            }
        }

        /* Actualizar poder */
        public void updatePoder(GameTime time)
        {
            if (poderActivo)
            {
                switch(TexturaNave)
                {
                    case 0:
                        animacionPoderGalactica.TerminaAnimacion(false);
                        if (contadorPoderGalactica < 200)
                        {
                            if (poderGalacticaTotal.Count > 0)
                            {
                                poderGalacticaTotal.ElementAt(poderGalacticaTotal.Count - 1)
                                    .setPosition(PositionNav);
                                poderGalactica.Add(poderGalacticaTotal[poderGalacticaTotal.Count - 1]);
                                poderGalacticaTotal.RemoveAt(poderGalacticaTotal.Count - 1);
                            }
                                contadorPoderGalactica++;
                        }

                        for (j = 0; j < poderGalactica.Count; j++)
                        {
                              poderGalactica[j].update(time);
                        }

                        for (j = 0; j < poderGalactica.Count; j++)
                        {
                            if (poderGalactica[j].getPosition().Y < (-20) )
                            {
                                poderGalacticaTotal.Add(poderGalactica[j]);
                                poderGalactica.RemoveAt(j);
                            }
                        }

                        if (poderGalactica.Count <= 0) 
                        { 
                            poderActivo = false;
                            animacionPoderGalactica.TerminaAnimacion(true);
                            contadorPoderGalactica = 0;
                        }

                        break;

                    case 1:
                        animacionPoderCarafe.TerminaAnimacion(false);
                        if (contadorPoderCarafe < 200)
                        {
                            if (poderCarafeMedioTotal.Count > 0)
                            {
                                poderCarafeMedioTotal.ElementAt(poderCarafeMedioTotal.Count - 1)
                                    .setPosition(PositionNav);
                                poderCarafe.Add(poderCarafeMedioTotal[poderCarafeMedioTotal.Count - 1]);
                                poderCarafeMedioTotal.RemoveAt(poderCarafeMedioTotal.Count - 1);
                            }

                            if (poderCarafeIzquierdaTotal.Count > 0)
                            {
                                poderCarafeIzquierdaTotal.ElementAt(poderCarafeIzquierdaTotal.Count - 1)
                                    .setPosition(PositionNav);
                                poderCarafe.Add(poderCarafeIzquierdaTotal[poderCarafeIzquierdaTotal.Count - 1]);
                                poderCarafeIzquierdaTotal.RemoveAt(poderCarafeIzquierdaTotal.Count - 1);
                            }

                            if (poderCarafeDerechaTotal.Count>0)
                            {
                                poderCarafeDerechaTotal.ElementAt(poderCarafeDerechaTotal.Count - 1)
                                    .setPosition(PositionNav);
                                poderCarafe.Add(poderCarafeDerechaTotal[poderCarafeDerechaTotal.Count - 1]);
                                poderCarafeDerechaTotal.RemoveAt(poderCarafeDerechaTotal.Count - 1);
                            }

                            contadorPoderCarafe++;
                        }

                        for (j = 0; j < poderCarafe.Count; j++)
                        {
                            poderCarafe[j].update(time);
                        }

                        for (j = 0; j < poderCarafe.Count; j++)
                        {
                            if (poderCarafe[j].getPosition().Y < (0) ||
                                poderCarafe[j].getPosition().X < (0) ||
                                poderCarafe[j].getPosition().X > (480))
                            {
                                poderCarafe[j].valAuxCarafe = 10;
                                if (poderCarafe[j].tipoPoderCarafeActual == Poderes.tipoPoderCarafe.pMedio)
                                {
                                    poderCarafeMedioTotal.Add(poderCarafe[j]);
                                }else
                                if (poderCarafe[j].tipoPoderCarafeActual == Poderes.tipoPoderCarafe.pIzquierda)
                                {
                                    poderCarafeIzquierdaTotal.Add(poderCarafe[j]);
                                }else
                                if (poderCarafe[j].tipoPoderCarafeActual == Poderes.tipoPoderCarafe.pDerecha)
                                {
                                    poderCarafeDerechaTotal.Add(poderCarafe[j]);
                                }
                                poderCarafe.RemoveAt(j);
                            }
                        }

                        if (poderCarafe.Count == 0)
                        { 
                            poderActivo = false;
                            animacionPoderCarafe.TerminaAnimacion(true);
                            contadorPoderCarafe = 0;
                        }
                        break;

                    case 2:
                        animacionPoderHelix.TerminaAnimacion(false);
                        if (time.TotalGameTime.TotalMilliseconds > ultimoTPoder + 1000)
                        {
                            if (contadorPoderHelix < 10)
                            {
                                if (poderHelixTotal.Count > 0)
                                {
                                    poderHelixTotal.ElementAt(poderHelixTotal.Count - 1).setPosition(PositionNav);
                                    poderHelix.Add(poderHelixTotal[poderHelixTotal.Count - 1]);
                                    poderHelixTotal.RemoveAt(poderHelixTotal.Count - 1);
                                }
                                contadorPoderHelix++;
                            }
                            ultimoTPoder = time.TotalGameTime.TotalMilliseconds;

                            if (poderHelix.Count <= 0)
                            {
                                poderActivo = false;
                                animacionPoderHelix.TerminaAnimacion(true);
                                contadorPoderHelix = 0;
                                ultimoTPoder = 0;
                            }

                        }

                        for (j = 0; j < poderHelix.Count; j++)
                        {
                              poderHelix[j].update(time);
                        }

                        for (j = 0; j < poderHelix.Count; j++)
                        {
                            if (poderHelix[j].getPosition().Y < (-20) )
                            {
                                poderHelixTotal.Add(poderHelix[j]);
                                poderHelix.RemoveAt(j);
                            }
                        }
                        break;

                    case 3:
                        animacionPoderArp.TerminaAnimacion(false);
                        if (time.TotalGameTime.TotalMilliseconds > ultimoTPoder + 4000)
                        {
                            if (contadorPoderArp < 2)
                            {
                                if (poderArpTotal.Count > 0)
                                {
                                    poderArpTotal.ElementAt(poderArpTotal.Count - 1).setPosition(PositionNav);
                                    poderArp.Add(poderArpTotal[poderArpTotal.Count - 1]);
                                    poderArpTotal.RemoveAt(poderArpTotal.Count - 1);
                                }
                                contadorPoderArp++;
                            }
                            ultimoTPoder = time.TotalGameTime.TotalMilliseconds;

                            if (poderArp.Count <= 0)
                            {
                                poderActivo = false;
                                animacionPoderArp.TerminaAnimacion(true);
                                contadorPoderArp = 0;
                                ultimoTPoder = 0;
                            }

                        }

                        for (j = 0; j < poderArp.Count; j++)
                        {
                            poderArp[j].update(time);
                        }

                        for (j = 0; j < poderArp.Count; j++)
                        {
                            if (poderArp[j].getPosition().Y < (-20))
                            {
                                poderArpTotal.Add(poderArp[j]);
                                poderArp.RemoveAt(j);
                            }
                        }
                        break;

                    case 4:
                        animacionPoderPerseus.TerminaAnimacion(false);
                        if (time.TotalGameTime.TotalMilliseconds > ultimoTPoder + 1000)
                        {
                            if (contadorPoderPerseus < 5)
                            {
                                if (poderPerseusTotal.Count>0)
                                {
                                    poderPerseusTotal.ElementAt(poderPerseusTotal.Count - 1).setPosition(PositionNav);
                                    poderPerseus.Add(poderPerseusTotal[poderPerseusTotal.Count - 1]);
                                    poderPerseusTotal.RemoveAt(poderPerseusTotal.Count - 1);
                                }
                                contadorPoderPerseus++;
                            }
                            ultimoTPoder = time.TotalGameTime.TotalMilliseconds;

                            if (poderPerseus.Count <= 0)
                            {
                                poderActivo = false;
                                animacionPoderPerseus.TerminaAnimacion(true);
                                contadorPoderPerseus = 0;
                                ultimoTPoder = 0;
                            }
                        }

                        for (j = 0; j < poderPerseus.Count; j++)
                        {
                            poderPerseus[j].update(time);
                        }

                        for (j = 0; j < poderPerseus.Count; j++)
                        {
                            if (poderPerseus[j].Escala > 3)
                            {
                                poderPerseus[j].Escala = 0.1f;
                                poderPerseusTotal.Add(poderPerseus[j]);
                                poderPerseus.RemoveAt(j);
                            }
                        }
                        break;

                    case 5:
                        animacionPoderPisces.TerminaAnimacion(false);
                        if (contadorPoderPisces < 200)
                        {
                            if (poderPiscesTotal.Count > 0)
                            {
                                poderPiscesTotal.ElementAt(poderPiscesTotal.Count - 1).setPosition(PositionNav);
                                poderPisces.Add(poderPiscesTotal[poderPiscesTotal.Count - 1]);
                                poderPiscesTotal.RemoveAt(poderPiscesTotal.Count - 1);
                            }
                            contadorPoderPisces++;
                        }

                        for (j = 0; j < poderPisces.Count; j++)
                        {
                            poderPisces[j].update(time);
                        }

                        for (j = 0; j < poderPisces.Count; j++)
                        {
                            if (poderPisces[j].getPosition().Y < (-20) ||
                                poderPisces[j].getPosition().X < (-20) ||
                                poderPisces[j].getPosition().X > (500))
                            {
                                poderPiscesTotal.Add(poderPisces[j]);
                                poderPisces.RemoveAt(j);
                            }
                        }

                        if (poderPisces.Count <= 0) 
                        { 
                            poderActivo = false;
                            animacionPoderPisces.TerminaAnimacion(true);
                            contadorPoderPisces = 0;
                        }
                        break;
                }
            }
        }

        /* Dibuja Poder */
        public void drawPoder(SpriteBatch sb)
        {
            if (poderActivo)
            {
                switch (TexturaNave)
                {
                    case 0:
                        for (j = 0; j < poderGalactica.Count; j++)
                        {
                            poderGalactica[j].draw(sb);
                        }
                        break;

                    case 1:
                        for (j = 0; j < poderCarafe.Count; j++)
                        {
                            poderCarafe[j].draw(sb);
                        }
                        break;

                    case 2:
                        for (j = 0; j < poderHelix.Count; j++)
                        {
                            poderHelix[j].draw(sb);
                        }
                        break;

                    case 3:
                        for (j = 0; j < poderArp.Count; j++)
                        {
                            poderArp[j].draw(sb);
                        }
                        break;

                    case 4:
                        for (j = 0; j < poderPerseus.Count; j++)
                        {
                            poderPerseus[j].draw(sb);
                        }
                        break;
                    case 5:
                        for (j = 0; j < poderPisces.Count; j++)
                        {
                            poderPisces[j].draw(sb);
                        }
                        break;
                }
            }
        }


        public void iniciaAnimacionesPoder()
        {
            animacionPoderGalactica = new Animation(Animation.tipoAnimacion.PoderGalactica);
            animacionPoderGalactica.TerminaAnimacion(true);
            animacionPoderGalactica.setPosition(Vector2.Zero);
            animacionPoderCarafe = new Animation(Animation.tipoAnimacion.PoderCarafe);
            animacionPoderCarafe.TerminaAnimacion(true);
            animacionPoderCarafe.setPosition(Vector2.Zero);
            animacionPoderHelix = new Animation(Animation.tipoAnimacion.PoderHelix);
            animacionPoderHelix.TerminaAnimacion(true);
            animacionPoderHelix.setPosition(Vector2.Zero);
            animacionPoderArp = new Animation(Animation.tipoAnimacion.PoderArp);
            animacionPoderArp.TerminaAnimacion(true);
            animacionPoderArp.setPosition(Vector2.Zero);
            animacionPoderPerseus = new Animation(Animation.tipoAnimacion.PoderPerseus);
            animacionPoderPerseus.TerminaAnimacion(true);
            animacionPoderPerseus.setPosition(Vector2.Zero);
            animacionPoderPisces = new Animation(Animation.tipoAnimacion.PoderPisces);
            animacionPoderPisces.TerminaAnimacion(true);
            animacionPoderPisces.setPosition(Vector2.Zero);
        }


        public void updateColisionesJefe(GameTime gt)
        {
            if (permiteColisionesJefe)
            { tiempoAyudaPorColision = gt.TotalGameTime.TotalMilliseconds; }

            if (!permiteColisionesJefe)
            {

                EscalaN = actualFrameAyuda;

                tiempoAyuda += gt.ElapsedGameTime;
                if (tiempoAyuda.TotalMilliseconds >=
                    ((gt.ElapsedGameTime.Milliseconds) * 2))
                   {
                          actualFrameAyuda = actualFrameAyuda + escalaInicial;
                          tiempoAyuda = TimeSpan.Zero;
                   }

                if (actualFrameAyuda > escalaInicial) { actualFrameAyuda = 0; }

                if (gt.TotalGameTime.TotalMilliseconds > tiempoAyudaPorColision + 2000)
                {
                    EscalaN = escalaInicial;
                    permiteColisionesJefe = true;
                }

            }
        }


        public static void loadDisparos()
        {
            for (int i = 0; i < 150; i++)
            {
                disparosTotales.Add(new Disparo(Vector2.Zero, Disparo.tipo.pequed));
                disparosTotales.Add(new Disparo(Vector2.Zero, Disparo.tipo.pequei));
            }

            disparosDiagDerechaTotales = new List<Disparo>();
            for (int i = 0; i < 100; i++)
            {
                disparosDiagDerechaTotales.Add(new Disparo(Vector2.Zero, Disparo.tipo.diagD));
            }

            disparosDiagIzquierdaTotales = new List<Disparo>();
            for (int i = 0; i < 100; i++)
            {
                disparosDiagIzquierdaTotales.Add(new Disparo(Vector2.Zero, Disparo.tipo.diagI));
            }

            disparosSuperTotales = new List<Disparo>();
            for (int i = 0; i < 100; i++)
            {
                disparosSuperTotales.Add(new Disparo(Vector2.Zero, Disparo.tipo.super));
            }
        }
    }
}
