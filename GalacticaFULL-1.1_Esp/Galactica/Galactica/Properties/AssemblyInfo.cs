using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Resources;

// La información general sobre los ensamblados se controla mediante el 
// conjunto de atributos siguiente. Cambie los valores de estos atributos para modificar la información
// asociada a un ensamblado.
[assembly: AssemblyTitle("Galactica")]
[assembly: AssemblyProduct("Galactica")]
[assembly: AssemblyDescription("Excelentes naves con poderes increíbles y enemigos formidables.")]
[assembly: AssemblyCompany("Julio Sanchez")]
[assembly: AssemblyCopyright("Copyright © Julio Sanchez 2012")]
[assembly: AssemblyTrademark("Galactica by Julio Sanchez")]
[assembly: AssemblyCulture("")]

// Si define ComVisible como false, los tipos del ensamblado no estarán visibles 
// para los componentes COM. Si necesita acceso a un tipo del ensamblado desde 
// COM, defina su atributo ComVisible como true. Solamente los ensamblados de Windows
// admiten COM.
[assembly: ComVisible(false)]

// En Windows, el GUID siguiente es para el id. de typelib si el
// proyecto está expuesto a COM. En otras plataformas, identifica de forma exclusiva el
// contenedor de almacenamiento de títulos al implementar el ensamblado en el dispositivo.
[assembly: Guid("f51107be-ca9a-4874-9f50-8fd8474dd001")]

// La información de la versión de un ensamblado incluye los cuatro valores siguientes:
//
//      Versión principal
//      Versión secundaria 
//      Número de compilación
//      Revisión
//
[assembly: AssemblyVersion("1.1.3.1")]
[assembly: NeutralResourcesLanguageAttribute("es")]
