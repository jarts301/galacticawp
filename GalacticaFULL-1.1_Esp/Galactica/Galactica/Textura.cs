using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;

namespace Galactica
{
    class Textura
    {
     
        private static List<Texture2D> Texturas;
        private static List<string> TexturasNom;
        public static List<Texture2D> TexturasFondo;
        private int i;


        public Textura()
        {
            Texturas = new List<Texture2D>();
            TexturasNom = new List<string>();
            TexturasFondo = new List<Texture2D>();
        }

        public void addTextura(Texture2D textur,string nom)
        {
            Texturas.Add(textur);
            TexturasNom.Add(nom);
        }

        public void addTexturaFondo(Texture2D textur)
        {
            TexturasFondo.Add(textur);
        }

        public Texture2D getTextura(string nombre)
        {
            for (i = 0; i < Texturas.Count; i++)
            {
                if (TexturasNom.ElementAt(i).Equals(nombre))
                {
                    return Texturas.ElementAt(i);
                }
            }

            return null;
        }

    }

}
