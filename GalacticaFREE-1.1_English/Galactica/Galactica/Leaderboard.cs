using Galactica;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Phone.Info;
using Microsoft.Phone.Notification;
using System.ComponentModel;
using Galactica.HighScores;
using Microsoft.Xna.Framework.Input.Touch;

namespace Galactica
{
    class Leaderboard:GameComun
    {
        private static List<Texture2D> textura;
        private static Vector2 positionTitulo,origenTitulo;
        private List<Boton> botones;
        private static SpriteFont font;
        private int i;

        //High Scores
        public static HighScoreClient highScores;
        public static string passcode;
        public static string[] highScoresList;
        //public static int submittedScore;
        //public static TouchCollection touches;
        public static string phoneURI;
        public static string userID;
        public static HttpNotificationChannel channel;
        public static string notificationMessage = string.Empty;

        public Leaderboard()
        {
            botones = new List<Boton>();
            botones.Add(new Boton(Boton.TipoBoton.BBack, 10, 730));
            //botones.Add(new Boton(Boton.TipoBoton.BGuardar, 250, 720));
        }

        public static void load(Textura t, SpriteFont f)
        {
            setTexturas(t);
            loadTitulo();
            font = f;

            //Get the users unique identifier
            string anid = UserExtendedProperties.GetValue("ANID") as string;
            if (anid == null)
            {
                anid = "##12345678912345678912345678912345";
            }
            userID = anid.Substring(2, 32);

            highScores = new HighScoreClient();
            highScores.IdentifyCompleted += new EventHandler<IdentifyCompletedEventArgs>(highScores_IdentifyCompleted);
            highScores.AddScoreCompleted += new EventHandler<AsyncCompletedEventArgs>(highScores_AddScoreCompleted);
            highScores.CurrentHighScoresCompleted += new EventHandler<CurrentHighScoresCompletedEventArgs>(highScores_CurrentHighScoresCompleted);

            CreateNotificationChannel();

        }

        public static void sendHighScore(String nombre, int score)
        {
            highScores.AddScoreAsync(phoneURI, userID, nombre, score, passcode);
        }

        public void update(GameTime time)
        {
            updateEstrellas(time);
            for (i = 0; i < botones.Count; i++)
            {
                botones[i].update(time);
            }

            //Bind the channel to Toast notifcations if it's open and hasn't been already bound
            /*if (channel != null && !channel.IsShellToastBound)
            {
                channel.BindToShellToast();
            }
            
            touches = TouchControl.touches;

            if (touches.Count > 0)
            {
                Random random = new Random();
                submittedScore = random.Next(0, 1000000000);
                highScores.AddScoreAsync(phoneURI, userID, "George", submittedScore, passcode);
            }*/
        }

        public void draw(SpriteBatch sb)
        {
            //CLEAR NEGRO
            sb.Draw(Textura.TexturasFondo[0], Vector2.Zero, null, Color.Black, 0.0f, Vector2.Zero,
                100.0f, SpriteEffects.None, 0.999f);

            drawEstrellas(sb);
            drawTitulo(sb);
            for (i = 0; i < botones.Count; i++)
            {
                botones[i].draw(sb);
            }


            //High Scores
            if (highScoresList != null)
            {
                int YOffset = 0;
                foreach (string score in highScoresList)
                {
                    try
                    {
                        sb.DrawString(font, score, new Vector2(10, 140 + YOffset), Color.Gold, 0.0f, Vector2.Zero, 1.1f,
                                      SpriteEffects.None, 0.01f);
                    }
                    catch (ArgumentException)
                    {
                    }
                    YOffset += 35;
                }
            }

            //sb.DrawString(font, "New Score: " + submittedScore.ToString(), new Vector2(50, 400), Color.White);

            if (passcode == null)
            {
                sb.DrawString(font, "Loading...", new Vector2(10, 700), Color.White);
            }
            else
            {
                //sb.DrawString(font, notificationMessage, new Vector2(50, 430), Color.White);
            }

        }

        /*******************************************/

        /******TITULO************/
        public static void loadTitulo()
        {
            positionTitulo = new Vector2(480/2,100);
            origenTitulo = new Vector2(textura[0].Width/2,textura[0].Height/2);
        }

        public void drawTitulo(SpriteBatch sb)
        {
            sb.Draw(textura[0], positionTitulo, null, Color.White, 0.0f, origenTitulo,
                1.3f, SpriteEffects.None, 0.1f);
        }

        /*********Texturas****************/
        public static void setTexturas(Textura t)
        {
            textura = new List<Texture2D>();
            textura.Add(t.getTextura("TituloLeaderboard"));//0
        }

        static void CreateNotificationChannel()
        {
            channel = HttpNotificationChannel.Find("HighScores");

            if (channel == null)
            {
                channel = new HttpNotificationChannel("HighScores");
                SetUpDelegates();
                channel.Open();
            }
            else
            {
                SetUpDelegates();
            }
        }

        static void SetUpDelegates()
        {
            channel.ChannelUriUpdated += new EventHandler<NotificationChannelUriEventArgs>(channel_ChannelUriUpdated);
            channel.ShellToastNotificationReceived += new EventHandler<NotificationEventArgs>(channel_ShellToastNotificationReceived);
        }

        static void channel_ChannelUriUpdated(object sender, NotificationChannelUriEventArgs e)
        {
            phoneURI = e.ChannelUri.ToString();
            highScores.IdentifyAsync(phoneURI, userID);
        }

        static void channel_ShellToastNotificationReceived(object sender, NotificationEventArgs e)
        {
            if (e.Collection != null)
            {
                Dictionary<string, string> collection = (Dictionary<string, string>)e.Collection;

                foreach (string elementValue in collection.Values)
                {
                    notificationMessage += " " + elementValue;
                }
            }
        }

        static void highScores_CurrentHighScoresCompleted(object sender, CurrentHighScoresCompletedEventArgs e)
        {
            highScoresList = e.Result.Split(":".ToCharArray());
        }

        static void highScores_AddScoreCompleted(object sender, AsyncCompletedEventArgs e)
        {
            highScores.CurrentHighScoresAsync();
        }

        static void highScores_IdentifyCompleted(object sender, IdentifyCompletedEventArgs e)
        {
            passcode = e.Result;
            highScores.CurrentHighScoresAsync();
        }

    }
}
