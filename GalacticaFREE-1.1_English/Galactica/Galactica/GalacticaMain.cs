using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Info;
using Microsoft.Advertising.Mobile.Xna;
using System.Diagnostics;
using System.Device.Location;
using Microsoft.Phone.Notification;
using System.ComponentModel;
using Galactica.HighScores;
using Microsoft.Xna.Framework.Input.Touch;

namespace Galactica
{
    public class GalacticaMain : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        public enum Estado {MainMenu,QuickGame,MisionMode,Options,
                            Help,About,SavedGame,Buy,Leaderboard}
        public static Estado estadoActual;
        private Textura Tex;
        private Audio audio;
        private Acelerometro acelerometro;
        private SpriteFont fuente;
        private Random random;
        private MainMenu mainMenu;
        private QuickMode quickGame;
        private MisionMode misionMode;
        private Opciones opciones;
        private Ayuda ayuda;
        private AcercaDe acercaDe;
        private SavedGame savedGame;
        private Buy buy;
        private Leaderboard leaderboard;
        private static bool enCarga=false, enJuego=false;
        private static double checkTrialModeDelay = 1000.0,introTime=4000.0;
        public static bool isTrialMode = true;
        public static bool dibujaIntro= true,empezarCarga=true;

        private double cont_draw=0,frames=0,ultima_actua=0;
        private double updates = 0, contUpdates = 0;
        public static int BESTSCORE=0;
        public static string NOMBRE;

        public static readonly string ApplicationId ="93934050-be7a-4f4b-b07b-1b98aac58615";/* "test_client";*/
        public static readonly string AdUnitId = "117380";/* "Image480_80";*/
        DrawableAd bannerAd,bannerAd2;
        // We will use this to find the device location for better ad targeting.
        private GeoCoordinateWatcher gcw = null;
        private GeoCoordinateWatcher gcw2 = null;

        //Object dat = DeviceExtendedProperties.GetValue("DeviceTotalMemory");
        //Object dat = DeviceExtendedProperties.GetValue("ApplicationCurrentMemoryUsage");
        public GalacticaMain()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            TargetElapsedTime = TimeSpan.FromSeconds(1.0f/30);

            graphics.PreferredBackBufferWidth = 480;
            graphics.PreferredBackBufferHeight = 800;

            // Allow portrait
            graphics.SupportedOrientations = DisplayOrientation.Portrait;
            graphics.IsFullScreen = true;

            //Guide.SimulateTrialMode = false;
        }

        protected override void Initialize()
        {
            // Initialize the AdGameComponent with your ApplicationId and add it to the game.
            AdGameComponent.Initialize(this, ApplicationId);
            Components.Add(AdGameComponent.Current);
            // Now create an actual ad for display.
            CreateAd();
            CreateAd2();

            base.Initialize();
        }
        
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            Tex = new Textura();
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/Intro"), "Intro");
        }

        public void cargarTodo()
        {
            estadoActual = Estado.MainMenu;
            random = new Random();
            acelerometro = new Acelerometro();
            fuente = Content.Load<SpriteFont>("Moire");
            cargarTexturas();
            cargarAudio();
            //almacenamiento.salvar();

            /*Lo que debe cargar antes que todo*/
            Poderes.load(Tex); Animation.load(Tex); BarraVida.load(Tex); Boton.load(Tex,fuente); Onda.load(Tex);
            Disparo.load(Tex); DisparoEnemigo.load(Tex); Enemigo.load(Tex); Plus.load(Tex);
            Estrella.load(Tex); GameComun.loadEstrellas();MainMenu.load(Tex);Nave.load(Tex, fuente); DisparoJefe.load(Tex);
            Jefe.load(Tex); QuickModeGame.load(Tex, fuente); QuickModePause.load(Tex); NMini.load(Tex);
            QuickModeMenu.load(Tex,fuente);QuickModeGOver.load(Tex,fuente); MisionModeMenu.load(Tex,fuente);
            MisionModeInfo.load(Tex, fuente); MisionModePause.load(Tex); MisionModeGOver.load(Tex,fuente);
            Control.load(Tex); Opciones.load(Tex); Ayuda.load(Tex); AcercaDe.load(Tex); SavedGame.load(Tex);
            Buy.load(Tex, fuente); Leaderboard.load(Tex,fuente);


            mainMenu = new MainMenu();
            quickGame = new QuickMode();
            misionMode = new MisionMode();
            opciones = new Opciones();
            ayuda = new Ayuda();
            acercaDe = new AcercaDe();
            savedGame = new SavedGame();
            buy=new Buy();
            leaderboard = new Leaderboard();

            //Almacenamiento.borrar();
            Almacenamiento.crearDatos();
            Almacenamiento.cargarOpciones();
            Almacenamiento.cargarScore();

        }

        protected override void UnloadContent()
        {

        }

        //*********** UPDATE ************
        protected override void Update(GameTime gameTime)
        {
            if (introTime >= 0)
            {
                introTime -=
                gameTime.ElapsedGameTime.TotalMilliseconds;
                dibujaIntro = true;
                if (introTime <= 0)
                {
                    dibujaIntro = false;
                }
            }
            if (enCarga)
            {
                cargarTodo();
                enJuego = true;
                enCarga = false;
            }
            if (enJuego)
            {
                contUpdates++;
                TouchControl.updateTouches();
                acelerometro.update();

                if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                {
                    procesaBotonBack(gameTime);
                }

                bannerAd.Visible = false;
                bannerAd2.Visible = false;
                switch (estadoActual)
                {
                    case Estado.MainMenu:
                        /*if (isTrialMode)
                        {*/
                            bannerAd.Visible = false;
                        //}
                        mainMenu.update(gameTime, this);
                        break;
                    case Estado.QuickGame:
                        if (QuickMode.estadoActual == QuickMode.Estado.GameOver ||
                            QuickMode.estadoActual == QuickMode.Estado.Pause)
                        {
                            /*if (isTrialMode)
                            {*/
                                bannerAd2.Visible = true;
                            //}
                        }
                        quickGame.update(gameTime);
                        break;
                    case Estado.MisionMode:
                        if (MisionMode.estadoActual == MisionMode.Estado.GameOver ||
                            MisionMode.estadoActual == MisionMode.Estado.Pause)
                        {
                            /*if (isTrialMode)
                            {*/
                                bannerAd2.Visible = true;
                            //}
                        }
                        misionMode.update(gameTime);
                        break;
                    case Estado.Options:
                        opciones.update(gameTime);
                        break;
                    case Estado.Help:
                        ayuda.update(gameTime);
                        break;
                    case Estado.About:
                        acercaDe.update(gameTime);
                        break;
                    case Estado.SavedGame:
                        savedGame.update(gameTime);
                        break;
                    case Estado.Buy:
                        buy.update(gameTime);
                        break;
                    case Estado.Leaderboard:
                        leaderboard.update(gameTime);
                        break;

                }

                //Trial mode comprobacion
                /*if (isTrialMode && checkTrialModeDelay >= 0)
                {
                    checkTrialModeDelay -=
                    gameTime.ElapsedGameTime.TotalMilliseconds;
                    if (checkTrialModeDelay <= 0)
                    {
                        isTrialMode = Guide.IsTrialMode;
                    }
                }*/


                //Fotogramas por segundo o FPS
                if (gameTime.TotalGameTime.TotalMilliseconds > ultima_actua + 1000)
                {
                    frames = cont_draw;
                    updates = contUpdates;
                    cont_draw = 0;
                    contUpdates = 0;

                    ultima_actua = gameTime.TotalGameTime.TotalMilliseconds;
                }
            }

            base.Update(gameTime);

        }

        // ************** DRAW ****************
        protected override void Draw(GameTime gameTime)
        {
            cont_draw++;
            GraphicsDevice.Clear(Color.Black);

            spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend);

            if (dibujaIntro)
            {
                spriteBatch.Draw(Tex.getTextura("Intro"), Vector2.Zero, null, Color.White, 0.0f, Vector2.Zero,
                2.0f, SpriteEffects.None, 0.001f);
                if (empezarCarga)
                {
                    enCarga = true;
                    empezarCarga = false;
                }
            }
            else
            {

                switch (estadoActual)
                {

                    case Estado.MainMenu:
                        spriteBatch.DrawString(fuente, "Best Score: " + BESTSCORE, new Vector2(10, 770), Color.Gold);
                        mainMenu.draw(spriteBatch);
                        break;
                    case Estado.QuickGame:
                        quickGame.draw(spriteBatch);
                        break;
                    case Estado.MisionMode:
                        misionMode.draw(spriteBatch);
                        break;
                    case Estado.Options:
                        opciones.draw(spriteBatch);
                        break;
                    case Estado.Help:
                        ayuda.draw(spriteBatch);
                        break;
                    case Estado.About:
                        acercaDe.draw(spriteBatch);
                        break;
                    case Estado.SavedGame:
                        savedGame.draw(spriteBatch);
                        break;
                    case Estado.Buy:
                        buy.draw(spriteBatch);
                        break;
                    case Estado.Leaderboard:
                        leaderboard.draw(spriteBatch);
                        break;

                }

                //spriteBatch.DrawString(fuente, "FPS: " + Microsoft.Phone.Info.DeviceStatus.ApplicationPeakMemoryUsage.ToString()/*+frames+"-"+almacenamiento.leer()*/, new Vector2(10, 750), Color.White);
                //spriteBatch.DrawString(fuente, "UPS: " + updates, new Vector2(130, 770), Color.White);
            }

            spriteBatch.End();
            base.Draw(gameTime);
        }

        protected override void OnActivated(object sender, EventArgs args)
        {
            //isTrialMode = true;
            checkTrialModeDelay = 1000.0f;
            GalacticaMain.estadoActual = GalacticaMain.Estado.MainMenu;
            QuickMode.estadoActual = QuickMode.Estado.Menu;
        }

        protected override void OnExiting(object sender, EventArgs args)
        {
            if (QuickMode.estadoActual == QuickMode.Estado.Game)
            {
                if (GalacticaMain.estadoActual == GalacticaMain.Estado.Buy ||
                   GalacticaMain.estadoActual == GalacticaMain.Estado.QuickGame)
                {
                    Almacenamiento.salvarInGame();
                }
            }
            if (QuickMode.estadoActual == QuickMode.Estado.GameOver)
            {
                if (GalacticaMain.estadoActual == GalacticaMain.Estado.QuickGame)
                {
                    Boton.sendScore2();
                }
            }
        }

        protected override void OnDeactivated(object sender, EventArgs args)
        {
            if (QuickMode.estadoActual == QuickMode.Estado.Game)
            {
                if (GalacticaMain.estadoActual == GalacticaMain.Estado.Buy ||
                   GalacticaMain.estadoActual == GalacticaMain.Estado.QuickGame)
                {
                    Almacenamiento.salvarInGame();
                }
            }
            if (QuickMode.estadoActual == QuickMode.Estado.GameOver)
            {
                if (GalacticaMain.estadoActual == GalacticaMain.Estado.QuickGame)
                {
                    Boton.sendScore2();
                }
            }
        }


        //Boton back procesamiento
        public void procesaBotonBack(GameTime gameTime)
        {
            (Audio.getEfecto("presionarBoton")).Play();

            if (GalacticaMain.estadoActual == GalacticaMain.Estado.MainMenu)
            {
                this.Exit();
            }else
            if (QuickMode.estadoActual == QuickMode.Estado.Menu &&
                GalacticaMain.estadoActual == GalacticaMain.Estado.QuickGame)
            {
                GalacticaMain.estadoActual = GalacticaMain.Estado.MainMenu;
            }else
            if (GalacticaMain.estadoActual == GalacticaMain.Estado.Options)
            {
                Almacenamiento.salvarOpciones();
                GalacticaMain.estadoActual = GalacticaMain.Estado.MainMenu;
            }else
            if (GalacticaMain.estadoActual == GalacticaMain.Estado.Help)
            {
                GalacticaMain.estadoActual = GalacticaMain.Estado.MainMenu;
            }
            else
            if (GalacticaMain.estadoActual == GalacticaMain.Estado.About)
            {
                GalacticaMain.estadoActual = GalacticaMain.Estado.Help;
            }else
            if (GalacticaMain.estadoActual == GalacticaMain.Estado.Leaderboard)
            {
                GalacticaMain.estadoActual = GalacticaMain.Estado.MainMenu;
            }
            else
            if (GalacticaMain.estadoActual == GalacticaMain.Estado.SavedGame)
            {
                GalacticaMain.estadoActual = GalacticaMain.Estado.MainMenu;
            }
            else
            if (MisionMode.estadoActual == MisionMode.Estado.Menu &&
                GalacticaMain.estadoActual == GalacticaMain.Estado.MisionMode)
            {
                GalacticaMain.estadoActual = GalacticaMain.Estado.MainMenu;
            }
            else
            if (MisionMode.estadoActual == MisionMode.Estado.Info &&
                GalacticaMain.estadoActual == GalacticaMain.Estado.MisionMode)
            {
                MisionMode.estadoActual = MisionMode.Estado.Menu;
                MisionModeMenu.cargarMisiones();
            }
            else
            if (QuickMode.estadoActual == QuickMode.Estado.Game &&
                GalacticaMain.estadoActual == GalacticaMain.Estado.QuickGame)
            {
                if (MediaPlayer.GameHasControl && Audio.musicaActiva)
                {
                    MediaPlayer.Pause();
                }
                QuickMode.estadoActual = QuickMode.Estado.Pause;
            }
            else
            if (MisionMode.estadoActual == MisionMode.Estado.Game &&
                GalacticaMain.estadoActual == GalacticaMain.Estado.MisionMode)
            {
                if (MediaPlayer.GameHasControl && Audio.musicaActiva)
                {
                    MediaPlayer.Pause();
                }
                MisionMode.estadoActual = MisionMode.Estado.Pause;
            }else
            if (QuickMode.estadoActual == QuickMode.Estado.Pause &&
                GalacticaMain.estadoActual == GalacticaMain.Estado.QuickGame)
            {
                if (MediaPlayer.GameHasControl && Audio.musicaActiva)
                {
                    MediaPlayer.Resume();
                }
                QuickModeGame.ultimoTPlus = gameTime.TotalGameTime.TotalMilliseconds;
                QuickMode.estadoActual = QuickMode.Estado.Game;
            }else
            if (MisionMode.estadoActual == MisionMode.Estado.Pause &&
                GalacticaMain.estadoActual == GalacticaMain.Estado.MisionMode)
            {
                if (MediaPlayer.GameHasControl && Audio.musicaActiva)
                {
                    MediaPlayer.Resume();
                }
                MisionMode.estadoActual = MisionMode.Estado.Game;
            }else
            if (QuickMode.estadoActual == QuickMode.Estado.GameOver &&
                GalacticaMain.estadoActual == GalacticaMain.Estado.QuickGame)
            {
                MainMenu.playMusic = true;
                Boton.sendScore2();
                QuickMode.estadoActual = QuickMode.Estado.Menu;
                GalacticaMain.estadoActual = GalacticaMain.Estado.MainMenu;
                Nave.reiniciar();
                QuickModeGame.reiniciar();
            }else
            if (MisionMode.estadoActual == MisionMode.Estado.GameOver &&
                GalacticaMain.estadoActual == GalacticaMain.Estado.MisionMode)
            {
                MainMenu.playMusic = true;

                MisionMode.estadoActual = MisionMode.Estado.Menu;
                GalacticaMain.estadoActual = GalacticaMain.Estado.MainMenu;
                Boton.reiniciarMision();
                
            }else
            if (GalacticaMain.estadoActual == GalacticaMain.Estado.Buy)
            {
                if (QuickMode.estadoActual == QuickMode.Estado.Game)
                {
                    GalacticaMain.estadoActual = GalacticaMain.Estado.MainMenu;
                    QuickMode.estadoActual = QuickMode.Estado.Menu;
                    Nave.reiniciar();
                    QuickModeGame.reiniciar();
                }
                else
                {
                    GalacticaMain.estadoActual = GalacticaMain.Estado.MisionMode;
                    MisionMode.estadoActual = MisionMode.Estado.Menu;
                    MisionModeMenu.cargarMisiones();
                }
            }

        }

        //*** Cargar Audio
        public void cargarAudio()
        {
            audio = new Audio();
            audio.addEfecto(this.Content.Load<SoundEffect>("Audio/disparo"), "disparo");
            audio.addEfecto(this.Content.Load<SoundEffect>("Audio/EstallidoEnemigo"), "EstallidoEnemigo");
            audio.addEfecto(this.Content.Load<SoundEffect>("Audio/inicioPoder"), "inicioPoder");
            audio.addEfecto(this.Content.Load<SoundEffect>("Audio/EstallidoJefe"), "EstallidoJefe");
            audio.addEfecto(this.Content.Load<SoundEffect>("Audio/presionarBoton"), "presionarBoton");
            audio.addEfecto(this.Content.Load<SoundEffect>("Audio/Estallido"), "Estallido");
            audio.addEfecto(this.Content.Load<SoundEffect>("Audio/Estrella"), "Estrella");
            audio.addEfecto(this.Content.Load<SoundEffect>("Audio/Error"), "Error");
            Audio.silencio=this.Content.Load<SoundEffect>("Audio/silencio");

            audio.addMusica(this.Content.Load<Song>("Audio/Galactic"),"Galactic");
            audio.addMusica(this.Content.Load<Song>("Audio/GameOver"), "GameOver");
            audio.addMusica(this.Content.Load<Song>("Audio/mainMenu"), "mainMenu");
        }


        /**********Carga Texturas***************************/
        public void cargarTexturas()
        {
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/NGalactica"), "NGalactica");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/NCarafe"), "NCarafe");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/NHelix"), "NHelix");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/NArp"), "NArp");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/NPerseus"), "NPerseus");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/NPisces"), "NPisces");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/PoderGalactica"), "PoderGalactica");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/PoderCarafe"), "PoderCarafe");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/PoderHelix"), "PoderHelix");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/PoderArp"), "PoderArp");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/PoderPerseus"), "PoderPerseus");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/PoderPisces"), "PoderPisces");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/ENor"), "ENor");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/EKami"), "EKami");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/ESid"), "ESid");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/Disparo"), "Disparo");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/SuperDisparo"), "SuperDisparo");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/Titulo"), "Titulo");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/DisparoSid"), "DisparoSid");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/DisparoNor"), "DisparoNor");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/Estrella"), "Estrella");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/animationDispANave"), "animationDispANave");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/animationDispAEnemigo"), "animationDispAEnemigo");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/animationPoder"), "animationPoder");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/EstallidoEnemigo"), "EstallidoEnemigo");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/EstallidoJefe"), "EstallidoJefe");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/EstallidoJefe"), "EstallidoJefe");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/EstallidoNave"), "EstallidoNave");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/PauseBoton"), "PauseBoton");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/TituloPause"), "TituloPause");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/TituloPauseMision"), "TituloPauseMision");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/TituloQuickGame"), "TituloQuickGame");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/TituloQuickGame"), "TituloQuickGame");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/TituloGameOver"), "TituloGameOver");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/TituloMisionMode"), "TituloMisionMode");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/Plus"), "Plus");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BarraVida"), "BarraVida");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BarraVidaMarco"), "BarraVidaMarco");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BQuickMode"), "BQuickMode");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BOpciones"), "BOpciones");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BResumeGame"), "BResumeGame");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BExitGame"), "BExitGame");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BFlecha"), "BFlecha");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BPlay"), "BPlay");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BAudio"), "BAudio");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BMision"), "BMision");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BMisionMode"), "BMisionMode");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BPower"), "BPower");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BContinue"), "BContinue");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BBack"), "BBack");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BNextMision"), "BNextMision");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BRestart"), "BRestart");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BSalir"), "BSalir");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BHelp"), "BHelp");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BAbout"), "BAbout");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BTilting"), "BTilting");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BButtons"), "BButtons");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BMedium"), "BMedium");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BLarge"), "BLarge");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BContinueGame"), "BContinueGame");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BNewGame"), "BNewGame");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BFacebook"), "BFacebook");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BTwitter"), "BTwitter");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BComent"), "BComent");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BGoBuy"), "BGoBuy");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BNotNow"), "BNotNow");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/BLeaderboard"), "BLeaderboard");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/TEscogerNave"), "TEscogerNave");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/TResultados"), "TResultados");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/TInfo"), "TInfo");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/TRecompensa"), "TRecompensa");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/TituloWin"), "TituloWin");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/TituloLose"), "TituloLose");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/TituloOpciones"), "TituloOpciones");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/TituloSonidosMusica"), "TituloSonidosMusica");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/TituloControlMode"), "TituloControlMode");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/TituloButtonSize"), "TituloButtonSize");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/TituloSavedGame"), "TituloSavedGame");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/TituloBuyGame"), "TituloBuyGame");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/TituloLeaderboard"), "TituloLeaderboard");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/Separador"), "Separador");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/JLaxo"), "JLaxo");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/JEsp"), "JEsp");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/JEspBrazos"), "JEspBrazos");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/JQuad"), "JQuad");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/JQuadMartillo"), "JQuadMartillo");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/JRued"), "JRued");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/JRuedFlecha"), "JRuedFlecha");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/JefeDisparoMax"), "JefeDisparoMax");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/JefeDisparoNormal"), "JefeDisparoNormal");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/Capitana"), "Capitana");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/NMini"), "NMini");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/Onda"), "Onda");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/Control"), "Control");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/Help"), "Help");
            Tex.addTextura(this.Content.Load<Texture2D>("Sprites/About"), "About");
            //Fondos
            Tex.addTexturaFondo(this.Content.Load<Texture2D>("Sprites/FondoBlack"));//0
            Tex.addTexturaFondo(this.Content.Load<Texture2D>("Sprites/FondoEstadisticas"));//1
            Tex.addTexturaFondo(this.Content.Load<Texture2D>("Sprites/FondoPause"));//2
        }


        



        /// <summary>
        /// Create a DrawableAd with desired properties.
        /// </summary>
        private void CreateAd()
        {
            // Create a banner ad for the game.
            int width = 480;
            int height = 80;
            int x = (GraphicsDevice.Viewport.Bounds.Width - width) / 2; // centered on the display
            int y = 0;

            bannerAd = AdGameComponent.Current.CreateAd(AdUnitId, new Rectangle(x, y, width, height), true);

            // Add handlers for events (optional).
            bannerAd.ErrorOccurred += new EventHandler<Microsoft.Advertising.AdErrorEventArgs>(bannerAd_ErrorOccurred);
            bannerAd.AdRefreshed += new EventHandler(bannerAd_AdRefreshed);
            bannerAd.BorderColor = Color.Red;
            bannerAd.BorderEnabled = true;

            // Set some visual properties (optional).
            bannerAd.BorderEnabled = true; // default is true
            bannerAd.BorderColor = Color.Red; // default is White
            //bannerAd.DropShadowEnabled = true; // default is true

            // Provide the location to the ad for better targeting (optional).
            // This is done by starting a GeoCoordinateWatcher and waiting for the location to be available.
            // The callback will set the location into the ad. 
            // Note: The location may not be available in time for the first ad request.
            AdGameComponent.Current.Enabled = false;
            this.gcw = new GeoCoordinateWatcher();
            this.gcw.PositionChanged += new EventHandler<GeoPositionChangedEventArgs<GeoCoordinate>>(gcw_PositionChanged);
            this.gcw.StatusChanged += new EventHandler<GeoPositionStatusChangedEventArgs>(gcw_StatusChanged);
            this.gcw.Start();
        }

        private void CreateAd2()
        {
            // Create a banner ad for the game.
            int width = 480;
            int height = 80;
            int x = (GraphicsDevice.Viewport.Bounds.Width - width) / 2; // centered on the display
            int y = 600;

            bannerAd2 = AdGameComponent.Current.CreateAd(AdUnitId, new Rectangle(x, y, width, height), true);

            // Add handlers for events (optional).
            bannerAd2.ErrorOccurred += new EventHandler<Microsoft.Advertising.AdErrorEventArgs>(bannerAd_ErrorOccurred);
            bannerAd2.AdRefreshed += new EventHandler(bannerAd_AdRefreshed);
            bannerAd2.BorderColor = Color.Red;
            bannerAd2.BorderEnabled = true;

            // Set some visual properties (optional).
            bannerAd2.BorderEnabled = true; // default is true
            bannerAd2.BorderColor = Color.Red; // default is White
            //bannerAd.DropShadowEnabled = true; // default is true

            // Provide the location to the ad for better targeting (optional).
            // This is done by starting a GeoCoordinateWatcher and waiting for the location to be available.
            // The callback will set the location into the ad. 
            // Note: The location may not be available in time for the first ad request.
            AdGameComponent.Current.Enabled = false;
            this.gcw2 = new GeoCoordinateWatcher();
            this.gcw2.PositionChanged += new EventHandler<GeoPositionChangedEventArgs<GeoCoordinate>>(gcw_PositionChanged2);
            this.gcw2.StatusChanged += new EventHandler<GeoPositionStatusChangedEventArgs>(gcw_StatusChanged);
            this.gcw2.Start();
        }

        /// <summary>
        /// This is called whenever a new ad is received by the ad client.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bannerAd_AdRefreshed(object sender, EventArgs e)
        {
            //Debug.WriteLine("Ad received successfully");
        }

        /// <summary>
        /// This is called when an error occurs during the retrieval of an ad.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">Contains the Error that occurred.</param>
        private void bannerAd_ErrorOccurred(object sender, Microsoft.Advertising.AdErrorEventArgs e)
        {
            //Debug.WriteLine("Ad error: " + e.Error.Message);
        }

        private void gcw_PositionChanged(object sender, GeoPositionChangedEventArgs<GeoCoordinate> e)
        {
            // Stop the GeoCoordinateWatcher now that we have the device location.
            this.gcw.Stop();

            bannerAd.LocationLatitude = e.Position.Location.Latitude;
            bannerAd.LocationLongitude = e.Position.Location.Longitude;

            AdGameComponent.Current.Enabled = true;

            //Debug.WriteLine("Device lat/long: " + e.Position.Location.Latitude + ", " + e.Position.Location.Longitude);
        }

        private void gcw_PositionChanged2(object sender, GeoPositionChangedEventArgs<GeoCoordinate> e)
        {
            // Stop the GeoCoordinateWatcher now that we have the device location.
            this.gcw2.Stop();

            bannerAd2.LocationLatitude = e.Position.Location.Latitude;
            bannerAd2.LocationLongitude = e.Position.Location.Longitude;

            AdGameComponent.Current.Enabled = true;

            ///Debug.WriteLine("Device lat/long: " + e.Position.Location.Latitude + ", " + e.Position.Location.Longitude);
        }

        private void gcw_StatusChanged(object sender, GeoPositionStatusChangedEventArgs e)
        {
            if (e.Status == GeoPositionStatus.Disabled || e.Status == GeoPositionStatus.NoData)
            {
                // in the case that location services are not enabled or there is no data
                // enable ads anyway
                AdGameComponent.Current.Enabled = true;
                //Debug.WriteLine("GeoCoordinateWatcher Status :" + e.Status);
            }
        }

        /// <summary>
        /// Clean up the GeoCoordinateWatcher
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (disposing)
            {
                if (this.gcw != null)
                {
                    this.gcw.Dispose();
                    this.gcw = null;
                }
            }
        }


    }
}
