using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework.Media;

namespace Galactica
{
    class MainMenu : GameComun
    {

        private static List<Texture2D> textura;
        private Vector2 positionTitulo, origenTitulo;
        private List<Boton> botones;
        public static bool playMusic;
        private int i;

        public MainMenu()
        {
            loadTitulo();
            botones = new List<Boton>();
            botones.Add(new Boton(Boton.TipoBoton.BQuickGame, 10, 200));
            botones.Add(new Boton(Boton.TipoBoton.BMisionMode, 200, 310));
            botones.Add(new Boton(Boton.TipoBoton.BLeaderboard, 20, 410));
            botones.Add(new Boton(Boton.TipoBoton.BOpciones, 200, 500));
            botones.Add(new Boton(Boton.TipoBoton.BHelp, 20, 590));
            botones.Add(new Boton(Boton.TipoBoton.BFacebook, 10, 695));
            botones.Add(new Boton(Boton.TipoBoton.BTwitter, 100, 695));
            botones.Add(new Boton(Boton.TipoBoton.BGoBuy, 290, 20));
            botones.Add(new Boton(Boton.TipoBoton.BSalir, 200, 680));
            playMusic = true;
        }

        public static void load(Textura t)
        {
            setTexturas(t);
        }

        public void update(GameTime time, GalacticaMain contexto)
        {
            updateEstrellas(time);
            for (i = 0; i < botones.Count - 1; i++)
            {
                botones[i].update(time);
            }

            botones[8].update(contexto);

            if (MediaPlayer.GameHasControl && Audio.musicaActiva && playMusic)
            {
                MediaPlayer.IsRepeating = true;
                MediaPlayer.Play(Audio.getMusica("mainMenu"));
                playMusic = false;
            }
        }

        public void draw(SpriteBatch sb)
        {
            //CLEAR NEGRO
            sb.Draw(Textura.TexturasFondo[0], Vector2.Zero, null, Color.Black, 0.0f, Vector2.Zero,
                100.0f, SpriteEffects.None, 0.999f);

            drawEstrellas(sb);
            drawTitulo(sb);
            for (i = 0; i < botones.Count; i++)
            {
                if (botones[i].tipoActual == Boton.TipoBoton.BGoBuy)
                {
                    botones[i].escala = 1.4f;
                }

                botones[i].draw(sb);
            }
        }

        /*******************************************/

        /******TITULO************/
        public void loadTitulo()
        {
            positionTitulo = new Vector2(480 / 2, 130);
            origenTitulo = new Vector2(textura[0].Width / 2, textura[0].Height / 2);
        }

        public void drawTitulo(SpriteBatch sb)
        {
            sb.Draw(textura[0], positionTitulo, null, Color.White, 0.0f, origenTitulo,
                1.0f, SpriteEffects.None, 0.1f);
        }

        /*********Texturas****************/
        public static void setTexturas(Textura t)
        {
            textura = new List<Texture2D>();
            textura.Add(t.getTextura("Titulo"));//0
        }


    }
}
