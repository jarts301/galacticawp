using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Galactica
{
    class Estrella:GameComun
    {
        private static Texture2D texEstrella;
        public Color color;
        public float escala;
        public int vel;

        public Estrella()
        {
        }

        public Estrella(Vector2 posIni)
        {
            setOrigen();
            setPosition(posIni);
        }

        public static void load(Textura t)
        {
            setTextura(t);
        }

        public void update(GameTime gameTime)
        {
            movimientoEstrella();
        }

        public void draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texEstrella,Position,null,color,0.0f,Origen,
                escala,SpriteEffects.None,0.9f);
        }

        /***********************************/

        public static void setTextura(Textura t)
        {
            texEstrella = t.getTextura("Estrella");
        }

        public void setPosition(Vector2 pos)
        {
            Position = pos;
        }

        public void setOrigen()
        {
            Origen.X = texEstrella.Width / 2;
            Origen.Y = texEstrella.Height / 2;
        }

        public void movimientoEstrella()
        {
            if (Position.Y < 810)
            {
                Position.Y = Position.Y + vel;
            }else{
                Position.Y = -10;
                Position.X = random.Next(0, 480);
            }
        }

    }
}
