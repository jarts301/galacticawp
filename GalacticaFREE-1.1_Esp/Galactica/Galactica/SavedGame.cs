using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Galactica
{
    class SavedGame:GameComun
    {

        private static List<Texture2D> textura;
        private Vector2 positionTitulo,origenTitulo;
        private List<Boton> botones;
        private int i;

        public SavedGame()
        {
            loadTitulo();
            botones = new List<Boton>();
            botones.Add(new Boton(Boton.TipoBoton.BContinueGame, 80, 220));
            botones.Add(new Boton(Boton.TipoBoton.BNewGame, 80, 340));
            botones.Add(new Boton(Boton.TipoBoton.BBack, 10, 730));
        }

        public static void load(Textura t)
        {
            setTexturas(t);
        }

        public void update(GameTime time)
        {
            updateEstrellas(time);
            for (i = 0; i < botones.Count; i++)
            {
                botones[i].update(time);
            }
            Boton.ultimaActualizacion = time.TotalGameTime.TotalMilliseconds;
            Nave.ultimaActualizacion = time.TotalGameTime.TotalMilliseconds;
        }

        public void draw(SpriteBatch sb)
        {
            //CLEAR NEGRO
            sb.Draw(Textura.TexturasFondo[0], Vector2.Zero, null, Color.Black, 0.0f, Vector2.Zero,
                100.0f, SpriteEffects.None, 0.999f);

            drawEstrellas(sb);
            drawTitulo(sb);
            for (i = 0; i < botones.Count; i++)
            {
                botones[i].draw(sb);
            }

        }

        /*******************************************/

        /******TITULO************/
        public void loadTitulo()
        {
            positionTitulo = new Vector2(480/2,130);
            origenTitulo = new Vector2(textura[0].Width/2,textura[0].Height/2);
        }

        public void drawTitulo(SpriteBatch sb)
        {
            sb.Draw(textura[0], positionTitulo, null, Color.White, 0.0f, origenTitulo,
                1.6f, SpriteEffects.None, 0.1f);
        }

        /*********Texturas****************/
        public static void setTexturas(Textura t)
        {
            textura = new List<Texture2D>();
            textura.Add(t.getTextura("TituloSavedGame"));//0
        }

    }
}
