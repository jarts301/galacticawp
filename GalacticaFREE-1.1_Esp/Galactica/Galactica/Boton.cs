using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.GamerServices;
using Galactica.HighScores;
using Microsoft.Phone.Tasks;

namespace Galactica
{
    class Boton : GameComun
    {
        private static List<Texture2D> textura;
        public enum TipoBoton
        {
            BQuickGame, BPause, BResumeGame,
            BExitGame, BFlecha, BPlay, BPower,
            BContinue, BMision, BMisionMode,
            BBack, BNextMision, BRestart,
            BAudio, BOpciones, BSalir,
            BTilting, BButtons, BMedium,
            BLarge, BHelp, BAbout, BContinueGame,
            BNewGame, BFacebook, BComent, BTwitter,
            BGoBuy, BNotNow, BLeaderboard
        }
        public TipoBoton tipoActual;
        public static Microsoft.Phone.Tasks.WebBrowserTask palFace =
            new Microsoft.Phone.Tasks.WebBrowserTask();
        public static Microsoft.Phone.Tasks.WebBrowserTask palTui =
            new Microsoft.Phone.Tasks.WebBrowserTask();
        public static Microsoft.Phone.Tasks.WebBrowserTask paComprar =
            new Microsoft.Phone.Tasks.WebBrowserTask();
        public int texActual;
        public float escala;
        public bool flechaDerecha;
        public float Rotacion;
        public static double ultimaActualizacion;
        public static int indiceMision = -1;
        public bool bPoderActivo;
        public int actualframeB;
        private int lineaAnim;
        private TouchCollection touches;
        private int i;
        private int j;
        public static int navesActivas, tiempoEsperaPoder;
        private static string nombre;
        private static SpriteFont fuente;
        public static bool newBestScore = false;

        public Boton(TipoBoton tb, int posX, int posY)
        {
            tipoActual = tb;
            switch (tb)
            {
                case TipoBoton.BQuickGame:
                    defineBQuickMode(posX, posY);
                    break;
                case TipoBoton.BPause:
                    defineBPause(posX, posY);
                    break;
                case TipoBoton.BResumeGame:
                    defineBResumeGame(posX, posY);
                    break;
                case TipoBoton.BExitGame:
                    defineBExitGame(posX, posY);
                    break;
                case TipoBoton.BPlay:
                    defineBPlay(posX, posY);
                    break;
                case TipoBoton.BPower:
                    defineBPower(posX, posY);
                    break;
                case TipoBoton.BContinue:
                    defineBContinue(posX, posY);
                    break;
                case TipoBoton.BMisionMode:
                    defineBMisionMode(posX, posY);
                    break;
                case TipoBoton.BMision:
                    defineBMision(posX, posY);
                    break;
                case TipoBoton.BBack:
                    defineBBack(posX, posY);
                    break;
                case TipoBoton.BNextMision:
                    defineBNextMision(posX, posY);
                    break;
                case TipoBoton.BRestart:
                    defineBRestart(posX, posY);
                    break;
                case TipoBoton.BAudio:
                    defineBAudio(posX, posY);
                    break;
                case TipoBoton.BOpciones:
                    defineBOpciones(posX, posY);
                    break;
                case TipoBoton.BSalir:
                    defineBSalir(posX, posY);
                    break;
                case TipoBoton.BTilting:
                    defineBTilting(posX, posY);
                    break;
                case TipoBoton.BButtons:
                    defineBButtons(posX, posY);
                    break;
                case TipoBoton.BMedium:
                    defineBMedium(posX, posY);
                    break;
                case TipoBoton.BLarge:
                    defineBLarge(posX, posY);
                    break;
                case TipoBoton.BHelp:
                    defineBHelp(posX, posY);
                    break;
                case TipoBoton.BAbout:
                    defineBAbout(posX, posY);
                    break;
                case TipoBoton.BContinueGame:
                    defineBContinueGame(posX, posY);
                    break;
                case TipoBoton.BNewGame:
                    defineBNewGame(posX, posY);
                    break;
                case TipoBoton.BFacebook:
                    defineBFacebook(posX, posY);
                    break;
                case TipoBoton.BTwitter:
                    defineBTwitter(posX, posY);
                    break;
                case TipoBoton.BComent:
                    defineBComent(posX, posY);
                    break;
                case TipoBoton.BGoBuy:
                    defineBGoBuy(posX, posY);
                    break;
                case TipoBoton.BNotNow:
                    defineBNotNow(posX, posY);
                    break;
                case TipoBoton.BLeaderboard:
                    defineBLeaderboard(posX, posY);
                    break;
            }
        }

        public Boton(TipoBoton tb, int posX, int posY, bool dirDerechaFlecha)
        {
            tipoActual = tb;
            if (tb == TipoBoton.BFlecha)
            {
                flechaDerecha = dirDerechaFlecha;
                defineBFlecha(posX, posY, dirDerechaFlecha);
            }
        }

        public static void load(Textura t, SpriteFont f)
        {
            setTexturas(t);
            fuente = f;
            ultimaActualizacion = 0;
            palFace.Uri = new Uri("https://www.facebook.com/jarts301", UriKind.Absolute);
            palTui.Uri = new Uri("https://twitter.com/jarts301", UriKind.Absolute);
            paComprar.Uri = new Uri("http://www.windowsphone.com/s?appid=cedea923-b22f-4ac1-b503-f42172cf0410", UriKind.Absolute);
        }

        public void update(GameTime time)
        {
            touches = TouchControl.touches;
            switch (tipoActual)
            {
                case TipoBoton.BQuickGame:
                    verificaTouchBQuickMode(TrozoAnim.Width, TrozoAnim.Height);
                    break;
                case TipoBoton.BPause:
                    verificaTouchBPause(40, 40);
                    break;
                case TipoBoton.BResumeGame:
                    verificaTouchBResumeGame(TrozoAnim.Width, TrozoAnim.Height, time);
                    break;
                case TipoBoton.BExitGame:
                    verificaTouchBExitGame(TrozoAnim.Width, TrozoAnim.Height);
                    break;
                case TipoBoton.BPlay:
                    verificaTouchBPlay(TrozoAnim.Width, TrozoAnim.Height, time);
                    break;
                case TipoBoton.BFlecha:
                    verificaTouchBFlecha(TrozoAnim.Width, TrozoAnim.Height);
                    break;
                case TipoBoton.BPower:
                    verificaTouchBPower(TrozoAnim.Width, TrozoAnim.Height, time);
                    break;
                case TipoBoton.BContinue:
                    verificaTouchBContinue(TrozoAnim.Width, TrozoAnim.Height, time);
                    break;
                case TipoBoton.BMisionMode:
                    verificaTouchBMisionMode(TrozoAnim.Width, TrozoAnim.Height, time);
                    break;
                case TipoBoton.BMision:
                    verificaTouchBMision(TrozoAnim.Width, TrozoAnim.Height, time);
                    break;
                case TipoBoton.BBack:
                    verificaTouchBBack(TrozoAnim.Width, TrozoAnim.Height, time);
                    break;
                case TipoBoton.BNextMision:
                    verificaTouchBNextMision(TrozoAnim.Width, TrozoAnim.Height, time);
                    break;
                case TipoBoton.BRestart:
                    verificaTouchBRestart(TrozoAnim.Width, TrozoAnim.Height, time);
                    break;
                case TipoBoton.BAudio:
                    verificaTouchBAudio(TrozoAnim.Width, TrozoAnim.Height, time);
                    break;
                case TipoBoton.BOpciones:
                    verificaTouchBOpciones(TrozoAnim.Width, TrozoAnim.Height, time);
                    break;
                case TipoBoton.BTilting:
                    verificaTouchBTilting(TrozoAnim.Width, TrozoAnim.Height);
                    break;
                case TipoBoton.BButtons:
                    verificaTouchBButtons(TrozoAnim.Width, TrozoAnim.Height);
                    break;
                case TipoBoton.BMedium:
                    verificaTouchBMedium(TrozoAnim.Width, TrozoAnim.Height);
                    break;
                case TipoBoton.BLarge:
                    verificaTouchBLarge(TrozoAnim.Width, TrozoAnim.Height);
                    break;
                case TipoBoton.BHelp:
                    verificaTouchBHelp(TrozoAnim.Width, TrozoAnim.Height);
                    break;
                case TipoBoton.BAbout:
                    verificaTouchBAbout(TrozoAnim.Width, TrozoAnim.Height);
                    break;
                case TipoBoton.BContinueGame:
                    verificaTouchBContinueGame(TrozoAnim.Width, TrozoAnim.Height, time);
                    break;
                case TipoBoton.BNewGame:
                    verificaTouchBNewGame(TrozoAnim.Width, TrozoAnim.Height);
                    break;
                case TipoBoton.BFacebook:
                    verificaTouchBFacebook(TrozoAnim.Width, TrozoAnim.Height);
                    break;
                case TipoBoton.BTwitter:
                    verificaTouchBTwitter(TrozoAnim.Width, TrozoAnim.Height);
                    break;
                case TipoBoton.BComent:
                    verificaTouchBComent(TrozoAnim.Width, TrozoAnim.Height);
                    break;
                case TipoBoton.BGoBuy:
                    verificaTouchBGoBuy(TrozoAnim.Width, TrozoAnim.Height);
                    break;
                case TipoBoton.BNotNow:
                    verificaTouchBNotNow(TrozoAnim.Width, TrozoAnim.Height);
                    break;
                case TipoBoton.BLeaderboard:
                    verificaTouchBLeaderboard(TrozoAnim.Width, TrozoAnim.Height);
                    break;
            }

        }

        //para boton salir
        public void update(GalacticaMain contexto)
        {
            touches = TouchControl.touches;
            if (tipoActual == TipoBoton.BSalir)
            {
                verificaTouchBSalir(contexto);
            }
        }

        public void draw(SpriteBatch sb)
        {
            sb.Draw(textura[texActual], Position, TrozoAnim, Color.White, Rotacion, Origen,
                escala, SpriteEffects.None, Capa);
        }

        /****************************************/

        /*************Boton de Quick mode ************/
        public void defineBQuickMode(int posX, int posY)
        {
            setPosition(posX, posY);
            actualframeB = 0;
            defineTrozoAnim(0, actualframeB * 42, 125, 42);
            Origen = Vector2.Zero;
            texActual = 0;
            Rotacion = 0.0f;
            escala = 2.2f;
            Capa = 0.1f;
        }

        /*************Boton de Pause ************/
        public void defineBPause(int posX, int posY)
        {
            setPosition(posX, posY);
            defineTrozoAnim(0, 0, 40, 40);
            Origen = Vector2.Zero;
            texActual = 1;
            Rotacion = 0.0f;
            escala = 1.1f;
            Capa = 0.4f;
        }

        /*************Boton de Resume Game ************/
        public void defineBResumeGame(int posX, int posY)
        {
            setPosition(posX, posY);
            actualframeB = 0;
            defineTrozoAnim(0, actualframeB * 42, 125, 42);
            Origen = Vector2.Zero;
            texActual = 2;
            Rotacion = 0.0f;
            escala = 2.6f;
            Capa = 0.1f;
        }

        /*************Boton de Exit Game ************/
        public void defineBExitGame(int posX, int posY)
        {
            setPosition(posX, posY);
            actualframeB = 0;
            defineTrozoAnim(0, actualframeB * 42, 125, 42);
            Origen = Vector2.Zero;
            texActual = 3;
            Rotacion = 0.0f;
            escala = 2.6f;
            Capa = 0.1f;
        }

        /*************Boton de Flecha ************/
        public void defineBFlecha(int posX, int posY, bool dirDerechaFlecha)
        {
            setPosition(posX, posY);
            actualframeB = 0;
            defineTrozoAnim(0, actualframeB * 40, 40, 40);
            Origen = Vector2.Zero;
            texActual = 4;
            if (dirDerechaFlecha) { Rotacion = MathHelper.ToRadians(180); }
            if (!dirDerechaFlecha) { Rotacion = MathHelper.ToRadians(0); }
            escala = 2.0f;
            Capa = 0.1f;
        }

        /*********Boton Play***********/
        public void defineBPlay(int posX, int posY)
        {
            setPosition(posX, posY);
            actualframeB = 0;
            defineTrozoAnim(0, actualframeB * 42, 125, 42);
            Origen = Vector2.Zero;
            texActual = 5;
            Rotacion = 0.0f;
            escala = 2.2f;
            Capa = 0.1f;
        }

        /*************Boton de Power ************/
        public void defineBPower(int posX, int posY)
        {
            bPoderActivo = false;
            setPosition(posX, posY);
            actualframeB = 0;
            defineTrozoAnim(0, actualframeB * 50, 50, 50);
            Origen = Vector2.Zero;
            texActual = 6;
            Rotacion = 0.0f;
            escala = 1.4f;
            Capa = 0.4f;
        }

        /*************Boton de Continue ************/
        public void defineBContinue(int posX, int posY)
        {
            setPosition(posX, posY);
            actualframeB = 0;
            defineTrozoAnim(0, actualframeB * 42, 125, 42);
            Origen = Vector2.Zero;
            texActual = 7;
            Rotacion = 0.0f;
            escala = 1.8f;
            Capa = 0.1f;
        }

        /*************Boton de Mision Mode ************/
        public void defineBMisionMode(int posX, int posY)
        {
            setPosition(posX, posY);
            actualframeB = 0;
            defineTrozoAnim(0, actualframeB * 42, 125, 42);
            Origen = Vector2.Zero;
            texActual = 9;
            Rotacion = 0.0f;
            escala = 2.2f;
            Capa = 0.1f;
        }

        /*************Boton de Mision ************/
        public void defineBMision(int posX, int posY)
        {
            setPosition(posX, posY);
            actualframeB = 1;
            defineTrozoAnim(actualframeB * 50, 0, 50, 50);
            Origen = Vector2.Zero;
            texActual = 8;
            Rotacion = 0.0f;
            escala = 2.0f;
            Capa = 0.1f;
        }

        /*************Boton de Back ************/
        public void defineBBack(int posX, int posY)
        {
            setPosition(posX, posY);
            actualframeB = 0;
            defineTrozoAnim(0, actualframeB * 42, 125, 42);
            Origen = Vector2.Zero;
            texActual = 10;
            Rotacion = 0.0f;
            escala = 1.4f;
            Capa = 0.1f;
        }

        /*************Boton de Next mision ************/
        public void defineBNextMision(int posX, int posY)
        {
            setPosition(posX, posY);
            actualframeB = 0;
            defineTrozoAnim(0, actualframeB * 42, 125, 42);
            Origen = Vector2.Zero;
            texActual = 11;
            Rotacion = 0.0f;
            escala = 2.6f;
            Capa = 0.1f;
        }

        /*************Boton de Restart ************/
        public void defineBRestart(int posX, int posY)
        {
            setPosition(posX, posY);
            actualframeB = 0;
            defineTrozoAnim(0, actualframeB * 42, 125, 42);
            Origen = Vector2.Zero;
            texActual = 12;
            Rotacion = 0.0f;
            escala = 1.8f;
            Capa = 0.1f;
        }

        /*************Boton de Audio ************/
        public void defineBAudio(int posX, int posY)
        {
            setPosition(posX, posY);
            if (Audio.musicaActiva && Audio.efectosActivos)
            {
                actualframeB = 0;
            }
            else
            {
                actualframeB = 1;
            }
            defineTrozoAnim(40 * actualframeB, 0, 40, 40);
            Origen = Vector2.Zero;
            texActual = 13;
            Rotacion = 0.0f;
            escala = 1.1f;
            Capa = 0.4f;
        }

        /*************Boton de Opciones ************/
        public void defineBOpciones(int posX, int posY)
        {
            setPosition(posX, posY);
            actualframeB = 0;
            defineTrozoAnim(0, actualframeB * 42, 125, 42);
            Origen = Vector2.Zero;
            texActual = 14;
            Rotacion = 0.0f;
            escala = 2.0f;
            Capa = 0.1f;
        }

        /*************Boton de Salir ************/
        public void defineBSalir(int posX, int posY)
        {
            setPosition(posX, posY);
            actualframeB = 0;
            defineTrozoAnim(0, actualframeB * 42, 125, 42);
            Origen = Vector2.Zero;
            texActual = 15;
            Rotacion = 0.0f;
            escala = 2.0f;
            Capa = 0.1f;
        }

        /*************Boton Tilting ************/
        public void defineBTilting(int posX, int posY)
        {
            setPosition(posX, posY);
            actualframeB = 0;
            defineTrozoAnim(0, actualframeB * 42, 125, 42);
            Origen = Vector2.Zero;
            texActual = 16;
            Rotacion = 0.0f;
            escala = 1.7f;
            Capa = 0.1f;
        }

        /*************Boton Buttons ************/
        public void defineBButtons(int posX, int posY)
        {
            setPosition(posX, posY);
            actualframeB = 0;
            defineTrozoAnim(0, actualframeB * 42, 125, 42);
            Origen = Vector2.Zero;
            texActual = 17;
            Rotacion = 0.0f;
            escala = 1.7f;
            Capa = 0.1f;
        }

        /*************Boton Medium ************/
        public void defineBMedium(int posX, int posY)
        {
            setPosition(posX, posY);
            actualframeB = 0;
            defineTrozoAnim(0, actualframeB * 42, 125, 42);
            Origen = Vector2.Zero;
            texActual = 18;
            Rotacion = 0.0f;
            escala = 1.7f;
            Capa = 0.1f;
        }

        /*************Boton Large ************/
        public void defineBLarge(int posX, int posY)
        {
            setPosition(posX, posY);
            actualframeB = 0;
            defineTrozoAnim(0, actualframeB * 42, 125, 42);
            Origen = Vector2.Zero;
            texActual = 19;
            Rotacion = 0.0f;
            escala = 1.7f;
            Capa = 0.1f;
        }

        /*************Boton de Help ************/
        public void defineBHelp(int posX, int posY)
        {
            setPosition(posX, posY);
            actualframeB = 0;
            defineTrozoAnim(0, actualframeB * 42, 125, 42);
            Origen = Vector2.Zero;
            texActual = 20;
            Rotacion = 0.0f;
            escala = 2.0f;
            Capa = 0.1f;
        }

        /*************Boton de Acerca de************/
        public void defineBAbout(int posX, int posY)
        {
            setPosition(posX, posY);
            actualframeB = 0;
            defineTrozoAnim(0, actualframeB * 42, 125, 42);
            Origen = Vector2.Zero;
            texActual = 21;
            Rotacion = 0.0f;
            escala = 1.4f;
            Capa = 0.1f;
        }

        /*************Boton de ContinueGame************/
        public void defineBContinueGame(int posX, int posY)
        {
            setPosition(posX, posY);
            actualframeB = 0;
            defineTrozoAnim(0, actualframeB * 42, 125, 42);
            Origen = Vector2.Zero;
            texActual = 22;
            Rotacion = 0.0f;
            escala = 2.6f;
            Capa = 0.1f;
        }

        /*************Boton de NewGame************/
        public void defineBNewGame(int posX, int posY)
        {
            setPosition(posX, posY);
            actualframeB = 0;
            defineTrozoAnim(0, actualframeB * 42, 125, 42);
            Origen = Vector2.Zero;
            texActual = 23;
            Rotacion = 0.0f;
            escala = 2.6f;
            Capa = 0.1f;
        }

        /*************Boton de Facebook************/
        public void defineBFacebook(int posX, int posY)
        {
            setPosition(posX, posY);
            actualframeB = 0;
            defineTrozoAnim(0, actualframeB * 64, 64, 64);
            Origen = Vector2.Zero;
            texActual = 24;
            Rotacion = 0.0f;
            escala = 1.1f;
            Capa = 0.1f;
        }

        /*************Boton de Facebook************/
        public void defineBTwitter(int posX, int posY)
        {
            setPosition(posX, posY);
            actualframeB = 0;
            defineTrozoAnim(0, actualframeB * 64, 64, 64);
            Origen = Vector2.Zero;
            texActual = 29;
            Rotacion = 0.0f;
            escala = 1.1f;
            Capa = 0.1f;
        }


        /*************Boton de Comentario************/
        public void defineBComent(int posX, int posY)
        {
            setPosition(posX, posY);
            actualframeB = 0;
            defineTrozoAnim(0, actualframeB * 45, 95, 45);
            Origen = Vector2.Zero;
            texActual = 25;
            Rotacion = 0.0f;
            escala = 2.0f;
            Capa = 0.1f;
        }

        /*************Boton de Go Buy************/
        public void defineBGoBuy(int posX, int posY)
        {
            setPosition(posX, posY);
            actualframeB = 0;
            defineTrozoAnim(0, actualframeB * 42, 125, 42);
            Origen = Vector2.Zero;
            texActual = 26;
            Rotacion = 0.0f;
            escala = 2.6f;
            Capa = 0.1f;
        }

        /*************Boton de NotNow************/
        public void defineBNotNow(int posX, int posY)
        {
            setPosition(posX, posY);
            actualframeB = 0;
            defineTrozoAnim(0, actualframeB * 42, 125, 42);
            Origen = Vector2.Zero;
            texActual = 27;
            Rotacion = 0.0f;
            escala = 2.6f;
            Capa = 0.1f;
        }

        /*************Boton de Leaderboard************/
        public void defineBLeaderboard(int posX, int posY)
        {
            setPosition(posX, posY);
            actualframeB = 0;
            defineTrozoAnim(0, actualframeB * 42, 125, 42);
            Origen = Vector2.Zero;
            texActual = 28;
            Rotacion = 0.0f;
            escala = 2.0f;
            Capa = 0.1f;
        }

        /*********Metodos Verificacion*************/
        //Boton pause
        public void verificaTouchBPause(int ancho, int alto)
        {
            if (touches.Count > 0)
            {
                for (i = 0; i < touches.Count; i++)
                {
                    if (TouchLocationState.Released.Equals(touches[i].State) &&
                        touches[i].Position.X >= Position.X &&
                        touches[i].Position.X <= Position.X + (TrozoAnim.Width * escala) &&
                        touches[i].Position.Y >= Position.Y &&
                        touches[i].Position.Y <= Position.Y + (TrozoAnim.Height * escala)
                        )
                    {

                        (Audio.getEfecto("presionarBoton")).Play();
                        if (MediaPlayer.GameHasControl && Audio.musicaActiva)
                        {
                            MediaPlayer.Pause();
                        }

                        if (QuickMode.estadoActual == QuickMode.Estado.Game &&
                            GalacticaMain.estadoActual == GalacticaMain.Estado.QuickGame)
                        {
                            QuickMode.estadoActual = QuickMode.Estado.Pause;
                        }
                        if (MisionMode.estadoActual == MisionMode.Estado.Game &&
                            GalacticaMain.estadoActual == GalacticaMain.Estado.MisionMode)
                        {
                            MisionMode.estadoActual = MisionMode.Estado.Pause;
                        }
                    }
                }
            }
        }

        //Boton Audio
        public void verificaTouchBAudio(int ancho, int alto, GameTime gt)
        {
            if (Audio.efectosActivos && Audio.musicaActiva)
            {
                actualframeB = 0;
                defineTrozoAnim(40 * actualframeB, 0, 40, 40);
            }
            else
            {
                actualframeB = 1;
                defineTrozoAnim(40 * actualframeB, 0, 40, 40);
            }

            if (touches.Count > 0)
            {
                for (i = 0; i < touches.Count; i++)
                {
                    if (TouchLocationState.Released.Equals(touches[i].State) &&
                        touches[i].Position.X >= Position.X &&
                        touches[i].Position.X <= Position.X + (TrozoAnim.Width * escala) &&
                        touches[i].Position.Y >= Position.Y &&
                        touches[i].Position.Y <= Position.Y + (TrozoAnim.Height * escala)
                        )
                    {
                        (Audio.getEfecto("presionarBoton")).Play();
                        if (Audio.musicaActiva && Audio.efectosActivos)
                        {
                            if (MediaPlayer.GameHasControl)
                            {
                                if (MediaPlayer.GameHasControl &&
                                (QuickMode.estadoActual == QuickMode.Estado.Game ||
                                 MisionMode.estadoActual == MisionMode.Estado.Game))
                                {
                                    MediaPlayer.Pause();
                                }
                                if (MediaPlayer.GameHasControl &&
                                (QuickMode.estadoActual != QuickMode.Estado.Game &&
                                 MisionMode.estadoActual != MisionMode.Estado.Game))
                                {
                                    MediaPlayer.Stop();
                                }
                            }
                            actualframeB = 1;
                            Audio.efectosActivos = false;
                            Audio.musicaActiva = false;
                        }
                        else
                        {
                            if (MediaPlayer.GameHasControl &&
                                (QuickMode.estadoActual != QuickMode.Estado.Game &&
                                 MisionMode.estadoActual != MisionMode.Estado.Game))
                            {
                                MediaPlayer.IsRepeating = true;
                                MediaPlayer.Play(Audio.getMusica("mainMenu"));
                            }

                            if (MediaPlayer.GameHasControl &&
                                (QuickMode.estadoActual == QuickMode.Estado.Game ||
                                 MisionMode.estadoActual == MisionMode.Estado.Game))
                            {
                                if (MediaPlayer.State == MediaState.Stopped)
                                {
                                    MediaPlayer.IsRepeating = true;
                                    MediaPlayer.Play(Audio.getMusica("Galactic"));
                                }
                                else
                                {
                                    MediaPlayer.Resume();
                                }
                            }

                            actualframeB = 0;
                            Audio.efectosActivos = true;
                            Audio.musicaActiva = true;
                        }
                        Almacenamiento.salvarOpciones();

                    }
                }
            }
        }

        //Boton Exit Game
        public void verificaTouchBExitGame(int ancho, int alto)
        {
            if (touches.Count > 0)
            {
                for (i = 0; i < touches.Count; i++)
                {
                    if (TouchLocationState.Released.Equals(touches[i].State) &&
                        touches[i].Position.X >= Position.X &&
                        touches[i].Position.X <= Position.X + (TrozoAnim.Width * escala) &&
                        touches[i].Position.Y >= Position.Y &&
                        touches[i].Position.Y <= Position.Y + (TrozoAnim.Height * escala)
                        )
                    {
                        (Audio.getEfecto("presionarBoton")).Play();
                        if (MediaPlayer.GameHasControl && Audio.musicaActiva)
                        {
                            MediaPlayer.Stop();
                        }
                        MainMenu.playMusic = true;

                        if (QuickMode.estadoActual == QuickMode.Estado.Pause &&
                            GalacticaMain.estadoActual == GalacticaMain.Estado.QuickGame)
                        {
                            Almacenamiento.salvarInGame();
                            QuickMode.estadoActual = QuickMode.Estado.Menu;
                            GalacticaMain.estadoActual = GalacticaMain.Estado.MainMenu;
                            Nave.reiniciar();
                            QuickModeGame.reiniciar();
                        }
                        if (MisionMode.estadoActual == MisionMode.Estado.Pause &&
                            GalacticaMain.estadoActual == GalacticaMain.Estado.MisionMode)
                        {
                            MisionMode.estadoActual = MisionMode.Estado.Menu;
                            GalacticaMain.estadoActual = GalacticaMain.Estado.MainMenu;
                            Nave.reiniciar();
                            reiniciarMision();
                        }
                    }
                }
            }
        }

        //Boton Resume game
        public void verificaTouchBResumeGame(int ancho, int alto, GameTime gt)
        {
            if (touches.Count > 0)
            {
                for (i = 0; i < touches.Count; i++)
                {
                    if (TouchLocationState.Released.Equals(touches[i].State) &&
                        touches[i].Position.X >= Position.X &&
                        touches[i].Position.X <= Position.X + (TrozoAnim.Width * escala) &&
                        touches[i].Position.Y >= Position.Y &&
                        touches[i].Position.Y <= Position.Y + (TrozoAnim.Height * escala)
                        )
                    {
                        (Audio.getEfecto("presionarBoton")).Play();
                        if (MediaPlayer.GameHasControl && Audio.musicaActiva)
                        {
                            MediaPlayer.Resume();
                        }
                        if (QuickMode.estadoActual == QuickMode.Estado.Pause &&
                            GalacticaMain.estadoActual == GalacticaMain.Estado.QuickGame)
                        {
                            QuickModeGame.ultimoTPlus = gt.TotalGameTime.TotalMilliseconds;
                            QuickMode.estadoActual = QuickMode.Estado.Game;
                        }
                        if (MisionMode.estadoActual == MisionMode.Estado.Pause &&
                            GalacticaMain.estadoActual == GalacticaMain.Estado.MisionMode)
                        {
                            MisionMode.estadoActual = MisionMode.Estado.Game;

                        }
                    }
                }
            }
        }

        //Boton Tilting
        public void verificaTouchBTilting(int ancho, int alto)
        {
            if (Acelerometro.acelActivo)
            {
                actualframeB = 1;
                defineTrozoAnim(0, actualframeB * 42, 125, 42);
            }
            else
            {
                actualframeB = 0;
                defineTrozoAnim(0, actualframeB * 42, 125, 42);
            }
            if (touches.Count > 0)
            {
                for (i = 0; i < touches.Count; i++)
                {
                    if (TouchLocationState.Released.Equals(touches[i].State) &&
                        touches[i].Position.X >= Position.X &&
                        touches[i].Position.X <= Position.X + (TrozoAnim.Width * escala) &&
                        touches[i].Position.Y >= Position.Y &&
                        touches[i].Position.Y <= Position.Y + (TrozoAnim.Height * escala)
                        )
                    {
                        (Audio.getEfecto("presionarBoton")).Play();
                        Acelerometro.acelActivo = true;
                        Control.controlActivo = false;
                        Acelerometro.acelerometro.Start();
                    }
                }
            }
        }

        //Boton Buttons
        public void verificaTouchBButtons(int ancho, int alto)
        {
            if (Control.controlActivo)
            {
                actualframeB = 1;
                defineTrozoAnim(0, actualframeB * 42, 125, 42);
            }
            else
            {
                actualframeB = 0;
                defineTrozoAnim(0, actualframeB * 42, 125, 42);
            }
            if (touches.Count > 0)
            {
                for (i = 0; i < touches.Count; i++)
                {
                    if (TouchLocationState.Released.Equals(touches[i].State) &&
                        touches[i].Position.X >= Position.X &&
                        touches[i].Position.X <= Position.X + (TrozoAnim.Width * escala) &&
                        touches[i].Position.Y >= Position.Y &&
                        touches[i].Position.Y <= Position.Y + (TrozoAnim.Height * escala)
                        )
                    {
                        (Audio.getEfecto("presionarBoton")).Play();
                        Acelerometro.acelActivo = false;
                        Control.controlActivo = true;
                        Acelerometro.acelerometro.Stop();
                    }
                }
            }
        }

        //Boton Medium
        public void verificaTouchBMedium(int ancho, int alto)
        {
            if (Control.tamanoGrande)
            {
                actualframeB = 0;
                defineTrozoAnim(0, actualframeB * 42, 125, 42);
            }
            else
            {
                actualframeB = 1;
                defineTrozoAnim(0, actualframeB * 42, 125, 42);
            }
            if (touches.Count > 0)
            {
                for (i = 0; i < touches.Count; i++)
                {
                    if (TouchLocationState.Released.Equals(touches[i].State) &&
                        touches[i].Position.X >= Position.X &&
                        touches[i].Position.X <= Position.X + (TrozoAnim.Width * escala) &&
                        touches[i].Position.Y >= Position.Y &&
                        touches[i].Position.Y <= Position.Y + (TrozoAnim.Height * escala)
                        )
                    {
                        (Audio.getEfecto("presionarBoton")).Play();
                        Control.tamanoGrande = false;
                    }
                }
            }
        }

        //Boton Large
        public void verificaTouchBLarge(int ancho, int alto)
        {
            if (Control.tamanoGrande)
            {
                actualframeB = 1;
                defineTrozoAnim(0, actualframeB * 42, 125, 42);
            }
            else
            {
                actualframeB = 0;
                defineTrozoAnim(0, actualframeB * 42, 125, 42);
            }
            if (touches.Count > 0)
            {
                for (i = 0; i < touches.Count; i++)
                {
                    if (TouchLocationState.Released.Equals(touches[i].State) &&
                        touches[i].Position.X >= Position.X &&
                        touches[i].Position.X <= Position.X + (TrozoAnim.Width * escala) &&
                        touches[i].Position.Y >= Position.Y &&
                        touches[i].Position.Y <= Position.Y + (TrozoAnim.Height * escala)
                        )
                    {
                        (Audio.getEfecto("presionarBoton")).Play();
                        Control.tamanoGrande = true;
                    }
                }
            }
        }


        //Boton Quick Mode
        public void verificaTouchBQuickMode(int ancho, int alto)
        {
            if (touches.Count > 0)
            {
                for (i = 0; i < touches.Count; i++)
                {
                    if (TouchLocationState.Released.Equals(touches[i].State) &&
                        touches[i].Position.X >= Position.X &&
                        touches[i].Position.X <= Position.X + (TrozoAnim.Width * escala) &&
                        touches[i].Position.Y >= Position.Y &&
                        touches[i].Position.Y <= Position.Y + (TrozoAnim.Height * escala)
                        )
                    {

                        (Audio.getEfecto("presionarBoton")).Play();
                        if (Almacenamiento.hayJuegoSalvado())
                        {
                            QuickModeMenu.reiniciar();
                            GalacticaMain.estadoActual = GalacticaMain.Estado.SavedGame;
                        }
                        else
                        {
                            QuickModeMenu.reiniciar();
                            Almacenamiento.cargarBonus();
                            GalacticaMain.estadoActual = GalacticaMain.Estado.QuickGame;
                        }
                    }
                }
            }
        }

        //Boton About
        public void verificaTouchBAbout(int ancho, int alto)
        {
            if (touches.Count > 0)
            {
                for (i = 0; i < touches.Count; i++)
                {
                    if (TouchLocationState.Released.Equals(touches[i].State) &&
                        touches[i].Position.X >= Position.X &&
                        touches[i].Position.X <= Position.X + (TrozoAnim.Width * escala) &&
                        touches[i].Position.Y >= Position.Y &&
                        touches[i].Position.Y <= Position.Y + (TrozoAnim.Height * escala)
                        )
                    {
                        (Audio.getEfecto("presionarBoton")).Play();

                        GalacticaMain.estadoActual = GalacticaMain.Estado.About;
                    }
                }
            }
        }

        //Boton Salir
        public void verificaTouchBSalir(GalacticaMain contexto)
        {
            if (touches.Count > 0)
            {
                for (i = 0; i < touches.Count; i++)
                {
                    if (TouchLocationState.Released.Equals(touches[i].State) &&
                        touches[i].Position.X >= Position.X &&
                        touches[i].Position.X <= Position.X + (TrozoAnim.Width * escala) &&
                        touches[i].Position.Y >= Position.Y &&
                        touches[i].Position.Y <= Position.Y + (TrozoAnim.Height * escala)
                        )
                    {
                        (Audio.getEfecto("presionarBoton")).Play();
                        if (MediaPlayer.GameHasControl)
                        {
                            MediaPlayer.Pause();
                        }

                        contexto.Exit();
                    }
                }
            }
        }

        //Boton Opciones
        public void verificaTouchBOpciones(int ancho, int alto, GameTime gt)
        {
            if (touches.Count > 0)
            {
                for (i = 0; i < touches.Count; i++)
                {
                    if (TouchLocationState.Released.Equals(touches[i].State) &&
                        touches[i].Position.X >= Position.X &&
                        touches[i].Position.X <= Position.X + (TrozoAnim.Width * escala) &&
                        touches[i].Position.Y >= Position.Y &&
                        touches[i].Position.Y <= Position.Y + (TrozoAnim.Height * escala)
                        )
                    {
                        (Audio.getEfecto("presionarBoton")).Play();
                        GalacticaMain.estadoActual = GalacticaMain.Estado.Options;
                    }
                }
            }
        }

        //Boton Mision Mode
        public void verificaTouchBMisionMode(int ancho, int alto, GameTime gt)
        {
            if (touches.Count > 0)
            {
                for (i = 0; i < touches.Count; i++)
                {
                    if (TouchLocationState.Released.Equals(touches[i].State) &&
                        touches[i].Position.X >= Position.X &&
                        touches[i].Position.X <= Position.X + (TrozoAnim.Width * escala) &&
                        touches[i].Position.Y >= Position.Y &&
                        touches[i].Position.Y <= Position.Y + (TrozoAnim.Height * escala)
                        )
                    {
                        (Audio.getEfecto("presionarBoton")).Play();
                        MisionModeMenu.cargarMisiones();
                        GalacticaMain.estadoActual = GalacticaMain.Estado.MisionMode;
                    }
                }
            }
        }

        //Boton Help
        public void verificaTouchBHelp(int ancho, int alto)
        {
            if (touches.Count > 0)
            {
                for (i = 0; i < touches.Count; i++)
                {
                    if (TouchLocationState.Released.Equals(touches[i].State) &&
                        touches[i].Position.X >= Position.X &&
                        touches[i].Position.X <= Position.X + (TrozoAnim.Width * escala) &&
                        touches[i].Position.Y >= Position.Y &&
                        touches[i].Position.Y <= Position.Y + (TrozoAnim.Height * escala)
                        )
                    {
                        (Audio.getEfecto("presionarBoton")).Play();
                        GalacticaMain.estadoActual = GalacticaMain.Estado.Help;
                    }
                }
            }
        }

        //Boton Mision
        public void verificaTouchBMision(int ancho, int alto, GameTime gt)
        {
            defineTrozoAnim(actualframeB * 50, 0, 50, 50);
            if (touches.Count > 0)
            {
                for (i = 0; i < touches.Count; i++)
                {
                    if (TouchLocationState.Released.Equals(touches[i].State) &&
                        touches[i].Position.X >= Position.X &&
                        touches[i].Position.X <= Position.X + (TrozoAnim.Width * escala) &&
                        touches[i].Position.Y >= Position.Y &&
                        touches[i].Position.Y <= Position.Y + (TrozoAnim.Height * escala)
                        )
                    {
                        (Audio.getEfecto("presionarBoton")).Play();
                        for (j = 0; j < MisionModeMenu.idMision.Count; j++)
                        {
                            if (touches[i].Position.X >= MisionModeMenu.idMision[j].X &&
                                touches[i].Position.X <= MisionModeMenu.idMision[j].X + (TrozoAnim.Width * escala) &&
                                touches[i].Position.Y >= MisionModeMenu.idMision[j].Y &&
                                touches[i].Position.Y <= MisionModeMenu.idMision[j].Y + (TrozoAnim.Height * escala)
                               )
                            {
                                if (MisionModeMenu.estaActivo[j])
                                {
                                    indiceMision = j;
                                    break;
                                }
                                else { indiceMision = -1; }
                            }
                            else { indiceMision = -1; }
                        }
                        comprobarMensMision();
                    }
                }
            }
        }

        //Boton Play
        public void verificaTouchBPlay(int ancho, int alto, GameTime gt)
        {
            if (touches.Count > 0)
            {
                for (i = 0; i < touches.Count; i++)
                {
                    if (TouchLocationState.Released.Equals(touches[i].State) &&
                        touches[i].Position.X >= Position.X &&
                        touches[i].Position.X <= Position.X + (TrozoAnim.Width * escala) &&
                        touches[i].Position.Y >= Position.Y &&
                        touches[i].Position.Y <= Position.Y + (TrozoAnim.Height * escala)
                        )
                    {
                        (Audio.getEfecto("presionarBoton")).Play();
                        if (MediaPlayer.GameHasControl)
                        {
                            MediaPlayer.Stop();
                        }
                        if (MediaPlayer.GameHasControl && Audio.musicaActiva)
                        {
                            MediaPlayer.IsRepeating = true;
                            MediaPlayer.Play(Audio.getMusica("Galactic"));
                        }

                        if (GalacticaMain.estadoActual == GalacticaMain.Estado.QuickGame)
                        {
                            QuickMode.estadoActual = QuickMode.Estado.Game;
                            QuickModeGame.reiniciar();
                            Nave.reiniciar();
                            QuickModeGOver.tEsperaActivaBoton = 1200.0;
                            QuickModeGame.ultimoTPlus = gt.TotalGameTime.TotalMilliseconds;
                            QuickModeGame.tiempoTitileoMensaje = gt.TotalGameTime.TotalMilliseconds;
                        }
                        if (GalacticaMain.estadoActual == GalacticaMain.Estado.MisionMode)
                        {
                            QuickModeGame.reiniciar();
                            reiniciarMision();
                            MisionMode.estadoActual = MisionMode.Estado.Game;
                            MisionModeGOver.tEsperaActivaBoton = 1200.0;
                            QuickModeGame.tiempoTitileoMensaje = gt.TotalGameTime.TotalMilliseconds;
                        }
                    }
                }

            }
        }

        //Boton Flecha
        public void verificaTouchBFlecha(int ancho, int alto)
        {
            if (touches.Count > 0)
            {
                for (i = 0; i < touches.Count; i++)
                {
                    if (flechaDerecha)
                        if (TouchLocationState.Released.Equals(touches[i].State) &&
                            touches[i].Position.X >= Position.X - (TrozoAnim.Width * escala) &&
                            touches[i].Position.X <= Position.X &&
                            touches[i].Position.Y >= Position.Y - (TrozoAnim.Height * escala) &&
                            touches[i].Position.Y <= Position.Y
                            )
                        {
                            (Audio.getEfecto("presionarBoton")).Play();
                            if (QuickModeMenu.texturaNaveActual < navesActivas - 1/*QuickModeMenu.texturaNaves.Count*/)
                            {
                                Nave.TexturaNave = QuickModeMenu.texturaNaveActual + 1;
                                QuickModeMenu.texturaNaveActual = QuickModeMenu.texturaNaveActual + 1;
                            }
                        }

                    if (!flechaDerecha)
                        if (TouchLocationState.Released.Equals(touches[i].State) &&
                             touches[i].Position.X >= Position.X &&
                             touches[i].Position.X <= Position.X + (TrozoAnim.Width * escala) &&
                             touches[i].Position.Y >= Position.Y &&
                             touches[i].Position.Y <= Position.Y + (TrozoAnim.Height * escala)
                           )
                        {
                            (Audio.getEfecto("presionarBoton")).Play();
                            if (QuickModeMenu.texturaNaveActual > 0)
                            {
                                Nave.TexturaNave = QuickModeMenu.texturaNaveActual - 1;
                                QuickModeMenu.texturaNaveActual = QuickModeMenu.texturaNaveActual - 1;
                            }
                        }
                }
            }
        }

        //Boton Power
        public void verificaTouchBPower(int ancho, int alto, GameTime time)
        {
            if ((time.TotalGameTime.TotalMilliseconds > ultimaActualizacion + tiempoEsperaPoder) &&
                !bPoderActivo &&
                !(QuickMode.estadoActual == QuickMode.Estado.Pause) &&
                !Nave.poderActivo)
            {
                actualframeB = actualframeB + 1;
                ultimaActualizacion = time.TotalGameTime.TotalMilliseconds;
            }

            dato += time.ElapsedGameTime;
            if ((dato.TotalMilliseconds >=
                ((time.ElapsedGameTime.Milliseconds) * 3)) && bPoderActivo)
            {
                actualframeB = actualframeB + 1;
                dato = TimeSpan.Zero;
            }

            if (actualframeB >= 6 && !bPoderActivo)
            {
                actualframeB = 0;
                lineaAnim = 50;
                bPoderActivo = true;
            }

            if (actualframeB >= 6 && bPoderActivo)
            {
                actualframeB = 0;
            }

            defineTrozoAnim(50 * actualframeB, lineaAnim, 50, 50);

            if (touches.Count > 0)
            {
                for (i = 0; i < touches.Count; i++)
                {
                    if (TouchLocationState.Released.Equals(touches[i].State) &&
                        touches[i].Position.X >= Position.X &&
                        touches[i].Position.X <= Position.X + (TrozoAnim.Width * escala) &&
                        touches[i].Position.Y >= Position.Y &&
                        touches[i].Position.Y <= Position.Y + (TrozoAnim.Height * escala) &&
                        bPoderActivo
                        )
                    {
                        (Audio.getEfecto("inicioPoder")).Play();
                        Nave.poderActivo = true;
                        reiniciaBPower();
                    }
                }
            }
        }

        //Boton Continue
        public void verificaTouchBContinue(int ancho, int alto, GameTime t)
        {

            if (touches.Count > 0)
            {
                for (i = 0; i < touches.Count; i++)
                {
                    if (TouchLocationState.Released.Equals(touches[i].State) &&
                        touches[i].Position.X >= Position.X &&
                        touches[i].Position.X <= Position.X + (TrozoAnim.Width * escala) &&
                        touches[i].Position.Y >= Position.Y &&
                        touches[i].Position.Y <= Position.Y + (TrozoAnim.Height * escala)
                        )
                    {
                        (Audio.getEfecto("presionarBoton")).Play();
                        MainMenu.playMusic = true;

                        if (QuickMode.estadoActual == QuickMode.Estado.GameOver &&
                            GalacticaMain.estadoActual == GalacticaMain.Estado.QuickGame)
                        {
                            sendScore();
                            QuickMode.estadoActual = QuickMode.Estado.Menu;
                            GalacticaMain.estadoActual = GalacticaMain.Estado.MainMenu;
                            Nave.reiniciar();
                            QuickModeGame.reiniciar();
                        }
                        if (MisionMode.estadoActual == MisionMode.Estado.GameOver &&
                            GalacticaMain.estadoActual == GalacticaMain.Estado.MisionMode)
                        {
                            MisionMode.estadoActual = MisionMode.Estado.Menu;
                            GalacticaMain.estadoActual = GalacticaMain.Estado.MainMenu;
                            reiniciarMision();
                        }
                    }
                }
            }
        }


        //Boton Back
        public void verificaTouchBBack(int ancho, int alto, GameTime gt)
        {
            if (touches.Count > 0)
            {
                for (i = 0; i < touches.Count; i++)
                {
                    if (TouchLocationState.Released.Equals(touches[i].State) &&
                        touches[i].Position.X >= Position.X &&
                        touches[i].Position.X <= Position.X + (TrozoAnim.Width * escala) &&
                        touches[i].Position.Y >= Position.Y &&
                        touches[i].Position.Y <= Position.Y + (TrozoAnim.Height * escala)
                        )
                    {
                        (Audio.getEfecto("presionarBoton")).Play();

                        if (QuickMode.estadoActual == QuickMode.Estado.Menu &&
                            GalacticaMain.estadoActual == GalacticaMain.Estado.QuickGame)
                        {
                            GalacticaMain.estadoActual = GalacticaMain.Estado.MainMenu;
                        }
                        else
                            if (GalacticaMain.estadoActual == GalacticaMain.Estado.Options)
                            {
                                Almacenamiento.salvarOpciones();
                                GalacticaMain.estadoActual = GalacticaMain.Estado.MainMenu;
                            }
                            else
                                if (GalacticaMain.estadoActual == GalacticaMain.Estado.Help)
                                {
                                    GalacticaMain.estadoActual = GalacticaMain.Estado.MainMenu;
                                }
                                else
                                    if (GalacticaMain.estadoActual == GalacticaMain.Estado.About)
                                    {
                                        GalacticaMain.estadoActual = GalacticaMain.Estado.Help;
                                    }
                                    else
                                        if (GalacticaMain.estadoActual == GalacticaMain.Estado.Leaderboard)
                                        {
                                            GalacticaMain.estadoActual = GalacticaMain.Estado.MainMenu;
                                        }
                                        else
                                            if (GalacticaMain.estadoActual == GalacticaMain.Estado.SavedGame)
                                            {
                                                GalacticaMain.estadoActual = GalacticaMain.Estado.MainMenu;
                                            }
                                            else
                                                if (MisionMode.estadoActual == MisionMode.Estado.Menu &&
                                                    GalacticaMain.estadoActual == GalacticaMain.Estado.MisionMode)
                                                {
                                                    GalacticaMain.estadoActual = GalacticaMain.Estado.MainMenu;
                                                }
                                                else
                                                    if (MisionMode.estadoActual == MisionMode.Estado.Info &&
                                                        GalacticaMain.estadoActual == GalacticaMain.Estado.MisionMode)
                                                    {
                                                        MisionMode.estadoActual = MisionMode.Estado.Menu;
                                                        MisionModeMenu.cargarMisiones();
                                                    }
                    }
                }
            }
        }

        //Boton Next Mision
        public void verificaTouchBNextMision(int ancho, int alto, GameTime gt)
        {
            if (touches.Count > 0)
            {
                for (i = 0; i < touches.Count; i++)
                {
                    if (TouchLocationState.Released.Equals(touches[i].State) &&
                        touches[i].Position.X >= Position.X &&
                        touches[i].Position.X <= Position.X + (TrozoAnim.Width * escala) &&
                        touches[i].Position.Y >= Position.Y &&
                        touches[i].Position.Y <= Position.Y + (TrozoAnim.Height * escala)
                        )
                    {
                        (Audio.getEfecto("presionarBoton")).Play();
                        if (MediaPlayer.GameHasControl)
                        {
                            MediaPlayer.IsRepeating = true;
                            MediaPlayer.Play(Audio.getMusica("mainMenu"));
                        }
                        MainMenu.playMusic = true;
                        reiniciarMision();
                        indiceMision = indiceMision + 1;
                        comprobarMensMision();
                    }
                }
            }
        }


        //Boton Restart
        public void verificaTouchBRestart(int ancho, int alto, GameTime gt)
        {
            if (touches.Count > 0)
            {
                for (i = 0; i < touches.Count; i++)
                {
                    if (TouchLocationState.Released.Equals(touches[i].State) &&
                        touches[i].Position.X >= Position.X &&
                        touches[i].Position.X <= Position.X + (TrozoAnim.Width * escala) &&
                        touches[i].Position.Y >= Position.Y &&
                        touches[i].Position.Y <= Position.Y + (TrozoAnim.Height * escala)
                        )
                    {
                        (Audio.getEfecto("presionarBoton")).Play();
                        if (MediaPlayer.GameHasControl && Audio.musicaActiva)
                        {
                            MediaPlayer.IsRepeating = true;
                            MediaPlayer.Play(Audio.getMusica("Galactic"));
                        }

                        if (QuickMode.estadoActual == QuickMode.Estado.GameOver &&
                            GalacticaMain.estadoActual == GalacticaMain.Estado.QuickGame)
                        {
                            sendScore2();
                            Nave.reiniciar();
                            QuickModeGame.reiniciar();
                            QuickModeGame.ultimoTPlus = gt.TotalGameTime.TotalMilliseconds;
                            QuickModeGOver.tEsperaActivaBoton = 1200.0;
                            QuickModeGame.tiempoTitileoMensaje = gt.TotalGameTime.TotalMilliseconds;
                            QuickMode.estadoActual = QuickMode.Estado.Game;
                        }
                        if (MisionMode.estadoActual == MisionMode.Estado.GameOver &&
                            GalacticaMain.estadoActual == GalacticaMain.Estado.MisionMode)
                        {
                            reiniciarMision();
                            MisionModeGOver.tEsperaActivaBoton = 1200.0;
                            MisionMode.estadoActual = MisionMode.Estado.Game;
                            QuickModeGame.tiempoTitileoMensaje = gt.TotalGameTime.TotalMilliseconds;
                        }

                    }
                }
            }
        }

        //Boton ContinueGame
        public void verificaTouchBContinueGame(int ancho, int alto, GameTime gt)
        {

            if (touches.Count > 0)
            {
                for (i = 0; i < touches.Count; i++)
                {
                    if (TouchLocationState.Released.Equals(touches[i].State) &&
                        touches[i].Position.X >= Position.X &&
                        touches[i].Position.X <= Position.X + (TrozoAnim.Width * escala) &&
                        touches[i].Position.Y >= Position.Y &&
                        touches[i].Position.Y <= Position.Y + (TrozoAnim.Height * escala)
                        )
                    {
                        (Audio.getEfecto("presionarBoton")).Play();

                        QuickModeGame.reiniciar();
                        Nave.reiniciar();
                        Almacenamiento.cargarBonus();
                        Almacenamiento.cargarInGame();
                        QuickModeGame.tiempoTitileoMensaje = gt.TotalGameTime.TotalMilliseconds;
                        QuickModeGame.ultimoTPlus = gt.TotalGameTime.TotalMilliseconds;
                        QuickMode.estadoActual = QuickMode.Estado.Game;
                        GalacticaMain.estadoActual = GalacticaMain.Estado.QuickGame;
                        if (MediaPlayer.GameHasControl && Audio.musicaActiva)
                        {
                            MediaPlayer.IsRepeating = true;
                            MediaPlayer.Play(Audio.getMusica("Galactic"));
                        }
                    }
                }
            }
        }


        //Boton NewGame
        public void verificaTouchBNewGame(int ancho, int alto)
        {
            if (touches.Count > 0)
            {
                for (i = 0; i < touches.Count; i++)
                {
                    if (TouchLocationState.Released.Equals(touches[i].State) &&
                        touches[i].Position.X >= Position.X &&
                        touches[i].Position.X <= Position.X + (TrozoAnim.Width * escala) &&
                        touches[i].Position.Y >= Position.Y &&
                        touches[i].Position.Y <= Position.Y + (TrozoAnim.Height * escala)
                        )
                    {
                        (Audio.getEfecto("presionarBoton")).Play();
                        Almacenamiento.borrarInGame();
                        QuickModeGame.reiniciar();
                        Nave.reiniciar();
                        Almacenamiento.cargarBonus();
                        GalacticaMain.estadoActual = GalacticaMain.Estado.QuickGame;
                        QuickMode.estadoActual = QuickMode.Estado.Menu;
                    }
                }
            }
        }

        //Boton Facebook
        public void verificaTouchBFacebook(int ancho, int alto)
        {
            if (touches.Count > 0)
            {
                for (i = 0; i < touches.Count; i++)
                {
                    if (TouchLocationState.Released.Equals(touches[i].State) &&
                        touches[i].Position.X >= Position.X &&
                        touches[i].Position.X <= Position.X + (TrozoAnim.Width * escala) &&
                        touches[i].Position.Y >= Position.Y &&
                        touches[i].Position.Y <= Position.Y + (TrozoAnim.Height * escala)
                        )
                    {
                        palFace.Show();
                    }
                }
            }
        }

        //Boton Facebook
        public void verificaTouchBTwitter(int ancho, int alto)
        {
            if (touches.Count > 0)
            {
                for (i = 0; i < touches.Count; i++)
                {
                    if (TouchLocationState.Released.Equals(touches[i].State) &&
                        touches[i].Position.X >= Position.X &&
                        touches[i].Position.X <= Position.X + (TrozoAnim.Width * escala) &&
                        touches[i].Position.Y >= Position.Y &&
                        touches[i].Position.Y <= Position.Y + (TrozoAnim.Height * escala)
                        )
                    {
                        palTui.Show();
                    }
                }
            }
        }

        //Boton Comentario
        public void verificaTouchBComent(int ancho, int alto)
        {
            if (touches.Count > 0)
            {
                for (i = 0; i < touches.Count; i++)
                {
                    if (TouchLocationState.Released.Equals(touches[i].State) &&
                        touches[i].Position.X >= Position.X &&
                        touches[i].Position.X <= Position.X + (TrozoAnim.Width * escala) &&
                        touches[i].Position.Y >= Position.Y &&
                        touches[i].Position.Y <= Position.Y + (TrozoAnim.Height * escala)
                        )
                    {
                        (Audio.getEfecto("presionarBoton")).Play();
                        MarketplaceReviewTask marketplaceReviewTask = new MarketplaceReviewTask();
                        marketplaceReviewTask.Show();
                        //Guide.ShowMarketplace(PlayerIndex.One);
                    }
                }
            }
        }

        //Boton GoBuy
        public void verificaTouchBGoBuy(int ancho, int alto)
        {
            if (touches.Count > 0)
            {
                for (i = 0; i < touches.Count; i++)
                {
                    if (TouchLocationState.Released.Equals(touches[i].State) &&
                        touches[i].Position.X >= Position.X &&
                        touches[i].Position.X <= Position.X + (TrozoAnim.Width * escala) &&
                        touches[i].Position.Y >= Position.Y &&
                        touches[i].Position.Y <= Position.Y + (TrozoAnim.Height * escala)
                        )
                    {
                        (Audio.getEfecto("presionarBoton")).Play();
                        paComprar.Show();
                        //Guide.ShowMarketplace(PlayerIndex.One);
                    }
                }
            }
        }

        //Boton GoBuy
        public void verificaTouchBNotNow(int ancho, int alto)
        {
            if (touches.Count > 0)
            {
                for (i = 0; i < touches.Count; i++)
                {
                    if (TouchLocationState.Released.Equals(touches[i].State) &&
                        touches[i].Position.X >= Position.X &&
                        touches[i].Position.X <= Position.X + (TrozoAnim.Width * escala) &&
                        touches[i].Position.Y >= Position.Y &&
                        touches[i].Position.Y <= Position.Y + (TrozoAnim.Height * escala)
                        )
                    {
                        (Audio.getEfecto("presionarBoton")).Play();
                        if (QuickMode.estadoActual == QuickMode.Estado.Game)
                        {
                            GalacticaMain.estadoActual = GalacticaMain.Estado.MainMenu;
                            QuickMode.estadoActual = QuickMode.Estado.Menu;
                            Nave.reiniciar();
                            QuickModeGame.reiniciar();
                        }
                        else
                        {
                            GalacticaMain.estadoActual = GalacticaMain.Estado.MisionMode;
                            MisionMode.estadoActual = MisionMode.Estado.Menu;
                            MisionModeMenu.cargarMisiones();
                        }
                    }
                }
            }
        }

        //Boton Leaderboard
        public void verificaTouchBLeaderboard(int ancho, int alto)
        {
            if (touches.Count > 0)
            {
                for (i = 0; i < touches.Count; i++)
                {
                    if (TouchLocationState.Released.Equals(touches[i].State) &&
                        touches[i].Position.X >= Position.X &&
                        touches[i].Position.X <= Position.X + (TrozoAnim.Width * escala) &&
                        touches[i].Position.Y >= Position.Y &&
                        touches[i].Position.Y <= Position.Y + (TrozoAnim.Height * escala)
                        )
                    {
                        (Audio.getEfecto("presionarBoton")).Play();
                        GalacticaMain.estadoActual = GalacticaMain.Estado.Leaderboard;
                    }
                }
            }
        }


        /**************Demas metodos****************/

        public void reiniciaBPower()
        {
            actualframeB = 0;
            lineaAnim = 0;
            bPoderActivo = false;
            ultimaActualizacion = 0;
        }

        public void defineTrozoAnim(int x, int y, int width, int height)
        {
            TrozoAnim.X = x;
            TrozoAnim.Y = y;
            TrozoAnim.Width = width;
            TrozoAnim.Height = height;
        }

        public void setPosition(int x, int y)
        {
            Position.X = x;
            Position.Y = y;
        }

        public void setOrigen(int x, int y)
        {
            Position.X = x;
            Position.Y = y;
        }


        public static void sendScore()
        {
            if (newBestScore)
            {
                if (!(Guide.IsVisible))
                {
                    // Show the input dialog to get text from the user
                    Guide.BeginShowKeyboardInput(PlayerIndex.One, "New high score!!", "Please enter your name", GalacticaMain.NOMBRE, InputCallback, null);
                }
            }
        }

        public static void sendScore2()
        {
            if (newBestScore)
            {
                Almacenamiento.cargarScore();
                Leaderboard.sendHighScore(GalacticaMain.NOMBRE, GalacticaMain.BESTSCORE);
                Almacenamiento.salvarScore();
                newBestScore = false;
            }
        }

        /// <summary>
        /// This function will be called when the SIP name entry is completed or cancelled
        /// </summary>
        /// <param name="result"></param>
        public static void InputCallback(IAsyncResult result)
        {

            string sipContent = Guide.EndShowKeyboardInput(result);
            string nombreFinal = "";

            // Did we get some input from the user?
            if (sipContent != null)
            {
                // Store it in the text object
                nombre = sipContent;
                try
                {
                    nombre = nombre.Substring(0, 8);
                }
                catch (ArgumentOutOfRangeException)
                {
                    nombre = sipContent;
                }


                for (int i = 0; i < nombre.Length; i++)
                {
                    if (fuente.Characters.Contains(Convert.ToChar(nombre.Substring(i, 1))))
                    {
                        nombreFinal = nombreFinal + nombre.Substring(i, 1);
                    }
                    else
                    {
                        nombreFinal = nombreFinal + "?";
                    }
                }

                GalacticaMain.NOMBRE = nombreFinal;
                Leaderboard.sendHighScore(nombreFinal, GalacticaMain.BESTSCORE);
                Almacenamiento.salvarScore();
                newBestScore = false;
            }
            else
            {
                Leaderboard.sendHighScore("Player", GalacticaMain.BESTSCORE);
                newBestScore = false;
            }
        }


        public static void setTexturas(Textura t)
        {
            textura = new List<Texture2D>();
            textura.Add(t.getTextura("BQuickMode"));//0
            textura.Add(t.getTextura("PauseBoton"));//1
            textura.Add(t.getTextura("BResumeGame"));//2
            textura.Add(t.getTextura("BExitGame"));//3
            textura.Add(t.getTextura("BFlecha"));//4
            textura.Add(t.getTextura("BPlay"));//5
            textura.Add(t.getTextura("BPower"));//6
            textura.Add(t.getTextura("BContinue"));//7
            textura.Add(t.getTextura("BMision"));//8
            textura.Add(t.getTextura("BMisionMode"));//9
            textura.Add(t.getTextura("BBack"));//10
            textura.Add(t.getTextura("BNextMision"));//11
            textura.Add(t.getTextura("BRestart"));//12
            textura.Add(t.getTextura("BAudio"));//13
            textura.Add(t.getTextura("BOpciones"));//14
            textura.Add(t.getTextura("BSalir"));//15
            textura.Add(t.getTextura("BTilting"));//16
            textura.Add(t.getTextura("BButtons"));//17
            textura.Add(t.getTextura("BMedium"));//18
            textura.Add(t.getTextura("BLarge"));//19
            textura.Add(t.getTextura("BHelp"));//20
            textura.Add(t.getTextura("BAbout"));//21
            textura.Add(t.getTextura("BContinueGame"));//22
            textura.Add(t.getTextura("BNewGame"));//23
            textura.Add(t.getTextura("BFacebook"));//24
            textura.Add(t.getTextura("BComent"));//25
            textura.Add(t.getTextura("BGoBuy"));//26
            textura.Add(t.getTextura("BNotNow"));//27
            textura.Add(t.getTextura("BLeaderboard"));//28
            textura.Add(t.getTextura("BTwitter"));//29
        }

        //****Reiniciar Mision
        public static void reiniciarMision()
        {
            if (indiceMision != -1)
            {
                Nave.reiniciar();
                switch (MisionModeGame.numMision)
                {
                    case 1:
                        Mision1.reiniciar();
                        break;
                    case 2:
                        Mision2.reiniciar();
                        break;
                    case 3:
                        Mision3.reiniciar();
                        break;
                    case 4:
                        Mision4.reiniciar();
                        break;
                    case 5:
                        Mision5.reiniciar();
                        break;
                    case 6:
                        Mision6.reiniciar();
                        break;
                    case 7:
                        Mision7.reiniciar();
                        break;
                    case 8:
                        Mision8.reiniciar();
                        break;
                    case 9:
                        Mision9.reiniciar();
                        break;
                    case 10:
                        Mision10.reiniciar();
                        break;
                    case 11:
                        Mision11.reiniciar();
                        break;
                    case 12:
                        Mision12.reiniciar();
                        break;
                    case 13:
                        Mision13.reiniciar();
                        break;
                    case 14:
                        Mision14.reiniciar();
                        break;
                    case 15:
                        Mision15.reiniciar();
                        break;
                    case 16:
                        Mision16.reiniciar();
                        break;
                    case 17:
                        Mision17.reiniciar();
                        break;
                    case 18:
                        Mision18.reiniciar();
                        break;
                    case 19:
                        Mision19.reiniciar();
                        break;
                    case 20:
                        Mision20.reiniciar();
                        break;
                }
            }
        }

        public void comprobarMensMision()
        {

            MisionModeGame.numMision = indiceMision + 1;

            if (indiceMision != -1)
            {
                switch (MisionModeGame.numMision)
                {
                    case 1:
                        MisionMode.estadoActual = MisionMode.Estado.Info;
                        MisionModeInfo.InfoMision = Mision1.mensajeMision;
                        MisionModeInfo.TituloMision = Mision1.tituloMision;
                        break;
                    case 2:
                        MisionMode.estadoActual = MisionMode.Estado.Info;
                        MisionModeInfo.InfoMision = Mision2.mensajeMision;
                        MisionModeInfo.TituloMision = Mision2.tituloMision;
                        break;
                    case 3:
                        MisionMode.estadoActual = MisionMode.Estado.Info;
                        MisionModeInfo.InfoMision = Mision3.mensajeMision;
                        MisionModeInfo.TituloMision = Mision3.tituloMision;
                        break;
                    case 4:
                        MisionMode.estadoActual = MisionMode.Estado.Info;
                        MisionModeInfo.InfoMision = Mision4.mensajeMision;
                        MisionModeInfo.TituloMision = Mision4.tituloMision;
                        break;
                    case 5:
                        MisionMode.estadoActual = MisionMode.Estado.Info;
                        MisionModeInfo.InfoMision = Mision5.mensajeMision;
                        MisionModeInfo.TituloMision = Mision5.tituloMision;
                        break;
                    case 6:
                        GalacticaMain.estadoActual = GalacticaMain.Estado.Buy;
                        break;
                    case 7:
                        MisionMode.estadoActual = MisionMode.Estado.Info;
                        MisionModeInfo.InfoMision = Mision7.mensajeMision;
                        MisionModeInfo.TituloMision = Mision7.tituloMision;
                        break;
                    case 8:
                        MisionMode.estadoActual = MisionMode.Estado.Info;
                        MisionModeInfo.InfoMision = Mision8.mensajeMision;
                        MisionModeInfo.TituloMision = Mision8.tituloMision;
                        break;
                    case 9:
                        MisionMode.estadoActual = MisionMode.Estado.Info;
                        MisionModeInfo.InfoMision = Mision9.mensajeMision;
                        MisionModeInfo.TituloMision = Mision9.tituloMision;
                        break;
                    case 10:
                        MisionMode.estadoActual = MisionMode.Estado.Info;
                        MisionModeInfo.InfoMision = Mision10.mensajeMision;
                        MisionModeInfo.TituloMision = Mision10.tituloMision;
                        break;
                    case 11:
                        MisionMode.estadoActual = MisionMode.Estado.Info;
                        MisionModeInfo.InfoMision = Mision11.mensajeMision;
                        MisionModeInfo.TituloMision = Mision11.tituloMision;
                        break;
                    case 12:
                        MisionMode.estadoActual = MisionMode.Estado.Info;
                        MisionModeInfo.InfoMision = Mision12.mensajeMision;
                        MisionModeInfo.TituloMision = Mision12.tituloMision;
                        break;
                    case 13:
                        MisionMode.estadoActual = MisionMode.Estado.Info;
                        MisionModeInfo.InfoMision = Mision13.mensajeMision;
                        MisionModeInfo.TituloMision = Mision13.tituloMision;
                        break;
                    case 14:
                        MisionMode.estadoActual = MisionMode.Estado.Info;
                        MisionModeInfo.InfoMision = Mision14.mensajeMision;
                        MisionModeInfo.TituloMision = Mision14.tituloMision;
                        break;
                    case 15:
                        MisionMode.estadoActual = MisionMode.Estado.Info;
                        MisionModeInfo.InfoMision = Mision15.mensajeMision;
                        MisionModeInfo.TituloMision = Mision15.tituloMision;
                        break;
                    case 16:
                        MisionMode.estadoActual = MisionMode.Estado.Info;
                        MisionModeInfo.InfoMision = Mision16.mensajeMision;
                        MisionModeInfo.TituloMision = Mision16.tituloMision;
                        break;
                    case 17:
                        MisionMode.estadoActual = MisionMode.Estado.Info;
                        MisionModeInfo.InfoMision = Mision17.mensajeMision;
                        MisionModeInfo.TituloMision = Mision17.tituloMision;
                        break;
                    case 18:
                        MisionMode.estadoActual = MisionMode.Estado.Info;
                        MisionModeInfo.InfoMision = Mision18.mensajeMision;
                        MisionModeInfo.TituloMision = Mision18.tituloMision;
                        break;
                    case 19:
                        MisionMode.estadoActual = MisionMode.Estado.Info;
                        MisionModeInfo.InfoMision = Mision19.mensajeMision;
                        MisionModeInfo.TituloMision = Mision19.tituloMision;
                        break;
                    case 20:
                        MisionMode.estadoActual = MisionMode.Estado.Info;
                        MisionModeInfo.InfoMision = Mision20.mensajeMision;
                        MisionModeInfo.TituloMision = Mision20.tituloMision;
                        break;
                }
            }
        }

    }
}
