using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galactica
{
    class Collition
    {
        private float distancia;
        private int i=0,j=0,w=0;
        public static Disparo.tipo ultimaColisionTipoDisparo;

        //**Colision Plus Nave
        public bool colisionPlusANave(Plus plus)
        {
            for (int i = 0; i < 11; i++)
            {
                distancia = (float)Math.Sqrt(
                      Math.Pow(plus.Position.X - (Nave.PositionNav.X + Nave.collitionX[i]), 2) +
                      Math.Pow(plus.Position.Y - (Nave.PositionNav.Y + Nave.collitionY[i]), 2)
                    );

                if (distancia <= plus.radioCollition + Nave.radioCollitionN)
                {
                    (Audio.getEfecto("Estrella")).Play();
                    return true;
                }
            }
            return false;
        }

        /*Colision disparo a enemigo*/
        public bool colisionDispAEnemigo(Enemigo enemigo,Enemigo.tipo tipoEnemigo)
        {
            if(tipoEnemigo== Enemigo.tipo.Nor && Nave.disparos.Count > 0)
            {
                for (i = 0; i < Nave.disparos.Count;i++ )
                {
                    distancia = (float)Math.Sqrt(
                      Math.Pow(Nave.disparos[i].Position.X - enemigo.Position.X, 2) +
                      Math.Pow(Nave.disparos[i].Position.Y - enemigo.Position.Y, 2)
                    );

                    if (distancia <= Nave.disparos[i].radioCollition + Enemigo.radioCollitionNor)
                    {
                        confirmaEliminaDisparoNave(i);
                        return true;
                    }

                }
            }else
            if (tipoEnemigo == Enemigo.tipo.Kami && Nave.disparos.Count > 0)
            {
                 for (i = 0; i < Nave.disparos.Count; i++)
                 {
                     distancia = (float)Math.Sqrt(
                       Math.Pow(Nave.disparos[i].Position.X - enemigo.Position.X, 2) +
                       Math.Pow(Nave.disparos[i].Position.Y - enemigo.Position.Y, 2)
                     );

                     if (distancia <= Nave.disparos[i].radioCollition + Enemigo.radioCollitionKami)
                     {
                         confirmaEliminaDisparoNave(i);
                         return true;
                     }
                 }
             }else
             if (tipoEnemigo == Enemigo.tipo.Sid && Nave.disparos.Count > 0)
             {
                 for (i = 0; i < Nave.disparos.Count; i++)
                 {
                     for (j = 0; j < 5; j++)
                     {
                         distancia = (float)Math.Sqrt(
                           Math.Pow(Nave.disparos[i].Position.X - (enemigo.Position.X + Enemigo.collitionXSid[j]), 2) +
                           Math.Pow(Nave.disparos[i].Position.Y - (enemigo.Position.Y + Enemigo.collitionYSid[j]), 2)
                         );

                         if (distancia <= Nave.disparos[i].radioCollition + Enemigo.radioCollitionSid)
                         {
                             confirmaEliminaDisparoNave(i);
                             return true;
                         }
                     }
                 }
             }else
            if (tipoEnemigo == Enemigo.tipo.HiNor && Nave.disparos.Count > 0)
            {
                for (i = 0; i < Nave.disparos.Count; i++)
                {
                    distancia = (float)Math.Sqrt(
                      Math.Pow(Nave.disparos[i].Position.X - enemigo.Position.X, 2) +
                      Math.Pow(Nave.disparos[i].Position.Y - enemigo.Position.Y, 2)
                    );

                    if (distancia <= Nave.disparos[i].radioCollition + Enemigo.radioCollitionHiNor)
                    {
                        confirmaEliminaDisparoNave(i);
                        return true;
                    }
                }
            }
            else
            if (tipoEnemigo == Enemigo.tipo.HiKami && Nave.disparos.Count > 0)
            {
                 for (i = 0; i < Nave.disparos.Count; i++)
                 {
                        distancia = (float)Math.Sqrt(
                          Math.Pow(Nave.disparos[i].Position.X - enemigo.Position.X, 2) +
                          Math.Pow(Nave.disparos[i].Position.Y - enemigo.Position.Y, 2)
                        );

                        if (distancia <= Nave.disparos[i].radioCollition + Enemigo.radioCollitionHiKami)
                        {
                            confirmaEliminaDisparoNave(i);
                            return true;
                        }
                  }
              }else
              if (tipoEnemigo == Enemigo.tipo.HiSid && Nave.disparos.Count > 0)
              {
                        for (i = 0; i < Nave.disparos.Count; i++)
                        {
                            for (j = 0; j < 5; j++)
                            {
                                distancia = (float)Math.Sqrt(
                                  Math.Pow(Nave.disparos[i].Position.X - (enemigo.Position.X + Enemigo.collitionXHiSid[j]), 2) +
                                  Math.Pow(Nave.disparos[i].Position.Y - (enemigo.Position.Y + Enemigo.collitionYHiSid[j]), 2)
                                );

                                if (distancia <= Nave.disparos[i].radioCollition + Enemigo.radioCollitionHiSid)
                                {
                                    confirmaEliminaDisparoNave(i);
                                    return true;
                                }
                            }
                        }
               }

            return false;
        }

        /*Colision PoderGalactica a enemigo*/
        public bool colisionPoderGalacticaAEnemigo(Enemigo enemigo, Enemigo.tipo tipoEnemigo)
        {

            if (tipoEnemigo == Enemigo.tipo.Nor && Nave.poderGalactica.Count > 0)
            {
                for (i = 0; i < Nave.poderGalactica.Count; i++)
                {
                    distancia = (float)Math.Sqrt(
                      Math.Pow(Nave.poderGalactica[i].Position.X - enemigo.Position.X, 2) +
                      Math.Pow(Nave.poderGalactica[i].Position.Y - enemigo.Position.Y, 2)
                    );

                    if (distancia <= Poderes.radioCollitPGalactica + Enemigo.radioCollitionNor)
                    {
                        Nave.poderGalacticaTotal.Add(Nave.poderGalactica[i]);
                        Nave.poderGalactica.RemoveAt(i);
                        return true;
                    }

                }
            }else
            if (tipoEnemigo == Enemigo.tipo.Kami && Nave.poderGalactica.Count > 0)
            {
                    for (i = 0; i < Nave.poderGalactica.Count; i++)
                    {
                        distancia = (float)Math.Sqrt(
                          Math.Pow(Nave.poderGalactica[i].Position.X - enemigo.Position.X, 2) +
                          Math.Pow(Nave.poderGalactica[i].Position.Y - enemigo.Position.Y, 2)
                        );

                        if (distancia <= Poderes.radioCollitPGalactica + Enemigo.radioCollitionKami)
                        {
                            Nave.poderGalacticaTotal.Add(Nave.poderGalactica[i]);
                            Nave.poderGalactica.RemoveAt(i);
                            return true;
                        }
                    }
             }else
             if (tipoEnemigo == Enemigo.tipo.Sid && Nave.poderGalactica.Count > 0)
             {
                    for (i = 0; i < Nave.poderGalactica.Count; i++)
                    {
                        for (j = 0; j < 5; j++)
                        {
                             distancia = (float)Math.Sqrt(
                               Math.Pow(Nave.poderGalactica[i].Position.X - 
                                     (enemigo.Position.X + Enemigo.collitionXSid[j]), 2) +
                               Math.Pow(Nave.poderGalactica[i].Position.Y - 
                                     (enemigo.Position.Y + Enemigo.collitionYSid[j]), 2)
                             );

                             if (distancia <= Poderes.radioCollitPGalactica + Enemigo.radioCollitionSid)
                             {
                                 Nave.poderGalacticaTotal.Add(Nave.poderGalactica[i]);
                                 Nave.poderGalactica.RemoveAt(i);
                                 return true;
                             }
                         }
                     }
              }else
              if (tipoEnemigo == Enemigo.tipo.HiNor && Nave.poderGalactica.Count > 0)
              {
                   for (i = 0; i < Nave.poderGalactica.Count; i++)
                   {
                         distancia = (float)Math.Sqrt(
                           Math.Pow(Nave.poderGalactica[i].Position.X - enemigo.Position.X, 2) +
                           Math.Pow(Nave.poderGalactica[i].Position.Y - enemigo.Position.Y, 2)
                         );

                         if (distancia <= Poderes.radioCollitPGalactica + Enemigo.radioCollitionHiNor)
                         {
                             Nave.poderGalacticaTotal.Add(Nave.poderGalactica[i]);
                             Nave.poderGalactica.RemoveAt(i);
                             return true;
                         }

                   }
              }else
              if (tipoEnemigo == Enemigo.tipo.HiKami && Nave.poderGalactica.Count > 0)
              {
                    for (i = 0; i < Nave.poderGalactica.Count; i++)
                    {
                          distancia = (float)Math.Sqrt(
                             Math.Pow(Nave.poderGalactica[i].Position.X - enemigo.Position.X, 2) +
                             Math.Pow(Nave.poderGalactica[i].Position.Y - enemigo.Position.Y, 2)
                          );

                          if (distancia <= Poderes.radioCollitPGalactica + Enemigo.radioCollitionHiKami)
                          {
                             Nave.poderGalacticaTotal.Add(Nave.poderGalactica[i]);
                             Nave.poderGalactica.RemoveAt(i);
                             return true;
                          }
                     }
              }else
              if (tipoEnemigo == Enemigo.tipo.HiSid && Nave.poderGalactica.Count > 0)
              {
                   for (i = 0; i < Nave.poderGalactica.Count; i++)
                   {
                        for (j = 0; j < 5; j++)
                        {
                             distancia = (float)Math.Sqrt(
                               Math.Pow(Nave.poderGalactica[i].Position.X -
                               (enemigo.Position.X + Enemigo.collitionXHiSid[j]), 2) +
                               Math.Pow(Nave.poderGalactica[i].Position.Y -
                               (enemigo.Position.Y + Enemigo.collitionYHiSid[j]), 2)
                             );

                             if (distancia <= Poderes.radioCollitPGalactica + Enemigo.radioCollitionHiSid)
                             {
                                Nave.poderGalacticaTotal.Add(Nave.poderGalactica[i]);
                                Nave.poderGalactica.RemoveAt(i);
                                return true;
                             }
                         }
                     }
               }

            return false;
        }

        /*Colision PoderCarafe a enemigo*/
        public bool colisionPoderCarafeAEnemigo(Enemigo enemigo, Enemigo.tipo tipoEnemigo)
        {
            if (tipoEnemigo == Enemigo.tipo.Nor && Nave.poderCarafe.Count > 0)
            {
                for (i = 0; i < Nave.poderCarafe.Count; i++)
                {
                    distancia = (float)Math.Sqrt(
                      Math.Pow(Nave.poderCarafe[i].Position.X - enemigo.Position.X, 2) +
                      Math.Pow(Nave.poderCarafe[i].Position.Y - enemigo.Position.Y, 2)
                    );

                    if (distancia <= Poderes.radioCollitPCarafe + Enemigo.radioCollitionNor)
                    {
                        confirmaEliminaPoderCarafe(i);
                        return true;
                    }
                }
            }else
            if (tipoEnemigo == Enemigo.tipo.Kami && Nave.poderCarafe.Count > 0)
            {
                for (i = 0; i < Nave.poderCarafe.Count; i++)
                {
                    distancia = (float)Math.Sqrt(
                      Math.Pow(Nave.poderCarafe[i].Position.X - enemigo.Position.X, 2) +
                      Math.Pow(Nave.poderCarafe[i].Position.Y - enemigo.Position.Y, 2)
                    );

                    if (distancia <= Poderes.radioCollitPCarafe + Enemigo.radioCollitionKami)
                    {
                        confirmaEliminaPoderCarafe(i);
                        return true;
                    }
                }
            }else
            if (tipoEnemigo == Enemigo.tipo.Sid && Nave.poderCarafe.Count > 0)
            {
                for (i = 0; i < Nave.poderCarafe.Count; i++)
                {
                    for (j = 0; j < 5; j++)
                    {
                        distancia = (float)Math.Sqrt(
                          Math.Pow(Nave.poderCarafe[i].Position.X -
                                (enemigo.Position.X + Enemigo.collitionXSid[j]), 2) +
                          Math.Pow(Nave.poderCarafe[i].Position.Y -
                                (enemigo.Position.Y + Enemigo.collitionYSid[j]), 2)
                        );

                        if (distancia <= Poderes.radioCollitPCarafe + Enemigo.radioCollitionSid)
                        {
                            confirmaEliminaPoderCarafe(i);
                            return true;
                        }
                    }
                }
            }else
            if (tipoEnemigo == Enemigo.tipo.HiNor && Nave.poderCarafe.Count > 0)
            {
                for (i = 0; i < Nave.poderCarafe.Count; i++)
                {
                    distancia = (float)Math.Sqrt(
                      Math.Pow(Nave.poderCarafe[i].Position.X - enemigo.Position.X, 2) +
                      Math.Pow(Nave.poderCarafe[i].Position.Y - enemigo.Position.Y, 2)
                    );

                    if (distancia <= Poderes.radioCollitPCarafe + Enemigo.radioCollitionHiNor)
                    {
                        confirmaEliminaPoderCarafe(i);
                        return true;
                    }
                }
            }else
            if (tipoEnemigo == Enemigo.tipo.HiKami && Nave.poderCarafe.Count > 0)
            {
                for (i = 0; i < Nave.poderCarafe.Count; i++)
                {
                    distancia = (float)Math.Sqrt(
                       Math.Pow(Nave.poderCarafe[i].Position.X - enemigo.Position.X, 2) +
                       Math.Pow(Nave.poderCarafe[i].Position.Y - enemigo.Position.Y, 2)
                    );

                    if (distancia <= Poderes.radioCollitPCarafe + Enemigo.radioCollitionHiKami)
                    {
                        confirmaEliminaPoderCarafe(i);
                        return true;
                    }
                }
            }else
            if (tipoEnemigo == Enemigo.tipo.HiSid && Nave.poderCarafe.Count > 0)
            {
                for (i = 0; i < Nave.poderCarafe.Count; i++)
                {
                    for (j = 0; j < 5; j++)
                    {
                        distancia = (float)Math.Sqrt(
                          Math.Pow(Nave.poderCarafe[i].Position.X -
                          (enemigo.Position.X + Enemigo.collitionXHiSid[j]), 2) +
                          Math.Pow(Nave.poderCarafe[i].Position.Y -
                          (enemigo.Position.Y + Enemigo.collitionYHiSid[j]), 2)
                        );

                        if (distancia <= Poderes.radioCollitPCarafe + Enemigo.radioCollitionHiSid)
                        {
                            confirmaEliminaPoderCarafe(i);
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        //Colision Poder Helix aEnemigo
        public bool colisionPoderHelixAEnemigo(Enemigo enemigo, Enemigo.tipo tipoEnemigo)
        {
            if (tipoEnemigo == Enemigo.tipo.Nor && Nave.poderHelix.Count > 0)
            {
                for (i = 0; i < Nave.poderHelix.Count; i++)
                {
                    distancia = (float)Math.Sqrt(
                      Math.Pow(Nave.poderHelix[i].Position.X - enemigo.Position.X, 2) +
                      Math.Pow(Nave.poderHelix[i].Position.Y - enemigo.Position.Y, 2)
                    );

                    if (distancia <= Poderes.radioCollitPHelix + Enemigo.radioCollitionNor)
                    {
                        return true;
                    }
                }
            }else
            if (tipoEnemigo == Enemigo.tipo.Kami && Nave.poderHelix.Count > 0)
            {
                for (i = 0; i < Nave.poderHelix.Count; i++)
                {
                    distancia = (float)Math.Sqrt(
                      Math.Pow(Nave.poderHelix[i].Position.X - enemigo.Position.X, 2) +
                      Math.Pow(Nave.poderHelix[i].Position.Y - enemigo.Position.Y, 2)
                    );

                    if (distancia <= Poderes.radioCollitPHelix + Enemigo.radioCollitionKami)
                    {
                        return true;
                    }
                }
            }else
            if (tipoEnemigo == Enemigo.tipo.Sid && Nave.poderHelix.Count > 0)
            {
                for (i = 0; i < Nave.poderHelix.Count; i++)
                {
                    for (j = 0; j < 5; j++)
                    {
                        distancia = (float)Math.Sqrt(
                          Math.Pow(Nave.poderHelix[i].Position.X -
                                (enemigo.Position.X + Enemigo.collitionXSid[j]), 2) +
                          Math.Pow(Nave.poderHelix[i].Position.Y -
                                (enemigo.Position.Y + Enemigo.collitionYSid[j]), 2)
                        );

                        if (distancia <= Poderes.radioCollitPHelix + Enemigo.radioCollitionSid)
                        {
                            return true;
                        }
                    }
                }
            }else
            if (tipoEnemigo == Enemigo.tipo.HiNor && Nave.poderHelix.Count > 0)
            {
                for (i = 0; i < Nave.poderHelix.Count; i++)
                {
                    distancia = (float)Math.Sqrt(
                      Math.Pow(Nave.poderHelix[i].Position.X - enemigo.Position.X, 2) +
                      Math.Pow(Nave.poderHelix[i].Position.Y - enemigo.Position.Y, 2)
                    );

                    if (distancia <= Poderes.radioCollitPHelix + Enemigo.radioCollitionHiNor)
                    {
                        return true;
                    }
                }
            }else
            if (tipoEnemigo == Enemigo.tipo.HiKami && Nave.poderHelix.Count > 0)
            {
                for (i = 0; i < Nave.poderHelix.Count; i++)
                {
                    distancia = (float)Math.Sqrt(
                       Math.Pow(Nave.poderHelix[i].Position.X - enemigo.Position.X, 2) +
                       Math.Pow(Nave.poderHelix[i].Position.Y - enemigo.Position.Y, 2)
                    );

                    if (distancia <= Poderes.radioCollitPHelix + Enemigo.radioCollitionHiKami)
                    {
                        return true;
                    }
                }
            }else
            if (tipoEnemigo == Enemigo.tipo.HiSid && Nave.poderHelix.Count > 0)
            {
                for (i = 0; i < Nave.poderHelix.Count; i++)
                {
                    for (j = 0; j < 5; j++)
                    {
                        distancia = (float)Math.Sqrt(
                          Math.Pow(Nave.poderHelix[i].Position.X -
                          (enemigo.Position.X + Enemigo.collitionXHiSid[j]), 2) +
                          Math.Pow(Nave.poderHelix[i].Position.Y -
                          (enemigo.Position.Y + Enemigo.collitionYHiSid[j]), 2)
                        );

                        if (distancia <= Poderes.radioCollitPHelix + Enemigo.radioCollitionHiSid)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        //******Colision poder Arp a enemigo
        public bool colisionPoderArpAEnemigo(Enemigo enemigo, Enemigo.tipo tipoEnemigo)
        {
            if (tipoEnemigo == Enemigo.tipo.Nor && Nave.poderArp.Count > 0)
            {
                for (i = 0; i < Nave.poderArp.Count; i++)
                {
                    for (j = 0; j < 6; j++)
                    {
                        distancia = (float)Math.Sqrt(
                          Math.Pow((Nave.poderArp[i].Position.X + Poderes.collitionPArpX[j])
                          - enemigo.Position.X, 2) +
                          Math.Pow((Nave.poderArp[i].Position.Y + Poderes.collitionPArpY[j])
                          - enemigo.Position.Y, 2)
                        );

                        if (distancia <= Poderes.radioCollitPArp + Enemigo.radioCollitionNor)
                        {
                            return true;
                        }
                    }
                }
            }else
            if (tipoEnemigo == Enemigo.tipo.Kami && Nave.poderArp.Count > 0)
            {
                for (i = 0; i < Nave.poderArp.Count; i++)
                {
                    for (j = 0; j < 6; j++)
                    {
                        distancia = (float)Math.Sqrt(
                          Math.Pow((Nave.poderArp[i].Position.X + Poderes.collitionPArpX[j])
                          - enemigo.Position.X, 2) +
                          Math.Pow((Nave.poderArp[i].Position.Y + Poderes.collitionPArpY[j])
                          - enemigo.Position.Y, 2)
                        );

                        if (distancia <= Poderes.radioCollitPArp + Enemigo.radioCollitionKami)
                        {
                            return true;
                        }
                    }
                }
            }else
            if (tipoEnemigo == Enemigo.tipo.Sid && Nave.poderArp.Count > 0)
            {
                for (i = 0; i < Nave.poderArp.Count; i++)
                {
                    for (j = 0; j < 6; j++)
                    {
                        for (w = 0; w < 5; w++)
                        {
                            distancia = (float)Math.Sqrt(
                              Math.Pow((Nave.poderArp[i].Position.X + Poderes.collitionPArpX[j])
                              - (enemigo.Position.X + Enemigo.collitionXSid[w]), 2) +
                              Math.Pow((Nave.poderArp[i].Position.Y + Poderes.collitionPArpY[j])
                              - (enemigo.Position.Y + Enemigo.collitionYSid[w]), 2)
                            );

                            if (distancia <= Poderes.radioCollitPArp + Enemigo.radioCollitionSid)
                            {
                                return true;
                            }
                        }
                    }
                }
            }else
            if (tipoEnemigo == Enemigo.tipo.HiNor && Nave.poderArp.Count > 0)
            {
                for (i = 0; i < Nave.poderArp.Count; i++)
                {
                    for (j = 0; j < 6; j++)
                    {
                        distancia = (float)Math.Sqrt(
                          Math.Pow((Nave.poderArp[i].Position.X + Poderes.collitionPArpX[j])
                          - enemigo.Position.X, 2) +
                          Math.Pow((Nave.poderArp[i].Position.Y + Poderes.collitionPArpY[j])
                          - enemigo.Position.Y, 2)
                        );

                        if (distancia <= Poderes.radioCollitPArp + Enemigo.radioCollitionHiNor)
                        {
                            return true;
                        }
                    }
                }
            }else
            if (tipoEnemigo == Enemigo.tipo.HiKami && Nave.poderArp.Count > 0)
            {
                for (i = 0; i < Nave.poderArp.Count; i++)
                {
                    for (j = 0; j < 6; j++)
                    {
                        distancia = (float)Math.Sqrt(
                          Math.Pow((Nave.poderArp[i].Position.X + Poderes.collitionPArpX[j])
                          - enemigo.Position.X, 2) +
                          Math.Pow((Nave.poderArp[i].Position.Y + Poderes.collitionPArpY[j])
                          - enemigo.Position.Y, 2)
                        );

                        if (distancia <= Poderes.radioCollitPArp + Enemigo.radioCollitionHiKami)
                        {
                            return true;
                        }
                    }
                }
            }else
            if (tipoEnemigo == Enemigo.tipo.HiSid && Nave.poderArp.Count > 0)
            {
                for (i = 0; i < Nave.poderArp.Count; i++)
                {
                    for (j = 0; j < 6; j++)
                    {
                        for (w = 0; w < 5; w++)
                        {
                            distancia = (float)Math.Sqrt(
                              Math.Pow((Nave.poderArp[i].Position.X + Poderes.collitionPArpX[j])
                              - (enemigo.Position.X + Enemigo.collitionXHiSid[w]), 2) +
                              Math.Pow((Nave.poderArp[i].Position.Y + Poderes.collitionPArpY[j])
                              - (enemigo.Position.Y + Enemigo.collitionYHiSid[w]), 2)
                            );

                            if (distancia <= Poderes.radioCollitPArp + Enemigo.radioCollitionHiSid)
                            {
                                return true;
                            }
                        }
                    }
                }
            }

            return false;
        }

        //*****Colision Poder Perseus a enemigo
        public bool colisionPoderPerseusAEnemigo(Enemigo enemigo, Enemigo.tipo tipoEnemigo)
        {
            if (tipoEnemigo == Enemigo.tipo.Nor && Nave.poderPerseus.Count > 0)
            {
                for (i = 0; i < Nave.poderPerseus.Count; i++)
                {
                    distancia = (float)Math.Sqrt(
                      Math.Pow(Nave.poderPerseus[i].Position.X - enemigo.Position.X, 2) +
                      Math.Pow(Nave.poderPerseus[i].Position.Y - enemigo.Position.Y, 2)
                    );

                    if (distancia <= Poderes.radioCollitPPerseus + Enemigo.radioCollitionNor)
                    {
                        return true;
                    }
                }
            }else
            if (tipoEnemigo == Enemigo.tipo.Kami && Nave.poderPerseus.Count > 0)
            {
                for (i = 0; i < Nave.poderPerseus.Count; i++)
                {
                    distancia = (float)Math.Sqrt(
                      Math.Pow(Nave.poderPerseus[i].Position.X - enemigo.Position.X, 2) +
                      Math.Pow(Nave.poderPerseus[i].Position.Y - enemigo.Position.Y, 2)
                    );

                    if (distancia <= Poderes.radioCollitPPerseus + Enemigo.radioCollitionKami)
                    {
                        return true;
                    }
                }
            }else
            if (tipoEnemigo == Enemigo.tipo.Sid && Nave.poderPerseus.Count > 0)
            {
                for (i = 0; i < Nave.poderPerseus.Count; i++)
                {
                    for (j = 0; j < 5; j++)
                    {
                        distancia = (float)Math.Sqrt(
                          Math.Pow(Nave.poderPerseus[i].Position.X -
                                (enemigo.Position.X + Enemigo.collitionXSid[j]), 2) +
                          Math.Pow(Nave.poderPerseus[i].Position.Y -
                                (enemigo.Position.Y + Enemigo.collitionYSid[j]), 2)
                        );

                        if (distancia <= Poderes.radioCollitPPerseus + Enemigo.radioCollitionSid)
                        {
                            return true;
                        }
                    }
                }
            }else
            if (tipoEnemigo == Enemigo.tipo.HiNor && Nave.poderPerseus.Count > 0)
            {
                for (i = 0; i < Nave.poderPerseus.Count; i++)
                {
                    distancia = (float)Math.Sqrt(
                      Math.Pow(Nave.poderPerseus[i].Position.X - enemigo.Position.X, 2) +
                      Math.Pow(Nave.poderPerseus[i].Position.Y - enemigo.Position.Y, 2)
                    );

                    if (distancia <= Poderes.radioCollitPPerseus + Enemigo.radioCollitionHiNor)
                    {
                        return true;
                    }
                }
            }else
            if (tipoEnemigo == Enemigo.tipo.HiKami && Nave.poderPerseus.Count > 0)
            {
                for (i = 0; i < Nave.poderPerseus.Count; i++)
                {
                    distancia = (float)Math.Sqrt(
                      Math.Pow(Nave.poderPerseus[i].Position.X - enemigo.Position.X, 2) +
                      Math.Pow(Nave.poderPerseus[i].Position.Y - enemigo.Position.Y, 2)
                    );

                    if (distancia <= Poderes.radioCollitPPerseus + Enemigo.radioCollitionHiKami)
                    {
                        return true;
                    }
                }
            }else
            if (tipoEnemigo == Enemigo.tipo.HiSid && Nave.poderPerseus.Count > 0)
            {
                for (i = 0; i < Nave.poderPerseus.Count; i++)
                {
                    for (j = 0; j < 5; j++)
                    {
                        distancia = (float)Math.Sqrt(
                          Math.Pow(Nave.poderPerseus[i].Position.X -
                                (enemigo.Position.X + Enemigo.collitionXHiSid[j]), 2) +
                          Math.Pow(Nave.poderPerseus[i].Position.Y -
                                (enemigo.Position.Y + Enemigo.collitionYHiSid[j]), 2)
                        );

                        if (distancia <= Poderes.radioCollitPPerseus + Enemigo.radioCollitionHiSid)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }


        //ColisionPoder Pisces a Enemigo
        public bool colisionPoderPiscesAEnemigo(Enemigo enemigo, Enemigo.tipo tipoEnemigo)
        {
            if (tipoEnemigo == Enemigo.tipo.Nor && Nave.poderPisces.Count > 0)
            {
                for (i = 0; i < Nave.poderPisces.Count; i++)
                {
                    distancia = (float)Math.Sqrt(
                      Math.Pow(Nave.poderPisces[i].Position.X - enemigo.Position.X, 2) +
                      Math.Pow(Nave.poderPisces[i].Position.Y - enemigo.Position.Y, 2)
                    );

                    if (distancia <= Poderes.radioCollitPPisces + Enemigo.radioCollitionNor)
                    {
                        return true;
                    }
                }
            }else
            if (tipoEnemigo == Enemigo.tipo.Kami && Nave.poderPisces.Count > 0)
            {
                for (i = 0; i < Nave.poderPisces.Count; i++)
                {
                    distancia = (float)Math.Sqrt(
                      Math.Pow(Nave.poderPisces[i].Position.X - enemigo.Position.X, 2) +
                      Math.Pow(Nave.poderPisces[i].Position.Y - enemigo.Position.Y, 2)
                    );

                    if (distancia <= Poderes.radioCollitPPisces + Enemigo.radioCollitionKami)
                    {
                        return true;
                    }
                }
            }else
            if (tipoEnemigo == Enemigo.tipo.Sid && Nave.poderPisces.Count > 0)
            {
                for (i = 0; i < Nave.poderPisces.Count; i++)
                {
                    for (j = 0; j < 5; j++)
                    {
                        distancia = (float)Math.Sqrt(
                          Math.Pow(Nave.poderPisces[i].Position.X -
                                (enemigo.Position.X + Enemigo.collitionXSid[j]), 2) +
                          Math.Pow(Nave.poderPisces[i].Position.Y -
                                (enemigo.Position.Y + Enemigo.collitionYSid[j]), 2)
                        );

                        if (distancia <= Poderes.radioCollitPPisces + Enemigo.radioCollitionSid)
                        {
                            return true;
                        }
                    }
                }
            }else
            if (tipoEnemigo == Enemigo.tipo.HiNor && Nave.poderPisces.Count > 0)
            {
                for (i = 0; i < Nave.poderPisces.Count; i++)
                {
                    distancia = (float)Math.Sqrt(
                      Math.Pow(Nave.poderPisces[i].Position.X - enemigo.Position.X, 2) +
                      Math.Pow(Nave.poderPisces[i].Position.Y - enemigo.Position.Y, 2)
                    );

                    if (distancia <= Poderes.radioCollitPPisces + Enemigo.radioCollitionHiNor)
                    {
                        return true;
                    }
                }
            }else
            if (tipoEnemigo == Enemigo.tipo.HiKami && Nave.poderPisces.Count > 0)
            {
                for (i = 0; i < Nave.poderPisces.Count; i++)
                {
                    distancia = (float)Math.Sqrt(
                      Math.Pow(Nave.poderPisces[i].Position.X - enemigo.Position.X, 2) +
                      Math.Pow(Nave.poderPisces[i].Position.Y - enemigo.Position.Y, 2)
                    );

                    if (distancia <= Poderes.radioCollitPPisces + Enemigo.radioCollitionHiKami)
                    {
                        return true;
                    }
                }
            }
            if (tipoEnemigo == Enemigo.tipo.HiSid && Nave.poderPisces.Count > 0)
            {
                for (i = 0; i < Nave.poderPisces.Count; i++)
                {
                    for (j = 0; j < 5; j++)
                    {
                        distancia = (float)Math.Sqrt(
                          Math.Pow(Nave.poderPisces[i].Position.X -
                                (enemigo.Position.X + Enemigo.collitionXHiSid[j]), 2) +
                          Math.Pow(Nave.poderPisces[i].Position.Y -
                                (enemigo.Position.Y + Enemigo.collitionYHiSid[j]), 2)
                        );

                        if (distancia <= Poderes.radioCollitPPisces + Enemigo.radioCollitionHiSid)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        //*****Disparos a nave*******
        public bool colisionDispANave(DisparoEnemigo d)
        {
            for (int i = 0; i < 11; i++)
            {
                distancia = (float)Math.Sqrt(
                      Math.Pow(d.Position.X - (Nave.PositionNav.X + Nave.collitionX[i]), 2) +
                      Math.Pow(d.Position.Y - (Nave.PositionNav.Y + Nave.collitionY[i]), 2)
                    );

                if (distancia <= d.radioCollition + Nave.radioCollitionN)
                {
                    return true;
                }
            }
            return false;
        }

        //colision disp Jefe a nave
        public bool colisionDispANave(DisparoJefe d)
        {
            for (int i = 0; i < 11; i++)
            {
                distancia = (float)Math.Sqrt(
                      Math.Pow(d.Position.X - (Nave.PositionNav.X + Nave.collitionX[i]), 2) +
                      Math.Pow(d.Position.Y - (Nave.PositionNav.Y + Nave.collitionY[i]), 2)
                    );

                if (distancia <= d.radioCollition + Nave.radioCollitionN)
                {
                    return true;
                }
            }

            return false;
        }

        //*****Nave y Enemigos*******
        public bool colisionNaveAEnemigo(Enemigo e)
        {
            if (e.tipoEnemigo == Enemigo.tipo.Nor)
                for (int i = 0; i < 11; i++)
                {
                    distancia = (float)Math.Sqrt(
                         Math.Pow(e.Position.X - (Nave.PositionNav.X + Nave.collitionX[i]), 2) +
                         Math.Pow(e.Position.Y - (Nave.PositionNav.Y + Nave.collitionY[i]), 2)
                         );

                    if (distancia <= Enemigo.radioCollitionNor + Nave.radioCollitionN)
                    {
                        return true;
                    }
                }

            if(e.tipoEnemigo==Enemigo.tipo.Kami)
            for (int i = 0; i < 11; i++)
            {
                distancia = (float)Math.Sqrt(
                      Math.Pow(e.Position.X - (Nave.PositionNav.X + Nave.collitionX[i]), 2) +
                      Math.Pow(e.Position.Y - (Nave.PositionNav.Y + Nave.collitionY[i]), 2)
                    );

                if (distancia <= Enemigo.radioCollitionKami + Nave.radioCollitionN)
                {
                    return true;
                }
            }

            if (e.tipoEnemigo == Enemigo.tipo.Sid)
            for (int i = 0; i < 11; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    distancia = (float)Math.Sqrt(
                     Math.Pow((e.Position.X + Enemigo.collitionXSid[j]) - 
                              (Nave.PositionNav.X + Nave.collitionX[i]), 2) +
                     Math.Pow((e.Position.Y + Enemigo.collitionYSid[j]) - 
                              (Nave.PositionNav.Y + Nave.collitionY[i]), 2)
                     );

                    if (distancia <= Enemigo.radioCollitionSid + Nave.radioCollitionN)
                    {
                        return true;
                    }
                }
            }

            if (e.tipoEnemigo == Enemigo.tipo.HiNor)
                for (int i = 0; i < 11; i++)
                {
                    distancia = (float)Math.Sqrt(
                         Math.Pow(e.Position.X - (Nave.PositionNav.X + Nave.collitionX[i]), 2) +
                         Math.Pow(e.Position.Y - (Nave.PositionNav.Y + Nave.collitionY[i]), 2)
                         );

                    if (distancia <= Enemigo.radioCollitionHiNor + Nave.radioCollitionN)
                    {
                        return true;
                    }
                }

            if (e.tipoEnemigo == Enemigo.tipo.HiKami)
                for (int i = 0; i < 11; i++)
                {
                    distancia = (float)Math.Sqrt(
                          Math.Pow(e.Position.X - (Nave.PositionNav.X + Nave.collitionX[i]), 2) +
                          Math.Pow(e.Position.Y - (Nave.PositionNav.Y + Nave.collitionY[i]), 2)
                        );

                    if (distancia <= Enemigo.radioCollitionHiKami + Nave.radioCollitionN)
                    {
                        return true;
                    }
                }

            if (e.tipoEnemigo == Enemigo.tipo.HiSid)
                for (int i = 0; i < 11; i++)
                {
                    for (int j = 0; j < 5; j++)
                    {
                        distancia = (float)Math.Sqrt(
                         Math.Pow((e.Position.X + Enemigo.collitionXHiSid[j]) -
                                  (Nave.PositionNav.X + Nave.collitionX[i]), 2) +
                         Math.Pow((e.Position.Y + Enemigo.collitionYHiSid[j]) -
                                  (Nave.PositionNav.Y + Nave.collitionY[i]), 2)
                         );

                        if (distancia <= Enemigo.radioCollitionHiSid + Nave.radioCollitionN)
                        {
                            return true;
                        }
                    }
                }

            return false;
        }


        //*****Disparos a Mini*******
        public bool colisionDispAMini(NMini mini,DisparoEnemigo d)
        {
            for (int i = 0; i < 11; i++)
            {
                distancia = (float)Math.Sqrt(
                      Math.Pow(d.Position.X - (mini.Position.X + mini.collitionX[i]), 2) +
                      Math.Pow(d.Position.Y - (mini.Position.Y + mini.collitionY[i]), 2)
                    );

                if (distancia <= d.radioCollition + mini.radioCollition)
                {
                    return true;
                }
            }
            return false;
        }

        //*****Mini a Enemigos*******
        public bool colisionMiniAEnemigo(NMini mini,Enemigo e)
        {
            if (e.tipoEnemigo == Enemigo.tipo.Nor)
                for (int i = 0; i < 11; i++)
                {
                    distancia = (float)Math.Sqrt(
                         Math.Pow(e.Position.X - (mini.Position.X + mini.collitionX[i]), 2) +
                         Math.Pow(e.Position.Y - (mini.Position.Y + mini.collitionY[i]), 2)
                         );

                    if (distancia <= Enemigo.radioCollitionNor + mini.radioCollition)
                    {
                        return true;
                    }
                }

            if (e.tipoEnemigo == Enemigo.tipo.Kami)
                for (int i = 0; i < 11; i++)
                {
                    distancia = (float)Math.Sqrt(
                          Math.Pow(e.Position.X - (mini.Position.X + mini.collitionX[i]), 2) +
                          Math.Pow(e.Position.Y - (mini.Position.Y + mini.collitionY[i]), 2)
                        );

                    if (distancia <= Enemigo.radioCollitionKami + mini.radioCollition)
                    {
                        return true;
                    }
                }

            if (e.tipoEnemigo == Enemigo.tipo.Sid)
                for (int i = 0; i < 11; i++)
                {
                    for (int j = 0; j < 5; j++)
                    {
                        distancia = (float)Math.Sqrt(
                         Math.Pow((e.Position.X + Enemigo.collitionXSid[j]) -
                                  (mini.Position.X + mini.collitionX[i]), 2) +
                         Math.Pow((e.Position.Y + Enemigo.collitionYSid[j]) -
                                  (mini.Position.Y + mini.collitionY[i]), 2)
                         );

                        if (distancia <= Enemigo.radioCollitionSid + mini.radioCollition)
                        {
                            return true;
                        }
                    }
                }

            if (e.tipoEnemigo == Enemigo.tipo.HiNor)
                for (int i = 0; i < 11; i++)
                {
                    distancia = (float)Math.Sqrt(
                         Math.Pow(e.Position.X - (mini.Position.X + mini.collitionX[i]), 2) +
                         Math.Pow(e.Position.Y - (mini.Position.Y + mini.collitionY[i]), 2)
                         );

                    if (distancia <= Enemigo.radioCollitionHiNor + mini.radioCollition)
                    {
                        return true;
                    }
                }

            if (e.tipoEnemigo == Enemigo.tipo.HiKami)
                for (int i = 0; i < 11; i++)
                {
                    distancia = (float)Math.Sqrt(
                          Math.Pow(e.Position.X - (mini.Position.X + mini.collitionX[i]), 2) +
                          Math.Pow(e.Position.Y - (mini.Position.Y + mini.collitionY[i]), 2)
                        );

                    if (distancia <= Enemigo.radioCollitionHiKami + mini.radioCollition)
                    {
                        return true;
                    }
                }

            if (e.tipoEnemigo == Enemigo.tipo.HiSid)
                for (int i = 0; i < 11; i++)
                {
                    for (int j = 0; j < 5; j++)
                    {
                        distancia = (float)Math.Sqrt(
                         Math.Pow((e.Position.X + Enemigo.collitionXHiSid[j]) -
                                  (mini.Position.X + mini.collitionX[i]), 2) +
                         Math.Pow((e.Position.Y + Enemigo.collitionYHiSid[j]) -
                                  (mini.Position.Y + mini.collitionY[i]), 2)
                         );

                        if (distancia <= Enemigo.radioCollitionHiSid + mini.radioCollition)
                        {
                            return true;
                        }
                    }
                }

            return false;
        }

        //Comprobacion de poder carafe al colisionar con enemigos
        public void confirmaEliminaPoderCarafe(int indice)
        {
            if (Nave.poderCarafe[indice].tipoPoderCarafeActual == Poderes.tipoPoderCarafe.pMedio)
            {
                Nave.poderCarafeMedioTotal.Add(Nave.poderCarafe[indice]);
            }
            else
            if (Nave.poderCarafe[indice].tipoPoderCarafeActual == Poderes.tipoPoderCarafe.pIzquierda)
            {
                Nave.poderCarafeIzquierdaTotal.Add(Nave.poderCarafe[indice]);
            }else
            if (Nave.poderCarafe[indice].tipoPoderCarafeActual == Poderes.tipoPoderCarafe.pDerecha)
            {
                Nave.poderCarafeDerechaTotal.Add(Nave.poderCarafe[indice]);
            }
            Nave.poderCarafe.RemoveAt(indice);
        }


        // Comprobacion de disparos de la nave al colisionar con enemigos
        public void confirmaEliminaDisparoNave(int indice)
        {
            if (Nave.disparos[indice].tipoActual == Disparo.tipo.pequed ||
                Nave.disparos[indice].tipoActual == Disparo.tipo.pequei
             )
            {
                ultimaColisionTipoDisparo = Nave.disparos[indice].tipoActual;
                Nave.disparosTotales.Add(Nave.disparos[indice]);
                Nave.disparos.Remove(Nave.disparos[indice]);
            }
            else
            if (Nave.disparos[indice].tipoActual == Disparo.tipo.diagD)
            {
                ultimaColisionTipoDisparo = Nave.disparos[indice].tipoActual;
                Nave.disparosDiagDerechaTotales.Add(Nave.disparos[indice]);
                Nave.disparos.Remove(Nave.disparos[indice]);
            }
            else
            if (Nave.disparos[indice].tipoActual == Disparo.tipo.diagI)
            {
                ultimaColisionTipoDisparo = Nave.disparos[indice].tipoActual;
                Nave.disparosDiagIzquierdaTotales.Add(Nave.disparos[indice]);
                Nave.disparos.Remove(Nave.disparos[indice]);
            }else
            if (Nave.disparos[indice].tipoActual == Disparo.tipo.super)
            {
                ultimaColisionTipoDisparo = Nave.disparos[indice].tipoActual;
                Nave.disparosSuperTotales.Add(Nave.disparos[indice]);
                Nave.disparos.Remove(Nave.disparos[indice]);
            }

        }

    }
}
