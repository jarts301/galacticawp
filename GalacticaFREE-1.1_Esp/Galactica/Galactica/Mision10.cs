using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Media;

namespace Galactica
{
    class Mision10:QuickModeGame
    {
        public static StringBuilder tituloMision = new StringBuilder("Mision 10");
        public static StringBuilder mensajeMision = new StringBuilder("Destruye al super\n enemigo metalico,\ndisparando\nen las zonas que cambian\nde color\nTip: Usa el poder,\nsera muy efectivo.");
        public static StringBuilder recompensa = new StringBuilder("Rescataste a la nave\nHelix, Usala en\nJuego Rapido.");
        public static StringBuilder noRecompensa = new StringBuilder("No hay recompensa...");
        private static Jefe elJefe = new Jefe(Jefe.tipo.Esp, 240, -800);

        public Mision10()
        {

        }

        public static new void reiniciar()
        {
            eliminarTodos();
            eliminaAnimaciones();
            velocidadEnem = 4;
            velocidadEnemY = 4;
            velocidadHiEnem = 6;
            velocidadHiEnemY = 6;
            maxEnemigos = 5;
            aumentoEnVidaEnemigo = 0;
            ultimoAumento = 0;
            eliminarTodos();
            elJefe.reinicia(240, -800, 0);
            eliminaAnimaciones();
            EscalaMensaje = escalaInicialMensaje = 1.5f;
            actualFrameMensaje = 0;
            tiempoTitileoMensaje = 0;
            mensajeInicialActivo = true;
            reiniciaJefe();
            Nave.disparoSuperActivo = true;
            Animation.animacionEstallidoJefe.animacionFinalizada=false;
            botones.Clear();
            botones.Add(new Boton(Boton.TipoBoton.BPause, 0, 0));
            botones.Add(new Boton(Boton.TipoBoton.BAudio, 430, 0));
            botones.Add(new Boton(Boton.TipoBoton.BPower,
                                  (int)(480 - (100 * 0.8f)),
                                  (int)(800 - (100 * 0.8f))));
        }

        public static new void load(Textura t,SpriteFont f)
        {
            fuente = f;
        }

        public static new void update(GameTime time)
        {
            nave.update(time);
            updateEstrellas(time);
            updateJefes(time);
            updateAnimaciones(time);
            for (i = 0; i < botones.Count; i++)
            {
                botones[i].update(time);
            }
            updateMensajeInicial(time);
            barraVidaNave.update(time, Nave.Vida, Nave.MaxVida);
            if (Nave.Vida <= 0) { Animation.animacionesEstallidoNave.update(time); }
            if (elJefe.vida <= 0) { Animation.animacionEstallidoJefe.update(time); }
            gameOver();
            if (Control.controlActivo)
            {
                control.update(time);
            }
        }

        public static new void draw(SpriteBatch sb)
        {
            //CLEAR NEGRO
            sb.Draw(Textura.TexturasFondo[0], Vector2.Zero, null, Color.Black, 0.0f, Vector2.Zero,
                40.0f, SpriteEffects.None, 0.999f);

            nave.draw(sb);
            drawEstrellas(sb);
            drawJefes(sb);
            drawEstadNave(sb);
            drawAnimaciones(sb);
            drawMensajeInicial(sb);
            if (Nave.Vida <= 0) { Animation.animacionesEstallidoNave.draw(sb); }
            if (elJefe.vida <= 0) { Animation.animacionEstallidoJefe.draw(sb); }

            for (i = 0; i < botones.Count; i++)
            {
                botones[i].draw(sb);
            }
            if (Control.controlActivo)
            {
                control.draw(sb);
            }
            //drawEstadisticas(sb);
        }

        /******************Estadisticas de la nave**************/
        public static new void drawEstadNave(SpriteBatch sb)
        {
            sb.Draw(Textura.TexturasFondo[1], Vector2.Zero, null,Color.Black, 0.0f, Vector2.Zero, 
                new Vector2(50.0f,2.3f),SpriteEffects.None, 0.401f);

            //** Vida de la nave
            sb.DrawString(fuente,Nave.Vida.ToString(),PositionSalud, Color.White, 0.0f,
                Vector2.Zero, 0.8f, SpriteEffects.None, 0.388f);
            barraVidaNave.draw(sb);

            //**Puntos
            sb.DrawString(fuente, "/", PositionLabelPuntos, Color.White, 0.0f,
                Vector2.Zero, 1.3f, SpriteEffects.None, 0.4f);
        }

        public static new void gameOver()
        {
            if (elJefe.vida <= 0)
            {
                reiniciaJefe();
                elJefe.vida = 0;
                elJefe.Escala = 0;
                Animation.animacionEstallidoJefe.setPosition(elJefe.Position);
                MisionModeGOver.gana = true;
                
                if (Animation.animacionEstallidoJefe.animacionFinalizada)
                {
                    (Audio.getEfecto("EstallidoJefe")).Play();
                    if (MediaPlayer.GameHasControl && Audio.musicaActiva)
                    {
                        MediaPlayer.IsRepeating = false;
                        MediaPlayer.Play(Audio.getMusica("GameOver"));
                    }

                    Almacenamiento.cargarMisiones();
                    if (Almacenamiento.getPremiosDados() == 9)
                    {
                        Almacenamiento.salvarBonus(3, Almacenamiento.tipoBonus.naves);
                        MisionModeGOver.cosasGanadas = recompensa;
                        Almacenamiento.salvarMisiones();
                    }
                    else
                    {
                        MisionModeGOver.cosasGanadas = Mision1.yaRecompensa;
                    }

                    MisionMode.estadoActual = MisionMode.Estado.GameOver;
                }
            }

            if (Nave.Vida <= 0)
            {
                Nave.Vida = 0;
                Nave.EscalaN = 0;
                Animation.animacionesEstallidoNave.setPosition(Nave.PositionNav);
                MisionModeGOver.gana = false;
                MisionModeGOver.cosasGanadas = noRecompensa;
                if (Animation.animacionesEstallidoNave.animacionFinalizada)
                {
                    if (MediaPlayer.GameHasControl && Audio.musicaActiva)
                    {
                        MediaPlayer.IsRepeating = false;
                        MediaPlayer.Play(Audio.getMusica("GameOver"));
                    }
                    MisionMode.estadoActual = MisionMode.Estado.GameOver;
                }
            }
        }

        //***Update Jefes
        public static new void updateJefes(GameTime time)
        {
                elJefe.update(time);
                if (elJefe.estaEnColisionDisp)
                {
                    elJefe.estaEnColisionDisp = false;
                    if (Animation.animacionesDispAEnemigoTotal.Count > 0)
                    {
                        Animation.animacionesDispAEnemigoTotal.ElementAt(Animation.animacionesDispAEnemigoTotal.Count - 1)
                            .setPosition(CollitionJefe.positionUltimoDisparo);
                        animacionesQ.Add(Animation.animacionesDispAEnemigoTotal[Animation.animacionesDispAEnemigoTotal.Count - 1]);
                        Animation.animacionesDispAEnemigoTotal.RemoveAt(Animation.animacionesDispAEnemigoTotal.Count - 1);
                    }
                }
                if (elJefe.estaEnColisionPoder)
                {
                    elJefe.estaEnColisionPoder = false;
                    if (Animation.animacionesEstallidoEnemigoTotal.Count > 0)
                    {
                        Animation.animacionesEstallidoEnemigoTotal.ElementAt(Animation.animacionesEstallidoEnemigoTotal.Count - 1)
                            .setPosition(CollitionJefe.positionUltimoDisparo);
                        animacionesQ.Add(Animation.animacionesEstallidoEnemigoTotal[Animation.animacionesEstallidoEnemigoTotal.Count - 1]);
                        Animation.animacionesEstallidoEnemigoTotal.RemoveAt(Animation.animacionesEstallidoEnemigoTotal.Count - 1);
                    }
                }
                barraVidaJefe.update(time, elJefe.vida, elJefe.maxVida);
        }

        //******Dibujar jefes
        public static new void drawJefes(SpriteBatch sb)
        {
            elJefe.draw(sb);
            barraVidaJefe.draw(sb);
        }

        //********Dibujo Mensaje Inicial
        public static new void drawMensajeInicial(SpriteBatch sb)
        {
            if (mensajeInicialActivo)
            {
                sb.DrawString(fuente,tituloMision+" Inicia!!", new Vector2(40, 320), 
                    Color.LightGreen, 0 ,Vector2.Zero, EscalaMensaje, SpriteEffects.None,0.4f);
            }
        }

        //Reiniciar Jefe
        public static void reiniciaJefe()
        {
            if (elJefe.animaciones.Count > 0)
            {
                for (int j = 0; j < elJefe.animaciones.Count; j++)
                {
                    if (elJefe.animaciones[j].tipoActual ==
                        Animation.tipoAnimacion.DisparoANave)
                    {
                        elJefe.animaciones[j].animacionFinalizada = false;
                        Animation.animacionesDispANaveTotal.Add(elJefe.animaciones[j]);
                        elJefe.animaciones.Remove(elJefe.animaciones[j]);
                    }
                }
            }

            if (elJefe.disparos.Count > 0)
            {
                for (int j = 0; j < elJefe.disparos.Count; j++)
                {
                    if (elJefe.disparos[j].tipoActual ==
                        DisparoJefe.tipo.normal)
                    {
                        Jefe.disparosNormalTotal.Add(elJefe.disparos[j]);
                    }
                }
                for (int j = 0; j < elJefe.disparos.Count; j++)
                {
                    if (elJefe.disparos[j].tipoActual ==
                        DisparoJefe.tipo.maximo)
                    {
                        Jefe.disparosMaxTotal.Add(elJefe.disparos[j]);
                    }
                }
            }
            elJefe.disparos.Clear();
            barraVidaJefe.reiniciaColor();
        }
    }
}
