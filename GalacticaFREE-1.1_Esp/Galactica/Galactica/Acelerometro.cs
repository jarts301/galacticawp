using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Devices.Sensors;

namespace Galactica
{
    class Acelerometro
    {
        private Vector3 AcelerDat;
        public static Accelerometer acelerometro=new Accelerometer();
        public static bool acelActivo;

        public Acelerometro()
        {
            acelerometro.ReadingChanged += AccelerometerReadingChanged;
            acelActivo = true;
            acelerometro.Start();
        }

        public void update()
        {
            if (acelActivo && 
                (QuickMode.estadoActual==QuickMode.Estado.Game ||
                 MisionMode.estadoActual == MisionMode.Estado.Game))
            {
                Nave.limiteShot = 0;
                if (AcelerDat.X < -0.0700)
                { Nave.auxPosition.X = Nave.auxPosition.X - Nave.velocidadNave; }
                if (AcelerDat.X > 0.0700)
                { Nave.auxPosition.X = Nave.auxPosition.X + Nave.velocidadNave; }
                if (AcelerDat.Y < -0.0700 && Nave.movEnY)
                { Nave.auxPosition.Y = Nave.auxPosition.Y + Nave.velocidadNave; }
                if (AcelerDat.Y > 0.0700 && Nave.movEnY)
                { Nave.auxPosition.Y = Nave.auxPosition.Y - Nave.velocidadNave; }

                Nave.setPosition(Nave.auxPosition);
            }
        }

        //para obtener datos del acelerometro
        void AccelerometerReadingChanged(object sender, AccelerometerReadingEventArgs e)
        {
            AcelerDat.X = (float)e.X;
            AcelerDat.Y = (float)e.Y;
            AcelerDat.Z = (float)e.Z;
        }

    }
}
