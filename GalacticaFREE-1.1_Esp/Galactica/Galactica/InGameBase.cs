using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Galactica
{
    class InGameBase
    {
        protected static TimeSpan tiempoAyuda;
        protected static Collition colision = new Collition();
        public static List<Animation> animaciones = new List<Animation>();
        public static List<DisparoEnemigo> disparosSid = new List<DisparoEnemigo>();
        public static List<DisparoEnemigo> disparosNor = new List<DisparoEnemigo>();
        public static Random random = new Random();

        protected static Nave nave = new Nave();
        protected static int i, j;
        //private static List<Texture2D> texturas;
        protected static SpriteFont fuente;
        protected static List<Estrella> estrellas=new List<Estrella>();
        protected static List<Enemigo> enemigos=new List<Enemigo>();
        public static List<Jefe> jefes = new List<Jefe>();
        public static int maxEnemigos, aumentoEnVidaEnemigo = 0, aumentoEnVidaJefe = 0;
        //private Color colorLife;
        protected static int velocidadEnem, velocidadEnemY, velocidadHiEnem, velocidadHiEnemY;
        //private int valIntercambio;
        protected static int num, num2;
        public static int ultimoAumento, ultimaAdicionJefe;
        protected static List<Animation> animacionesQ = new List<Animation>();

        //Para pintar vida
        protected static Vector2 PositionSalud = new Vector2(60, 14);
        protected static Vector2 PositionVistaSalud = new Vector2(63, 11);
        protected static Vector2 PositionBarraSalud = new Vector2(55, 3);
        protected static BarraVida barraVidaJefe = new BarraVida(new Vector2(5, 50), BarraVida.tipo.jefe);
        protected static BarraVida barraVidaNave = new BarraVida(new Vector2(55, 3), BarraVida.tipo.nave);
        public static bool llegaA1500;
        protected static Plus plus = new Plus(Vector2.Zero);
        public static double ultimoTPlus, tiempoEsperaPlus;// ultimoTSalvado;

        //**Mensaje Inicial
        public static float EscalaMensaje, actualFrameMensaje, escalaInicialMensaje;
        public static double tiempoTitileoMensaje;
        public static bool mensajeInicialActivo;

        /*Fondo*/
        /*private Vector2 position1 = new Vector2(0, 0);
        private Vector2 position2 = new Vector2(0, -810);*/

        //Para pintar puntaje
        protected static Vector2 PositionPuntos = new Vector2(220, 0);
        protected static Vector2 PositionLabelPuntos = new Vector2(200, 0);
        protected static List<Boton> botones = new List<Boton>();

        //Control
        protected static Control control = new Control(Vector2.Zero);

    }
}
