using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Galactica
{
    class DisparoEnemigo : GameComun
    {
        private static List<Texture2D> Texturas;
        public enum tipo { dispSid, dispNor}
        private tipo tipoActual;
        private int textura;
        private bool disDerecha;
        private Vector2 posNave;
        private float x,y,Tiempo;
        private int velocidadX,velocidadY;
        public static int adicionDanoDisNave;
        public bool Disparar,postDisparo;
        public bool estaEnColision;

        public DisparoEnemigo()
        { 
        }

        public DisparoEnemigo(Vector2 posEnem,tipo t,bool derecha)
        {
            if (t == tipo.dispSid)
            {
                Position.X = posEnem.X;
                Position.Y = posEnem.Y;
                disDerecha = derecha;
                tipoActual = t;
                textura = 0;
                Escala = 1.0f;
                setOrigen(0);
                radioCollition = (int)(6*Escala);
            }else
            if (t == tipo.dispNor)
            {
                Position.X = posEnem.X;
                Position.Y = posEnem.Y;
                Disparar = false;
                postDisparo = false;
                Tiempo = 0;
                tipoActual = t;
                textura = 1;
                Escala = 1.2f;
                setOrigen(1);
                radioCollition = (int)(6*Escala);
            }
            estaEnColision = false;
        }


        public static void load(Textura t)
        {
            adicionDanoDisNave = 0;
            setTextura(t);
        }

        public void update(GameTime gameTime)
        {
            if (tipoActual == tipo.dispSid)
            { movimDisparoSid(); }
            else
            if (tipoActual == tipo.dispNor)
            { movimDisparoNor(); }
        }

        public void draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Texturas[textura], Position, null, Color.White,
                0.0f, Origen, Escala, SpriteEffects.None, 0.45f);

        }

        ///*************************************///////

        public void movimDisparoSid()
        {
            if (disDerecha)
            {
                Position.X = Position.X + 20;
            }
            if (!disDerecha)
            {
                Position.X = Position.X - 20;
            }

            if (colision.colisionDispANave(this))
            {
                estaEnColision = true;
                Nave.Vida = Nave.Vida - (3 + adicionDanoDisNave);
            }
        }

        public void movimDisparoNor()
        {
            if (Disparar)
            {
                Tiempo = 0;
                posNave = Nave.PositionNav;
                x = Position.X;
                y = Position.Y;
                velocidadX = (int)((posNave.X - x)) * 1 / 35;
                velocidadY = (int)((posNave.Y - y)) * 1 / 35;
                postDisparo = true;
                Disparar = false;
            }
            if (postDisparo)
            {
                Tiempo++;
                Position.X = x + velocidadX * Tiempo;
                Position.Y = y + velocidadY * Tiempo;
            }

            if (colision.colisionDispANave(this))
            {
                estaEnColision = true;
                Nave.Vida = Nave.Vida - (3 + adicionDanoDisNave);
            }
        }

        public void setPosition(Vector2 p)
        {
            Position.X = p.X;
            Position.Y = p.Y;
        }
        public Vector2 getPosition()
        {
            return Position;
        }

        public void setLadoMovimiento(bool derecha)
        {
            disDerecha = derecha;
        }

        public static void setTextura(Textura t)
        {
            Texturas = new List<Texture2D>();
            Texturas.Add(t.getTextura("DisparoSid"));//0
            Texturas.Add(t.getTextura("DisparoNor"));//1
        }

        public void setOrigen(int numTex)
        {
            Origen.X = Texturas[numTex].Width / 2;
            Origen.Y = Texturas[numTex].Height / 2;
        }
    }
}
