using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework.Graphics;

namespace Galactica
{
    class GameComun
    {
        public Vector2 Position;
        protected Vector2 Origen;
        protected Rectangle TrozoAnim;
        protected int actualframe;
        protected TimeSpan dato,tiempoAyuda;
        protected int velocidad;
        protected int numeroCuadros;
        public float Escala;
        public int radioCollition;
        public static int e;
        protected static List<Estrella> estrellas;
        protected Collition colision=new Collition();
        public List<Animation> animaciones = new List<Animation>();
        public List<DisparoEnemigo> disparosSid=new List<DisparoEnemigo>();
        public List<DisparoEnemigo> disparosNor = new List<DisparoEnemigo>();
        public Random random=new Random();
        public static Random rand = new Random();
        protected float Capa;


        //***********************ESTRELLAS*********************/

        /****Carga estrellas**/
        public static void loadEstrellas()
        {
            estrellas = new List<Estrella>();
            for (e = 0; e < 50; e++)
            {
                estrellas.Add(new Estrella(new Vector2(rand.Next(0, 479),
                                                       rand.Next(0, 799))));
                estrellas[e].color = new Color(rand.Next(200, 255), rand.Next(200, 255),
                                               rand.Next(200, 255));
                estrellas[e].escala = (float)(rand.NextDouble() + 0.8);
                estrellas[e].vel = rand.Next(8, 30);
            }
        }

        /****Actualiza estrellas***/
        public void updateEstrellas(GameTime time)
        {
            for (e = 0; e < 50; e++)
            {
                estrellas[e].update(time);
            }
        }

        /****Dibuja estrellas**/
        public void drawEstrellas(SpriteBatch sb)
        {
            for (e = 0; e < 50; e++)
            {
                estrellas[e].draw(sb);
            }
        }
    }
}
