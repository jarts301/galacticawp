using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Galactica
{
    class NMini:GameComun
    {
         private static List<Texture2D> texDisparo;
        private int texturaActual;
        private int num;
        public float Rotacion;
        public int vida = 80, maxVida = 80;
        public int velocidadX, velocidadY;
        public bool cambiarMov;
        public List<int> collitionX = new List<int>();
        public List<int> collitionY = new List<int>();

        public NMini()
        {
            Rotacion=0;
            Escala = 0.7f;
            Position.X=280;
            Position.Y=700;
            cambiarMov=true;
            radioCollition = (int)(10 * Escala);
            setOrigen(0);
            texturaActual = 0;
            defineCollitionBox();
        }

        public void reiniciar()
        {
            vida = maxVida;
            Rotacion=0;
            Escala = 0.7f;
            Position.X=280;
            Position.Y=700;
            cambiarMov=true;
            radioCollition = (int)(7 * Escala);
            setOrigen(0);
            texturaActual = 0;
        }

        public static void load(Textura t)
        {
            setTextura(t);
        }

        public void update(GameTime gameTime)
        {
            velocidadAnimacion(gameTime,3);
            setTrozoAnimacion();
            setOrigen();
            movimiento(gameTime);
        }

        public void draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texDisparo[texturaActual], Position, TrozoAnim, Color.White,
                MathHelper.ToRadians(Rotacion),Origen,Escala,SpriteEffects.None,0.39f);
        }

        /***********************************/

        public static void setTextura(Textura t)
        {
            texDisparo = new List<Texture2D>();
            texDisparo.Add(t.getTextura("NMini"));
        }

        public void setPosition(Vector2 posNave)
        {
            Position.X = posNave.X;
            Position.Y = posNave.Y;
        }

        public Vector2 getPosition()
        {
            return Position;
        }

        public void setOrigen(int textura)
        {
            Origen.X = texDisparo[textura].Width / 2;
            Origen.Y = texDisparo[textura].Height / 2;
        }

        //define la velocidad de la animacion de la nave
        public void velocidadAnimacion(GameTime gameTime, int velocidad)
        {
            dato += gameTime.ElapsedGameTime;
            if (dato.TotalMilliseconds >=
                ((gameTime.ElapsedGameTime.Milliseconds) * velocidad))
            {
                actualframe = actualframe + 1;
                dato = TimeSpan.Zero;
            }
            if (actualframe >= 2) { actualframe = 0; }
        }

        public void setOrigen()
        {
            Origen.X = TrozoAnim.Width / 2;
            Origen.Y = TrozoAnim.Height / 2;
        }

        //el cuadro que debe mostrar en el siguiente draw
        public void setTrozoAnimacion()
        {
            TrozoAnim.X = actualframe * 100;
            TrozoAnim.Y = 0;
            TrozoAnim.Width = 100;
            TrozoAnim.Height = 113;
        }

        //DETECTOR DE COLISIONES
        public void defineCollitionBox()
        {
            collitionX = new List<int>();
            collitionY = new List<int>();
            //Centros de los circulos de colision
            //suma necesaria a partir de la pos actual de la nave
            collitionX.Add((int)(-1 * Escala)); collitionY.Add((int)(-36 * Escala));
            collitionX.Add((int)(-10 * Escala)); collitionY.Add((int)(-18 * Escala));
            collitionX.Add((int)(-20 * Escala)); collitionY.Add((int)(-2 * Escala));
            collitionX.Add((int)(-26 * Escala)); collitionY.Add((int)(16 * Escala));
            collitionX.Add((int)(-34 * Escala)); collitionY.Add((int)(33 * Escala));
            collitionX.Add((int)(-9 * Escala)); collitionY.Add((int)(17 * Escala));
            collitionX.Add((int)(9 * Escala)); collitionY.Add((int)(17 * Escala));
            collitionX.Add((int)(34 * Escala)); collitionY.Add((int)(33 * Escala));
            collitionX.Add((int)(26 * Escala)); collitionY.Add((int)(16 * Escala));
            collitionX.Add((int)(18 * Escala)); collitionY.Add((int)(-2 * Escala));
            collitionX.Add((int)(8 * Escala)); collitionY.Add((int)(-18 * Escala));
        }

        //Movimiento nave mini
        public void movimiento(GameTime gt)
        {
            if (cambiarMov)
            {
                velocidadX = random.Next(5, 7);
                velocidadY = random.Next(5, 7);
                num = random.Next(1, 5);
                switch (num)
                {
                    case 1:
                        velocidadX = velocidadX * (1);
                        velocidadY = velocidadY * (1);
                        break;
                    case 2:
                        velocidadX = velocidadX * (-1);
                        velocidadY = velocidadY * (1);
                        break;
                    case 3:
                        velocidadX = velocidadX * (1);
                        velocidadY = velocidadY * (-1);
                        break;
                    case 4:
                        velocidadX = velocidadX * (-1);
                        velocidadY = velocidadY * (-1);
                        break;

                }
                cambiarMov = false;
            }

            if(Position.Y>=800 && velocidadY>0)
            {
                    velocidadY = velocidadY * (-1);
                    //cambiarMov = true;
            }
            if (Position.Y <= 550 && velocidadY<0)
            {
                velocidadY = velocidadY * (-1);
                cambiarMov = true;
            }
            if (Position.X >= 480 && velocidadX > 0)
            {
                velocidadX = velocidadX * (-1);
                //cambiarMov = true;
            }
            if (Position.X <= 0 && velocidadX < 0)
            {
                velocidadX = velocidadX * (-1);
                cambiarMov = true;
            }

            Position.X = Position.X + velocidadX;
            Position.Y = Position.Y + velocidadY;
        }

    }
}
