using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Galactica
{
    class Poderes : GameComun
    {
        private static List<Texture2D> texPoderes;
        public enum tipo{pGalactica, pCarafe, pHelix, pArp, pPerseus, pPisces}
        public enum tipoPoderCarafe {pMedio,pDerecha,pIzquierda,pNull}
        public tipoPoderCarafe tipoPoderCarafeActual;
        public int texturaActual;
        private tipo tipoActual;
        public int valAuxCarafe=10;
        public static List<int> collitionPArpX, collitionPArpY;
        public static int radioCollitPGalactica,radioCollitPCarafe,radioCollitPHelix;
        public static int radioCollitPArp, radioCollitPPerseus, radioCollitPPisces;
        public int velocidadRandX, velocidadRandY, direccionPoder;
        
        public float Rotacion;

        public Poderes()
        {
        }

        public Poderes(Vector2 posN,tipo t,tipoPoderCarafe tc)
        {
            tipoActual = t;

            switch (t)
            {
                case tipo.pGalactica:
                       texturaActual = 0;
                       setPosition(posN);
                       setOrigen(0);
                       Escala = 1.2f;
                       radioCollitPGalactica = (int)(20 * Escala);
                       Rotacion = 0;
                    break;

                case tipo.pCarafe:
                       texturaActual = 1;
                       tipoPoderCarafeActual = tc;
                       setPosition(posN);
                       setOrigen(1);
                       Escala = 0.6f;
                       radioCollitPCarafe = (int)(20 * Escala);
                       Rotacion = 0;
                    break;

                case tipo.pHelix:
                       texturaActual = 2;
                       setPosition(posN);
                       setOrigen(2);
                       Escala = 1.0f;
                       radioCollitPHelix = (int)(50 * Escala);
                       Rotacion = 0;
                    break;

                case tipo.pArp:
                       texturaActual = 3;
                       setPosition(posN);
                       setOrigen(3);
                       Escala = 2.0f;
                       radioCollitPArp = (int)(20 * Escala);
                       Rotacion = 0;
                    break;

                case tipo.pPerseus:
                      texturaActual = 4;
                      setPosition(posN);
                      setOrigen(4);
                      Escala = 0.1f;
                      radioCollitPPerseus = (int)(200 * Escala);
                      Rotacion = 0;
                    break;

                case tipo.pPisces:
                      texturaActual = 5;
                      setPosition(posN);
                      setOrigen(5);
                      Escala = 1.2f;
                      velocidadRandX = velocidadRandY = direccionPoder= 0;
                      radioCollitPPisces = (int)(25 * Escala);
                      Rotacion = 0;
                    break;
            }
        }

        public static void load(Textura t)
        {
            setTextura(t);
            defineCollitionBoxPArp();
        }

        public void update(GameTime gameTime)
        {
            movimiento(gameTime);
        }

        public void draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texPoderes[texturaActual],Position,null,Color.White,
                MathHelper.ToRadians(Rotacion),Origen,Escala,SpriteEffects.None,0.55f);
        }

        /***********************************/

        public static void setTextura(Textura t)
        {
            texPoderes = new List<Texture2D>();
            texPoderes.Add(t.getTextura("PoderGalactica"));//0
            texPoderes.Add(t.getTextura("PoderCarafe"));//1
            texPoderes.Add(t.getTextura("PoderHelix"));//2
            texPoderes.Add(t.getTextura("PoderArp"));//3
            texPoderes.Add(t.getTextura("PoderPerseus"));//4
            texPoderes.Add(t.getTextura("PoderPisces"));//5
        }

        public Vector2 getPosition()
        {
            return Position;
        }

        public void setPosition(Vector2 pos)
        {
            switch(tipoActual)
            {
                case tipo.pGalactica:
                       Position.X = pos.X;
                       Position.Y = pos.Y + 40;
                    break;

                case tipo.pCarafe:
                       Position.X = pos.X;
                       Position.Y = pos.Y;
                    break;

                case tipo.pHelix:
                    Position.X = pos.X;
                    Position.Y = pos.Y-25;
                    break;

                case tipo.pArp:
                    Position.X = 240;
                    Position.Y = 800;
                    break;

                case tipo.pPerseus:
                    Position.X = pos.X;
                    Position.Y = pos.Y;
                    break;

                case tipo.pPisces:
                    Position.X = pos.X;
                    Position.Y = pos.Y;

                    direccionPoder = random.Next(1, 3);
                    if (direccionPoder == 1) { direccionPoder = 1; }
                    else
                    if (direccionPoder == 2) { direccionPoder = -1; }

                    velocidadRandX = (random.Next(0, 20))*(direccionPoder);
                    velocidadRandY = (random.Next(15, 25))*(-1);
                    break;
            }
        }

        public void movimiento(GameTime gameTime)
        {
            switch (tipoActual)
            {
                case tipo.pGalactica:
                    Position.Y = Position.Y - 20;
                    break;

                case tipo.pCarafe:
                    switch (tipoPoderCarafeActual)
                    {
                        case tipoPoderCarafe.pMedio:
                            Position.Y = Position.Y - 20;
                            break;

                        case tipoPoderCarafe.pDerecha:
                            Position.X = Position.X + valAuxCarafe;
                            Position.Y = Position.Y - 20;
                            break;

                        case tipoPoderCarafe.pIzquierda:
                            Position.X = Position.X - valAuxCarafe;
                            Position.Y = Position.Y - 20;
                            break;
                    }
                    break;

                case tipo.pHelix:
                    Position.Y = Position.Y - 25;
                    break;

                case tipo.pArp:
                    Position.Y = Position.Y - 7;
                    break;

                case tipo.pPerseus:
                    Escala=Escala+0.18f;
                    radioCollitPPerseus = (int)(100 * Escala);
                    break;

                case tipo.pPisces:
                    Position.X = Position.X + velocidadRandX;
                    Position.Y = Position.Y + velocidadRandY;
                    Rotacion = Rotacion + 15;
                    if (Rotacion >= 360)
                    {
                        Rotacion = 0;
                    }
                    break;
            }
        }

        public void setOrigen(int tex)
        {
            Origen.X = texPoderes[tex].Width / 2;
            Origen.Y = texPoderes[tex].Height / 2;
        }

        //DETECTOR DE COLISIONES DE PoderArp
        public static void defineCollitionBoxPArp()
        {
            collitionPArpX = new List<int>();
            collitionPArpY = new List<int>();
            //Centros de los circulos de colision
            //suma necesaria a partir de la pos actual del poder
            collitionPArpX.Add((int)(-200 * 1.0f)); collitionPArpY.Add(0);
            collitionPArpX.Add((int)(-120 * 1.0f)); collitionPArpY.Add(0);
            collitionPArpX.Add((int)(-40 * 1.0f)); collitionPArpY.Add(0);
            collitionPArpX.Add((int)(40 * 1.0f)); collitionPArpY.Add(0);
            collitionPArpX.Add((int)(120 * 1.0f)); collitionPArpY.Add(0);
            collitionPArpX.Add((int)(200 * 1.0f)); collitionPArpY.Add(0);
        }

    }
}
